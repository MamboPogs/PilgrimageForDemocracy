<?php
$page = new Page();
$page->h1('Environment');
$page->keywords('environment');
$page->stars(0);

$page->snp('description', 'For our generation and future generations.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p>Our natural environment, and its protection, is one of the major critical topics for our generation and for future generations.</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Besides $democracy and ${'social justice'},
	our natural environment and its protection is one of the major critical topics of our time.</p>
	HTML;

$list = new ListOfPages();
$list->add('environmental_defenders.html');
$list->add('global_natural_resources.html');
$list->add('global_witness.html');
$list->add('green_new_deal.html');
$list->add('greenwashing.html');
$list->add('keep_america_beautiful.html');
$list->add('renewable_energy.html');
$print_list = $list->print();

$div_list = new ContentSection();
$div_list->content = <<<HTML
	<p>In alphabetical order:</p>
	$print_list
	HTML;


$page->parent('lists.html');
$page->body($div_introduction);
$page->body($div_list);


