<?php
$page = new CountryPage('Costa Rica');
$page->h1('Costa Rica');
$page->stars(1);
$page->keywords('Costa Rica');

$page->preview( <<<HTML
	<p>A democracy in Central America, with good press freedom.</p>
	HTML );

$page->snp('description', '5.2 million inhabitants.');
$page->snp('image', "/copyrighted/chalo-garcia-pnmp3SOTROg.1200-630.jpg");

$r1 = $page->ref('https://rsf.org/en/index?year=2022', 'RSF Press Freedom Index 2022');
$r2 = $page->ref('https://www.aljazeera.com/news/2023/4/14/criminalising-journalism-famous-salvadoran-paper-to-relocate', '‘Criminalising journalism’: Famous Salvadoran outlet to relocate');

$div_codeberg = new CodebergContentSection();
$div_codeberg->setTitleText('Costa Rica');
$div_codeberg->setTitleLink('https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/23');
$div_codeberg->content = <<<HTML
	<p>How does Costa Rica have the highest press freedom score in the Americas?
	And why is it so different to its neighbours, which have much lower scores?</p>
	HTML;

$div_costa_rica_gem = new ContentSection();
$div_costa_rica_gem->content = <<<HTML
	<h3>Press freedom</h3>

	<p>In the 2022 Reporters Without Borders Press Freedom index,
	Costa Rica is ranked 8th country in the world, with a score of 85.92 ("Good").
	This excellent score is even more noteworthy because neighbouring countries
	Panama is ranked 74th with a score of 62.78 ("Difficult")
	and Nicaragua is ranked 160th with a score of 37.09 ("Very serious"). ${r1}</p>

	<p>Costa Rica is seen as a safe haven for journalism.
	A Salvadoran independent media company
	decided in 2023 to move its administrative and legal operations to Costa Rica,
	in order to escape from what they called "a campaign of government harassment" in El Salvador. ${r2}</p>
	HTML;


$div_Environmental_policy = new ContentSection();
$div_Environmental_policy->content = <<<HTML
	<h3>Environmental policy</h3>

	<p>The ${'World Future Council'} has featured Costa Rica's environmental policies,
	and its drive to use 100% renewable energy.</p>
	HTML;


$div_wpc_renewable_energy_in_Costa_Rica = new WorldPolicyCouncilContentSection();
$div_wpc_renewable_energy_in_Costa_Rica->setTitleText('100% renewable energy in Costa Rica ');
$div_wpc_renewable_energy_in_Costa_Rica->setTitleLink('https://www.worldfuturecouncil.org/100-renewable-energy-costa-rica/');
$div_wpc_renewable_energy_in_Costa_Rica->content = <<<HTML
	<p>Costa Rica is a frontrunner when it comes to renewable energy.
	Roughly, 95-98% of the country’s electricity has come from renewable sources since 2014.
	However, around 70% of the country’s overall energy still comes from oil and gas.
	Costa Rica’s President Carlos Alvarado has vowed to fully decarbonize the country’s economy and
	make the Latin American country the first carbon-neutral nation in the world no later than 2020.
	In addition, the government has launched its Decarbonization Plan in February 2019 to support its contribution to the Paris Agreement.
	The ambitious Plan aims to eliminate the country’s greenhouse gas emissions by 2050 and promote the modernization of the country through green growth.</p>
	HTML;




$div_costa_rica_history = new ContentSection();
$div_costa_rica_history->content = <<<HTML
	<h3>Costa Rican history</h3>

	<p>The following Costa Rica articles are of particular interest (Wikipedia):</p>

	<ul>
	<li><a class="wikipedia" href="https://en.wikipedia.org/wiki/History_of_Costa_Rica">History of Costa Rica</a></li>
	<li><a class="wikipedia" href="https://en.wikipedia.org/wiki/Politics_of_Costa_Rica">Politics of Costa Rica</a></li>
	<li><a class="wikipedia" href="https://en.wikipedia.org/wiki/Legislative_Assembly_of_Costa_Rica">Legislative Assembly of Costa Rica</a></li>
	<li><a class="wikipedia" href="https://en.wikipedia.org/wiki/Foreign_relations_of_Costa_Rica">Foreign relations of Costa Rica</a></li>
	<li><a class="wikipedia" href="https://en.wikipedia.org/wiki/Immigration_to_Costa_Rica">Immigration to Costa Rica</a></li>
	<li><a class="wikipedia" href="https://en.wikipedia.org/wiki/Human_rights_in_Costa_Rica">Human rights in Costa Rica</a></li>
	<li><a class="wikipedia" href="https://en.wikipedia.org/wiki/Prostitution_in_Costa_Rica">Prostitution in Costa Rica</a></li>
	<li><a class="wikipedia" href="https://en.wikipedia.org/wiki/Human_trafficking_in_Costa_Rica">Human trafficking in Costa Rica</a></li>
	</ul>

	HTML;

$page->parent('world.html');

$page->body($div_stub);
$page->body($div_codeberg);
$page->body('Country indices');
$page->body($div_costa_rica_gem);
$page->body($div_costa_rica_history);

$page->body($div_Environmental_policy);
$page->body($div_wpc_renewable_energy_in_Costa_Rica);
