<?php
$page = new Page();
$page->h1('Economist Democracy Index');
$page->stars(0);
$page->keywords('Economist Democracy Index', 'Democracy Index');

$page->preview( <<<HTML
	<p>
	</p>
	HTML );


$page->snp('description', "Index measuring democracy compiled by the Economist Intelligence Unit");
//$page->snp('image', "/copyrighted/");



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>For each country in the $world, we display the Democracy Index published by the Economist Intelligence Unit.</p>
	HTML;



$div_wikipedia_The_Economist_Democracy_Index = new WikipediaContentSection();
$div_wikipedia_The_Economist_Democracy_Index->setTitleText('The Economist Democracy Index');
$div_wikipedia_The_Economist_Democracy_Index->setTitleLink('https://en.wikipedia.org/wiki/The_Economist_Democracy_Index');
$div_wikipedia_The_Economist_Democracy_Index->content = <<<HTML
	<p>The Democracy Index is an index measuring democracy compiled by the Economist Intelligence Unit of the Economist Group,
	a UK-based private company which publishes the weekly newspaper The Economist.</p>
	HTML;

$page->parent('list_of_indices.html');
$page->body($div_stub);

$page->body($div_introduction);

$page->body($div_wikipedia_The_Economist_Democracy_Index);
