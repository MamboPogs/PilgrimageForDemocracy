<?php
$page = new Page();
$page->h1('Accountability');
$page->keywords('Accountability');
$page->stars(0);

$page->preview( <<<HTML
	<p></p>
	HTML );

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>It is clear that elected officials should be held accountable for their actions
	that are directly relevant to the post they were elected to.</p>

	<p>One obvious example is former US president Donald Trump for its 2020~2021 coup attempt.</p>

	<p>Another example is the Supreme Court of Israel considering a petition against a law
	that would protect PM Netanyahu from being removed from office while on trial on corruption charges.</p>
	HTML;

$div_wikipedia_Accountability = new WikipediaContentSection();
$div_wikipedia_Accountability->setTitleText('Accountability');
$div_wikipedia_Accountability->setTitleLink('https://en.wikipedia.org/wiki/Accountability');
$div_wikipedia_Accountability->content = <<<HTML
	<p>Political accountability is when a politician makes choices on behalf of the people,
	and the people have the ability to reward or sanction the politician.</p>
	HTML;



$page->parent('democracy.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body($div_wikipedia_Accountability);
