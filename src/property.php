<?php
$page = new Page();
$page->h1('Property');
$page->keywords('Property', 'property', 'private property', 'public property');
$page->stars(1);

$page->preview( <<<HTML
	<p></p>
	HTML );

$page->snp('description', 'Public property vs. private property.');
//$page->snp('image',       '/copyrighted/');



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The $Pilgrimage shall make a critical, precise distinction between public property and private property.</p>

	<p>We need a clear philosophical distinction between what ought to be considered public property
	and what should by right be deemed private property.</p>

	<p>The notion of property will be a critical one for determining what a fair $tax system should look like.</p>

	<p>Another critical aspect is making a proper distinction between the private property of an individual
	and the private property of another individual.
	Although obvious at first, this distinction will be key within for-profit corporations,
	especially as we consider the respective rights of the shareholders, the management and the workforce.</p>
	HTML;




$div_wikipedia_Property = new WikipediaContentSection();
$div_wikipedia_Property->setTitleText('Property');
$div_wikipedia_Property->setTitleLink('https://en.wikipedia.org/wiki/Property');
$div_wikipedia_Property->content = <<<HTML
	<p>Property is a system of rights that gives people legal control of valuable things, and also refers to the valuable things themselves.</p>
	HTML;

$div_wikipedia_Property_rights_economics = new WikipediaContentSection();
$div_wikipedia_Property_rights_economics->setTitleText('Property rights economics');
$div_wikipedia_Property_rights_economics->setTitleLink('https://en.wikipedia.org/wiki/Property_rights_(economics)');
$div_wikipedia_Property_rights_economics->content = <<<HTML
	<p>Property rights are constructs in economics for determining how a resource or economic good is used and owned,
	which have developed over ancient and modern history, from Abrahamic law to Article 17 of the Universal Declaration of Human Rights.
	Resources can be owned by (and hence be the property of) individuals, associations, collectives, or governments.</p>
	HTML;

$div_wikipedia_Public_property = new WikipediaContentSection();
$div_wikipedia_Public_property->setTitleText('Public property');
$div_wikipedia_Public_property->setTitleLink('https://en.wikipedia.org/wiki/Public_property');
$div_wikipedia_Public_property->content = <<<HTML
	<p>Public property is property that is dedicated to public use.
	The term may be used either to describe the use to which the property is put,
	or to describe the character of its ownership (owned collectively by the population of a state).</p>
	HTML;

$div_wikipedia_Private_property = new WikipediaContentSection();
$div_wikipedia_Private_property->setTitleText('Private property');
$div_wikipedia_Private_property->setTitleLink('https://en.wikipedia.org/wiki/Private_property');
$div_wikipedia_Private_property->content = <<<HTML
	<p>Private property is a legal designation for the ownership of property by non-governmental legal entities.
	Private property is distinguishable from public property, which is owned by a state entity,
	and from collective or cooperative property, which is owned by one or more non-governmental entities.</p>
	HTML;

$div_wikipedia_Personal_property = new WikipediaContentSection();
$div_wikipedia_Personal_property->setTitleText('Personal property');
$div_wikipedia_Personal_property->setTitleLink('https://en.wikipedia.org/wiki/Personal_property');
$div_wikipedia_Personal_property->content = <<<HTML
	<p>In political/economic theory, notably socialist, Marxist, and many anarchist philosophies,
	the distinction between private and personal property is an important one.
	In capitalism private and personal property are considered to be of equal importance and significance
	without the need for making a distinction.</p>
	HTML;

$div_wikipedia_Common_ownership = new WikipediaContentSection();
$div_wikipedia_Common_ownership->setTitleText('Common ownership');
$div_wikipedia_Common_ownership->setTitleLink('https://en.wikipedia.org/wiki/Common_ownership');
$div_wikipedia_Common_ownership->content = <<<HTML
	<p>Common ownership refers to holding the assets of an organization, enterprise or community
	indivisibly rather than in the names of the individual members or groups of members as common property.</p>
	HTML;


$page->parent('menu.html');
$page->body($div_stub);
$page->body($div_introduction);

$page->body($div_wikipedia_Property);
$page->body($div_wikipedia_Property_rights_economics);
$page->body($div_wikipedia_Public_property);
$page->body($div_wikipedia_Private_property);
$page->body($div_wikipedia_Personal_property);
$page->body($div_wikipedia_Common_ownership);
