<?php
$page = new Page();
$page->h1('The Art of Thinking Clearly');
$page->stars(0);
$page->keywords('The Art of Thinking Clearly', 'the Art of Thinking Clearly', 'Art of Thinking Clearly');

$page->preview( <<<HTML
	<p>
	</p>
	HTML );


//$page->snp('description', "");
//$page->snp('image', "/copyrighted/");



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p><strong>The Art of Thinking Clearly</strong> is a book by ${'Rolf Dobelli'}.</p>

	<h3>Table of Contents</h3>
	<ul>
		<li>01.  Survivorship Bias</li>
		<li>02.  Swimmer’s Body Illusion</li>
		<li>03.  Clustering Illusion</li>
		<li>04.  Social Proof</li>
		<li>05.  Sunk Cost Fallacy</li>
		<li>06.  Reciprocity</li>
		<li>07.  Confirmation Bias (Part 1)</li>
		<li>08.  Confirmation Bias (Part 2)</li>
		<li>09.  Authority Bias</li>
		<li>10.  Contrast Effect</li>
		<li>11.  Availability Bias</li>
		<li>12.  The It’ll-Get-Worse-Before-It-Gets-Better Fallacy</li>
		<li>13.  Story Bias</li>
		<li>14.  Hindsight Bias</li>
		<li>15.  Overconfidence Effect</li>
		<li>16.  Chauffeur Knowledge</li>
		<li>17.  Illusion of Control</li>
		<li>18.  Incentive Super-Response Tendency</li>
		<li>19.  Regression to Mean</li>
		<li>20.  Outcome Bias</li>
		<li>21.  Paradox of Choice</li>
		<li>22.  Liking Bias</li>
		<li>23.  Endowment Effect</li>
		<li>24.  Coincidence</li>
		<li>25.  Groupthink</li>
		<li>26.  Neglect of Probability</li>
		<li>27.  Scarcity Error</li>
		<li>28.  Base-Rate Neglect</li>
		<li>29.  Gambler’s Fallacy</li>
		<li>30.  The Anchor</li>
		<li>31.  Induction</li>
		<li>32.  Loss Aversion</li>
		<li>33.  Social Loafing</li>
		<li>34.  Exponential Growth</li>
		<li>35.  Winner’s Curse</li>
		<li>36.  Fundamental Attribution Error</li>
		<li>37.  False Causality</li>
		<li>38.  Halo Effect</li>
		<li>39.  Alternative Paths</li>
		<li>40.  Forecast Illusion</li>
		<li>41.  Conjunction Fallacy</li>
		<li>42.  Framing</li>
		<li>43.  Action Bias</li>
		<li>44.  Omission Bias</li>
		<li>45.  Self-Serving Bias</li>
		<li>46.  Hedonic Treadmill</li>
		<li>47.  Self-Selection Bias</li>
		<li>48.  Association Bias</li>
		<li>49.  Beginner’s Luck</li>
		<li>50.  Cognitive Dissonance</li>
		<li>51.  Hyperbolic Discounting</li>
		<li>52.  Because Justification</li>
		<li>53.  Decision Fatigue</li>
		<li>54.  Contagion Bias</li>
		<li>55.  The Problem with Averages</li>
		<li>56.  Motivation Crowding</li>
		<li>57.  Twaddle Tendency</li>
		<li>58.  Will Rogers Phenomenon</li>
		<li>59.  Information Bias</li>
		<li>60.  Effort Justification</li>
		<li>61.  The Law of Small Numbers</li>
		<li>62.  Expectations</li>
		<li>63.  Simple Logic</li>
		<li>64.  Forer Effect</li>
		<li>65.  Volunteer’s Folly</li>
		<li>66.  Affect Heuristic</li>
		<li>67.  Introspection Illusion</li>
		<li>68.  Inability to Close Doors</li>
		<li>69.  Neomania</li>
		<li>70.  Sleeper Effect</li>
		<li>71.  Alternative Blindness</li>
		<li>72.  Social Comparison Bias</li>
		<li>73.  Primacy and Recency Effects</li>
		<li>74.  Not-Invented-Here Syndrome</li>
		<li>75.  The Black Swan</li>
		<li>76.  Domain Dependence</li>
		<li>77.  False-Consensus Effect</li>
		<li>78.  Falsification of History</li>
		<li>79.  In-Group Out-Group Bias</li>
		<li>80.  Ambiguity Aversion</li>
		<li>81.  Default Effect</li>
		<li>82.  Fear of Regret</li>
		<li>83.  Salience Effect</li>
		<li>84.  House-Money Effect</li>
		<li>85.  Procrastination</li>
		<li>86.  Envy</li>
		<li>87.  Personification</li>
		<li>88.  Illusion of Attention</li>
		<li>89.  Strategic Misrepresentation</li>
		<li>90.  Overthinking</li>
		<li>91.  Planning Fallacy</li>
		<li>92.  Déformation Professionnelle</li>
		<li>93.  Zeigarnik Effect</li>
		<li>94.  Illusion of Skill</li>
		<li>95.  Feature-Positive Effect</li>
		<li>96.  Cherry Picking</li>
		<li>97.  Fallacy of the Single Cause</li>
		<li>98.  Intention-to-Treat Error</li>
		<li>99.  News Illusion</li>
	</ul>
	HTML;



$div_wikipedia_The_Art_of_Thinking_Clearly = new WikipediaContentSection();
$div_wikipedia_The_Art_of_Thinking_Clearly->setTitleText('The Art of Thinking Clearly');
$div_wikipedia_The_Art_of_Thinking_Clearly->setTitleLink('https://en.wikipedia.org/wiki/The_Art_of_Thinking_Clearly');
$div_wikipedia_The_Art_of_Thinking_Clearly->content = <<<HTML
	<p>The Art of Thinking Clearly is a 2013 book by the Swiss writer Rolf Dobelli
	which describes in short chapters 99 of the most common thinking errors
	– ranging from cognitive biases to envy and social distortions.</p>
	HTML;

$page->parent('list_of_books.html');
$page->body($div_stub);

$page->body($div_introduction);
$page->body($div_wikipedia_The_Art_of_Thinking_Clearly);
