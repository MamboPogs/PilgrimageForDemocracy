<?php
$page = new Page();
$page->h1('The Hidden Dimension in Democracy');
$page->stars(0);

$page->snp('description', "Data analysis reveals trade-off between electoral control and citizen freedom.");
//$page->snp('image', "/copyrighted/");

$page->preview( <<<HTML
	<p><strong>Abstract</strong>:
	"<em>The multidimensional dataset provided by the Varieties of Democracy (V-Dem) project reflects
	the complexity of the concept of democracy. And yet, all standard democracy measures are
	one-dimensional indices. Through a statistics-based approach, we identify two so-far hidden
	dimensions in the dataset. The first dimension aligns well with the V-Dem index of Electoral Democracy
	but the second dimension represents the so-far overlooked trade-off between
	electoral control and citizen freedom, which clearly distinguishes electoral autocracies from
	countries in which citizens are free but which struggle with corruption and violence.
	We interpret this second dimension as capturing a crucial element of stability for non-democracies.
	Using this second dimension relative to the first, we clarify long-standing debates on ‘waves of
	democracy’ and open up new avenues in understanding the recent phenomenon of ‘backsliding’
	among consolidated democracies.</em>"
	</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p><strong>Abstract</strong>:

	<div class="download-pdf">
		<a href="https://www.v-dem.net/media/publications/wp_137.pdf">
			<img src="/copyrighted/pdf/wp_137-01.png">
			<div class="download"><i class="outline file pdf white large icon"></i>Download</div>
		</a>
	</div>

	"<em>The multidimensional dataset provided by the Varieties of Democracy (V-Dem) project reflects
	the complexity of the concept of democracy. And yet, all standard democracy measures are
	one-dimensional indices. Through a statistics-based approach, we identify two so-far hidden
	dimensions in the dataset. The first dimension aligns well with the V-Dem index of Electoral Democracy
	but the second dimension represents the so-far overlooked trade-off between
	electoral control and citizen freedom, which clearly distinguishes electoral autocracies from
	countries in which citizens are free but which struggle with corruption and violence.
	We interpret this second dimension as capturing a crucial element of stability for non-democracies.
	Using this second dimension relative to the first, we clarify long-standing debates on ‘waves of
	democracy’ and open up new avenues in understanding the recent phenomenon of ‘backsliding’
	among consolidated democracies.</em>"
	</p>
	HTML;



$page->parent('list_of_research_and_reports.html');
$page->body($div_stub);

$page->body($div_introduction);
$page->body('karoline_wiesner.html');
