<?php
$page = new Page();
$page->h1('The Four Agreements');
$page->keywords('The Four Agreements');
$page->stars(0);

$page->snp('description', 'A code of conduct based on ancient wisdom.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The Four Agreements is a book by ${'Don Miguel Ruiz'}.</p>

	<p>It is related to ${'interpersonal relationships'}.</p>
	HTML;

$div_wikipedia_The_Four_Agreements = new WikipediaContentSection();
$div_wikipedia_The_Four_Agreements->setTitleText('The Four Agreements');
$div_wikipedia_The_Four_Agreements->setTitleLink('https://en.wikipedia.org/wiki/The_Four_Agreements');
$div_wikipedia_The_Four_Agreements->content = <<<HTML
	<p>The Four Agreements: A Practical Guide to Personal Freedom is a self-help book by bestselling author Don Miguel Ruiz.
	The book offers a code of conduct claiming to be based on ancient Toltec wisdom
	that advocates freedom from self-limiting beliefs that may cause suffering and limitation in a person's life.</p>
	HTML;


$page->parent('list_of_books.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_wikipedia_The_Four_Agreements);
