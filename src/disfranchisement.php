<?php
$page = new Page();
$page->h1('Disfranchisement');
$page->keywords('disfranchisement');
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>See: ${'election integrity'}.</p>
	HTML;

$div_wikipedia_Disfranchisement = new WikipediaContentSection();
$div_wikipedia_Disfranchisement->setTitleText('Disfranchisement');
$div_wikipedia_Disfranchisement->setTitleLink('https://en.wikipedia.org/wiki/Disfranchisement');
$div_wikipedia_Disfranchisement->content = <<<HTML
	<p>Disfranchisement, also called disenfranchisement, or voter disqualification is
	the restriction of suffrage (the right to vote) of a person or group of people,
	or a practice that has the effect of preventing a person exercising the right to vote.
	Disfranchisement can also refer to the revocation of power or control of a particular individual, community or being to the natural amenity they have;
	that is to deprive of a franchise, of a legal right, of some privilege or inherent immunity.
	Disfranchisement may be accomplished explicitly by law or implicitly
	through requirements applied in a discriminatory fashion, through intimidation,
	or by placing unreasonable requirements on voters for registration or voting.
	High barriers to entry to the political competition can disenfranchise political movements.</p>
	HTML;

$div_wikipedia_Voter_suppression_in_the_United_States = new WikipediaContentSection();
$div_wikipedia_Voter_suppression_in_the_United_States->setTitleText('Voter suppression in the United States');
$div_wikipedia_Voter_suppression_in_the_United_States->setTitleLink('https://en.wikipedia.org/wiki/Voter_suppression_in_the_United_States');
$div_wikipedia_Voter_suppression_in_the_United_States->content = <<<HTML
	<p>Voter suppression in the United States consists of various legal and illegal efforts
	to prevent eligible citizens from exercising their right to vote.
	Such voter suppression efforts vary by state, local government, precinct, and election.
	Voter suppression has historically been used for racial, economic, gender, age and disability discrimination.</p>
	HTML;


$page->parent('election_integrity.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_wikipedia_Disfranchisement);
$page->body($div_wikipedia_Voter_suppression_in_the_United_States);
