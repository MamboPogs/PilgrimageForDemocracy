<?php
$page = new Page();
$page->h1('Vision of Humanity');
$page->stars(0);
$page->keywords('Vision of Humanity');

$page->preview( <<<HTML
	<p>
	</p>
	HTML );


$page->snp('description', "Data analysis for peace, security and development.");
//$page->snp('image', "/copyrighted/");



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Vision of Humanity is a project and website by the ${'Institute for Economics and Peace'}.</p>
	HTML;



$div_Vision_of_Humanity = new WebsiteContentSection();
$div_Vision_of_Humanity->setTitleText('Vision of Humanity ');
$div_Vision_of_Humanity->setTitleLink('https://www.visionofhumanity.org/');
$div_Vision_of_Humanity->content = <<<HTML
	<p>Founded in 2008, Vision of Humanity is a destination for peace
	providing analysis, data, and editorial through a lens of peace, security and development.</p>
	HTML;



$page->parent('list_of_organisations.html');
$page->body($div_stub);

$page->body($div_introduction);
$page->body($div_Vision_of_Humanity);
$page->body('institute_for_economics_and_peace.html');
