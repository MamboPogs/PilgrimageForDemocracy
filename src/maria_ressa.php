<?php
$page = new Page();
$page->h1('Maria Ressa');
$page->stars(1);
$page->keywords('Maria Ressa');

$page->snp('description', "Filipino journalist and 2021 Nobel Peace Prize laureate.");
//$page->snp('image', "/copyrighted/");

$page->preview( <<<HTML
	<p>Filipino journalist and 2021 Nobel Peace Prize laureate.</p>
	HTML );

$r1 = $page->ref('https://www.taipeitimes.com/News/front/archives/2023/06/29/2003802360', 'Tsai Ing-wen urges unity in fight to end disinformation');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Maria Ressa is a journalist from the $Philippines and 2021 Nobel Peace Prize laureate.</p>

	<p>Ressa was a guest speaker at the World News Media Congress in Taipei on June 28, 2023. ${r1}
	In her remarks, she said that AI could be "weaponized" and used against democratic societies by mining private data or spreading false information.
	She added that "the corruption of our information system leads to the corruption of our democracy."</p>
	HTML;



$div_wikipedia_Maria_Ressa = new WikipediaContentSection();
$div_wikipedia_Maria_Ressa->setTitleText('Maria Ressa');
$div_wikipedia_Maria_Ressa->setTitleLink('https://en.wikipedia.org/wiki/Maria_Ressa');
$div_wikipedia_Maria_Ressa->content = <<<HTML
	<p>Maria Ressa is a Filipino and American journalist. She is the co-founder and CEO of Rappler.
	Ressa is one of the 25 leading figures on the Information and Democracy Commission launched by Reporters Without Borders.
	She was awarded the 2021 Nobel Peace Prize jointly with Dmitry Muratov
	for "their efforts to safeguard freedom of expression, which is a precondition for democracy and lasting peace."</p>
	HTML;

$div_wikipedia_We_Hold_the_Line = new WikipediaContentSection();
$div_wikipedia_We_Hold_the_Line->setTitleText('We Hold the Line');
$div_wikipedia_We_Hold_the_Line->setTitleLink('https://en.wikipedia.org/wiki/We_Hold_the_Line');
$div_wikipedia_We_Hold_the_Line->content = <<<HTML
	<p>"We Hold the Line" is a 2020 documentary film about the Philippine drug war and corruption,
	repression and violence under the regime of Philippine president Rodrigo Duterte,
	featuring Ressa and her journalistic work and struggles in the Philippines.</p>
	HTML;

$div_wikipedia_A_Thousand_Cuts = new WikipediaContentSection();
$div_wikipedia_A_Thousand_Cuts->setTitleText('A Thousand Cuts');
$div_wikipedia_A_Thousand_Cuts->setTitleLink('https://en.wikipedia.org/wiki/A_Thousand_Cuts');
$div_wikipedia_A_Thousand_Cuts->content = <<<HTML
	<p>"A Thousand Cuts" is a 2020 documentary film about Ressa and her journalistic work and struggles in the Philippines.</p>
	HTML;

$div_wikipedia_Censorship_in_the_Philippines = new WikipediaContentSection();
$div_wikipedia_Censorship_in_the_Philippines->setTitleText('Censorship in the Philippines');
$div_wikipedia_Censorship_in_the_Philippines->setTitleLink('https://en.wikipedia.org/wiki/Censorship_in_the_Philippines');
$div_wikipedia_Censorship_in_the_Philippines->content = <<<HTML
	<p>Historical and present day censorship in the Philippines.</p>
	HTML;

$div_wikipedia_List_of_journalists_killed_in_the_Philippines = new WikipediaContentSection();
$div_wikipedia_List_of_journalists_killed_in_the_Philippines->setTitleText('List of journalists killed in the Philippines');
$div_wikipedia_List_of_journalists_killed_in_the_Philippines->setTitleLink('https://en.wikipedia.org/wiki/List_of_journalists_killed_in_the_Philippines');
$div_wikipedia_List_of_journalists_killed_in_the_Philippines->content = <<<HTML
	<p>Reporters Without Borders (RSF) had said that the Philippines is one of the world's deadliest country for journalists,
	adding that violence against them continued even with the establishment of the Presidential Task Force on Media Security (PTFoMS) in 2016.
	In its press freedom index for 2022, the country, out of 180 evaluated by RSF, ranks 147th.</p>
	HTML;

$page->parent('list_of_people.html');
$page->body($div_stub);

$page->body($div_introduction);

$page->body($div_wikipedia_Maria_Ressa);
$page->body($div_wikipedia_We_Hold_the_Line);
$page->body($div_wikipedia_A_Thousand_Cuts);
$page->body($div_wikipedia_Censorship_in_the_Philippines);
$page->body($div_wikipedia_List_of_journalists_killed_in_the_Philippines);
