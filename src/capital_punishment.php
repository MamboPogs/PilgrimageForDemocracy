<?php
$page = new Page();
$page->h1('Capital punishment');
$page->keywords('Capital punishment', 'death penalty', 'capital punishment');
$page->stars(0);

$page->preview( <<<HTML
	<p></p>
	HTML );

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Capital_punishment = new WikipediaContentSection();
$div_wikipedia_Capital_punishment->setTitleText('Capital punishment');
$div_wikipedia_Capital_punishment->setTitleLink('https://en.wikipedia.org/wiki/Capital_punishment');
$div_wikipedia_Capital_punishment->content = <<<HTML
	<p>Capital punishment is controversial,
	with many people, organizations, and religious groups holding differing views on whether or not it is ethically permissible.
	Amnesty International declares that the death penalty breaches human rights,
	stating "the right to life and the right to live free from torture or cruel, inhuman or degrading treatment or punishment."
	These rights are protected under the Universal Declaration of Human Rights, adopted by the United Nations in 1948.
	In the European Union (EU), Article 2 of the Charter of Fundamental Rights of the European Union prohibits the use of capital punishment.</p>
	HTML;

$div_wikipedia_Capital_punishment_by_country = new WikipediaContentSection();
$div_wikipedia_Capital_punishment_by_country->setTitleText('Capital punishment by country');
$div_wikipedia_Capital_punishment_by_country->setTitleLink('https://en.wikipedia.org/wiki/Capital_punishment_by_country');
$div_wikipedia_Capital_punishment_by_country->content = <<<HTML
	<p>As of the end of 2022, 53 countries retain capital punishment,
	111 countries have completely abolished it de jure for all crimes,
	seven have abolished it for ordinary crimes (while maintaining it for special circumstances such as war crimes),
	and 24 are abolitionist in practice.</p>
	HTML;

$div_wikipedia_Wrongful_execution = new WikipediaContentSection();
$div_wikipedia_Wrongful_execution->setTitleText('Wrongful execution');
$div_wikipedia_Wrongful_execution->setTitleLink('https://en.wikipedia.org/wiki/Wrongful_execution');
$div_wikipedia_Wrongful_execution->content = <<<HTML
	<p>Wrongful execution is a miscarriage of justice occurring when an innocent person is killed by capital punishment.</p>
	HTML;


$page->parent('democracy.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_wikipedia_Capital_punishment);
$page->body($div_wikipedia_Capital_punishment_by_country);
$page->body($div_wikipedia_Wrongful_execution);
