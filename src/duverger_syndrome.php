<?php
$page = new Page();
$page->h1('1: Duverger Syndrome');
$page->stars(1);
$page->keywords('Duverger Syndrome');

$page->preview( <<<HTML
	<p>
	</p>
	HTML );

//$page->snp('description', "");
//$page->snp('image', "/copyrighted/");


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$h2_duverger_syndrome = new h2HeaderContent('Duverger symptoms');
$h2_duverger_theory = new h2HeaderContent("Duverger's Law");
$h2_duverger_examples = new h2HeaderContent('Historical examples');


$page->body($div_introduction);

$page->body($h2_duverger_syndrome);
$page->body('duverger_syndrome_duality.html');
$page->body('duverger_syndrome_alternance.html');
$page->body('duverger_syndrome_party_politics.html');
$page->body('duverger_syndrome_negative_campaigning.html');
$page->body('duverger_syndrome_lack_choice.html');

$page->body($h2_duverger_theory);
$page->body('duverger_law.html');

$page->body($h2_duverger_examples);
$page->body('duverger_usa_19_century.html');
$page->body('duverger_taiwan_2000_2004.html');
$page->body('duverger_france_alternance_danger.html');
