<?php
$page = new Page();
$page->h1('Taxes');
$page->stars(1);
$page->keywords('tax', 'taxes', 'Taxes');

$page->preview( <<<HTML
	<p>It is said that there are only two things that are certain in life: death and taxes.	</p>
	<p>The government needs the financial resources to perform its duties.
	Taxes are obviously necessary.
	However, not all taxes are created equal.</p>

	<p>What criteria can we use to evaluate the usefulness of a tax, and its contribution to running a balanced society?
	What taxes are harmful and should be abolished?
	What taxes would, overall, be more beneficial?
	What proportions do harmful taxes take in a government's budget, as compared to more benign taxes?
	</p>
	HTML );

$page->snp('description', "A necessary part of life");
//$page->snp('image', "/copyrighted/");


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>It is said that the two things that one cannot avoid in life are: death, and taxes.</p>

	<p>In this section, we shall explore:
	the definition of taxes,
	the necessity of taxes,
	the amount of taxes,
	and the nature, good or bad, of taxes.</p>

	<p>The government needs the financial resources to perform its duties. Taxes are obviously necessary. However, not all taxes are created equal.</p>

	<p>The idea of fair taxes cannot be discussed without making reference to ${'public property'} and ${'private property'}.</p>

	<p>What criteria can we use to evaluate the usefulness of a tax,
	and its contribution to running a balanced society?
	What taxes are harmful and should be abolished?
	What taxes would, overall, be more beneficial?
	What proportions do harmful taxes take in a government's budget, as compared to more benign taxes?</p>

	<p>${'Mo Gawdat'}, former chief business officer for Google X, suggested to tax
	${'Artificial intelligence'} businesses at 98% to support the people who are going to be displaced by AI,
	and to deal with the economic and social consequences of the technology.</p>
	HTML;


$page->parent('menu.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body('flat_tax.html');
