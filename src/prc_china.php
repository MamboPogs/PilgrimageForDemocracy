<?php
$page = new CountryPage('China');
$page->h1("The People's Republic of China");
$page->stars(1);
$page->keywords("People's Republic of China", 'China', 'PRC', 'CCP', 'Chinese Communist Party');

$page->preview( <<<HTML
	<p>The People's Republic of China represents one of the most critical frontlines for democracy in the world.</p>
	HTML );

$page->snp('description', '1.4 billion inhabitants.');
//$page->snp('image',       '/copyrighted/');

$r1 = $page->ref('https://news.ltn.com.tw/news/world/breakingnews/4191842', '白紙運動秋後算帳！小粉紅竟嗆「重判別放出來」 網嘆：沒救了...');
$r2 = $page->ref('https://www.taipeitimes.com/News/front/archives/2023/07/02/2003802508', 'New Beijing law puts companies at risk: US');
$r3 = $page->ref('https://edition.cnn.com/2023/07/03/politics/china-travel-advisory-state-department/index.html',
                 'Americans should reconsider travel to China due to the risk of wrongful detention, US State Department warns');
$r4 = $page->ref('https://www.state.gov/resources-for-families-of-wrongful-detainees/', 'U.S. Government Response to Wrongful Detention');
$r5 = $page->ref('https://travel.state.gov/content/travel/en/traveladvisories/traveladvisories/china-travel-advisory.html',
                 'US State Dpt: Reconsider travel due to the arbitrary enforcement of local laws, including in relation to exit bans, and the risk of wrongful detentions.');

$div_codeberg = new CodebergContentSection();
$div_codeberg->setTitleText("Research the People's Republic of China's claims to being a democracy");
$div_codeberg->setTitleLink('https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/12');
$div_codeberg->content = <<<HTML
	<p>The PRC is pushing the idea that the PRC represents a Chinese-style democracy.
	We need to analyze and deconstruct the claim.</p>
	HTML;

$div_wikipedia_china_democracy = new WikipediaContentSection();
$div_wikipedia_china_democracy->setTitleText('Democracy in China');
$div_wikipedia_china_democracy->setTitleLink('https://en.wikipedia.org/wiki/Democracy_in_China');
$div_wikipedia_china_democracy->content = <<<HTML
	<p>The Constitution of the People's Republic of China (PRC) states that its form of government is a "people's democratic dictatorship".
	The CCP says that China is a "socialist democracy", in which the CCP is the central authority and acts in the people's interest.</p>
	HTML;

$div_wikipedia_china_human_right = new WikipediaContentSection();
$div_wikipedia_china_human_right->setTitleText('Human rights in China');
$div_wikipedia_china_human_right->setTitleLink('https://en.wikipedia.org/wiki/Human_rights_in_China');
$div_wikipedia_china_human_right->content = <<<HTML
	<p>A long, well-documented Wikipedia article on the Human Rights situation in the People's Republic of China.</p>
	HTML;


$div_wikipedia_2022_covid_protests_china = new WikipediaContentSection();
$div_wikipedia_2022_covid_protests_china->setTitleText('2022 COVID-19 protests in China');
$div_wikipedia_2022_covid_protests_china->setTitleLink('https://en.wikipedia.org/wiki/2022_COVID-19_protests_in_China');
$div_wikipedia_2022_covid_protests_china->content = <<<HTML
	<p>A series of protests against COVID-19 lockdowns began in mainland China in November 2022.
	Colloquially referred to as the White Paper Protests (白纸抗议) or the A4 Revolution (白纸革命),
	the demonstrations started in response to measures taken by the Chinese government to prevent the spread of COVID-19 in the country,
	including implementing a zero-COVID policy.</p>

	<p>It is reported that in the weeks and months following the protests,
	the Chinese authorities started to round up and arrest people who had participated in the protests. {$r1}</p>
	HTML;


$h2_Authoritarianism = new h2HeaderContent('Authoritarianism');

$div_Chinese_society = new ContentSection();
$div_Chinese_society->content = <<<HTML
	<h3>Chinese society</h3>

	<p>${'Human Rights Watch'} has found $racist content targeting Black people
	is becoming increasingly prevalent on China's social media platforms.</p>
	HTML;

$div_Transnational_authoritarianism = new ContentSection();
$div_Transnational_authoritarianism->content = <<<HTML
	<h3>Transnational authoritarianism</h3>

	<p>According to ${'Freedom House'},
	China "conducts the most sophisticated, global, and comprehensive campaign of ${'transnational repression'} in the world."</p>

	<p>The People's Republic of China has opened secret Chinese police stations within Western countries
	is order to pursue, monitor and put pressure on Chinese refugees living abroad.</p>

	<p>The PRC routinely harasses family members who still live in China.</p>

	<p>Freedom House has published a comprehensive report, linked below,
	with case studies of ${'transnational authoritarianism'} perpetrated by the PRC.</p>
	HTML;



$div_freedomhouse_China_Transnational_Repression_Origin_Country_Case_Study = new FreedomHouseContentSection();
$div_freedomhouse_China_Transnational_Repression_Origin_Country_Case_Study->setTitleText('China: Transnational Repression Origin Country Case Study');
$div_freedomhouse_China_Transnational_Repression_Origin_Country_Case_Study->setTitleLink('https://freedomhouse.org/report/transnational-repression/china');
$div_freedomhouse_China_Transnational_Repression_Origin_Country_Case_Study->content = <<<HTML
	<p>China conducts the most sophisticated, global, and comprehensive campaign of transnational repression in the world. Efforts by the Chinese Communist Party (CCP) to pressure and control the overseas population of Chinese and members of minority communities are marked by three distinctive characteristics. First, the campaign targets many groups, including multiple ethnic and religious minorities, political dissidents, human rights activists, journalists, and former insiders accused of corruption. Second, it spans the full spectrum of tactics: from direct attacks like renditions, to co-opting other countries to detain and render exiles, to mobility controls, to threats from a distance like digital threats, spyware, and coercion by proxy. Third, the sheer breadth and global scale of the campaign is unparalleled. Freedom House’s conservative catalogue of direct, physical attacks since 2014 covers 214 cases originating from China, far more than any other country.</p>
	HTML;



$h2_Doing_business_China = new h2HeaderContent('Doing business in/with China');

$div_doing_business_with_china = new ContentSection();
$div_doing_business_with_china->content = <<<HTML
	<p>For Western businesses to do business in or with the People's Republic of China, there is always a cost.
	Besides the ethical cost, which is not always obvious to well-meaning business people,
	there is also a business cost and a real risk.</p>

	<p>This year, Chinese lawmakers passed a wide-ranging update to Beijing’s anti-espionage legislation,
	banning the transfer of any information related to national security.
	The new legislation is broadening the definition of spying without giving very precise boundaries
	of what might or might not constitute a crime in the eyes of the communist regime. $r2</p>

	<p>These hardening of the legislation is putting Western business people into real danger of being randomly accused of spying.</p>

	<p>The US State Department is telling Americans to reconsider traveling to China due to the risk of wrongful detention. $r3
	The US State Department website includes resources for families of wrongful detainees. $r4</p>

	<p>The US travel advisory reads: $r5:</p>

	<blockquote>
	<p>Reconsider travel due to the <b>arbitrary enforcement of local laws</b>,
	<b>including in relation to exit bans, and the</b> <b>risk of wrongful detentions</b>.</p>

	<p><b>Summary:&nbsp;</b>The People’s Republic of China (PRC) government arbitrarily enforces local laws,
	including issuing exit bans on U.S. citizens and citizens of other countries, without fair and transparent process under the law.</p>

	<p>The Department of State has determined the risk of wrongful detention of U.S. nationals by the PRC government exists in the PRC.</p>

	<p>U.S. citizens traveling or residing in the PRC may be detained
	without access to U.S. consular services or information about their alleged crime.&nbsp;U.S. citizens in the PRC
	may be subjected to interrogations and detention without fair and transparent treatment under the law.</p>

	<p>Foreigners in the PRC, including but not limited to businesspeople, former foreign-government personnel, academics, relatives of PRC citizens
	involved in legal disputes, and journalists have been interrogated and detained by PRC officials
	for alleged violations of PRC national security laws.
	The PRC has also interrogated, detained, and expelled U.S. citizens living and working in the PRC.</p>

	<p>PRC authorities appear to have broad discretion to deem a wide range of documents, data, statistics, or materials as state secrets
	and to detain and prosecute foreign nationals for alleged espionage.
	There is increased official scrutiny of U.S. and third-country firms, such as professional service and due diligence companies,
	operating in the PRC. Security personnel could detain U.S. citizens or subject them to prosecution for conducting research
	or accessing publicly available material inside the PRC.</p>

	<p>Security personnel could detain and/or deport U.S. citizens
	for sending private electronic messages critical of the PRC, Hong Kong SAR, or Macau SAR governments.</p>

	<p>In addition, the PRC government has used restrictions on travel or departure from the PRC, or so-called exit bans, to:</p>
	<ul>
		<li>compel individuals to participate in PRC government investigations;</li>
		<li>pressure family members of the restricted individual to return to the PRC from abroad;</li>
		<li>resolve civil disputes in favor of PRC citizens; and</li>
		<li>gain bargaining leverage over foreign governments.</li>
	</ul>
	</blockquote>
	HTML;


$page->parent('world.html');
$page->body($div_stub);

$page->body($h2_Authoritarianism);
$page->body($div_Chinese_society);
$page->body($div_Transnational_authoritarianism);
$page->body($div_freedomhouse_China_Transnational_Repression_Origin_Country_Case_Study);

$page->body($h2_Doing_business_China);
$page->body($div_doing_business_with_china);

$page->body('china_and_taiwan.html');
$page->body('chinese_expansionism.html');
$page->body('one_country_two_systems.html');
$page->body($div_codeberg);
$page->body('Country indices');

$page->body($div_wikipedia_china_democracy);
$page->body($div_wikipedia_china_human_right);
$page->body($div_wikipedia_2022_covid_protests_china);
