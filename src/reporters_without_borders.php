<?php
$page = new Page();
$page->h1('Reporters Without Borders');
$page->stars(0);
$page->keywords('Reporters Without Borders', 'RSF', 'Press Freedom Index');

$page->preview( <<<HTML
	<p>Reporters Without Borders is an international non-profit and non-governmental organisation
	with the stated aim of safeguarding the right to freedom of information.
	It describes its advocacy as founded on the belief that everyone requires access to the news and information,
	in line with Article 19 of the Universal Declaration of Human Rights that recognizes the right to receive and share information regardless of frontiers.</p>
	HTML );

$page->snp('description', "For press freedom.");
$page->snp('image', "/copyrighted/Reporters_Without_Borders.1200-630.png");


$div_Introduction = new ContentSection();
$div_Introduction->content = <<<HTML
	<p>Reporters Without Borders is a non-governmental $organisation
	aiming to safeguard the right of freedom of information.</p>

	<p>See also: ${'freedom of speech'}.</p>
	HTML;


$div_rwb_official_website = new WebsiteContentSection();
$div_rwb_official_website->content = <<<HTML
	<h3><a href="https://rsf.org/en">Reporters Without Borders</a></h3>

	<p>The organisation's official website.</p>
	HTML;


$h2_rwb_press_freedom_index = new h2HeaderContent('Press Freedom Index');
$h2_rwb_press_freedom_index->id('press-freedom-index');

$div_rwb_press_freedom_index = new ContentSection();
$div_rwb_press_freedom_index->content = <<<HTML
	<p>The Press Freedom Index is an annual ranking of $countries compiled and published by Reporters Without Borders.
	The score and ranking of each country as calculated by Reporters Without Borders
	is included in our own country profiles.</p>

	<p>See the full data in their website:</p>

	<ul>
		<li><a href="https://rsf.org/en/index?year=2022">Press Freedom Index (2022)</a></li>
	</ul>
	HTML;


$div_wikipedia_reporters_without_border = new WikipediaContentSection();
$div_wikipedia_reporters_without_border->setTitleText('Reporters Without Borders');
$div_wikipedia_reporters_without_border->setTitleLink('https://en.wikipedia.org/wiki/Reporters_Without_Borders');
$div_wikipedia_reporters_without_border->content = <<<HTML
	<p>Reporters Without Borders is an international non-profit and non-governmental organisation
	with the stated aim of safeguarding the right to freedom of information.
	It describes its advocacy as founded on the belief that everyone requires access to the news and information,
	in line with Article 19 of the Universal Declaration of Human Rights that recognizes the right to receive and share information regardless of frontiers.</p>
	HTML;

$div_wikipedia_press_freedom_index = new WikipediaContentSection();
$div_wikipedia_press_freedom_index->setTitleText('Press Freedom Index');
$div_wikipedia_press_freedom_index->setTitleLink('https://en.wikipedia.org/wiki/Press_Freedom_Index');
$div_wikipedia_press_freedom_index->content = <<<HTML
	<p>The Press Freedom Index is an annual ranking of countries compiled and published by Reporters Without Borders since 2002
	based upon the organisation's own assessment of the countries' press freedom records in the previous year.
	Reporters Without Borders is careful to note that the index only deals with press freedom
	and does not measure the quality of journalism in the countries it assesses,
	nor does it look at human rights violations in general.</p>
	HTML;

$div_wikipedia_the_uncensored_library = new WikipediaContentSection();
$div_wikipedia_the_uncensored_library->setTitleText('The Uncensored Library');
$div_wikipedia_the_uncensored_library->setTitleLink('https://en.wikipedia.org/wiki/The_Uncensored_Library');
$div_wikipedia_the_uncensored_library->content = <<<HTML
	<p>The Uncensored Library is a Minecraft server and map	released by Reporters Without Borders
	as an attempt to circumvent censorship in countries without freedom of the press.
	The library contains banned reporting from Mexico, Russia, Vietnam, Saudi Arabia, and Egypt.</p>
	HTML;


$page->parent('media.html');
$page->parent('list_of_organisations.html');
$page->body($div_stub);
$page->body($div_Introduction);

$page->body($div_rwb_official_website);
$page->body($div_wikipedia_reporters_without_border);

$page->body($h2_rwb_press_freedom_index);
$page->body($div_rwb_press_freedom_index);
$page->body('list_of_indices.html');
$page->body($div_wikipedia_press_freedom_index);

$page->body($div_wikipedia_the_uncensored_library);
