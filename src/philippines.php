<?php
$page = new CountryPage('Philippines');
$page->h1('Philippines');
$page->stars(1);
$page->keywords('Philippines');

$page->preview( <<<HTML
	<p></p>
	HTML );



$page->snp('description', "109 million inhabitants.");
$page->snp('image', "/copyrighted/vernon-raineil-cenzon-JZ7OowQQVgg.1200-630.jpg");


$r1 = $page->ref('https://en.wikipedia.org/wiki/List_of_presidents_of_the_Philippines', 'List of presidents of the Philippines');
$r2 = $page->ref('https://en.wikipedia.org/wiki/Coup_attempts_against_Corazon_Aquino', 'Coup attempts against Corazon Aquino');
$r3 = $page->ref('https://en.wikipedia.org/wiki/1989_Philippine_coup_d%27%C3%A9tat_attempt', "1989 Philippine coup d'état attempt");
$r4 = $page->ref('https://en.wikipedia.org/wiki/Presidency_of_Fidel_V._Ramos#Death_penalty', 'Presidency of Fidel V. Ramos ¶Death penalty');
$r5 = $page->ref('https://en.wikipedia.org/wiki/Presidency_of_Fidel_V._Ramos#Charter_change', 'Presidency of Fidel V. Ramos ¶Charter change');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The Philippines is a country with plenty of resources and full of potential.</p>
	HTML;

$h2_environment_and_economy = new h2HeaderContent('Environment and economy');


$div_Environmental_policy = new ContentSection();
$div_Environmental_policy->content = <<<HTML
	<h3>Environmental policy</h3>

	<p>In 2021, the Philippines received the Future Policy Special Award from the ${'World Future Council'}
	for its groundbreaking legislation to curb the use of toxic lead and lead compounds in industrial paints.</p>
	HTML;


$div_wpc_Philippines_Chemical_Control_Order_for_Lead_and_Lead_Compounds_CCO_2013_24 = new FuturePolicyContentSection();
$div_wpc_Philippines_Chemical_Control_Order_for_Lead_and_Lead_Compounds_CCO_2013_24->setTitleText('Philippines: Chemical Control Order for Lead and Lead Compounds (CCO, 2013-24)');
$div_wpc_Philippines_Chemical_Control_Order_for_Lead_and_Lead_Compounds_CCO_2013_24->setTitleLink('https://www.futurepolicy.org/global/philippines-chemical-control-order-for-lead-and-lead-compounds-cco-2013-24/');
$div_wpc_Philippines_Chemical_Control_Order_for_Lead_and_Lead_Compounds_CCO_2013_24->content = <<<HTML
	<p>With the Chemical Control Order for Lead and Lead Compounds (CCO),
	the Philippines became the first Southeast Asian country to successfully implement legislation towards lead-safe paint.
	The policy’s objective is to increase awareness of the toxicity of lead exposure and to provide safer alternatives
	to protect the health of the population and the environment.
	It comprises a roadmap with clear definitions, phase-out plans, and decisive instruments with special attention to children.
	The CCO combines a collaborative top-down and bottom-up strategy with successful implementation.
	While globally only a few countries have enacted comprehensive bans on the use of lead additives in all paints,
	the Philippines demonstrate that it is entirely possible to restrict the use of lead in all paints to the maximum limit of 90 ppm,
	including in industrial paints, which generally have lead concentrations that are up to 10 times higher.</p>
	HTML;


$h2_Philippines_and_China = new h2HeaderContent('Philippines and China');


$div_China_in_Philippine_economy = new ContentSection();
$div_China_in_Philippine_economy->content = <<<HTML
	<h3>China in Philippine economy</h3>

	<p>Private Chinese investors as well as the Chinese government own large swathes of the Philippines economy.
	For example, the Philippine maritime transportation giant 2Go is owned by the Chinese Communist Party.</p>

	<p>It is a major challenge for the Philippines' democratic future and economic independence
	to reduce the share of its economy owned by Chinese interests.</p>
	HTML;

$div_Philippines_and_Chinese_expansionism = new ContentSection();
$div_Philippines_and_Chinese_expansionism->content = <<<HTML
	<h3>Philippines and Chinese expansionism</h3>

	<p>${'Chinese expansionism'} is affecting all the countries neighbouring the South China Sea,
	especially since $China claims most of the sea with its so called ${'nine-dash line'}.
	Thus confrontations between Chinese interests and the Philippines are frequent.</p>

	<p>See the article below for more details.</p>

	<p>In 2023, $Australia held joint military drills in with the Philippines in the South China Sea.
	Approximately 1,200 Australian soldiers and 560 Filipino marines took part in military exercises that involved storming a beach.</p>
	HTML;



$h2_democracy = new h2HeaderContent('Democracy');

$div_Historical_trend = new ContentSection();
$div_Historical_trend->content = <<<HTML
	<h3>Historical trend</h3>

	<p>The Philippines has a chequered history, with democratic ups and downs.<p>

	<p>During the years 1965~1886, the Philippines was autocratically ruled by Ferdinand Marcos, officially the 10th President of the Philippines.
	His rule saw martial law, human rights abuses and widespread corruption.</p>

	<p>Marcos' reign was put to an end
	after the contested 1986 presidential elections
	and by the subsequent People Power Revolution.
	Corazon Aquino was proclaimed president of the Philippines.
	She immediately issued a proclamation for a new constitution to be drafted.
	The ${'Constitution of the Philippines'} was formally adopted by plebiscite in 1987.</p>

	<p>Under Corazon Aquino's presidency (1986 ~ 1992), progress was made in revitalizing democratic institutions and respect for civil liberties.
	However there were ten coup attempts during her presidency. $r2 $r3</p>

	<p>President Fidel Ramos (1992 ~ 1998) revived the practice of the ${'death penalty'}, $r4
	which was abolished again in 2006.
	Ramos also tried to amend the constitution so that he could run for a second term. $r5
	The move was met with large-scale protests and Ramos declared he would not seek re-election.</p>

	<p>The constitution limits presidents to a single 6-year term.
	Joseph Estrada did not finish his term as he was forced to resign in 2001 by popular pressure,
	so his vice president, Gloria Macapagal Arroyo finished his term before being elected for her own full 6-year term.</p>

	<p>A few presidents were tempted to stay longer in power and tried to arrange for a constitutional amendment
	so that they could run for a second term.
	</p>
	<table>
		<tr>
			<th></th>
			<th colspan="2">Presidents of the 5th Republic of the Philippines $r1</th>
		</tr>
		<tr>
			<td>11</td>
			<td>1986 ~ 1992</td>
			<td>Corazon Aquino</td>
		</tr>
		<tr>
			<td>12</td>
			<td>1992 ~ 1998</td>
			<td>Fidel V. Ramos</td>
		</tr>
		<tr>
			<td>13</td>
			<td>1998 ~ 2001</td>
			<td>Joseph Estrada</td>
		</tr>
		<tr>
			<td>14</td>
			<td>2001 ~ 2010</td>
			<td>Gloria Macapagal Arroyo</td>
		</tr>
		<tr>
			<td>15</td>
			<td>2010 ~ 2016</td>
			<td>Benigno Aquino III</td>
		</tr>
		<tr>
			<td>16</td>
			<td>2016 ~ 2022</td>
			<td>Rodrigo Duterte</td>
		</tr>
		<tr>
			<td>17</td>
			<td>2022 ~ present</td>
			<td>Bongbong Marcos</td>
		</tr>
	</table>
	<table>
		<tr>
			<th>Years</th>
			<th>${'Freedom House'}'s rating </th>
		</tr>
		<tr>
			<td>1973 ~ 1986</td>
			<td>Partly Free</td>
		</tr>
		<tr>
			<td>1987 ~ 1989</td>
			<td>Free</td>
		</tr>
		<tr>
			<td>1990 ~ 1995</td>
			<td>Partly Free</td>
		</tr>
		<tr>
			<td>1996 ~ 2004</td>
			<td>Free</td>
		</tr>
		<tr>
			<td>2005 ~ 2022</td>
			<td>Partly Free</td>
		</tr>
	</table>
	HTML;


$div_freedom_of_the_press = new ContentSection();
$div_freedom_of_the_press->content = <<<HTML
	<h3>Freedom of the press</h3>

	<p>One of the most important challenges to Philippine democracy is the freedom of the press.
	Many reporters (like ${'Maria Ressa'}) suffer or have suffered  harassment, many even having been killed.</p>
	HTML;


$div_wikipedia_Human_rights_in_the_Philippines = new WikipediaContentSection();
$div_wikipedia_Human_rights_in_the_Philippines->setTitleText('Human rights in the Philippines');
$div_wikipedia_Human_rights_in_the_Philippines->setTitleLink('https://en.wikipedia.org/wiki/Human_rights_in_the_Philippines');
$div_wikipedia_Human_rights_in_the_Philippines->content = <<<HTML
	<p>The concept and practice of human rights within the Philippines is defined by Article III of the Philippine Constitution,
	as well as the United Nations' International Bill of Human Rights, to which the Philippines is a signatory.</p>
	HTML;

$div_wikipedia_Commission_on_Human_Rights_Philippines = new WikipediaContentSection();
$div_wikipedia_Commission_on_Human_Rights_Philippines->setTitleText('Commission on Human Rights (Philippines)');
$div_wikipedia_Commission_on_Human_Rights_Philippines->setTitleLink('https://en.wikipedia.org/wiki/Commission_on_Human_Rights_(Philippines)');
$div_wikipedia_Commission_on_Human_Rights_Philippines->content = <<<HTML
	<p>The Commission on Human Rights is an independent constitutional office created under the 1987 Constitution of the Philippines,
	with the primary function of investigating all forms of human rights violations involving civil and political rights in the Philippines.</p>
	HTML;

$div_wikipedia_Campaign_for_Human_Rights_in_the_Philippines = new WikipediaContentSection();
$div_wikipedia_Campaign_for_Human_Rights_in_the_Philippines->setTitleText('Campaign for Human Rights in the Philippines');
$div_wikipedia_Campaign_for_Human_Rights_in_the_Philippines->setTitleLink('https://en.wikipedia.org/wiki/Campaign_for_Human_Rights_in_the_Philippines');
$div_wikipedia_Campaign_for_Human_Rights_in_the_Philippines->content = <<<HTML
	<p>The Campaign for Human Rights in the Philippines (CHRP) is a small but highly active human rights watchdog based in the United Kingdom.
	It has the backing of the British T.U.C, Amnesty International,
	and several educational institutions including a very close relationship with the School of Oriental and African Studies.</p>
	HTML;

$div_wikipedia_Philippine_Human_Rights_Information_Center = new WikipediaContentSection();
$div_wikipedia_Philippine_Human_Rights_Information_Center->setTitleText('Philippine Human Rights Information Center');
$div_wikipedia_Philippine_Human_Rights_Information_Center->setTitleLink('https://en.wikipedia.org/wiki/Philippine_Human_Rights_Information_Center');
$div_wikipedia_Philippine_Human_Rights_Information_Center->content = <<<HTML
	<p>The Philippine Human Rights Information Center (PhilRights) is a non-profit, national human rights organization in the Philippines, Manila.</p>
	HTML;

$div_wikipedia_Extrajudicial_killings_and_forced_disappearances_in_the_Philippines = new WikipediaContentSection();
$div_wikipedia_Extrajudicial_killings_and_forced_disappearances_in_the_Philippines->setTitleText('Extrajudicial killings and forced disappearances in the Philippines');
$div_wikipedia_Extrajudicial_killings_and_forced_disappearances_in_the_Philippines->setTitleLink('https://en.wikipedia.org/wiki/Extrajudicial_killings_and_forced_disappearances_in_the_Philippines');
$div_wikipedia_Extrajudicial_killings_and_forced_disappearances_in_the_Philippines->content = <<<HTML
	<p>The Philippine Alliance of Human Rights Advocates (PAHRA) is a non-profit, national human rights organization in the Philippines, Manila.</p>
	HTML;


$div_wikipedia_Capital_punishment_in_the_Philippines = new WikipediaContentSection();
$div_wikipedia_Capital_punishment_in_the_Philippines->setTitleText('Capital punishment in the Philippines');
$div_wikipedia_Capital_punishment_in_the_Philippines->setTitleLink('https://en.wikipedia.org/wiki/Capital_punishment_in_the_Philippines');
$div_wikipedia_Capital_punishment_in_the_Philippines->content = <<<HTML
	<p>After Marcos was deposed in 1986,
	the newly drafted 1987 Constitution prohibited the death penalty
	but allowed Congress to reinstate it "hereafter" for "heinous crimes",
	making the Philippines the first Asian country to abolish capital punishment.
	The capital punishment has been reinstated several times thereafter.</p>
	HTML;



$page->parent('world.html');
$page->body($div_stub);

$page->body($div_introduction);

$page->body($h2_environment_and_economy);
$page->body($div_Environmental_policy);
$page->body($div_wpc_Philippines_Chemical_Control_Order_for_Lead_and_Lead_Compounds_CCO_2013_24);

$page->body($h2_Philippines_and_China);
$page->body($div_China_in_Philippine_economy);
$page->body($div_Philippines_and_Chinese_expansionism);
$page->body('chinese_expansionism.html');

$page->body($h2_democracy);
$page->body($div_Historical_trend);
$page->body('constitution_of_the_philippines.html');
$page->body($div_freedom_of_the_press);
$page->body('maria_ressa.html');
$page->body('Country indices');

$page->body($div_wikipedia_Extrajudicial_killings_and_forced_disappearances_in_the_Philippines);
$page->body($div_wikipedia_Human_rights_in_the_Philippines);
$page->body($div_wikipedia_Philippine_Human_Rights_Information_Center);
$page->body($div_wikipedia_Campaign_for_Human_Rights_in_the_Philippines);
$page->body($div_wikipedia_Commission_on_Human_Rights_Philippines);
$page->body($div_wikipedia_Capital_punishment_in_the_Philippines);
