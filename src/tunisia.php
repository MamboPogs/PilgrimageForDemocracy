<?php
$page = new CountryPage('Tunisia');
$page->h1('Tunisia');
$page->keywords('Tunisia');
$page->stars(0);

$page->snp('description', '11.7 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The 2011 democratization process started in 2011 was short lived.
	Since then, Tunisia has been on a steady course of $democratic erosion.</p>

	<p>Many $refugees from other African countries attempt to transit through Tunisia
	before crossing the Mediterranean See onwards to Europe.
	Europe, while concerned with the democratic backsliding,
	must contend with Tunisia in order to control $immigration into Europe.
	According to the Italian Ministry of Interior, 42,719 people have left for Europe from Tunisia so far in 2023.</p>

	<p>In 2023, the ${'European Union'} offered Tunisia 100 million euros for border management, search and rescue,
	and returns “rooted in respect for human $rights ” to address migration,
	as part of a larger 1 billion euros loan package to help Tunisia's faltering economy.</p>

	<p>Tunisia seeks to join the $BRICS block.</p>
	HTML;

$div_wikipedia_Tunisia = new WikipediaContentSection();
$div_wikipedia_Tunisia->setTitleText('Tunisia');
$div_wikipedia_Tunisia->setTitleLink('https://en.wikipedia.org/wiki/Tunisia');
$div_wikipedia_Tunisia->content = <<<HTML
	<p>In 2011, the Tunisian Revolution, which was triggered by dissatisfaction with the lack of freedom and $democracy
	under the 24-year rule of President Zine El Abidine Ben Ali, overthrew his regime
	and catalyzed the broader Arab Spring movement across the region.
	Free multiparty parliamentary elections were held shortly thereafter;
	the country again voted for parliament on 26 October 2014, and for president on 23 November 2014.
	From 2014 to 2020, it was considered the only democratic state in the Arab world,
	according to the ${'Democracy Index'} (The Economist).
	After a democratic backsliding Tunisia is rated a hybrid regime.
	It is one of the few countries in Africa ranking high in the Human Development Index,
	with one of the highest per capita incomes on the continent, ranking 129th in GDP per capita income.</p>
	HTML;

$div_wikipedia_2022_Tunisian_constitutional_referendum = new WikipediaContentSection();
$div_wikipedia_2022_Tunisian_constitutional_referendum->setTitleText('2022 Tunisian constitutional referendum');
$div_wikipedia_2022_Tunisian_constitutional_referendum->setTitleLink('https://en.wikipedia.org/wiki/2022_Tunisian_constitutional_referendum');
$div_wikipedia_2022_Tunisian_constitutional_referendum->content = <<<HTML
	<p>A constitutional referendum was held in Tunisia on 25 July 2022 by the Independent High Authority for Elections.
	The referendum was supported by the Tunisian president, Kais Saied, one year into a political crisis that began on 25 July 2021.</p>
	HTML;


$page->parent('world.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_Tunisia);
$page->body($div_wikipedia_2022_Tunisian_constitutional_referendum);
