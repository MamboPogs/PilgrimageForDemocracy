<?php
$page = new CountryPage('Sweden');
$page->h1('Sweden');
$page->keywords('Sweden');
$page->stars(0);

$page->snp('description', '10,5 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Amidst a series of Quran burning provocative stunts (See $blasphemy),
	currently permitted under laws protecting ${'freedom of speech'},
	Sweden is considering following $Denmark's example in introducing legislation to make such sacred book burning illegal.</p>
	HTML;

$div_wikipedia_Sweden = new WikipediaContentSection();
$div_wikipedia_Sweden->setTitleText('Sweden');
$div_wikipedia_Sweden->setTitleLink('https://en.wikipedia.org/wiki/Sweden');
$div_wikipedia_Sweden->content = <<<HTML
	<p>Sweden, formally the Kingdom of Sweden, is a Nordic country located on the Scandinavian Peninsula in Northern Europe.
	It borders Norway to the west and north, Finland to the east,
	and is connected to Denmark in the southwest by a bridge–tunnel across the Öresund.</p>
	HTML;


$page->parent('world.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_Sweden);
