<?php
$page = new CountryPage('Liberia');
$page->h1('Liberia');
$page->keywords('Liberia');
$page->stars(0);

$page->snp('description', '5.5 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Liberia = new WikipediaContentSection();
$div_wikipedia_Liberia->setTitleText('Liberia');
$div_wikipedia_Liberia->setTitleLink('https://en.wikipedia.org/wiki/Liberia');
$div_wikipedia_Liberia->content = <<<HTML
	<p>Liberia, officially the Republic of Liberia, is a country on the West African coast.
	It is bordered by Sierra Leone to its northwest, Guinea to its north, Ivory Coast to its east,
	and the Atlantic Ocean to its south and southwest.</p>
	HTML;


$page->parent('world.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_Liberia);
