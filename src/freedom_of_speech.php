<?php
$page = new Page();
$page->h1('Freedom of speech');
$page->stars(0);
$page->keywords('Freedom of speech', 'freedom of speech');

$page->preview( <<<HTML
	<p>Freedom of speech is not what people think it is...</p>
	HTML );

$page->snp('description', "Freedom of speech is not what people think it is...");
//$page->snp('image', "/copyrighted/");



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>While the concept of freedom of speech is well documented (see Wikipedia articles below),
	it is however often misunderstood.</p>

	<p>The purpose of this page will be to explain a few salient points and try to correct some misconceptions.</p>

	<p>Many $organisations promote and aim to safeguard freedom of speech and freedom of information,
	like ${'Reporters Without Borders'}.</p>
	HTML;



$div_wikipedia_Freedom_of_speech = new WikipediaContentSection();
$div_wikipedia_Freedom_of_speech->setTitleText('Freedom of speech');
$div_wikipedia_Freedom_of_speech->setTitleLink('https://en.wikipedia.org/wiki/Freedom_of_speech');
$div_wikipedia_Freedom_of_speech->content = <<<HTML
	<p>Freedom of speech is a principle that supports the freedom of an individual or a community to articulate their opinions and ideas
	without fear of retaliation, censorship, or legal sanction.
	The right to freedom of expression has been recognised as a human right in the Universal Declaration of Human Rights
	and international human rights law by the United Nations.
	Many countries have constitutional law that protects free speech.</p>
	HTML;

$div_wikipedia_Freedom_of_speech_by_country = new WikipediaContentSection();
$div_wikipedia_Freedom_of_speech_by_country->setTitleText('Freedom of speech by country');
$div_wikipedia_Freedom_of_speech_by_country->setTitleLink('https://en.wikipedia.org/wiki/Freedom_of_speech_by_country');
$div_wikipedia_Freedom_of_speech_by_country->content = <<<HTML
	<p>The list is partially composed of the respective countries' government claims and does not fully reflect the de facto situation.</p>
	HTML;

$div_wikipedia_Speech_crimes = new WikipediaContentSection();
$div_wikipedia_Speech_crimes->setTitleText('Speech crimes');
$div_wikipedia_Speech_crimes->setTitleLink('https://en.wikipedia.org/wiki/Speech_crimes');
$div_wikipedia_Speech_crimes->content = <<<HTML
	<p>Speech crimes are certain kinds of speech that are criminalized by promulgated laws or rules.
	Criminal speech is a direct preemptive restriction on freedom of speech, and the broader concept of freedom of expression.</p>
	HTML;

$div_wikipedia_Censorship = new WikipediaContentSection();
$div_wikipedia_Censorship->setTitleText('Censorship');
$div_wikipedia_Censorship->setTitleLink('https://en.wikipedia.org/wiki/Censorship');
$div_wikipedia_Censorship->content = <<<HTML
	<p>Direct censorship may or may not be legal, depending on the type, location, and content.
	Many countries provide strong protections against censorship by law, but none of these protections are absolute
	and frequently a claim of necessity to balance conflicting rights is made, in order to determine what could and could not be censored.</p>
	HTML;


$page->parent('democracy.html');
$page->body($div_stub);

$page->body($div_introduction);

$page->body($div_wikipedia_Freedom_of_speech);
$page->body($div_wikipedia_Freedom_of_speech_by_country);
$page->body($div_wikipedia_Speech_crimes);
$page->body($div_wikipedia_Censorship);
