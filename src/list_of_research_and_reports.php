<?php
$page = new Page();
$page->h1('List of research and reports');
$page->stars(0);

$page->preview( <<<HTML
	<p>Research for peace, democracy and social justice.</p>
	HTML );


$page->snp('description', "Research for peace, democracy and social justice.");
//$page->snp('image', "/copyrighted/");



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Research for peace, democracy and social justice.</p>
	HTML;

$list = new ListOfPages();
$list->add('the_hidden_dimension_in_democracy.html');
$print_list = $list->print();

$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>List</h3>

	$print_list
	HTML;


$page->parent('lists.html');
$page->body($div_stub);

$page->body($div_introduction);
$page->body($div_list);
