<?php
$page = new Page();
$page->h1('Blasphemy');
$page->keywords('Blasphemy', 'blasphemy');
$page->stars(1);

$page->snp('description', 'Blasphemy, freedom of conscience and democracy.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Blasphemy is a difficult topic to address from a $democratic perspective.
	The $freedom to follow, or not, any $religion, is guaranteed by ${'freedom of conscience'},
	one of the key ${'primary features of democracy'}.</p>

	<p>Such freedom however includes the freedom <em>not</em> to have any religious practices
	be imposed upon oneself.
	Anyone person shall remain free not to practice any religion, perform rituals, etc.</p>

	<p>We have to balance that with the ${'secondary features of democracy'}:
	we do not live isolated but within communities.
	We have an obligation not to harm others.</p>

	<p>From these democratic perspective, we shall differentiate between two types of blasphemy,
	which are better explained by giving concrete examples.</p>

	<p><strong>Blasphemy and personal freedom</strong>:
	In Islam, it is considered blasphemous for women not to cover their hair and wear the proper attire
	(See: ${'social dress code'}).
	However, any person shall be free not to adhere to any religious precepts.
	Thus, in a $democracy, any woman should feel completely free to dress as she pleases, with or without head covering.</p>

	<p><strong>Blasphemy and hurting others</strong>:
	in any religion, desecrating a sacred book, e.g. the Bible, the Koran, etc., is seen as a blasphemy.
	Wilfully burning a Bible or a Koran is gravely blasphemous.
	The harm caused on the religious devotees is all the more unjustified that
	the burning of the sacred book does not benefit in any way the person performing the sacrilege.
	We see such acts as gratuitous and without merit.
	The book burning might be done in order to "make a point".
	Our point would be that it is better to learn to live together in harmony, despite our differences,
	and respect each other's freedom of conscience.</p>

	<p>We offer this preliminary analysis as a starting point for further discussion among
	active participants of the $Pilgrimage project.</p>
	HTML;


$div_Denmark = new ContentSection();
$div_Denmark->content = <<<HTML
	<h3>Denmark</h3>

	<p>In 2023, $Denmark proposed a bill that could see ban on Quran burnings,
	aimed at ending the provocative stunts.</p>

	<p>Peter Hummelgaard, the justice minister, explained that the proposed law is intended
	to be written into the same regulation that currently bans the desecration of other countries’ flags.</p>

	<p>The Danish law would prohibit the “improper treatment of objects of significant religious significance
	to a religious community”, he said.</p>

	<p>Similar legislation has been considered in $Sweden.</p>
	HTML;


$div_Pakistan = new ContentSection();
$div_Pakistan->content = <<<HTML
	<h3>Pakistan</h3>

	<p>As in many $countries, $Pakistan is rife with many religious conflicts.
	Christian churches have been burned in retaliation for Korans having been desecrated.</p>

	<p>$Pakistan's blasphemy law is probably a good example of what a proper blasphemy law should not look like.</p>

	<p>The Pakistani law states that:
	"<em>any “derogatory remarks, etc, in respect of the Holy Prophet [Muhammad] either spoken or written,
	or by visible representation, or by any imputation, innuendo or insinuation, directly or indirectly
	shall be punished with death, or imprisonment for life, and shall also be liable to fine”.</em></p>

	<p>First, according to the principle of separation of Church and State,
	the law should not be based on any specific religion.
	In order to respect the ${'freedom of conscience'} of every single citizen,
	the law should treat every religion equally.</p>

	<p>Secondly, the punition imposed for sacrilege, the death penalty, is obviously not acceptable.</p>
	HTML;

$div_wikipedia_Blasphemy = new WikipediaContentSection();
$div_wikipedia_Blasphemy->setTitleText('Blasphemy');
$div_wikipedia_Blasphemy->setTitleLink('https://en.wikipedia.org/wiki/Blasphemy');
$div_wikipedia_Blasphemy->content = <<<HTML
	<p>Blasphemy, as defined in some religions or religion-based laws, is an insult that shows contempt,
	disrespect or lack of reverence concerning a deity, an object considered sacred or something considered inviolable.
	Some religions regard blasphemy as a religious crime, especially the Abrahamic religions,
	including insulting the Islamic prophet Muhammad in Islam, speaking the "sacred name" in Judaism,
	and the "eternal sin" in Christianity.</p>
	HTML;

$div_wikipedia_Blasphemy_law = new WikipediaContentSection();
$div_wikipedia_Blasphemy_law->setTitleText('Blasphemy law');
$div_wikipedia_Blasphemy_law->setTitleLink('https://en.wikipedia.org/wiki/Blasphemy_law');
$div_wikipedia_Blasphemy_law->content = <<<HTML
	<p>A blasphemy law is a law prohibiting blasphemy, which is the act of insulting or showing contempt or lack of reverence to a deity,
	or sacred objects, or toward something considered sacred or inviolable.
	According to Pew Research Center, about a quarter of the world's countries and territories (26%)
	had anti-blasphemy laws or policies as of 2014.</p>
	HTML;


$page->parent('religion.html');
$page->body($div_stub);
$page->body($div_introduction);

$page->body($div_Denmark);
$page->body($div_Pakistan);

$page->body($div_wikipedia_Blasphemy);
$page->body($div_wikipedia_Blasphemy_law);
