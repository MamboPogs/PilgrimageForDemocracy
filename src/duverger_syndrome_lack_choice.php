<?php
$page = new Page();
$page->h1('Duverger symptom 5: lack of choice, lack of good candidates');
$page->stars(0);

$page->preview( <<<HTML
	HTML );

$r1 = $page->ref('https://thehill.com/homenews/senate/4124078-senate-gop-romney-winnowing-anti-trump-field/', 'Senate GOP rallies behind Romney call for winnowing anti-Trump field');

//$page->snp('description', "");
//$page->snp('image', "/copyrighted/");


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Many times, voters never get to vote for their favourite candidate,
	who, more often than not, drops out of the race for one reason or another before the general election.</p>

	<p>Because of the systemic failure of the electoral process, political parties must use strategy to remain competitive at the ballot box.
	It happens in every democratic countries at every election cycles.</p>

	<p>For the 2024 US presidential election, the anti-Trump GOP establishment are already trying to prevent
	a multitude of anti-Trump republican candidates from running in the Republican primary
	in order to concentrate the party's efforts on one single candidates
	who, in their view, would have a better chance of winning the race against Joe Biden, the Democratic incumbent. $r1</p>
	HTML;


$page->parent('duverger_syndrome.html');
$page->body($div_stub);
$page->body($div_introduction);
