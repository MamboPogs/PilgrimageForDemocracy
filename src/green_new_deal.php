<?php
$page = new Page();
$page->h1('Green New Deal');
$page->keywords('Green New Deal');
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The Green New Deal is a series of proposals in various countries for ${'social justice'}, $environment protection.</p>

	<p>See also the organisation: the ${'Green New Deal Network'}.</p>
	HTML;

$div_wikipedia_Green_New_Deal = new WikipediaContentSection();
$div_wikipedia_Green_New_Deal->setTitleText('Green New Deal');
$div_wikipedia_Green_New_Deal->setTitleLink('https://en.wikipedia.org/wiki/Green_New_Deal');
$div_wikipedia_Green_New_Deal->content = <<<HTML
	<p>Green New Deal (GND) proposals call for public policy to address climate change
	along with achieving other social aims like job creation, economic growth and reducing economic inequality.</p>
	HTML;


$page->parent('environment.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_wikipedia_Green_New_Deal);
