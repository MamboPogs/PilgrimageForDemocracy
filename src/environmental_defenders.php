<?php
$page = new Page();
$page->h1('Environmental defenders');
$page->keywords('Environmental defenders', 'environmental defenders');
$page->stars(1);

$page->preview( <<<HTML
	<p></p>
	HTML );

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>In $Colombia, 611 environmental defenders have been assassinated since 2016,
	including 332 Indigenous people.</p>

	<p>In 2021 alone, as per a ${'Global Witness'} report, 200 land and environmental defenders were killed worldwide.</p>
	HTML;

$div_wikipedia_Environmental_defender = new WikipediaContentSection();
$div_wikipedia_Environmental_defender->setTitleText('Environmental defender');
$div_wikipedia_Environmental_defender->setTitleLink('https://en.wikipedia.org/wiki/Environmental_defender');
$div_wikipedia_Environmental_defender->content = <<<HTML
	<p>Environmental defenders or environmental human rights defenders are individuals or collectives
	who protect the environment from harms resulting from resource extraction, hazardous waste disposal,
	infrastructure projects, land appropriation, or other dangers.
	In 2019, the UN Human Rights Council unanimously recognised their importance to environmental protection.</p>
	HTML;



$div_Who_are_environmental_defenders = new WebsiteContentSection();
$div_Who_are_environmental_defenders->setTitleText('Who are environmental defenders?');
$div_Who_are_environmental_defenders->setTitleLink('https://www.unep.org/explore-topics/environmental-rights-and-governance/what-we-do/advancing-environmental-rights/who');
$div_Who_are_environmental_defenders->content = <<<HTML
	<p>The United Nations has recognized the threats to environmental defenders and called for their protection.
	UNEP builds on this work to support environmental defenders through its Defenders Policy, through which we:</p>

	<ul>
		<li><strong>Denounce</strong> the attacks, torture, intimidation and murders of environmental defenders;</li>
		<li><strong>Advocate</strong> with states and non-state actors, including business,
		for better protection of environmental rights and the people standing up for these rights;</li>
		<li><strong>Support</strong> the responsible management of natural resources;</li>
		<li><strong>Request</strong> government and companies’ accountability for the different events
		where environmental defenders have been affected / murdered.</li>
	</ul>
	HTML;



$div_Decade_of_defiance = new WebsiteContentSection();
$div_Decade_of_defiance->setTitleText('Decade of defiance');
$div_Decade_of_defiance->setTitleLink('https://www.globalwitness.org/en/campaigns/environmental-activists/decade-defiance/');
$div_Decade_of_defiance->content = <<<HTML
	<p><strong>Ten years of reporting land and environmental activism worldwide</strong></p>

	<p>While we at Global Witness were familiar with the targeting of our partners as they were defending their land and environment,
	the murder of our former colleague, Wutty, prompted us to confront a range of questions.
	What was the global picture, what were the implications of such attacks and what could be done to prevent them?</p>

	<p>All over the world, Indigenous peoples and environmental defenders risk their lives
	for the fight against climate change and biodiversity loss.
	Activists and communities play a crucial role as a first line of defence against ecological collapse,
	as well as being frontrunners in the campaign to prevent it.
	This report aims to share reflections on how we think about these questions now – ten years on from Wutty’s death –
	and makes an urgent appeal for global efforts to protect and reduce attacks against defenders.</p>
	HTML;



$page->parent('global_natural_resources.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_Who_are_environmental_defenders);
$page->body($div_Decade_of_defiance);
$page->body('global_witness.html');

$page->body($div_wikipedia_Environmental_defender);
