<?php
$page = new Page();
$page->h1('Housing');
$page->keywords('Housing', 'housing');
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Housing is and shelter is one of the most important basic human needs, and one of the social $rights.</p>

	<p>Also, let's remember the millions of people who have no housing at all, or who live in cramped ${'refugee camps'}.</p>
	HTML;

$div_wikipedia_Housing = new WikipediaContentSection();
$div_wikipedia_Housing->setTitleText('Housing');
$div_wikipedia_Housing->setTitleLink('https://en.wikipedia.org/wiki/Housing');
$div_wikipedia_Housing->content = <<<HTML
	<p>Housing, or more generally, living spaces, refers to the construction and assigned usage
	of houses or buildings individually or collectively, for the purpose of shelter.
	Housing is a basic human need, and it plays a critical role
	in shaping the quality of life for individuals, families, and communities.</p>
	HTML;

$div_wikipedia_Right_to_housing = new WikipediaContentSection();
$div_wikipedia_Right_to_housing->setTitleText('Right to housing');
$div_wikipedia_Right_to_housing->setTitleLink('https://en.wikipedia.org/wiki/Right_to_housing');
$div_wikipedia_Right_to_housing->content = <<<HTML
	<p>The right to housing (occasionally right to shelter) is the economic, social and cultural right
	to adequate housing and shelter.
	It is recognized in some national constitutions and
	in the Universal Declaration of Human Rights and International Covenant on Economic, Social and Cultural Rights.</p>
	HTML;

$div_wikipedia_Affordable_housing = new WikipediaContentSection();
$div_wikipedia_Affordable_housing->setTitleText('Affordable housing');
$div_wikipedia_Affordable_housing->setTitleLink('https://en.wikipedia.org/wiki/Affordable_housing');
$div_wikipedia_Affordable_housing->content = <<<HTML
	<p>Affordable housing is housing which is deemed affordable to those with a household income at or below the median
	as rated by the national government or a local government by a recognized housing affordability index.</p>
	HTML;


$page->parent('social_justice.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_wikipedia_Housing);
$page->body($div_wikipedia_Right_to_housing);
$page->body($div_wikipedia_Affordable_housing);
