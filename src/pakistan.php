<?php
$page = new CountryPage('Pakistan');
$page->h1('Pakistan');
$page->keywords('Pakistan');
$page->stars(0);

$page->snp('description', '241 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>As in many $countries, Pakistan is rife with many religious conflicts.
	Christian churches have been burned in retaliation for Korans having been desecrated.</p>

	<p>Pakistan's $blasphemy law single out a single religion, in breach of the ${'freedom of conscience'}.</p>

	HTML;

$div_wikipedia_Pakistan = new WikipediaContentSection();
$div_wikipedia_Pakistan->setTitleText('Pakistan');
$div_wikipedia_Pakistan->setTitleLink('https://en.wikipedia.org/wiki/Pakistan');
$div_wikipedia_Pakistan->content = <<<HTML
	<p>Pakistan, officially the Islamic Republic of Pakistan, is a country in South Asia.
	It is the world's fifth-most populous country, with a population of 241.5 million people,
	and has the world's largest Muslim population as of year 2023.</p>
	HTML;

$div_wikipedia_Human_rights_in_Pakistan = new WikipediaContentSection();
$div_wikipedia_Human_rights_in_Pakistan->setTitleText('Human rights in Pakistan');
$div_wikipedia_Human_rights_in_Pakistan->setTitleLink('https://en.wikipedia.org/wiki/Human_rights_in_Pakistan');
$div_wikipedia_Human_rights_in_Pakistan->content = <<<HTML
	<p>The situation of Human Rights in Pakistan is complex as a result of the country's diversity, large population,
	its status as a developing country and a sovereign Islamic democracy with a mixture of both Islamic and secular law.</p>
	HTML;


$page->parent('world.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_Pakistan);
$page->body($div_wikipedia_Human_rights_in_Pakistan);
