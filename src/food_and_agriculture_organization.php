<?php
$page = new Page();
$page->h1('Food and Agriculture Organization');
$page->stars(0);

$page->preview( <<<HTML
	<p></p>
	HTML );

$page->snp('description', 'UN organization for nutrition and food security.');
//$page->snp('image',       '/copyrighted/');



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>See also: $hunger.</p>
	HTML;



$div_Food_and_Agriculture_Organization_website = new WebsiteContentSection();
$div_Food_and_Agriculture_Organization_website->setTitleText('Food and Agriculture Organization');
$div_Food_and_Agriculture_Organization_website->setTitleLink('https://www.fao.org/home/en/');
$div_Food_and_Agriculture_Organization_website->content = <<<HTML
	<p>FAO's official website.</p>
	HTML;




$div_wikipedia_Food_and_Agriculture_Organization = new WikipediaContentSection();
$div_wikipedia_Food_and_Agriculture_Organization->setTitleText('Food and Agriculture Organization');
$div_wikipedia_Food_and_Agriculture_Organization->setTitleLink('https://en.wikipedia.org/wiki/Food_and_Agriculture_Organization');
$div_wikipedia_Food_and_Agriculture_Organization->content = <<<HTML
	<p>The Food and Agriculture Organization of the United Nations is a specialized agency of the United Nations
	that leads international efforts to defeat hunger and improve nutrition and food security.
	Its Latin motto, fiat panis, translates to "let there be bread". It was founded on 16 October 1945.</p>
	HTML;

$page->parent('list_of_organisations.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body($div_Food_and_Agriculture_Organization_website);
$page->body('hunger.html');

$page->body($div_wikipedia_Food_and_Agriculture_Organization);
