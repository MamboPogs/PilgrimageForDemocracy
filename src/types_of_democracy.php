<?php
$page = new Page();
$page->h1('Types of democracy');
$page->stars(0);
$page->keywords('types of democracy');

$page->preview( <<<HTML
	<p>
	</p>
	HTML );


$page->snp('description', "Different ways to categorize democracies.");
//$page->snp('image', "/copyrighted/");



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>See the $Pilgrimage five-levelled features of democracies.</p>

	<p>See also the ${'V-Dem Institute'}'s types of democracies.</p>
	HTML;



$div_wikipedia_Types_of_democracy = new WikipediaContentSection();
$div_wikipedia_Types_of_democracy->setTitleText('Types of democracy');
$div_wikipedia_Types_of_democracy->setTitleLink('https://en.wikipedia.org/wiki/Types_of_democracy');
$div_wikipedia_Types_of_democracy->content = <<<HTML
	<p>Types of $democracy refers to pluralism of governing structures such as governments (local through to global)
	and other constructs like workplaces, families, community associations, and so forth.
	Types of democracy can cluster around values.</p>
	HTML;

$page->parent('democracy.html');
$page->body($div_stub);

$page->body($div_introduction);
$page->body('decentralized_autonomous_organization.html');

$page->body($div_wikipedia_Types_of_democracy);
