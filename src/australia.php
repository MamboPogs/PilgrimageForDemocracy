<?php
$page = new CountryPage('Australia');
$page->h1('Australia');
$page->keywords('Australia');
$page->stars(0);

$page->snp('description', '26 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>A $democratic ally of the United States, Australia is a strategic partner in countering $China's authoritarian influence in the Pacific region.</p>
	HTML;


$div_countering_China = new ContentSection();
$div_countering_China->content = <<<HTML
	<h3>Countering China</h3>

	<p>In 2023, Australia held joint military drills in with the $Philippines in the South China Sea.
	Approximately 1,200 Australian soldiers and 560 Filipino marines took part in military exercises that involved storming a beach.</p>
	HTML;


$div_wikipedia_Australia = new WikipediaContentSection();
$div_wikipedia_Australia->setTitleText('Australia');
$div_wikipedia_Australia->setTitleLink('https://en.wikipedia.org/wiki/Australia');
$div_wikipedia_Australia->content = <<<HTML
	<p>Australia, officially the Commonwealth of Australia, is a sovereign country
	comprising the mainland of the Australian continent, the island of Tasmania, and numerous smaller islands.</p>
	HTML;


$page->parent('world.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body($div_countering_China);

$page->body('Country indices');

$page->body($div_wikipedia_Australia);
