<?php
$page = new Page();
$page->h1('Transparency International');
$page->keywords('Transparency International', 'Corruption Perception Index');
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>See their <a href="https://www.transparency.org/en/cpi/2021">2021 Corruption Perception Index</a>.</p>
	HTML;


$div_Transparency_International = new WebsiteContentSection();
$div_Transparency_International->setTitleText('Transparency International ');
$div_Transparency_International->setTitleLink('https://www.transparency.org/');
$div_Transparency_International->content = <<<HTML
	<p>The vision of <strong>Transparency International</strong> is of a world in which
	government, politics, business, civil society and people's daily lives are free of corruption.
	In order to get there, their mission is to stop corruption and promote
	transparency, accountability and integrity at all levels and across all sectors of society.</p>

	<p>The Corruption Perceptions Index reveals that the continued failure of most countries
	to significantly control corruption is contributing to a crisis in democracy around the world.</p>

	<p>Corruption chips away at democracy to produce a vicious cycle,
	where corruption undermines democratic institutions and, in turn, weak institutions are less able to control corruption.</p>
	HTML;



$div_wikipedia_Transparency_International = new WikipediaContentSection();
$div_wikipedia_Transparency_International->setTitleText('Transparency International');
$div_wikipedia_Transparency_International->setTitleLink('https://en.wikipedia.org/wiki/Transparency_International');
$div_wikipedia_Transparency_International->content = <<<HTML
	<p>Transparency International is a German registered association founded in 1993 by former employees of the World Bank.
	Based in Berlin, its nonprofit and non-governmental purpose is to take action
	to combat global corruption with civil societal anti-corruption measures
	and to prevent criminal activities arising from corruption.
	Its most notable publications include the Global Corruption Barometer and the Corruption Perceptions Index.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->body($div_stub);
$page->body($div_introduction);

$page->body($div_Transparency_International);

$page->body('corruption.html');
$page->body('list_of_indices.html');

$page->body($div_wikipedia_Transparency_International);
