<?php
$page = new Page();
$page->h1('Mo Gawdat');
$page->keywords('Mo Gawdat');
$page->stars(1);

$page->snp('description', 'Engineer, entrepreneur, writer and former chief business officer for Google X.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>He speaks on the topic of ${'artificial intelligence'}.</p>
	HTML;



$div_Ex_Google_Officer_Finally_Speaks_Out_On_The_Dangers_Of_AI = new WebsiteContentSection();
$div_Ex_Google_Officer_Finally_Speaks_Out_On_The_Dangers_Of_AI->setTitleText('Ex-Google Officer Finally Speaks Out On The Dangers Of AI ');
$div_Ex_Google_Officer_Finally_Speaks_Out_On_The_Dangers_Of_AI->setTitleLink('https://www.youtube.com/watch?v=bk-nQ7HF6k4');
$div_Ex_Google_Officer_Finally_Speaks_Out_On_The_Dangers_Of_AI->content = <<<HTML
	<p>In this new episode Steven sits down with the Egyptian entrepreneur and writer, Mo Gawdat.</p>
	HTML;

$div_Tax_AI = new ContentSection();
$div_Tax_AI->content = <<<HTML
	<h3>Taxing Artificial Intelligence</h3>

	<p>Here are a few excerpts of the above interview:</p>

	<blockquote>
	<p><strong>Mo Gawdat</strong>:
	The genie is out of the bottle.
	AI is game over for our way of life.
	This is a very disruptive moment where, maybe not tomorrow but in the near future, our way of life will differ.
	What I'm asking people to do is to start considering what that means to your life.
	What I'm asking governments to do is:
	don't wait until the first patient, start doing something.
	We're about to see mass job losses.
	We're about to see replacements of categories of jobs at large.
	It may take a year; it may take seven: it doesn't matter how long it takes but it's about to happen.
	Are you ready?
	I have a very very clear call to action for governments:
	$tax AI powered businesses at 98%.
	So, suddenly you do what the open letter was trying to do: slow them down a little bit
	and at the same time get enough money to pay for all of those people that will be disrupted by the technology.<p>

	<p><strong>Host</strong>:
	<em>The open letter for anybody that doesn't know was a letter signed by the likes of Elon Musk
	and a lot of industry leaders calling for AI to be stopped
	until we could basically figure out what's going
	and put legislation in place.
	You're saying tax those companies 98% and give the money to the humans
	that are going to be displaced.</em></p>

	<p><strong>Mo Gawdat</strong>:
	Yes, or give the money to other humans that can build control code that can figure out how we can stay safe.</p>
	</blockquote>



	<p>Further:</p>

	<blockquote>
	<p><strong>Mo Gawdat</strong>:
	Government needs to act now! Honestly: now, like we are late!
	Government needs to find a clever way.
	The open letter would not work. To stop AI would not work.
	AI needs to become expensive, so that we continue to develop it, we pour money on it, and we grow it,
	but we collect enough revenue to remedy the impact of AI.</p>

	<p><strong>Host</strong>:
	But the issue of one government making it expensive...
	Say the UK make AI really expensive is:
	we as a country will then lose the economic upside as a country,
	and the U.S and Silicon Valley will once again eat all the lunch.
	We'll just slow our country.</p>

	<p><strong>Mo Gawdat</strong>:
	What's the alternative?
	The alternative is that you don't have the funds that you need to deal with AI
	as it starts to affect people's lives, and people start to lose jobs...
	You need to have a universal basic income much closer than people think.
	Just like we had with furlough during Covid.
	I expect that there will be furlough with AI within the next year.
	(...)
	The countries that will not do this will eventually end up in a place where they are out of resources
	because the funds and the success went to the business and not to the people.
	</p>

	<p><strong>Host</strong>:
	But what happens when you make it expensive here,
	is all the developers move to where it's cheap.
	That's happened in web3 as well: everyone's gone to Dubai.
	It's kind of like what's going to happen in Silicon Valley, there'll be these centers which are "tax efficient":
	founders get good capital gains.
	Portugal said that there's no tax on crypto.
	Dubai said there's no tax on crypto.
	So loads of my friends have got into a plane, and they're building their crypto companies
	where there's no tax.
	That's the selfishness and kind of greed we talked about.</p>
	</blockquote>



	<p>About the cost of power:</p>

	<blockquote>
	<p><strong>Mo Gawdat</strong>:
	There is a disconnect between the power and the responsibility.
	The problem we have today is that there is a disconnect between those who are writing the code of $AI
	and the responsibility of what's going about to happen because of that code.
	I feel compassion for the rest of the world.</p>
	</blockquote>



	<p>The revolution is coming:</p>

	<blockquote>
	<p><strong>Mo Gawdat</strong>:
	Open AI and chatGPT are not going to be the only contributor to the next revolution.
	Then you get to that moment where you tell yourself:
	"Holy shit! This is going to kill a hundred thousand people!"
	I always go back to that Covid moment: patient zero.
	If we were upon patient zero if the whole world United and said:
	"Okay, hold on! Something is wrong. Let's all take a week off.
	No cross-border travel; everyone stays at home!"
	Covid would have ended two weeks. That's all we needed.
	But that's not what happened.
	What happens is: first ignorance then arrogance then debate, then blame, then agendas
	and my own benefits: my tribe versus your tribe.
	That's how humanity always reacts.</p>
	</blockquote>
	HTML;


$div_wikipedia_Mo_Gawdat = new WikipediaContentSection();
$div_wikipedia_Mo_Gawdat->setTitleText('Mo Gawdat');
$div_wikipedia_Mo_Gawdat->setTitleLink('https://en.wikipedia.org/wiki/Mo_Gawdat');
$div_wikipedia_Mo_Gawdat->content = <<<HTML
	<p>Mohammad "Mo" Gawdat is an Egyptian entrepreneur and writer.
	He is the former chief business officer for Google X and
	author of the books 'Solve for Happy' and 'Scary Smart'.</p>
	HTML;


$page->parent('list_of_people.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_Ex_Google_Officer_Finally_Speaks_Out_On_The_Dangers_Of_AI);
$page->body($div_Tax_AI);

$page->body($div_wikipedia_Mo_Gawdat);
