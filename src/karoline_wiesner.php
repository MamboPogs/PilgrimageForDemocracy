<?php
$page = new Page();
$page->h1('Karoline Wiesner');
$page->stars(0);
$page->keywords('Karoline Wiesner');


//$page->snp('description', "");
//$page->snp('image', "/copyrighted/");

$page->preview( <<<HTML
	<p>Professor of Complexity Science, University of Potsdam and a collaborator at the <a href='/v_dem_institute.html'>V-Dem Institute</a>.</p>
	HTML );

$r1 = $page->ref('https://www.csh.ac.at/researcher/karoline-wiesner/', 'Karoline Wiesner, University of Potsdam & CSH External Faculty');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Karoline Wiesner is Professor of Complexity Sciences at the University of Potsdam.
	She obtained a PhD in physics from Uppsala University in 2004.
	Wiesner's research focuses on the use of information theory in the study of formation, maintenance and stability of complex systems.
	Current topics include philosophical and mathematical foundations of complexity, complexity in climate systems, stability of democracy. ${r1}</p>

	<p>She is a collaborator at the ${'V-Dem Institute'}.</p>
	HTML;



$div_Karoline_Wiesner = new WebsiteContentSection();
$div_Karoline_Wiesner->setTitleText('Karoline Wiesner ');
$div_Karoline_Wiesner->setTitleLink('https://www.karowiesner.org/');
$div_Karoline_Wiesner->content = <<<HTML
	<p>Karoline Wiesner's website.</p>
	HTML;



$div_googlescholar_Karoline_Wiesner = new GoogleScholarContentSection();
$div_googlescholar_Karoline_Wiesner->setTitleText('Karoline Wiesner');
$div_googlescholar_Karoline_Wiesner->setTitleLink('https://scholar.google.com/citations?user=TUL8h7QAAAAJ&hl=en');
$div_googlescholar_Karoline_Wiesner->content = <<<HTML
	<p>Professor of Complexity Science, University of Potsdam.</p>
	HTML;




$page->parent('list_of_people.html');
$page->body($div_stub);
$page->body($div_introduction);

$page->body($div_Karoline_Wiesner);
$page->body($div_googlescholar_Karoline_Wiesner);
$page->body('the_hidden_dimension_in_democracy.html');
