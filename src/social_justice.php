<?php
$page = new Page();
$page->h1('Social justice');
$page->keywords('Social justice', 'social justice', 'social');
$page->stars(0);

$page->snp('description', 'What social justice is, and what it is not.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p>What social justice is, and what it is not.</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Social justice and $democracy are the two main interests of the present $project.</p>

	<p>"Social justice" may mean different things to different people.
	The implicit references when using this term are different across $countries and has changed over time.
	We shall strive to define the concept through a variety of related topics,
	providing a global picture both of what social justice is, and what it is not.</p>

	<p>We shall explore social justice especially in relation to $democracy and in a global context.
	Every single country in the $world, and their citizens,
	should have a realistic prospect of achieving peace and justice.</p>

	<p>We shall explore aspects of social justice in relation to $taxes, to the $environment
	and countless other topics.</p>

	<p>Our current political system is artificially dividing our democratic societies
	into two broad camps, often called the "left" and the "right".
	The reality is much more complex and we claim as our own the values of both sides.</p>
	HTML;



$div_codeberg_How_to_define_social_justice = new CodebergContentSection();
$div_codeberg_How_to_define_social_justice->setTitleText('How to define social justice?');
$div_codeberg_How_to_define_social_justice->setTitleLink('https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/43');
$div_codeberg_How_to_define_social_justice->content = <<<HTML
	<p>As we are just starting to write on the topic of social justice, our first task shall be to define the term and the project's objective.
	Your comments and ideas are welcome.</p>
	HTML;


$div_Objectives = new ContentSection();
$div_Objectives->content = <<<HTML
	<h3>Objectives</h3>

	<p>Defining social justice may be a first step, but it is not the real objective.
	Reading through the Wikipedia article about social justice (linked below),
	one can observe that Wikipedians have already made a great job at defining it and providing an overview of its different aspects.
	At the same time, whatever one's perspective, a cursory look at the $world today is enough to notice that
	our global society thoroughly lacks social justice.
	So the question is: what is lacking?
	What are the root causes of such injustice?
	What policies exist that ought to be studied, understood and implemented?</p>

	<p>See also the $OECD's "States of Fragility" report.</p>
	HTML;



$list = new ListOfPages();
$list->add('clark_cunningham.html');
$list->add('housing.html');
$list->add('hunger.html');
$list->add('immigration.html');
$list->add('oecd.html');
$list->add('poverty.html');
$list->add('racism.html');
$list->add('refugees.html');
$list->add('refugee_camps.html');
$print_list = $list->print();


$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>list</h3>

	<p>Listed of relevant pages, in alphabetical order.</p>

	$print_list
	HTML;


$div_wikipedia_Social_justice = new WikipediaContentSection();
$div_wikipedia_Social_justice->setTitleText('Social justice');
$div_wikipedia_Social_justice->setTitleLink('https://en.wikipedia.org/wiki/Social_justice');
$div_wikipedia_Social_justice->content = <<<HTML
	<p>Social justice is justice in terms of the distribution of wealth, opportunities, and privileges within a society.</p>
	HTML;


$page->parent('menu.html');
$page->body($div_stub);

$page->body($div_introduction);
$page->body($div_codeberg_How_to_define_social_justice);

$page->body($div_Objectives);

$page->body($div_list);


$page->body($div_wikipedia_Social_justice);
