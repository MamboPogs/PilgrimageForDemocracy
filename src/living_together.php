<?php
$page = new Page();
$page->h1('Living Together');
$page->stars(2);
$page->keywords('living together');

$page->snp('description', "Bridging differences, creating a new society together: inspiring stories and ideas.");
$page->snp('image', "/copyrighted/my-life-through-a-lens-bq31L0jQAjU.1200-630.jpg");

$page->preview( <<<HTML
	<p>If we learn how to bridge our differences and learn how to live together,
	we can create a better society for all.
	Here is a collection of inspiring stories and ideas.</p>
	HTML );

$r1 = $page->ref('https://www.dw.com/en/the-germans-curing-loneliness-with-homes-for-young-and-old/a-65703756', 'The Germans curing loneliness with homes for young and old');
$r2 = $page->ref('https://news.yahoo.com/three-jewish-arab-families-swapped-090115611.html?fr=sychp_catchall', 'How three Jewish and Arab families swapped kidneys, saved their mothers and made history');
$r3 = $page->ref('https://www.tiyul-rihla.org/', 'Tiyul-Rihla (“Trip” in Hebrew and Arabic)');
$r4 = $page->ref('https://k4p.org/', 'Kids4Peace');
$r5 = $page->ref('https://sfpeace.org/', 'The School for Peace (SFP) at Neve Shalom – Wahat al-Salam (NSWAS)');
$r6 = $page->ref('https://www.theparentscircle.org/en/pcff-home-page-en/', 'The Parents Circle – Families Forum (PCFF)');
$r7 = $page->ref('https://interfaith-encounter.org/en/', 'Interfaith Encounter Association');



$h2_introduction = new h2HeaderContent('Introduction');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>As stated in the article on the <a href="/secondary_features_of_democracy.html">secondary features of democracy</a>,
	the fact that we live in a society, together with people who have different beliefs, lifestyles and backgrounds,
	should not be seen as a hindrance or an annoyance.
	Instead, our communal life can be a source of strength and inspiration,
	if only we knew how to bridge our differences.</p>

	<p>We can live together despite all our differences.</p>
	HTML;





$h2_living_together_age = new h2HeaderContent('Age');

$div_living_together_age = new ContentSection();
$div_living_together_age->content = <<<HTML
	<h3>Communal living</h3>

	<p>In highly individualistic western societies, people are segregated in many ways, including according to age.
	In olden times, families used to live with three or even four generations under the same roof.
	Now, at least in the western part of the world, elderly people are routinely living separated from younger generations,
	spending the last years of their lives in retirement homes and nursing homes.
	</p>

	<p>Some people are now trying to buck the trend.
	For example, in Germany, multi-generational housing projects are becoming increasingly popular. ${r1}
	Instead of living away and feeling lonely, older people live under the same roof as very young families,
	and all benefit from the communal living arrangements.
	They no longer feel to be living alone in an anonymous city.
	Instead young and old can have breakfast together in the common room,
	where there is always someone to talk to.
	At the same time, residents each maintain their own private living quarters.</p>

	<p>Communal living solves many problems in rapidly ageing societies.
	It is more cost-effective than housing elderly people in nursing facilities.
	The older generation take care of young children while working adults are away at work,
	so that instead of being isolated and feeling lonely,
	they contribute to the group and feel valued.</p>
	HTML;



$h2_living_together_religions = new h2HeaderContent('Religions');

$div_living_together_religions = new ContentSection();
$div_living_together_religions->content = <<<HTML
	<h3>Israelis and Arabs</h3>

	<p>While the Israeli-Palestinian conflict has been ongoing for so many decades
	and still make the headlines in the news,
	many try to overcome all that separates Israelis from Arabs and Palestinians,
	Jews from Muslims.
	Here are a few stories.</p>

	<h4>Kidney diplomacy</h4>

	<blockquote>
	"I never thought of my donor being a Muslim or a Jew,
	because I believe that <strong>all human beings are equal</strong>."
	<br>
	— Asiag Yadid Yafit,<br>
	an Israeli Jew who received a kidney from an Israeli Arab.
	</blockquote>

	<p>We can start from this truly inspiring story of a <strong>series of "pay-it-forward" kidney donations</strong>
	involving three families who were originally taught by history and politics to hate each other.
	Instead, in tightly coordinated acts of kindness, a series of surgeries were performed on six people within a critical 18 hour window,
	involving a family in the United Arab Emirates, an Arab Israeli family and a Jewish Israeli family.</p>

	<p>For patients critically needing a new organ, it is always difficult to find a matching donor.
	For doctors, it helps to have a large pool to chose from as it increase the chances of finding a match.
	The arithmetic is simple: the bigger the pool, the more likely you are to find matches.
	Humans need only one of two kidneys to live,
	so doctors rely on donors who are willing to give a kidney to an unknown recipient to ultimately benefit their own loved one.
	However, politics, religion, national boundaries and mutual hatred deeply rooted in history
	drastically narrow the pool of potential donors.
	In this story, the donors all stemmed from the other side of the political and religious divide.</p>

	<p>A mother from the United Arab Emirates critically needed a new kidney,
	but her loving daughter, who was willing to donate her own kidney, was not a match.
	Instead, the UAE daughter was a match to a stranger, an Israeli Arab woman.
	The husband of the Israeli Arab woman was not a match for his wife,
	but he was a match for an Israeli Jewish woman,
	whose daughter turned out to be a match for the UAE mother,
	completing the cycle of transplants, matching three donors to three recipients,
	forever creating a bond between three families that originally all separated. ${r2}</p>

	<p>The UAE donor daughter wrote:</p>

	<blockquote>
	"The situation we were in is a medical one and is all about saving lives.
	<strong>It did not matter to me at all from which gender, ethnicity, religion or nationality the parties in this exchange would be</strong>."
	</blockquote>

	<p>An employee of the <em>Paired Kidney Donation</em> had to overcome many obstacles to make the three-way donation possible,
	just when a flare of violence between Israel and the Palestinian group Hamas, resulting in dozens of deaths from bombs and airstrikes,
	were threatening the fragile thaw in Arab-Israeli relations.
	He said:</p>

	<blockquote>
	"There were a lot of issues to overcome, from hospital details to cultural differences,
	but <strong>you don't get anywhere without at least starting to talk</strong>."
	</blockquote>

	<h4>Associations for dialogue and peace</h4>

	<p>Here are a few groups working for Israeli-Palestinian dialogue and peace.</p>

	<p>Tiyul-Rihla / Trip ${r3}:</p>

	<blockquote>
	Tiyul-Rihla (“Trip” in Hebrew and Arabic) is a bi-national educational initiative creating opportunities
	for Palestinians and Israelis to explore issues of historical narrative, culture, and identity.
	Our core trip program brings mixed groups of Palestinians and Israelis
	on multi-day tours which provide a unique framework for learning about each other from each other,
	using visits to contemporary and ancient sites as the foundation for the experience.
	</blockquote>

	<p>Kids4Peace ${r4}:</p>

	<blockquote>
	Founded in Jerusalem in 2002, Kids4Peace is a <strong>global interfaith youth movement</strong>,
	dedicated to ending conflict and  inspiring hope in divided societies around the world.<br>
	Through a network of local chapters, Kids4Peace operates award-winning <strong>dialogue, leadership and social action programs</strong>
	for over 500 youth in Jerusalem, North America and Europe.<br>
	We believe youth have the power to bring new questions and <strong>new answers to the struggle for peace and justice</strong>.
	</blockquote>

	<p>The School for Peace (SFP) at Neve Shalom – Wahat al-Salam (NSWAS) ${r5}:</p>

	<blockquote>
	The School for Peace (SFP) at Neve Shalom – Wahat al-Salam (NSWAS) was established in 1979
	as the first educational institution in Israel promoting <strong>broad scale change
	towards peace and more humane, egalitarian and just relations between Palestinians and Jews.</strong>
	<br>
	The School for Peace works with Jewish and Palestinian professional groups, women and youth,
	creating a genuine <strong>egalitarian dialogue</strong> between the two people.
	Through workshops, training programs and special projects,
	the SFP develops participants’ awareness of the conflict and their role in it,
	enabling them to <strong>take responsibility to change the present relations between Jews and Palestinians</strong>.
	</blockquote>

	<p>The Parents Circle – Families Forum (PCFF) ${r6}:</p>

	<blockquote>
	The Parents Circle – Families Forum (PCFF) is a <strong>joint Israeli-Palestinian organization of over 600 families</strong>.
	<br>
	The Parents Circle-Families Forum was created in 1995 by Mr. Yitzhak Frankenthal and a few Israeli families.
	The first meeting between bereaved Palestinians from Gaza  and Israeli families took place in 1998.
	<br>
	The Parents Circle – Families Forum (PCFF) is a joint Israeli-Palestinian organization of over 600 families,
	all of whom have lost an immediate family member to the ongoing conflict.
	Moreover, the PCFF has concluded that <strong>the process of reconciliation between nations is a prerequisite to achieving a sustainable peace</strong>.
	The organization thus utilizes all resources available in education, public meetings and the media, to spread these ideas.
	</blockquote>

	<p>Interfaith Encounter Association ${r7}:</p>

	<blockquote>
	The IEA invites people from different traditional and cultural backgrounds and faiths to join our groups.
	<br>
	Within the groups participants have meaningful encounters which bring them closer to each other and build the bridges.
	<br>
	<strong>Prejudice, hostility and suspicion are transformed into direct acquaintance, mutual respect and friendship.</strong>
	<br>
	Our groups are both a model for inter-communal relations of appreciation and care, and vehicles to promote them.
	</blockquote>
	HTML;


$page->parent('secondary_features_of_democracy.html');
$page->body($div_stub);


$page->body($h2_introduction);
$page->body($div_introduction);

$page->body($h2_living_together_age);
$page->body($div_living_together_age);

$page->body($h2_living_together_religions);
$page->body($div_living_together_religions);

$page->body('global_issues.html');
