<?php
$page = new Page();
$page->h1('United Nations High Commissioner for Refugees');
$page->keywords('United Nations High Commissioner for Refugees', 'UNHCR');
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The United Nations High Commissioner for Refugees is administering most ${'refugee camps'} around the world.</p>
	HTML;

$div_wikipedia_United_Nations_High_Commissioner_for_Refugees = new WikipediaContentSection();
$div_wikipedia_United_Nations_High_Commissioner_for_Refugees->setTitleText('United Nations High Commissioner for Refugees');
$div_wikipedia_United_Nations_High_Commissioner_for_Refugees->setTitleLink('https://en.wikipedia.org/wiki/United_Nations_High_Commissioner_for_Refugees');
$div_wikipedia_United_Nations_High_Commissioner_for_Refugees->content = <<<HTML
	<p>The United Nations High Commissioner for Refugees (UNHCR) is a United Nations agency mandated to aid and protect refugees,
	forcibly displaced communities, and stateless people,
	and to assist in their voluntary repatriation, local integration or resettlement to a third country.</p>
	HTML;


$page->parent('united_nations.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_wikipedia_United_Nations_High_Commissioner_for_Refugees);
