<?php
$page = new Page();
$page->h1('Constitution of the Philippines');
$page->keywords('Constitution of the Philippines');
$page->stars(0);

$page->preview( <<<HTML
	<p>The 1987 constitution was adopted by plebiscite on February 2nd,
	a date which is now celebrated as Constitution day.</p>
	HTML );

$page->snp('description', 'Adopted by referendum in 1987.');
//$page->snp('image',       '/copyrighted/');



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The 1987 $constitution of the $Philippines was adopted by plebiscite on February 2nd,
	a date which is now celebrated as Constitution day.</p>
	HTML;

$div_wikipedia_Constitution_of_the_Philippines = new WikipediaContentSection();
$div_wikipedia_Constitution_of_the_Philippines->setTitleText('Constitution of the Philippines');
$div_wikipedia_Constitution_of_the_Philippines->setTitleLink('https://en.wikipedia.org/wiki/Constitution_of_the_Philippines');
$div_wikipedia_Constitution_of_the_Philippines->content = <<<HTML
	<p>The Constitution of the Philippines is the constitution or the supreme law of the Republic of the Philippines.
	Its final draft was completed by the Constitutional Commission on October 12, 1986,
	and ratified by a nationwide plebiscite on February 2, 1987.</p>
	HTML;

$div_wikipedia_Constitutional_reform_in_the_Philippines = new WikipediaContentSection();
$div_wikipedia_Constitutional_reform_in_the_Philippines->setTitleText('Constitutional reform in the Philippines');
$div_wikipedia_Constitutional_reform_in_the_Philippines->setTitleLink('https://en.wikipedia.org/wiki/Constitutional_reform_in_the_Philippines');
$div_wikipedia_Constitutional_reform_in_the_Philippines->content = <<<HTML
	<p>Constitutional reform in the Philippines, also known as charter change
	refers to the political and legal processes needed to amend the current 1987 Constitution of the Philippines.
	Under the common interpretation of the Constitution, amendments can be proposed by one of three methods:
	a People's Initiative, a Constituent Assembly or a Constitutional Convention.</p>
	HTML;


$div_wikipedia_1987_Philippine_constitutional_plebiscite = new WikipediaContentSection();
$div_wikipedia_1987_Philippine_constitutional_plebiscite->setTitleText('1987 Philippine constitutional plebiscite');
$div_wikipedia_1987_Philippine_constitutional_plebiscite->setTitleLink('https://en.wikipedia.org/wiki/1987_Philippine_constitutional_plebiscite');
$div_wikipedia_1987_Philippine_constitutional_plebiscite->content = <<<HTML
	<p>A constitutional plebiscite was held in the Philippines on 2 February 1987.
	On February 11, 1987, President Aquino, other government officials, and the Armed Forces of the Philippines,
	pledged allegiance to the Constitution.
	Since then, February 2 has been celebrated as Constitution Day, the date of the plebiscite.</p>
	HTML;



$page->parent('philippines.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_wikipedia_Constitution_of_the_Philippines);
$page->body($div_wikipedia_Constitutional_reform_in_the_Philippines);
$page->body($div_wikipedia_1987_Philippine_constitutional_plebiscite);
