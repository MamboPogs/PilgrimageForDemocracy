<?php
$page = new CountryPage('Georgia');

$page->h1('Georgia');
$page->stars(1);
$page->keywords('Georgia');

$page->preview( <<<HTML
	<p>A former Soviet Union republic, Georgia is caught in the repercutions of Russia's war in Ukraine.</p>
	HTML );

$page->snp('description', '3.6 million inhabitants.');
$page->snp('image', "/copyrighted/roman-odintsov-8747731.1200-630.jpg");

$h2_introduction = new h2HeaderContent('Georgia today');

$r1 = $page->ref('https://www.aljazeera.com/news/2023/3/8/georgians-protest-against-draft-law-on-media-nonprofits', 'Georgians protest ‘foreign agents’ draft law on media, nonprofits');
$r2 = $page->ref('https://www.aljazeera.com/news/2023/3/8/foreign-agents-law-why-are-protests-taking-place-in', '‘Foreign agents’ law: Why are protests taking place in Georgia?');
$r3 = $page->ref('https://www.aljazeera.com/program/inside-story/2023/3/8/whats-behind-the-recent-protests-in-georgia', 'What’s behind the recent protests in Georgia?');
$r4 = $page->ref('https://www.aljazeera.com/news/2023/3/9/no-to-the-russian-law-georgians-protest-foreign-agents-law', '‘No to the Russian law’: Georgians protest ‘foreign agents’ bill');
$r5 = $page->ref('https://www.aljazeera.com/news/2023/3/9/georgia-withdraws-foreign-agents-bill-after-days-of-protests', 'Georgia withdraws ‘foreign agents’ bill after days of protests');
$r6 = $page->ref('https://www.aljazeera.com/news/2023/3/10/georgias-parliament-drops-foreign-agents-bill', 'Georgia’s parliament drops controversial ‘foreign agents’ bill');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<h3>Overview</h3>

	<p>Georgia applied for EU membership together with Ukraine and Moldova days after Russia invaded Ukraine in February 2022.</p>


	<h3>Protests against ‘Foreign agents’ law</h3>

	<p>In March 2023, the parliament of Georgia was reviewing a bill “On Transparency of Foreign Influence”,
	stipulating that media outlets and NGOs could be classified as “foreign agents”
	if they receive more than 20 percent of their funding from abroad.</p>

	<p>The law is curtailing freedom of the press.
	Critics have pointed out the similarities between such a proposed law and a law enacted in 2012 in Russia,
	used to shut down or discredit organisations critical of the government.
	The law could also harm Georgia’s chances of EU membership.</p>

	<p>In Georgia, a large share of the media is controlled by the government.
	A large part of independent media gets outside support.</p>

	<p>Thousands of protesters gathered in Tbilisi in opposition of the proposed law.
	Police forces used water cannons and tear gas against the protesters, and arrested hundreds.</p>

	<p>After three days of protests, Georgia’s governing party announced it would withdraw the bill
	which was subsequently voted down by the parliament in the second reading.
	See ${r1} ${r2} ${r3} ${r4} ${r5} ${r6}.</p>
	HTML;


$div_wikipedia_2023_georgian_protests = new WikipediaContentSection();
$div_wikipedia_2023_georgian_protests->setTitleText('2023 Georgian protests');
$div_wikipedia_2023_georgian_protests->setTitleLink('https://en.wikipedia.org/wiki/2023_Georgian_protests');
$div_wikipedia_2023_georgian_protests->content = <<<HTML
	<p>The 2023 Georgian protests were a series of street demonstrations taking place throughout Georgia
	over parliamentary backing of a proposed "Law on Transparency of Foreign Influence",
	which requires NGOs to register as "agents of foreign influence" if the funds they receive from abroad amount to more than 20% of their total revenue.</p>
	HTML;


$div_wikipedia_history = new WikipediaContentSection();
$div_wikipedia_history->setTitleText('History of Georgia');
$div_wikipedia_history->setTitleLink('https://en.wikipedia.org/wiki/History_of_Georgia_(country)#Independent_Georgia');
$div_wikipedia_history->content = <<<HTML
	<p>Georgia declared its independence from the Soviet Union in 1991.</p>
	HTML;


$div_wikipedia_politics = new WikipediaContentSection();
$div_wikipedia_politics->setTitleText('Politics of Georgia (country)');
$div_wikipedia_politics->setTitleLink('https://en.wikipedia.org/wiki/Politics_of_Georgia_(country)');
$div_wikipedia_politics->content = <<<HTML
	<p>Georgia is parliamentary representative democratic republic with a multi-party system.</p>
	HTML;


$div_wikipedia_corruption = new WikipediaContentSection();
$div_wikipedia_corruption->setTitleText('Corruption in Georgia');
$div_wikipedia_corruption->setTitleLink('https://en.wikipedia.org/wiki/Corruption_in_Georgia');
$div_wikipedia_corruption->content = <<<HTML
	<p>Before the 2003 Rose Revolution, according to Foreign Policy,
	Georgia was among the most corrupt nations in Eurasia.
	After the revolution, the level of corruption abated dramatically.
	In 2010, Transparency International said that
	Georgia was "the best corruption-buster in the world."</p>
	HTML;


$div_wikipedia_human_rights = new WikipediaContentSection();
$div_wikipedia_human_rights->setTitleText('Human rights in Georgia');
$div_wikipedia_human_rights->setTitleLink('https://en.wikipedia.org/wiki/Human_rights_in_Georgia_(country)');
$div_wikipedia_human_rights->content = <<<HTML
	<p>Human rights in Georgia are guaranteed by the country's constitution.
	However, it has been alleged by Amnesty International, Rights Watch,
	the United States Department of State and the Georgian opposition
	that these rights are often breached.</p>
	HTML;



$page->parent('world.html');
$page->body($div_stub);
$page->body('Country indices');

$page->body($h2_introduction);
$page->body($div_introduction);

$page->body($div_wikipedia_history);
$page->body($div_wikipedia_politics);
$page->body($div_wikipedia_corruption);
$page->body($div_wikipedia_human_rights);
$page->body($div_wikipedia_2023_georgian_protests);
