<?php
$page = new Page();
$page->h1('Peaceful resistance in times of war');
$page->stars(1);

$page->preview( <<<HTML
	<p>Stories of peaceful resistance to bellicose autocratic regimes.</p>
	HTML );

$page->snp('description', "Stories of peaceful resistance to bellicose autocratic regimes.");
$page->snp('image', "/copyrighted/miha-rekar-7B_u675zmLU.1200-630.jpg");


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Throughout history and across the globe, people have demonstrated against war.
	It takes however a different kind of courage to do so when living under an autocratic regime
	that is hell-bent on pursuing a path of oppression and war.</p>
	HTML;

$div_codeberg = new CodebergContentSection();
$div_codeberg->setTitleText('Examples of anti-war peaceful resistance');
$div_codeberg->setTitleLink('https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/31');
$div_codeberg->content = <<<HTML
	<p>List of examples of anti-war resistance through time and across nations
	published by an anti-war feminist group.</p>
	HTML;

$h2_Stories_of_peaceful_resistance = new h2HeaderContent('Stories of peaceful resistance');


$div_Russia_occupied_Ukraine = new ContentSection();
$div_Russia_occupied_Ukraine->content = <<<HTML
	<h3>Russia occupied Ukraine</h3>

	<p>Ever since Russia launched its full scale invasion of Ukraine,
	the Ukrainian population in territories occupied by Russia have been resisting the invaders.
	In 2022, the resistance took the form of large scale protests.
	As Russia increased its oppressive measures, including arrests, torture and deportation,
	Ukrainian resistance took more subtle and covert forms,
	like painting or leaving Ukrainian symbols in public places under the cover of the night.</p>
	HTML;

$div_wikipedia_2022_protests_in_Russian_occupied_Ukraine = new WikipediaContentSection();
$div_wikipedia_2022_protests_in_Russian_occupied_Ukraine->setTitleText('2022 protests in Russian occupied Ukraine');
$div_wikipedia_2022_protests_in_Russian_occupied_Ukraine->setTitleLink('https://en.wikipedia.org/wiki/2022_protests_in_Russian-occupied_Ukraine');
$div_wikipedia_2022_protests_in_Russian_occupied_Ukraine->content = <<<HTML
	<p>During the 2022 Russian invasion of Ukraine and the resulting Russian occupation of multiple Ukrainian towns and cities,
	numerous cases of non-violent resistance against the invasion took place.
	Local residents organised protests against the invasion and blocked the movement of Russian military equipment.
	The Russian military dispersed the protests, sometimes with live fire, injuring many and killing some.
	Most of the large protests ended in March.</p>
	HTML;


$div_Russia_under_Putin = new ContentSection();
$div_Russia_under_Putin->content = <<<HTML
	<h3>Russia under Putin</h3>

	<p>
	</p>
	HTML;

$div_wikipedia_Anti_war_protests_in_Russia_2022_present = new WikipediaContentSection();
$div_wikipedia_Anti_war_protests_in_Russia_2022_present->setTitleText('Anti war protests in Russia 2022 present');
$div_wikipedia_Anti_war_protests_in_Russia_2022_present->setTitleLink('https://en.wikipedia.org/wiki/Anti-war_protests_in_Russia_(2022–present)');
$div_wikipedia_Anti_war_protests_in_Russia_2022_present->content = <<<HTML
	<p>Following the Russian invasion of Ukraine on 24 February 2022, anti-war demonstrations and protests broke out across Russia.
	The protests have been met with widespread repression by the Russian authorities.</p>
	HTML;

$div_wikipedia_Feminist_Anti_War_Resistance = new WikipediaContentSection();
$div_wikipedia_Feminist_Anti_War_Resistance->setTitleText('Feminist Anti War Resistance');
$div_wikipedia_Feminist_Anti_War_Resistance->setTitleLink('https://en.wikipedia.org/wiki/Feminist_Anti-War_Resistance');
$div_wikipedia_Feminist_Anti_War_Resistance->content = <<<HTML
	<p>Feminist Anti-War Resistance is a group of Russian feminists founded in February 2022
	to protest against the 2022 Russian invasion of Ukraine.</p>
	HTML;





$div_Nazi_Germany = new ContentSection();
$div_Nazi_Germany->content = <<<HTML
	<h3>Nazi Germany</h3>

	<p>
	</p>
	HTML;


$div_wikipedia_White_Rose = new WikipediaContentSection();
$div_wikipedia_White_Rose->setTitleText('White Rose');
$div_wikipedia_White_Rose->setTitleLink('https://en.wikipedia.org/wiki/White_Rose');
$div_wikipedia_White_Rose->content = <<<HTML
	<p>The White Rose was a non-violent, intellectual resistance group in Nazi Germany which was led by five students and one professor at the University of Munich.
	The group conducted an anonymous leaflet and graffiti campaign that called for active opposition to the Nazi regime.
	Their activities started in Munich on 27 June 1942; they ended with the arrest of the core group by the Gestapo on 18 February 1943.</p>
	HTML;

$div_wikipedia_Hans_and_Sophie_Scholl = new WikipediaContentSection();
$div_wikipedia_Hans_and_Sophie_Scholl->setTitleText('Hans and Sophie Scholl');
$div_wikipedia_Hans_and_Sophie_Scholl->setTitleLink('https://en.wikipedia.org/wiki/Hans_and_Sophie_Scholl');
$div_wikipedia_Hans_and_Sophie_Scholl->content = <<<HTML
	<p>Hans and Sophie Scholl were a brother and sister who were members of the White Rose,
	a student group in Munich that was active in the non-violent resistance movement in Nazi Germany,
	especially in distributing flyers against the war and the dictatorship of Adolf Hitler.
	In post-war Germany, Hans and Sophie Scholl are recognized as symbols of German resistance against the totalitarian Nazi regime.</p>
	HTML;




$h2_Other_resources = new h2HeaderContent('Other resources');



$div_wikipedia_Nonviolent_resistance = new WikipediaContentSection();
$div_wikipedia_Nonviolent_resistance->setTitleText('Nonviolent resistance');
$div_wikipedia_Nonviolent_resistance->setTitleLink('https://en.wikipedia.org/wiki/Nonviolent_resistance');
$div_wikipedia_Nonviolent_resistance->content = <<<HTML
	<p>Nonviolent resistance, or nonviolent action, sometimes called civil resistance,
	is the practice of achieving goals such as social change through symbolic protests, civil disobedience,
	economic or political noncooperation, satyagraha, constructive program, or other methods,
	while refraining from violence and the threat of violence.</p>
	HTML;

$div_wikipedia_Anti_war_movement = new WikipediaContentSection();
$div_wikipedia_Anti_war_movement->setTitleText('Anti war movement');
$div_wikipedia_Anti_war_movement->setTitleLink('https://en.wikipedia.org/wiki/Anti-war_movement');
$div_wikipedia_Anti_war_movement->content = <<<HTML
	<p>Anti-war activists work through protest and other grassroots means to attempt to pressure a government (or governments)
	to put an end to a particular war or conflict or to prevent it in advance.</p>
	HTML;

$div_wikipedia_List_of_peace_activists = new WikipediaContentSection();
$div_wikipedia_List_of_peace_activists->setTitleText('List of peace activists');
$div_wikipedia_List_of_peace_activists->setTitleLink('https://en.wikipedia.org/wiki/List_of_peace_activists');
$div_wikipedia_List_of_peace_activists->content = <<<HTML
	<p>This list of peace activists includes people who have proactively advocated
	diplomatic, philosophical, and non-military resolution of major territorial or ideological disputes
	through nonviolent means and methods.</p>
	HTML;

$page->parent('global_issues.html');

$page->body($div_stub);

$page->body($div_introduction);
$page->body($div_codeberg);

$page->body($h2_Stories_of_peaceful_resistance);

$page->body($div_Russia_occupied_Ukraine);
$page->body($div_wikipedia_2022_protests_in_Russian_occupied_Ukraine);

$page->body($div_Russia_under_Putin);
$page->body($div_wikipedia_Anti_war_protests_in_Russia_2022_present);
$page->body($div_wikipedia_Feminist_Anti_War_Resistance);

$page->body($div_Nazi_Germany);
$page->body($div_wikipedia_Hans_and_Sophie_Scholl);
$page->body($div_wikipedia_White_Rose);

$page->body($h2_Other_resources);
$page->body($div_wikipedia_List_of_peace_activists);
$page->body($div_wikipedia_Anti_war_movement);
$page->body($div_wikipedia_Nonviolent_resistance);
