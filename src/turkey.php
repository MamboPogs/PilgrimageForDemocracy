<?php
$page = new CountryPage('Turkey');
$page->h1('Turkey');
$page->stars(0);
$page->keywords('Turkey');

$page->preview( <<<HTML
	<p>
	</p>
	HTML );

$page->snp('description', "85 million inhabitants.");
$page->snp('image', "/copyrighted/meg-jerrard-JnLp1kus3Ks.1200-630.jpg");

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Recep Tayyip Erdoğan changed the $constitution of Turkey so that he could remain in power.</p>
	HTML;

$div_wikipedia_Human_rights_in_Turkey = new WikipediaContentSection();
$div_wikipedia_Human_rights_in_Turkey->setTitleText('Human rights in Turkey');
$div_wikipedia_Human_rights_in_Turkey->setTitleLink('https://en.wikipedia.org/wiki/Human_rights_in_Turkey');
$div_wikipedia_Human_rights_in_Turkey->content = <<<HTML
	<p>Acute human rights issues include in particular the status of Kurds in Turkey.
	The Kurdish–Turkish conflict has caused numerous human rights violations over the years.
	There is an ongoing debate in the country on the right to life, torture,
	freedom of expression as well as freedoms of religion, assembly and association.</p>
	HTML;

$div_wikipedia_Politics_of_Turkey = new WikipediaContentSection();
$div_wikipedia_Politics_of_Turkey->setTitleText('Politics of Turkey');
$div_wikipedia_Politics_of_Turkey->setTitleLink('https://en.wikipedia.org/wiki/Politics_of_Turkey');
$div_wikipedia_Politics_of_Turkey->content = <<<HTML
	<p>Turkey is a presidential representative democracy and a constitutional republic within a pluriform multi-party system,
	in which the president (the head of state and head of government), parliament, and judiciary share powers reserved to the national government.</p>
	HTML;

$div_wikipedia_Elections_in_Turkey = new WikipediaContentSection();
$div_wikipedia_Elections_in_Turkey->setTitleText('Elections in Turkey');
$div_wikipedia_Elections_in_Turkey->setTitleLink('https://en.wikipedia.org/wiki/Elections_in_Turkey');
$div_wikipedia_Elections_in_Turkey->content = <<<HTML
	<p>Elections in Turkey are held for six functions of government:
	presidential elections, parliamentary elections, municipality mayors, district mayors, provincial or municipal council members and muhtars.</p>
	HTML;

$div_wikipedia_2016_present_purges_in_Turkey = new WikipediaContentSection();
$div_wikipedia_2016_present_purges_in_Turkey->setTitleText('2016 present purges in Turkey');
$div_wikipedia_2016_present_purges_in_Turkey->setTitleLink('https://en.wikipedia.org/wiki/2016–present_purges_in_Turkey');
$div_wikipedia_2016_present_purges_in_Turkey->content = <<<HTML
	<p>The 2016–present purges in Turkey are a series of purges by the Government of Turkey
	enabled by a state of emergency in reaction to the 15 July failed coup d'état.</p>
	HTML;

$div_wikipedia_List_of_journalists_killed_in_Turkey = new WikipediaContentSection();
$div_wikipedia_List_of_journalists_killed_in_Turkey->setTitleText('List of journalists killed in Turkey');
$div_wikipedia_List_of_journalists_killed_in_Turkey->setTitleLink('https://en.wikipedia.org/wiki/List_of_journalists_killed_in_Turkey');
$div_wikipedia_List_of_journalists_killed_in_Turkey->content = <<<HTML
	<p>Following the killing of Armenian journalist Hrant Dink in Istanbul on 19 January 2007
	various lists of journalists killed in Turkey since the early 20th century were published.</p>
	HTML;

$div_wikipedia_Torture_in_Turkey = new WikipediaContentSection();
$div_wikipedia_Torture_in_Turkey->setTitleText('Torture in Turkey');
$div_wikipedia_Torture_in_Turkey->setTitleLink('https://en.wikipedia.org/wiki/Torture_in_Turkey');
$div_wikipedia_Torture_in_Turkey->content = <<<HTML
	<p>The Human Rights Foundation of Turkey estimates there are around one million victims of torture in Turkey.</p>
	HTML;

$div_wikipedia_Freedom_of_religion_in_Turkey = new WikipediaContentSection();
$div_wikipedia_Freedom_of_religion_in_Turkey->setTitleText('Freedom of religion in Turkey');
$div_wikipedia_Freedom_of_religion_in_Turkey->setTitleLink('https://en.wikipedia.org/wiki/Freedom_of_religion_in_Turkey');
$div_wikipedia_Freedom_of_religion_in_Turkey->content = <<<HTML
	<p>Turkey is a secular state in accordance with Article 24 of its constitution.</p>
	HTML;

$div_wikipedia_Censorship_in_Turkey = new WikipediaContentSection();
$div_wikipedia_Censorship_in_Turkey->setTitleText('Censorship in Turkey');
$div_wikipedia_Censorship_in_Turkey->setTitleLink('https://en.wikipedia.org/wiki/Censorship_in_Turkey');
$div_wikipedia_Censorship_in_Turkey->content = <<<HTML
	<p>Despite legal provisions, freedom of the press in Turkey has steadily deteriorated from 2010 onwards,
	with a precipitous decline following the attempted coup in July 2016.
	By some accounts, Turkey currently accounts for one-third of all journalists imprisoned around the world.</p>
	HTML;

$div_wikipedia_Human_rights_of_Kurdish_people_in_Turkey = new WikipediaContentSection();
$div_wikipedia_Human_rights_of_Kurdish_people_in_Turkey->setTitleText('Human rights of Kurdish people in Turkey');
$div_wikipedia_Human_rights_of_Kurdish_people_in_Turkey->setTitleLink('https://en.wikipedia.org/wiki/Human_rights_of_Kurdish_people_in_Turkey');
$div_wikipedia_Human_rights_of_Kurdish_people_in_Turkey->content = <<<HTML
	<p>Kurds have had a long history of discrimination perpetrated against them by the Turkish government.
	Massacres have periodically occurred against the Kurds since the establishment of the Republic of Turkey in 1923.</p>
	HTML;

$div_wikipedia_Human_trafficking_in_Turkey = new WikipediaContentSection();
$div_wikipedia_Human_trafficking_in_Turkey->setTitleText('Human trafficking in Turkey');
$div_wikipedia_Human_trafficking_in_Turkey->setTitleLink('https://en.wikipedia.org/wiki/Human_trafficking_in_Turkey');
$div_wikipedia_Human_trafficking_in_Turkey->content = <<<HTML
	<p>Turkey is a top destination for victims of human trafficking, according to a report produced by the United Nations Office on Drugs and Crime.</p>
	HTML;

$div_wikipedia_Racism_and_discrimination_in_Turkey = new WikipediaContentSection();
$div_wikipedia_Racism_and_discrimination_in_Turkey->setTitleText('Racism and discrimination in Turkey');
$div_wikipedia_Racism_and_discrimination_in_Turkey->setTitleLink('https://en.wikipedia.org/wiki/Racism_and_discrimination_in_Turkey');
$div_wikipedia_Racism_and_discrimination_in_Turkey->content = <<<HTML
	<p>In Turkey, racism and ethnic discrimination are present in its society and throughout its history,
	including institutional racism against non-Muslim and non-Sunni minorities.</p>
	HTML;


$page->parent('world.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_Racism_and_discrimination_in_Turkey);
$page->body($div_wikipedia_Human_trafficking_in_Turkey);
$page->body($div_wikipedia_Human_rights_of_Kurdish_people_in_Turkey);
$page->body($div_wikipedia_Censorship_in_Turkey);
$page->body($div_wikipedia_Freedom_of_religion_in_Turkey);
$page->body($div_wikipedia_Torture_in_Turkey);
$page->body($div_wikipedia_List_of_journalists_killed_in_Turkey);
$page->body($div_wikipedia_2016_present_purges_in_Turkey);
$page->body($div_wikipedia_Elections_in_Turkey);
$page->body($div_wikipedia_Politics_of_Turkey);
$page->body($div_wikipedia_Human_rights_in_Turkey);
