<?php
$page = new Page();
$page->h1('1: Primary features of democracy — We, the Individuals');
$page->stars(3);
$page->keywords('primary features of democracy');

$page->preview( <<<HTML
	<p>This is the starting point in our journey towards a proper and complete understanding of what a democracy is.</p>
	<p>The etymological meaning is simple enough to comprehend, but the implications are easily overlooked.</p>
	<p>These primary features of democracy are fundamental.
	They are the basis upon which everything else is built.</p>
	<p>The primary flaws of democracy are:
		<span class="democracy flaw">individualism</span>,
		<span class="democracy flaw">selfishness</span>,
		<span class="democracy flaw">indifference</span>,
		<span class="democracy flaw">populism</span>.</p>
	<p>The primary virtues of democracy are:
		<span class="democracy virtue">freedom</span>,
		<span class="democracy virtue">peace</span>,
		<span class="democracy virtue">achievement</span>,
		<span class="democracy virtue">inspiration</span>,
		<span class="democracy virtue">aspiration</span>,
		<span class="democracy virtue">personal human development</span>,
		<span class="democracy virtue">personal spiritual development</span>.</p>
	HTML );

$page->snp('description', "Freedom and liberty");
$page->snp('image', "/copyrighted/noah-silliman-gzhyKEo_cbU.1200-630.jpg");

$div_before_you_read_definition_of_democracy = new ContentSection();
$div_before_you_read_definition_of_democracy->content = <<<HTML
	<h3>An experiment to make before reading</h3>

	<p>Here is a little experiment that you can make to test what kind of persons you, your friends, and your family are.
	Before you read the four main features of democracy, take a moment and ask yourself and the people around you:
	<br>"<em>How would you define democracy?</em>"
	<br>"<em>What does democracy represent for you?</em>"
	<br>"<em>Is democracy something good or something bad?</em>"</p>

	<p>Take your time to think it through.
	Take the time to ask people around you for their definition of democracy, and what they think about it.</p>

	<p>Then, make sure to come back here to read the features below.
	You can compare the definition and ideas you have gathered from everybody to those presented below.
	See where each of your ideas fit in each of the four features of democracy.</p>

	<p>After reading, you can see the type of persons the people around you are,
	by seeing which ones among the four features of democracy they most identify with.</p>
	HTML;



$h2_definition = new h2HeaderContent('Definition');

$div_start = new ContentSection();
$div_start->content = <<<HTML
	<h3>Foundation</h3>

	<p>Democracy, by nature, can be complex and very messy.
	It is only by properly understanding its basic premises
	that we can hope to unravel the numerous challenges currently faced by established democracies,
	and point out what is critically lacking in totalitarian regimes.</p>

	<p>The primary features of democracy are the most obvious ones; they can easily be understood.
	However, they must still be clearly stated,
	because only then can we fully understand their relationship with the secondary, tertiary and quaternary features of democracy.</p>

	<p>So, vowing to patiently take one step at a time,
	let's take our first step and start our exploration of the multi-faceted nature of democracy.</p>
	HTML;

$div_etymology = new ContentSection();
$div_etymology->content = <<<HTML
	<h3>Etymology</h3>

	<p>The most obvious starting point is to look at the etymology of the word "<em>democracy</em>".</p>

	<p>The word "<em>democracy</em>" comes from the ancient Greek "<em>δημοκρατία</em>" (dēmokratia),
	which is composed of:</p>
		<ul>
			<li>"<em>δῆμος</em>" (dêmos), which means: people,</li>
			<li>"<em>κράτος</em>" (krátos), which means: force, power or rule.</li>
		</ul>

	<p>Thus, from this very primitive analysis of the word,
	we can establish that, at its core, a democracy is when the people themselves have the power.
	The people can rule over their own destiny.
	Individuals, ideally, are in control of their own lives.</p>
	HTML;

$div_power = new ContentSection();
$div_power->content = <<<HTML
	<h3>Power to the individual</h3>

	<p>In this first approach towards defining democracy,
	we shall not consider the society as a whole, nor the people as a group.
	Instead, let's start by considering single individuals.
	Thus, at its core, democracy is a social structure wherein the individual has power.<p>

	<p>The very first things individuals should have power over are	their own lives.
	Individuals are the masters of their own destiny, of their own human and spiritual development.
	(Obviously, individuals also have the power to decide what "<em>human and spiritual development</em>" entails for them!)</p>

	<p>Secondarily, individuals should have power, or at least some level of control,
	over all external factors that affect their own lives.
	We shall address the mutual interaction between the individual and external forces at a later stage.</p>
	HTML;


$h2_all_individuals = new h2HeaderContent('');

$div_all_individuals = new ContentSection();
$div_all_individuals->content = <<<HTML
	<h3>All individuals</h3>

	<p>The basic principles articulated here should, theoretically at least, be applicable to <em>all</em> individuals.
	All people should enjoy the same basic powers, the same rights and opportunities,
	and it should be so not only for all individuals within a given country,
	but, eventually, to all individuals worldwide, in every country.</p>

	<p>We are aiming for <strong>sustainable world democracy</strong>.
	We do not want a political system that is viable only paired with an economic system
	whereby few rich countries are exploiting natural resources and cheap labor from poor countries.
	For pursuing democracy to be a worthy goal, then it must be a system that is, theoretically at least,
	possibly applicable to the whole world and still be <strong>stable at the economic and social level</strong>.</p>

	<p>That is why <strong>democracy</strong> and <strong>social justice</strong>
	should be pursued together.
	They are a pair: one without the other is not a worthy goal.
	In fact, they depend on each other.</p>
	HTML;




$h2_individuals = new h2HeaderContent('We, the Individuals');


$div_individuals = new ContentSection();
$div_individuals->content = <<<HTML
	<p>Since, by definition, We, the Individuals, have power,
	we are then free to act on our own behalf, and pursue our own dreams.</p>

	<p>If individuals could decide everything for themselves, if they had full control over their lives,
	what would they want? What would they do?</p>
	HTML;

$div_freedom_and_liberty = new ContentSection();
$div_freedom_and_liberty->content = <<<HTML
	<h3>Freedom and liberty</h3>

	<p>What do we all want if not peace, freedom and prosperity?
	If so, since we the individuals have power,
	democracy should be the best societal system to precisely get these things.</p>



	<p>Probably, by far, we as individuals would like to have a sense of freedom,
	the assurance that we can do as we please.
	We want to have influence on things that affect us.
	We want to feel in control of our lives.</p>

	<p>The notion of liberty and freedom are so sacred that many democracies have enshrined them as unalienable rights in their constitutions,
	like: freedom of expression, freedom of assembly, freedom of religion, freedom of enterprise, etc.</p>

	<p>Individual freedom is probably the foundation of any democracy,
	although we shall see later how this freedom is limited.</p>
	HTML;

$div_peace = new ContentSection();
$div_peace->content = <<<HTML
	<h3>Peace</h3>

	<p>If a war can be avoided at all, it is doubtful that anybody would look forward to fighting in the front lines and dying in the trenches.
	People undoubtedly want peace and would rather take care of their own lives and loved ones rather than fight a foreign enemy.<p>

	<p>Individuals want peace and would certainly favor dialogue, negotiations, mutual understanding and compromise to any outright war.</p>

	<p>According to the <strong>Democratic peace theory</strong>,
	democracies do not wage war on each other, or at least are much less likely to do so.
	Wars and military drafts, are obviously unpopular.
	It is precisely because of this, and because democratic governments derive their power from the people,
	that democratic countries are reluctant and very unlikely to start a war with their democratic neighbors,
	regardless of any disagreement the two countries might have with each other.
	There have been instances of wars between democracies, but they mostly involved weak or newly established democracies.</p>
	HTML;

$div_hierarchy_of_needs = new ContentSection();
$div_hierarchy_of_needs->content = <<<HTML
	<h3>Hierarchy of needs</h3>

	<p>Besides high-level, core ideals like peace and liberty,
	individuals are more concerned about their immediate needs,
	in accordance to the well-known pyramid of needs.</p>

	<p>Starting at the base of the pyramid of needs,
	people must have access to the most necessary resources for biological survival:
	<strong>air</strong>, <strong>water</strong> and <strong>food</strong>.
	This is so obvious that it may sound strange to mention it at all.
	Disregarding the fact that the air is rarely completely clean, the water is often polluted,
	and the food not necessarily healthy,
	we must keep in mind that countless people literally starve to death every single day.
	Totalitarian regimes are often to blame for terrible famines
	(for example: during the Great Leap Forward in the People's Republic of China,
	Holodomor in Ukraine under Stalin,
	and the North Korean famine in the 1990s).
	In addition, international trade is set up in such a way that it is partly responsible for inhumane injustice on a global scale,
	with widespread starvation in the poorest countries and excessive luxury in the wealthiest countries.
	Those are issues we need to address if we want one day to achieve worldwide peace and worldwide democracy.</p>

	<p>After securing clean air, fresh water and healthy food, individuals need shelter and clothing,
	about which a lot could be said as well.</p>

	<p>We all long for a certain level of material and economic prosperity.
	Children and adults need education and training.
	Individuals need to develop skills to use in order to make a living and provide for their material needs.</p>

	<p>Moving up the pyramid, we reach immaterial needs, such as emotional, intellectual and spiritual needs.</p>

	<p>People, by and large, all long to find a partner to live together with.
	Most people want to find love, probably get married, create a home, have children, nurture them.</p>

	<p>Finally, people all have a creative side, an artistic side and a human or spiritual side they long to develop.
	<strong>Freedom of expression</strong>, <strong>freedom of conscience</strong> and <strong>freedom of religion</strong>
	are some of the most important hallmarks of democracy.</p>

	<p>By definition, in a democracy, the people have power over their own destinies.
	Thus, a perfect democracy would be where each human being, each individual, can achieve precisely all of the above.</p>
	HTML;


$div_responsibility = new ContentSection();
$div_responsibility->content = <<<HTML
	<h3>Responsibility</h3>

	<p>To the extent that individuals enjoy power and freedom in a democracy,
	those same individuals must also bear responsibility for their own lives.</p>

	<p>In a perfect democracy, individuals must bear the full personal responsibility for their actions and choices in life.</p>

	<p>Since the citizens are the ultimate masters of their own destiny,
	they get the democracy that they deserve.
	Also, as we shall discuss later, we get the leaders that we deserve.
	If we are not satisfied with our current democratic system,
	it is up to each individual to strive to improve it.
	It is the sum of our individual actions that will improve our democratic society as a whole.</p>
	HTML;


$list = new ListOfPages();
$list->add('beliefs.html');
$list->add('freedom.html');
$list->add('freedom_of_conscience.html');
$list->add('religion.html');
$print_list = $list->print();

$div_List_of_topics_related_to_the_primary_features_of_democracy = new ContentSection();
$div_List_of_topics_related_to_the_primary_features_of_democracy->content = <<<HTML
	<h3>List of topics related to the primary features of democracy</h3>

	$print_list
	HTML;


$page->body($div_before_you_read_definition_of_democracy);
$page->body($h2_definition);
$page->body($div_start);
$page->body($div_etymology);
$page->body($div_power);
$page->body($div_all_individuals);

$page->body($h2_individuals);
$page->body($div_individuals);
$page->body($div_freedom_and_liberty);
$page->body($div_peace);
$page->body($div_hierarchy_of_needs);
$page->body($div_responsibility);

$page->body('secondary_features_of_democracy.html');
$page->body($div_List_of_topics_related_to_the_primary_features_of_democracy);
