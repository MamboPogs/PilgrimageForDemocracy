<?php
$page = new Page();
$page->h1('Technology and Democracy');
$page->stars(1);

$page->snp('description',"The impact of social networks and new technologies on our democracies.");
//$page->snp('image', "/copyrighted/");

$page->preview( <<<HTML
	<p>Social networks and new technologies shape our world view and have an often detrimental impact on our democracies.</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Social networks and new technologies shape our world view and have an often detrimental impact on our democracies.</p>

	<p>${'Data-Pop Alliance'} has a research and outreach program on technology and democracy.</p>
	HTML;

$list = new ListOfPages();
$list->add('artificial_intelligence.html');
$list->add('social_networks.html');
$print_list = $list->print();

$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>Related topics</h3>

	$print_list
	HTML;


$div_program_technology_democracy = new WebsiteContentSection();
$div_program_technology_democracy->setTitleText('Data-Pop Alliance program: technology democracy');
$div_program_technology_democracy->setTitleLink('https://datapopalliance.org/program-technology-and-democracy/');
$div_program_technology_democracy->content = <<<HTML
	<p>While the tech industry thrives, we are also living through a crisis of liberal democracy and trust.
	Researchers have shown that “trust crisis” is connected to the loss of a “shared reality”,
	including the ability to agree on basic facts or to argue disagreements civilly.
	Historian Yuval Harari suggests the crisis of liberal democracy, despite being multifaceted,
	“appears to be intertwined with current technological developments” and that it will likely worsen.
	Recently, revelations from the “Facebook whistleblower” Frances Haugen,
	seem to confirm what many scholars and some technologists have argued for a while:
	monetization imperatives shape functions and forms of mainstream social networks,
	which in turn are leading to antidemocratic processes, such as the amplification of hate and disinformation,
	hyperpolarization, and even anti-scientific theories that have regained followers.
	Our Technology and Democracy Program aims to nurture a civic-tech movement to bolster democracy in the digital age,
	respond to anti-democratic processes fueled online such as disinformation
	and produce actionable knowledge about the impacts of technology for societies.</p>
	HTML;





$page->parent('democracy.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body($div_list);
$page->body($div_program_technology_democracy);
