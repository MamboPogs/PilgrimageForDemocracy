<?php
$page = new Page();
$page->h1('Constitution');
$page->stars(1);
$page->keywords('constitution', 'constitutional');


//$page->snp('description', "");
//$page->snp('image', "/copyrighted/");


$page->preview( <<<HTML
	<p>A constitution sets the ground rules and provides stability for the democracy and resilience to subversion.</p>
	HTML );





$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>A constitution of a country is the core legal document superseding all other laws and legal documents.</p>

	<p>A constitution has two important functions.
	First, it provides the ground rules for government and the country to operate.
	Second, it should provide stability for the democracy by being resilient to subversion by would-be autocrats.</p>
	HTML;

$h2_Eligibility = new h2HeaderContent('Eligibility');

$div_eligibility = new ContentSection();
$div_eligibility->content = <<<HTML
	<p>The constitution is responsible for elaborating on the eligibility of people to various offices.</p>
	HTML;


$div_Donald_Trump_eligibility = new ContentSection();
$div_Donald_Trump_eligibility->content = <<<HTML
	<h3>Donald Trump eligibility</h3>

	<p>Legal scholars have made the case that Donald Trump is ineligible to be a candidate in the 2024 presidential election.
	The progressive constitution law professor ${'Laurence Tribe'} of Harvard and the retired conservative judge ${'Michael Luttig'}
	wrote in the Atlantic that " the constitution prohibits Trump from ever being president again".
	Section 3 of the 14th Amendment of the Constitution says that anyone who has taken an oath to uphold the constitution
	and then engaged in an insurrection cannot hold office again.</p>
	HTML;



$h2_Constitutional_amendment = new h2HeaderContent('Constitutional amendment');

$div_constitutional_amendment = new ContentSection();
$div_constitutional_amendment->content = <<<HTML
	<p>Nothing is immutable and nothing is conceived perfect.
	For these reasons, a constitution should be amendable.
	A constitution that can be amended only with great difficulty risks of getting ossified and inadequate for modern times.
	The constitution of the United States sets very difficult hurdles to pass in order to amend it.</p>

	<p>On the other hand, a constitution should not be too easily amended to prevent bad actors from subverting the democracy.</p>

	<p>Several autocratic leaders have had constitutional amendments passed so that they could stay in power essentially for life.</p>

	<ul>
		<li>Vladimir Putin changed the constitution of $Russia.</li>
		<li>Recep Tayyip Erdoğan changed the constitution of $Turkey.</li>
		<li>Xi Jinping changed the charter of the Chinese Communist Party.</li>
		<li>See also the 2023 referendum in $Mali.</li>
		<li>In $Tunisia, President Kaïs Saïed took a number of emergency measures
			aimed at transforming the political system and expanding his own executive power.</li>
	</ul>
	HTML;

$list = new ListOfPages();
$list->add('american_constitution_society.html');
$list->add('constitution_of_the_philippines.html');
$list->add('clark_cunningham.html');
$list->add('laurence_tribe.html');
$print_list = $list->print();

$div_list_of_constitutions = new ContentSection();
$div_list_of_constitutions->content = <<<HTML
	$print_list
	HTML;



$div_wikipedia_Constitution = new WikipediaContentSection();
$div_wikipedia_Constitution->setTitleText('Constitution');
$div_wikipedia_Constitution->setTitleLink('https://en.wikipedia.org/wiki/Constitution');
$div_wikipedia_Constitution->content = <<<HTML
	<p>A constitution is the aggregate of fundamental principles or established precedents that constitute the legal basis of a polity,
	organisation or other type of entity and commonly determine how that entity is to be governed.</p>
	HTML;

$div_wikipedia_Constitutional_amendment = new WikipediaContentSection();
$div_wikipedia_Constitutional_amendment->setTitleText('Constitutional amendment');
$div_wikipedia_Constitutional_amendment->setTitleLink('https://en.wikipedia.org/wiki/Constitutional_amendment');
$div_wikipedia_Constitutional_amendment->content = <<<HTML
	<p>Most constitutions require that amendments cannot be enacted unless they have passed a special procedure
	that is more stringent than that required of ordinary legislation.</p>
	HTML;



$page->parent('institutions.html');
$page->body($div_stub);
$page->body($div_introduction);

$page->body($h2_Eligibility);
$page->body($div_eligibility);
$page->body($div_Donald_Trump_eligibility);

$page->body($h2_Constitutional_amendment);
$page->body($div_constitutional_amendment);
$page->body($div_list_of_constitutions);

$page->body($div_wikipedia_Constitution);
$page->body($div_wikipedia_Constitutional_amendment);
