<?php
$page = new Page();
$page->h1("China (People's Republic of China) and Taiwan (Republic of China)");
$page->stars(0);
$page->keywords('China and Taiwan');

$page->preview( <<<HTML
	<p>
	</p>
	HTML );


$page->snp('description', "A tale of two Chinas.");
//$page->snp('image', "/copyrighted/");

$r1 = $page->ref('https://www.taipeitimes.com/News/front/archives/2023/07/01/2003802464', 'Blockade would cost world US$2.7tn: report');
$r2 = $page->ref('https://www.visionofhumanity.org/assessing-the-global-economic-ramifications-of-a-chinese-blockade-on-taiwan/', 'Assessing the Global Economic Ramifications of a Chinese Blockade on Taiwan');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>This article is about the two Chinas: the People's Republic of China (PRC, commonly simply named: $China),
	and the Republic of China (ROC, commonly known as $Taiwan).
	In particular, the article shall cover the different ways China attempts to gain control of Taiwan.</p>

	<p>Despite China's insistence over its "One China policy", there exist, de facto, two Chinas: the ROC and the PRC.
	Taiwanese people would be willing to change the official name of their country to "Republic of Taiwan",
	such that there would be only one China left,
	but it cannot be done for fear of triggering a full on war with the PRC.</p>
	HTML;


$div_blockade = new ContentSection();
$div_blockade->content = <<<HTML
	<h3>PRC's blockade of Taiwan</h3>

	<p>Any attempt by the $PRC to militarily invade $Taiwan would certainly include and attempt to blockade the island of Taiwan
	so as to prevent military aid and other resourced to be shipped to Taiwan from allied countries.</p>

	<p>A July 2023 report estimated that a blockade of Taiwan would cost the world economy US$2.7tn. ${r1} $r2</p>
	HTML;


$div_wikipedia_Political_status_of_Taiwan = new WikipediaContentSection();
$div_wikipedia_Political_status_of_Taiwan->setTitleText('Political status of Taiwan');
$div_wikipedia_Political_status_of_Taiwan->setTitleLink('https://en.wikipedia.org/wiki/Political_status_of_Taiwan');
$div_wikipedia_Political_status_of_Taiwan->content = <<<HTML
	<p>The controversy surrounding the political status of Taiwan or the Taiwan issue
	is a result of World War II, the second phase of the Chinese Civil War (1945–1949), and the Cold War.</p>
	HTML;

$div_wikipedia_Taiwan_China = new WikipediaContentSection();
$div_wikipedia_Taiwan_China->setTitleText('"Taiwan, China"');
$div_wikipedia_Taiwan_China->setTitleLink('https://en.wikipedia.org/wiki/Taiwan,_China');
$div_wikipedia_Taiwan_China->content = <<<HTML
	<p>"Taiwan, China", "Taiwan, Province of China", and "Taipei, China"
	are controversial political terms that claim Taiwan and its associated territories as a province or territory of "China".</p>
	HTML;

$page->parent('world.html');
$page->body($div_stub);

$page->body($div_introduction);
$page->body($div_blockade);

$page->body('chinese_expansionism.html');
$page->body('prc_china.html');
$page->body('taiwan.html');
$page->body('one_country_two_systems.html');

$page->body($div_wikipedia_Political_status_of_Taiwan);
$page->body($div_wikipedia_Taiwan_China);
