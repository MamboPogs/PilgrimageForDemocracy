<?php
$page = new Page();
$page->h1('Organization for Security and Co-operation in Europe (OSCE)');
$page->stars(0);
$page->keywords('OSCE');

$page->snp('description', "Arms control, promotion of human rights, freedom of the press, and free and fair elections.");
$page->snp('image', "/public_domain/OSCEcountries.png");

$page->preview( <<<HTML
	<p>The OSCE's mandate includes issues such as arms control, promotion of human rights, freedom of the press, and free and fair elections.</p>
	HTML );

$r1 = $page->ref('https://www.securitycouncilreport.org/monthly-forecast/2014-04/ukraine.php', 'Security Council Report: April 2014 Monthly Forecast');
$r2 = $page->ref('https://www.osce.org/project-coordinator-in-ukraine-closed', 'OSCE Project Co-ordinator in Ukraine (closed)');


$div_osce_website = new ContentSection();
$div_osce_website->content = <<<HTML
	<h3><a href="https://www.osce.org/">osce.org - OSCE</a></h3>

	<p><q>The OSCE has a comprehensive approach to security that encompasses politico-military, economic and environmental, and human aspects.
	It, therefore, addresses a wide range of security-related concerns, including arms control, confidence- and security-building measures,
	human rights, national minorities, democratization, policing strategies, counter-terrorism and economic and environmental activities.
	All 57 participating States enjoy equal status, and decisions are taken by consensus on a politically, but not legally, binding basis.</q></p>
	HTML;


$div_codeberg = new CodebergContentSection();
$div_codeberg->setTitleText('OSCE (Organization for Security and Co-operation in Europe)');
$div_codeberg->setTitleLink('https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/21');
$div_codeberg->content = <<<HTML
	<p>Help us check news from the OSCE, their action and the resources that they provide.</p>
	HTML;


$div_wikipedia_osce = new WikipediaContentSection();
$div_wikipedia_osce->setTitleText('Organization for Security and Co-operation in Europe');
$div_wikipedia_osce->setTitleLink('https://en.wikipedia.org/wiki/Organization_for_Security_and_Co-operation_in_Europe');
$div_wikipedia_osce->content = <<<HTML
	<p>The OSCE's mandate includes issues such as arms control, promotion of human rights, freedom of the press, and free and fair elections.</p>
	HTML;

$div_osce_ukraine = new ContentSection();
$div_osce_ukraine->content = <<<HTML
	<h3>The OSCE and Ukraine</h3>

	<p>Following a request to the OSCE by Ukraine’s Government,
	an OSCE Project Co-ordinator in Ukraine started its activities on 1 June, 1999
	but discontinued its operations on 30 June, 2022, after Russia's full scale invasion of Ukraine. ${r2}</p>

	<p>Following Russia's annexation of Ukraine's Crimea, an OSCE monitoring mission for Ukraine was approved on 21 March, 2014,
	but given Russia's participation in the OSCE, it was doubtful from the beginning that the mission would be granted access to Crimea. ${r1}
	The monitoring mission was dissolved on 31 March 2022.</p>

	<p>On wikipedia:</p>
	<ul>
	<li><a href="https://en.wikipedia.org/wiki/OSCE_Special_Monitoring_Mission_to_Ukraine">OSCE Special Monitoring Mission to Ukraine</a></li>
	</ul>
	HTML;



$page->parent('world.html');
$page->body($div_stub);
$page->body($div_codeberg);
$page->body($div_wikipedia_osce);
$page->body($div_osce_website);
$page->body($div_osce_ukraine);
