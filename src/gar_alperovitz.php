<?php
$page = new Page();
$page->h1('Gar Alperovitz');
$page->keywords('Gar Alperovitz');
$page->stars(0);

$page->preview( <<<HTML
	<p></p>
	HTML );

$page->snp('description', 'Historian and political economist.');
//$page->snp('image',       '/copyrighted/');



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$div_Historian_political_economist_activist_writer = new WebsiteContentSection();
$div_Historian_political_economist_activist_writer->setTitleText('Historian, political economist, activist, writer');
$div_Historian_political_economist_activist_writer->setTitleLink('https://garalperovitz.org/');
$div_Historian_political_economist_activist_writer->content = <<<HTML
	<p>Gar Alperovitz's website.</p>
	HTML;



$div_Building_a_Democratic_Economy_Sketch_of_a_Pluralist_Commonwealth = new WebsiteContentSection();
$div_Building_a_Democratic_Economy_Sketch_of_a_Pluralist_Commonwealth->setTitleText('Building a Democratic Economy: Sketch of a Pluralist Commonwealth');
$div_Building_a_Democratic_Economy_Sketch_of_a_Pluralist_Commonwealth->setTitleLink('https://nonprofitquarterly.org/building-a-democratic-economy-sketch-of-a-pluralist-commonwealth/');
$div_Building_a_Democratic_Economy_Sketch_of_a_Pluralist_Commonwealth->content = <<<HTML
	<p>For a time, after the collapse of communism in the Soviet Union and the retreat of social democracy at the hands of neoliberalism in the West,
	it was proclaimed that unencumbered corporate capitalism—with all its inequality and environmental costs—was the only game in town, the last system left standing.</p>

	<p>Especially since the Great Recession, this judgment has begun to change.
	We see hints of this in the rise of Senator Bernie Sanders as a serious candidate for president
	and the prominence of Representative Alexandria Ocasio-Cortez in Congress.</p>

	<p>At the same time, there has been an explosion of on-the-ground experimentation and new institutional development that includes worker cooperatives
	(and public support for their development), community land trusts,
	and rising activism around a range of proposals that would expand the scope of the public sector, such as Medicare for All and public banking.</p>

	<p>But what would it take to go from proposals to a new economic system?</p>
	HTML;



$div_wikipedia_Gar_Alperovitz = new WikipediaContentSection();
$div_wikipedia_Gar_Alperovitz->setTitleText('Gar Alperovitz');
$div_wikipedia_Gar_Alperovitz->setTitleLink('https://en.wikipedia.org/wiki/Gar_Alperovitz');
$div_wikipedia_Gar_Alperovitz->content = <<<HTML
	<p>Gar Alperovitz is an American historian and political economist.</p>
	HTML;


$page->parent('list_of_people.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_Historian_political_economist_activist_writer);
$page->body($div_Building_a_Democratic_Economy_Sketch_of_a_Pluralist_Commonwealth);

$page->body($div_wikipedia_Gar_Alperovitz);
