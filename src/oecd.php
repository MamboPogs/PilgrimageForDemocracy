<?php
$page = new Page();
$page->h1('Organisation for Economic Co-operation and Development (OECD)');
$page->keywords('OECD');
$page->stars(0);

$page->snp('description', 'Stimulate economic progress and seeking best practice.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The Organisation for Economic Co-operation and Development (OECD) was founded in 1961,
	and comprises 38 member states.</p>

	<p>Check the OECD's policy observatory on ${'artificial intelligence'}.</p>
	HTML;


$div_OECD_website = new WebsiteContentSection();
$div_OECD_website->setTitleText('OECD website ');
$div_OECD_website->setTitleLink('https://www.oecd.org/');
$div_OECD_website->content = <<<HTML
	<p>The Organisation for Economic Co-operation and Development (OECD) is
	an international organisation that works to build better policies for better lives.
	Our goal is to shape policies that foster prosperity, equality, opportunity and well-being for all.
	We draw on 60 years of experience and insights to better prepare the world of tomorrow.</p>

	<p>Together with governments, policy makers and citizens, we work on establishing evidence-based international standards and
	finding solutions to a range of social, economic and environmental challenges.
	From improving economic performance and creating jobs to fostering strong education and fighting international tax evasion,
	we provide a unique forum and knowledge hub for data and analysis, exchange of experiences, best-practice sharing,
	and advice on public policies and international standard-setting.</p>
	HTML;


$div_States_of_Fragility_2022 = new WebsiteContentSection();
$div_States_of_Fragility_2022->setTitleText('States of Fragility 2022 ');
$div_States_of_Fragility_2022->setTitleLink('https://www.oecd.org/dac/states-of-fragility-fa5a6770-en.htm');
$div_States_of_Fragility_2022->content = <<<HTML
	<p>States of Fragility 2022 arrives during an ‘age of crises’,
	where multiple, concurring crises are disproportionately affecting the 60 fragile contexts identified in this year’s report.
	Chief among these crises are COVID-19, Russia's invasion of Ukraine, and climate change,
	with the root causes of multidimensional fragility playing a central role in shaping their scale and severity.
	The report outlines the state of fragility in 2022, reviews current responses to it,
	and presents options to guide better policies for better lives in fragile contexts.
	At the halfway point of the 2030 Agenda for Sustainable Development,
	it is more critical than ever for development partners to focus on the furthest behind:
	the 1.9 billion people in fragile contexts that account for 24% of the world’s population but 73% of the world’s extreme poor.</p>
	HTML;




$div_OECD_Reinforcing_Democracy_Initiative = new WebsiteContentSection();
$div_OECD_Reinforcing_Democracy_Initiative->setTitleText('OECD: Reinforcing Democracy Initiative ');
$div_OECD_Reinforcing_Democracy_Initiative->setTitleLink('https://www.oecd.org/governance/reinforcing-democracy/');
$div_OECD_Reinforcing_Democracy_Initiative->content = <<<HTML
	<p>$Democracies are under unprecedented levels of pressure from within and without.
	The polarisation of ${'political discourse'}, geopolitical tensions, public health and economic crises,
	and creeping foreign influence in democratic processes – all also fueled by mis- and disinformation – have tested citizens’ trust in public institutions
	and are driving many governments to strengthen and protect democratic values and processes.</p>

	<p>Launched at the 2022 OECD Global Forum and Ministerial on Building Trust and Reinforcing Democracy,
	the OECD’s Reinforcing Democracy Initiative provides evidence-based guidance and good international practices
	to help countries reinforce democratic values and institutions.</p>
	HTML;


$div_wikipedia_OECD = new WikipediaContentSection();
$div_wikipedia_OECD->setTitleText('OECD');
$div_wikipedia_OECD->setTitleLink('https://en.wikipedia.org/wiki/OECD');
$div_wikipedia_OECD->content = <<<HTML
	<p>The Organisation for Economic Co-operation and Development is an intergovernmental organisation
	with 38 Member countries, founded in 1961 to stimulate economic progress and world trade.
	It is a forum whose member countries describe themselves as committed to democracy and the market economy,
	providing a platform to compare policy experiences, seek answers to common problems,
	identify good practices, and coordinate domestic and international policies of its members.</p>
	HTML;


$page->parent('world.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_OECD_website);
$page->body($div_States_of_Fragility_2022);
$page->body($div_OECD_Reinforcing_Democracy_Initiative);
$page->body($div_wikipedia_OECD);
