<?php
$page = new Page();
$page->h1('6: Democracy &mdash; a Work in Progress');
$page->stars(3);

$page->preview( <<<HTML
	<p>Almost by definition, a democracy is a <em>work in progress</em>.
	The accronym <em>WIP</em> should be tagged after the name of each democratic country:
	<em>$USA (WIP)</em>, <em>$Germany (WIP)</em>, <em>$France (WIP)</em>, etc.
	This article explains why.</p>
	HTML );


$page->snp('description', "Improve it or lose it!");
$page->snp('image', "/copyrighted/sandy-millar-yvpexJFLTSU.1200-630.jpg");

$r1 = $page->ref('https://en.wikipedia.org/wiki/List_of_amendments_to_the_United_States_Constitution', 'List of amendments to the United States Constitution');


$div_codeberg = new CodebergContentSection();
$div_codeberg->setTitleText('Democracy: a work in progress');
$div_codeberg->setTitleLink('https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/30');
$div_codeberg->content = <<<HTML
	<p>This article is itself a work in progress.
	Do you have any ideas on how to improve it?</p>
	HTML;

$div_A_more_perfect_Union = new ContentSection();
$div_A_more_perfect_Union->content = <<<HTML
	<h3>A more perfect Union</h3>

	<blockquote>
	We the People of the United States, in Order to form a more perfect Union…
	</blockquote>
	<p>Thus are the opening words of the American Constitution.
	Note the founding fathers of America did not use of the definite: "<em>the perfect union</em>",
	much less the superlative: "<em>the most perfect union</em>".
	Instead, they use the comparative because democracy is about striving to do <em>better</em> than in the past.</p>

	<p>American democracy was extremely flawed from its inception:
	the founders had excluded everyone but white, male, landed gentry from “<em>We the People</em>.”
	And yet, embedded within the American constitution,
	were the means to improve the institutions of the nation.
	Several series of constitutional amendments ${r1} improved the safeguards for liberty, justice and civil rights,
	thereby rectifying some of the initial shortcomings of American democracy.</p>

	<p>And yet, as the 2020~2021 coup attempt demonstrated,
	not only there is still a long way to go to improve American democracy,
	but also continuous efforts must be made to ensure
	that there shall not be any backsliding on over two hundred years of achievements.</p>

	<p>In any democracy, there is no standing still: either we move forward, or we regress.
	If we do not actively evolve, then we risk to degenerate and devolve.</p>

	We cannot stop improving the state of democracy in any country.</p>
	HTML;

$div_people_heart_of_democracy = new ContentSection();
$div_people_heart_of_democracy->content = <<<HTML
	<h3>People are the heart of democracy</h3>

	<p>Let's remember that democracy is a form of government where the citizens, the people, have the power.
	Human beings are inherently flawed.
	As we lead our lives, we are all more or less clumsily trying to improve our being and our condition, become better versions of ourselves.
	It stands to reason that a form of government created by imperfect beings who are trying to perfect themselves
	would itself be equally imperfect and improvable.</p>

	<p>Maybe the best way to improve the very country we are living in,
	is simply to improve ourselves,
	and through our example, inspire others to do the same.
	Let's start with individual efforts:
	Eventually, it is the culmination of such individual efforts that make a difference.</p>
	HTML;

$div_regression_of_democracy = new ContentSection();
$div_regression_of_democracy->content = <<<HTML
	<h3>Regression of democracy</h3>

	<p>While it is very true that a democracy can always be improved and made better,
	We have to be mindful of the fact that the state of democracy in any country can also degenerate.
	If we do not preciously guard what we have, we run the risk of losing it all.</p>

		<blockquote>
		People too often think that democracy is like water from the tap:<br>
		you open it; it comes out; and it will always be there.<br>
		— ${'Anne Applebaum'}
		</blockquote>

	<p>Western countries where democracy has old and deep roots are not free from danger.
	The obvious example is how frighteningly close US democracy came to be overthrown.
	Western European countries also face their share of worrisome challenges,
	with rising populism, endemic individualism, etc.</p>

	<p>And elsewhere in the world, we have seen countless countries who struggled to establish a rule for the people by the people,
	only to see here a president indefinitely extending the length of his term,
	there a military junta taking over civilian rule.</p>

	<p>We have to stop taking what we have for granted.
	We have to cherish our civil liberties and use them to preserve and improve the institutions that we have,
	lest we lose it all.</p>
	HTML;


$div_democracy_wicked_problem = new ContentSection();
$div_democracy_wicked_problem->content = <<<HTML
	<h3>Democracy as a Wicked Problem</h3>

	<p>Democracy has all the hallmarks of a <em><a href="#wikipedia-wicked-problem">Wicked Problem</a></em>.
	Already, defining democracy presented us with a challenge, as there is no clear-cut, definite and commonly agreed upon definition of the word.
	Even though the broad characteristics are understood,
	each institute specialized in promoting democracy would have their own definition and emphasis.</p>

	<p>while it is obvious that democracy is facing scores of problems and challenges,
	there is no existing end line, a specific goal, that must be reached before we can definitely rest.
	A marathon may be a long race, but the finish line is clearly established,
	and having reached it, the runner can stop running in satisfaction of having completed the race.
	It is not so with democracy.</p>

	<p>Like in a wicked problem, every problem that democracy is facing can be considered to be a symptom of another problem.</p>

	<p>Also, solutions are never clear cut.
	No proposal would ever be an absolute panacea.
	Given a particular problem, the best we can hope is to make things <em>better</em>, lest they become <em>worse</em>.</p>

	<p>Thus, we can only hope and try to make things better, and try to climb one hill at a time,
	in the absolute knowledge that behind each hill, there is another hill.
	We can try to solve one specific problem to the best of our understanding and ability,
	and trust that tomorrow we shall have the wisdom and the courage to fix tomorrow's problems.</p>
	HTML;

$div_priorities = new ContentSection();
$div_priorities->content = <<<HTML
	<h3>Priorities</h3>

	<p>While there is no country that does not have room for improvement,
	the most pressing needs for each country would be different.
	Maybe the possibilities for improvement for any two Nordic countries would be quite similar,
	but compared to some of the worst regimes on Earth, it is obvious that the priorities would differ drastically.</p>

	<p>Thus it is necessary to make separate assessments for each region of the world, for each individual country,
	for each type of regime and according to the current stage of democratic development.</p>

	<p>It is one of the long term goals of the <em>Pilgrimage for Democracy and Social Justice</em> project
	to assess the democratic shortcomings in each region and each country, including in the most democratically evolved countries.</p>
	HTML;



$div_wikipedia_wicked_problem = new WikipediaContentSection();
$div_wikipedia_wicked_problem->setTitleText('Wicked problem');
$div_wikipedia_wicked_problem->setTitleLink('https://en.wikipedia.org/wiki/Wicked_problem');
$div_wikipedia_wicked_problem->id('wikipedia-wicked-problem');
$div_wikipedia_wicked_problem->content = <<<HTML
	<p>In planning and policy, a wicked problem is a problem that is difficult or impossible to solve
	because of incomplete, contradictory, and changing requirements that are often difficult to recognize.
	It refers to an idea or problem that cannot be fixed,
	where there is no single solution to the problem;
	and "wicked" denotes resistance to resolution, rather than evil.</p>
	HTML;



$page->parent('democracy.html');

$page->body($div_A_more_perfect_Union);
$page->body($div_people_heart_of_democracy);
$page->body($div_regression_of_democracy);
$page->body($div_democracy_wicked_problem);
$page->body($div_wikipedia_wicked_problem);
$page->body($div_priorities);



$page->body($div_codeberg);
include('section/features_of_democracy.php');
$page->body('primary_features_of_democracy.html');
$page->body($div_section_democracy_soloist_choir);
