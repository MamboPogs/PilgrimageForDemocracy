<?php
$page = new CountryPage('Vietnam');
$page->h1('Vietnam');
$page->keywords('Vietnam');
$page->stars(0);

$page->snp('description', '100 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Vietnam = new WikipediaContentSection();
$div_wikipedia_Vietnam->setTitleText('Vietnam');
$div_wikipedia_Vietnam->setTitleLink('https://en.wikipedia.org/wiki/Vietnam');
$div_wikipedia_Vietnam->content = <<<HTML
	<p>Vietnam, officially the Socialist Republic of Vietnam, is a country at the eastern edge of mainland Southeast Asia,
	with an area of 331,212 square kilometres (127,882 sq mi) and a population of over 100 million,
	making it the world's fifteenth-most populous country.</p>
	HTML;

$div_wikipedia_Human_rights_in_Vietnam = new WikipediaContentSection();
$div_wikipedia_Human_rights_in_Vietnam->setTitleText('Human rights in Vietnam');
$div_wikipedia_Human_rights_in_Vietnam->setTitleLink('https://en.wikipedia.org/wiki/Human_rights_in_Vietnam');
$div_wikipedia_Human_rights_in_Vietnam->content = <<<HTML
	<p>Human rights in Vietnam are among the poorest in the world, as considered by various domestic and international academics,
	dissidents and non-governmental organizations (NGOs) such as Amnesty International (AI), Human Rights Watch (HRW),
	and the United Nations High Commissioner for Human Rights (OHCHR).</p>
	HTML;

$div_wikipedia_Freedom_of_religion_in_Vietnam = new WikipediaContentSection();
$div_wikipedia_Freedom_of_religion_in_Vietnam->setTitleText('Freedom of religion in Vietnam');
$div_wikipedia_Freedom_of_religion_in_Vietnam->setTitleLink('https://en.wikipedia.org/wiki/Freedom_of_religion_in_Vietnam');
$div_wikipedia_Freedom_of_religion_in_Vietnam->content = <<<HTML
	<p>While the Constitution of Vietnam officially provides for freedom of religion,
	in practice the government imposes a range of legislative measures restricting religious practice
	(such as registration requirements, control boards, and surveillance).
	All religious groups must register and seek approval from the government.
	The government requires all Buddhist monks to be approved by and work under
	the officially recognized Buddhist organization, the Vietnam Buddhist Sangha (VBS).</p>
	HTML;


$page->parent('world.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_Vietnam);
$page->body($div_wikipedia_Human_rights_in_Vietnam);
$page->body($div_wikipedia_Freedom_of_religion_in_Vietnam);
