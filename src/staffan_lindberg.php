<?php
$page = new Page();
$page->h1('Staffan Lindberg');
$page->stars(0);
$page->keywords('Staffan Lindberg');

//$page->snp('description', "");
//$page->snp('image', "/copyrighted/");

$page->preview( <<<HTML
	<p>Staffan Lindberg is a Swedish political scientist and Director of the <a href='/v_dem_institute.html'>V-Dem Institute</a>.</p>
	HTML );




$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Staffan Lindberg is a Swedish political scientist and Director of the ${'V-Dem Institute'}.</p>
	HTML;



$div_Staffan_I_Lindberg = new WebsiteContentSection();
$div_Staffan_I_Lindberg->setTitleText('Staffan I. Lindberg');
$div_Staffan_I_Lindberg->setTitleLink('https://v-dem.net/about/v-dem-project/global-team/staffan-i-lindberg/');
$div_Staffan_I_Lindberg->content = <<<HTML
	<p>Staffan I. Lindberg is the Director of the V-Dem Institute at University of Gothenburg
	and one of five Principal Investigators for Varieties of Democracy (V-Dem).</p>
	HTML;



$div_googlescholar_Staffan_I_Lindberg = new GoogleScholarContentSection();
$div_googlescholar_Staffan_I_Lindberg->setTitleText('Staffan I. Lindberg');
$div_googlescholar_Staffan_I_Lindberg->setTitleLink('https://scholar.google.se/citations?user=aW96DOEAAAAJ&hl=en');
$div_googlescholar_Staffan_I_Lindberg->content = <<<HTML
	<p>V-Dem Institute & Dept. of Polisci., Univ. of Gothenburg.</p>
	HTML;



$div_wikipedia_Staffan_I_Lindberg = new WikipediaContentSection();
$div_wikipedia_Staffan_I_Lindberg->setTitleText('Staffan I Lindberg');
$div_wikipedia_Staffan_I_Lindberg->setTitleLink('https://en.wikipedia.org/wiki/Staffan_I._Lindberg');
$div_wikipedia_Staffan_I_Lindberg->content = <<<HTML
	<p>Staffan I. Lindberg is a Swedish political scientist, Principal Investigator for Varieties of Democracy (V-Dem) Institute
	and Director of the V-Dem Institute at the University of Gothenburg.
	He is a professor in the Department of Political Science, and member of the Board of University of Gothenburg, Sweden.</p>
	HTML;


$page->parent('list_of_people.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body('v_dem_institute.html');

$page->body($div_Staffan_I_Lindberg);
$page->body($div_googlescholar_Staffan_I_Lindberg);
$page->body($div_wikipedia_Staffan_I_Lindberg);
