<?php
$page = new Page();
$page->h1('Taiwan–Ukraine relations');
$page->stars(0);

$page->preview( <<<HTML
	<p>Two democracies threatened by aggressive totalitarian neighbours...</p>
	HTML );


$page->snp('description', "two democracies under threat");
//$page->snp('image', "/copyrighted/");

$r1 = $page->ref('https://www.taipeitimes.com/News/taiwan/archives/2023/07/02/2003802520', 'Changhua man fixes up 48 emergency vehicles for Ukraine');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	Following $Russia's invasion of $Ukraine, the government of the Republic of China ($Taiwan) has condemned Russia and imposed sanctions.
	At the same time, Taiwan has sent a lot of humanitarian aid to Ukraine.</p>
	HTML;


$div_humanitarian_aid = new ContentSection();
$div_humanitarian_aid->content = <<<HTML
	<h3>Humanitarian aid</h3>

	<p>What follows is a very incomplete list of humanitarian aid sent by Taiwan to Ukraine.</p>

	<p>In July 2023, 48 refurbished emergency vehicles were to be sent to Ukraine.
	About NT$10 million (US$321,182) were spent to  refurbish the vehicles
	which were donated by governmental organizations. ${r1}</p>
	HTML;


$div_wikipedia_Taiwan_Ukraine_relations = new WikipediaContentSection();
$div_wikipedia_Taiwan_Ukraine_relations->setTitleText('Taiwan Ukraine relations');
$div_wikipedia_Taiwan_Ukraine_relations->setTitleLink('https://en.wikipedia.org/wiki/Taiwan–Ukraine_relations');
$div_wikipedia_Taiwan_Ukraine_relations->content = <<<HTML
	<p>Taiwan–Ukraine relations refer to the international relations between Taiwan and Ukraine.
	Bilateral relations after Ukraine's independence began in 1992.</p>
	HTML;

$page->parent('taiwan.html');
$page->parent('ukraine.html');
$page->body($div_stub);

$page->body($div_introduction);
$page->body($div_humanitarian_aid);


$page->body($div_wikipedia_Taiwan_Ukraine_relations);
