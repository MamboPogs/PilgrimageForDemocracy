<?php
$page = new Page();
$page->h1('Ground News');
$page->stars(1);
$page->keywords('Ground News');

//$page->snp('description', "");
//$page->snp('image', "/copyrighted/");

$page->preview( <<<HTML
	<p>Ground News aims to be "a platform that makes it easy to compare news sources,
	read between the lines of media bias and break free from algorithms."</p>
	HTML );


$r1 = $page->ref('https://ground.news/about', 'Ground News: about us');
$r2 = $page->ref('https://ground.news/mission', 'Ground News: mission');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Ground News claims to be "a platform that makes it easy to compare news sources,
	read between the lines of media bias and break free from algorithms." ${r1}</p>

	<p>They collate article from over 50,000 different news sources,
	group articles by topic and for each news source provide
	a rating about their bias (left, centre or right),
	their factuality and their ownership.</p>

	<p>Their vision is "positive coexistence where
	cooperative, civil debate is the norm,
	media is accountable,
	and critical thought is the baseline of our news, media, and information consumption. ${r2}</p>
	HTML;



$div_Top_News_StoriesTop_News_Stories = new WebsiteContentSection();
$div_Top_News_StoriesTop_News_Stories->setTitleText('Ground News');
$div_Top_News_StoriesTop_News_Stories->setTitleLink('https://ground.news/');
$div_Top_News_StoriesTop_News_Stories->content = <<<HTML
	<p>Ground News homepage.</p>
	HTML;



$page->parent('media.html');
$page->body($div_stub);

$page->body($div_introduction);
$page->body($div_Top_News_StoriesTop_News_Stories);
