<?php
$page = new Page();
$page->h1('Data-Pop Alliance');
$page->stars(1);
$page->keywords('Data-Pop Alliance');

$page->preview( <<<HTML
	<p>
	</p>
	HTML );


$page->snp('description', "Using data science for development.");
//$page->snp('image', "/copyrighted/");

$r1 = $page->ref('https://datapopalliance.org/about/about-dpa/', 'About DPA: We want to change the world with data.');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Since 2013, Data-Pop Alliance has worked to apply cutting edge data science to solve the world’s most pressing problems.
	As the 2030 deadline for the UN sustainable development goals approaches,
	they advocate the use of novel approaches to enhance the efficacy of the global development sector and reach those most in need. $r1</p>

	<p>Data-Pop Alliance has a research and outreach program on ${'technology and democracy'}.</p>
	HTML;


$div_Data_Pop_Alliance = new WebsiteContentSection();
$div_Data_Pop_Alliance->setTitleText('Data-Pop Alliance');
$div_Data_Pop_Alliance->setTitleLink('https://datapopalliance.org/');
$div_Data_Pop_Alliance->content = <<<HTML
	<p>Data-Pop Alliance's website.</p>
	HTML;



$div_wikipedia_Data_Pop_Alliance = new WikipediaContentSection();
$div_wikipedia_Data_Pop_Alliance->setTitleText('Data Pop Alliance');
$div_wikipedia_Data_Pop_Alliance->setTitleLink('https://en.wikipedia.org/wiki/Data-Pop_Alliance');
$div_wikipedia_Data_Pop_Alliance->content = <<<HTML
	<p>Data-Pop Alliance is a non-profit think tank founded by the Harvard Humanitarian Initiative, MIT Media Lab and the Overseas Development Institute.
	Its research areas includes public policy, inequality, privacy, crime, climate change and human rights.
	Data-Pop Alliance is a partner for the United Nations Global Partnership for Sustainable Development Data
	and the Sustainable Development Learning & Training Course Partnerships.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->body($div_stub);

$page->body($div_introduction);

$page->body($div_Data_Pop_Alliance);
$page->body($div_wikipedia_Data_Pop_Alliance);
