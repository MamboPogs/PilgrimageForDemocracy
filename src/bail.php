<?php
$page = new Page();
$page->h1('Bail');
$page->keywords('Bail', 'bail');
$page->stars(0);

$page->snp('description', 'Pay up or sit in jail awaiting trial.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Bail is an amount of money that a defendant must pay the court in order to go home free while awaiting trial.</p>
	HTML;

$div_In_the_United_States = new ContentSection();
$div_In_the_United_States->content = <<<HTML
	<h3>In the United States</h3>

	<p>An industry has grown around the bail system: bound bails are a non-refundable amount that defendants pay to private companies
	who would then lend the full bail amount money.</p>

	<p>According to a 2022 report by the U.S Commission on civil rights, on any given day,
	there are roughly 650 000 individuals in jail awaiting trial in America.
	Roughly 75 percent of these people, around half a million people, are being held in pre-trial detention,
	They have not been convicted, and are  presumed innocent until proven guilty.
	They are sitting in jail just waiting for their day in court.
	the vast majority of them are there because they can't afford to post bail.
	According to the American Bar Association nationally the median bail amount is about ten thousand dollars
	and the median annual income of people who are in pre-trial detention is about fifteen thousand dollars.
	These numbers vary a lot by state.
	In 2019,the New Jersey Administrative Office of the courts found that the average bail amount for defendants who could not
	afford it was 2,500 dollars.
	800 defendants in New Jersey were unable to post bail of five hundred dollars that year, so they sit in jail.
	The system puts poor low wealth and low-income Americans at a huge disadvantage.
	According to the U.S Commission on civil rights, nearly 60 percent of pre-trial detainees are held
	because they can't afford to post bail.
	The report also found that black and Latino men faced higher financial conditions of release compared to other demographics.
	In many cases, they are asked to post roughly twice the amount as their white counterparts for committing exactly the same crime.
	Studies have shown that pre-trial detention increases the rate of recidivism, of mental health issues,
	of homelessness and unemployment.
	Some states like Alaska, Illinois, and New Jersey have ended money bail altogether,
	other states like California and New York take into account a person's wealth and income when deciding on bail amounts.</p>
	HTML;

$div_wikipedia_Bail = new WikipediaContentSection();
$div_wikipedia_Bail->setTitleText('Bail');
$div_wikipedia_Bail->setTitleLink('https://en.wikipedia.org/wiki/Bail');
$div_wikipedia_Bail->content = <<<HTML
	<p>Bail is a set of pre-trial restrictions that are imposed on a suspect to ensure that they will not hamper the judicial process.
	Bail is the conditional release of a defendant with the promise to appear in court when required.
	In some countries, especially the United States, bail usually implies a bail bond,
	a deposit of money or some form of property to the court by the suspect in return for the release from pre-trial detention.</p>
	HTML;

$div_wikipedia_2019_New_York_bail_reform = new WikipediaContentSection();
$div_wikipedia_2019_New_York_bail_reform->setTitleText('2019 New York bail reform');
$div_wikipedia_2019_New_York_bail_reform->setTitleLink('https://en.wikipedia.org/wiki/2019_New_York_bail_reform');
$div_wikipedia_2019_New_York_bail_reform->content = <<<HTML
	<p>The U.S. state of New York enacted bail reform, in an act that stood from January to June 2020.
	As part of the New York State Fiscal Year Budget for 2019–2020, passed on April 1, 2019,
	cash bail was eliminated for most misdemeanor and non-violent felony charges, including stalking,
	assault without serious injury, burglary, many drug offenses, and even some kinds of arson and robbery.</p>
	HTML;


$page->parent('justice.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body($div_In_the_United_States);


$page->body($div_wikipedia_Bail);
$page->body($div_wikipedia_2019_New_York_bail_reform);
