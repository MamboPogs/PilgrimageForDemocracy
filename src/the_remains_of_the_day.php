<?php
$page = new Page();
$page->h1('The Remains of the Day');
$page->stars(3);
$page->keywords('The Remains of the Day');

$page->preview( <<<HTML
	<p>The novel "The Remains of the Day", and the film adapted from it, is set in Britain in the 1930's.
	Pre-World War II Britain is seen through the eyes of Stevens,
	a butler who dedicated his whole life to be at the service of nobility,
	taking care of the large mansion of a Lord Darlington.</p>
	HTML );


$page->snp('description', "A 1989 novel by the Nobel Prize-winning British author Kazuo Ishiguro.");
//$page->snp('image', "/copyrighted/");

$r1 = $page->ref('https://thebookerprizes.com/the-booker-library/features/how-the-remains-of-the-day-changed-the-way-i-think-about-england', 'How The Remains of the Day changed the way I think about England');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The novel "The Remains of the Day", and the film adapted from it, is set in Britain in the 1930's.
	Pre-World War II Britain is seen through the eyes of Stevens, a butler who dedicated his whole life to be at the service of nobility,
	taking care of the large mansion of a Lord Darlington.</p>
	HTML;

$div_themes = new ContentSection();
$div_themes->content = <<<HTML
	<img src="/copyrighted/Remains_of_the_day.jpg"  style="float:right; margin-left: 1em;">

	<h3>Themes</h3>

	<p>One major theme is the class structure, the self-effacing attitude of all the servants, aids, cooks, etc.
	who are subservient to the lord of the house and his noble guests.
	The ever so humble butler Stevens led a life of service to those he considered more important than himself.</p>

	<p>Another major theme is the political background of pre-war Britain,
	and the conciliatory and sometimes even sympathizing attitude of the British ruling class towards the Nazis
	who are taking hold of Germany and setting the stage for the war.</p>

	<p>The two themes are often enmeshed together, like for example when the obedient Stevens
	unquestioningly defer to Lord Darlington’s decision to sack two Jewish housemaids:
	"<em>There are many things you and I are simply not in a position to understand concerning, say, the nature of Jewry,</em>" says Stevens,
	"<em>Whereas his lordship, I might venture, is somewhat better placed to judge what is for the best.</em>"</p>
	HTML;


$div_opinion_man_in_the_street = new ContentSection();
$div_opinion_man_in_the_street->content = <<<HTML
	<h3>The opinion of common folks...</h3>

	<p>In one notable passage in the story, some guests of Lord Darlington are gathered together in the smoking room.
	The guests are Nazi-sympathising nobility and gentlemen, one of whom, Mr Spencer,
	is arguing that it would be foolish to allow common people, uneducated in political affairs, to have their say.</p>

	<blockquote>
	<strong>Guest</strong>: I dare say that you cannot go wrong if you listen to the opinions of your ordinary man in the street.
	They're perfectly entitled to give an opinion on politics or whatever questions-<br><br>

	<strong>Mr Spencer</strong>: They've got no qualifications whatsoever!<br><br>

	<strong>Guest</strong>: Of course they have! <br><br>

	<strong>Lord Darlington</strong> [<em>calling the butler</em>]: Stevens! Mr Spencer would like a word with you. <br><br>

	<strong>Mr Spencer</strong>: My good man, I have a question for you.<br><br>

	<strong>Stevens</strong>: Yes, Sir?<br><br>

	<strong>Mr Spencer</strong>: Do you suppose the debt situation regarding America factors significantly in the present low levels of trade?
	Or is this a red herring and the abandonment of the gold standard is the cause of the problem?<br><br>

	<strong>Stevens</strong> [<em>embarrassed</em>]: I'm sorry, sir, but I am unable to be of assistance in this matter.<br><br>

	<strong>Mr Spencer</strong>: Oh, dear. What a pity.<br>
	Perhaps you'd help us on another matter.
	Do you think Europe's currency problem would be alleviated by an arms agreement between the French and the Bolsheviks?<br><br>

	<strong>Mr Spencer</strong> [<em>confused</em>]: I'm sorry, sir, but I'm unable to be of assistance in this matter.<br><br>

	<strong>Lord Darlington</strong> [<em>to Stevens</em>]: Very well, that'll be all.<br><br>

	<strong>Mr Spencer</strong>: One moment, Darlington, I have another question to put to our good man here.<br>
	[<em>to Stevens</em>] My good fellow do you share our opinion that M. Daladier's recent speech on North Africa
	was simply a ruse to scupper the nationalist fringe of his own domestic party?<br><br>

	<strong>Mr Spencer</strong> [<em>as polite as ever</em>]: I'm sorry, sir. I am unable to help in any of these matters.<br><br>

	<strong>Mr Spencer</strong> [<em>to the other guests</em>]: You see, our good man here is "<em>unable to assist us in these matters.</em>"
	Yet we still go along with the notion that this nation's decisions be left to our good man here and a few millions like him.
	You may as well ask the Mothers' Union to organize a war campaign.<br><br>

	<strong>Lord Darlington</strong> [<em>to Stevens, dismissing him</em>]: Thank you, Stevens.<br><br>

	<strong>Stevens</strong> [<em>to Lord Darlington</em>]: Thank you, My Lord.<br>
	[<em>to Mr Spencer</em>] Thank you, Sir.<br><br>

	<strong>Guest</strong>: You certainly proved your point.<br><br>

	<strong>Mr Spencer</strong>: Q.E.D., I think.<br>

	</blockquote>

	<p>Here is what Max Liu, a British writer, said about the above scene in the book: ${r1}</p>

	<blockquote>When I read it in 2002, the hard-won progress of the post-war era, which created a more equal Britain, was being consolidated.
	Subsequent events have undermined that confidence and sown uncertainty.
	Brexit has been compared to the Suez Crisis, which happened in the year in which The Remains of the Day is set.
	At the same time, <strong>scenes involving Lord Darlington and his Nazi-sympathising allies,
	who think ‘the world’s far too complicated a place for universal suffrage and such like’,
	show that when we lose faith in democracy we enter dangerous territory.</strong></blockquote>
	HTML;



$div_wikipedia_The_Remains_of_the_Day = new WikipediaContentSection();
$div_wikipedia_The_Remains_of_the_Day->setTitleText('The Remains of the Day');
$div_wikipedia_The_Remains_of_the_Day->setTitleLink('https://en.wikipedia.org/wiki/The_Remains_of_the_Day');
$div_wikipedia_The_Remains_of_the_Day->content = <<<HTML
	<p>The Remains of the Day is a 1989 novel by the Nobel Prize-winning British author Kazuo Ishiguro.</p>

	<p>The novel tells the story of Stevens, an English butler who has dedicated his life to the loyal service of Lord Darlington.
	Lord Darlington was a Nazi sympathizer...</p>
	HTML;

$div_wikipedia_The_Remains_of_the_Day_film = new WikipediaContentSection();
$div_wikipedia_The_Remains_of_the_Day_film->setTitleText('The Remains of the Day (film)');
$div_wikipedia_The_Remains_of_the_Day_film->setTitleLink('https://en.wikipedia.org/wiki/The_Remains_of_the_Day_(film)');
$div_wikipedia_The_Remains_of_the_Day_film->content = <<<HTML
	<p>The Remains of the Day is a 1993 drama film adapted from the novel of the same name.</p>

	<p>It stars Anthony Hopkins as James Stevens and Emma Thompson as Miss Kenton,
	with James Fox, Christopher Reeve, Hugh Grant, Ben Chaplin, and Lena Headey in supporting roles.
	The film was a critical and box office success and it was nominated for eight Academy Awards,
	including Best Picture, Best Actor (Hopkins), Best Actress (Thompson) and Best Adapted Screenplay (Jhabvala).</p>
	HTML;

$page->parent('list_of_movies.html');

$page->body($div_introduction);
$page->body($div_themes);
$page->body($div_opinion_man_in_the_street);
$page->body('professionalism_without_elitism.html');

$page->body($div_wikipedia_The_Remains_of_the_Day);
$page->body($div_wikipedia_The_Remains_of_the_Day_film);
