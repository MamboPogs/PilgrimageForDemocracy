<?php
$page = new Page();
$page->h1('Data activism');
$page->stars(1);
$page->keywords('Data activism', 'data activism');

$page->preview( <<<HTML
	<p>Using data to bring attention to social and political issues.</p>
	HTML );


$page->snp('description', "Using data to bring attention to social and political issues.");
//$page->snp('image', "/copyrighted/");



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p> Data activism refers to using data to bring attention to social and political issues
	and critically resists big data collection methods that infringe on users’ privacy.
	Data activists are individuals, organizations, and communities who wish to empower people with information,
	raise awareness, and hold those in power accountable through the use of data.</p>
	HTML;



$div_Empowering_Change_through_Insight = new WebsiteContentSection();
$div_Empowering_Change_through_Insight->setTitleText('Data Activism: Empowering Change through Insight');
$div_Empowering_Change_through_Insight->setTitleLink('https://datapopalliance.org/lwl-44-data-activism-empowering-change-through-insight/');
$div_Empowering_Change_through_Insight->content = <<<HTML
	<blockquote>"The most common way people give up their power is by thinking they don't have any" - Alice Walker</blockquote>

	<p>In today’s digital age, data is everywhere.
	From social media posts and online transactions to government records and scientific research,
	data is generated and collected at an unprecedented rate.
	But beyond its sheer volume, data has the potential to act as a powerful tool for promoting social and political change,
	in a phenomenon that has become known as “data activism”.</p>
	HTML;



$div_wikipedia_Data_activism = new WikipediaContentSection();
$div_wikipedia_Data_activism->setTitleText('Data activism');
$div_wikipedia_Data_activism->setTitleLink('https://en.wikipedia.org/wiki/Data_activism');
$div_wikipedia_Data_activism->content = <<<HTML
	<p>Data activism is a specific type of activism which is enabled and constrained by the data infrastructure.
	It can use the production and collection of digital, volunteered, open data to challenge existing power relations.
	It is a form of media activism.</p>
	HTML;



$page->parent('democracy.html');
$page->body($div_stub);

$page->body($div_introduction);

$page->body($div_Empowering_Change_through_Insight);
$page->body($div_wikipedia_Data_activism);
$page->body('technology_and_democracy.html');
