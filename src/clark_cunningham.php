<?php
$page = new Page();
$page->h1('Clark D. Cunningham');
$page->keywords('Clark Cunningham');
$page->stars(1);

$page->snp('description', 'Georgia professor of constitutional law, law & ethics.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );


$r1 = $page->ref('https://www.youtube.com/watch?v=6p1NTpJQm00', 'D.A. Fani Willis Arrested Trump: See how Georgia GOP is trying to remove her');
$r2 = $page->ref('https://www.facebook.com/georgiastatelaw/videos/clark-cunningham-professor-of-law-at-georgia-state-university-college-of-law-spo/1140074300274731/',
                 'Clark Cunningham, professor of law at Georgia State University College of Law spoke to reporters');
$r3 = $page->ref('http://www.clarkcunningham.org/Cunningham-Justice.html', 'Clark D. Cunningham: social justice');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Clark Cunningham is professor of law at Georgia State University College of Law.</p>

	<p>He specializes in $constitutional law, law and ethics.</p>

	<p>He made media appearances$r1 $r2 as Georgia Republicans made moves to have District Attorney Fani Willis removed,
	in an apparent attempt at ${'obstruction of justice'}.</p>

	<p>He also writes on the topic of ${'social justice'}. $r3</p>
	HTML;



$div_Clark_D_Cunningham_website = new WebsiteContentSection();
$div_Clark_D_Cunningham_website->setTitleText('Clark D. Cunningham\'s website');
$div_Clark_D_Cunningham_website->setTitleLink('http://www.clarkcunningham.org/');
$div_Clark_D_Cunningham_website->content = <<<HTML
	<p>W. Lee Burge Chair in Law & Ethics:
	The Burge Chair was established by an endowment from the U.S. District Court for the Middle District of Georgia,
	using funds collected for alleged lawyer misconduct to promote ethics, professionalism and access to justice.</p>
	HTML;



$div_Clark_D_Cunningham = new WebsiteContentSection();
$div_Clark_D_Cunningham->setTitleText('Georgia State University: Clark D. Cunningham');
$div_Clark_D_Cunningham->setTitleLink('https://law.gsu.edu/profile/clark-d-cunningham/');
$div_Clark_D_Cunningham->content = <<<HTML
	<p>Clark D. Cunningham, professor of law and the inaugural holder of the W. Lee Burge Chair in Law & Ethics,
	is the director of the National Institute for Teaching Ethics & Professionalism,
	a consortium of ethics centers at six universities,
	and the co-editor of the International Forum on Teaching Legal Ethics & Professionalism.
	He is one of the world’s leading experts on teaching legal ethics and on reform in legal education.</p>

	<p>He designed and teaches two innovative courses that provide an accelerated transition to practice
	by teaching fundamental knowledge, skills and values needed to begin a legal career in a wide variety of settings:
	Fundamentals of Law Practice and Transition to Practice.
	In both courses, students appear in Superior Court representing domestic violence victims in civil protection order proceedings
	and do fieldwork with a private attorney working in a practice area of interest to them.
	He also teaches The Client Relationship, which satisfies the professional responsibility requirement.</p>
	HTML;



$div_International_forum_on_teaching_legal_ethics_and_professionalism = new WebsiteContentSection();
$div_International_forum_on_teaching_legal_ethics_and_professionalism->setTitleText('International forum on teaching legal ethics and professionalism ');
$div_International_forum_on_teaching_legal_ethics_and_professionalism->setTitleLink('http://www.teachinglegalethics.org/');
$div_International_forum_on_teaching_legal_ethics_and_professionalism->content = <<<HTML
	<p>The International Forum on Teaching Legal Ethics and Professionalism website
	is designed as an online gathering place, resource repository, and clearinghouse
	for an international community of ethics teachers, scholars, and practitioners.
	The website provides an organizing tool for efforts to change the culture of legal education
	and to increase the emphasis on ethics and professionalism education across jurisdictions
	and throughout law schools’ curricula.</p>
	HTML;




$page->parent('list_of_people.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_Clark_D_Cunningham_website);
$page->body($div_Clark_D_Cunningham);
$page->body($div_International_forum_on_teaching_legal_ethics_and_professionalism);
