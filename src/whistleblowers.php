<?php
$page = new Page();
$page->h1('Whistleblower');
$page->keywords('whistleblower', 'whistleblowers');
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Whistleblower = new WikipediaContentSection();
$div_wikipedia_Whistleblower->setTitleText('Whistleblower');
$div_wikipedia_Whistleblower->setTitleLink('https://en.wikipedia.org/wiki/Whistleblower');
$div_wikipedia_Whistleblower->content = <<<HTML
	<p>A whistleblower is a person, often an employee, who reveals
	information about activity within a private or public organization that is deemed illegal, immoral, illicit, unsafe or fraudulent.
	Whistleblowers can use a variety of internal or external channels to communicate information or allegations.
	Over 83% of whistleblowers report internally to a supervisor, human resources, compliance,
	or a neutral third party within the company, hoping that the company will address and correct the issues.
	A whistleblower can also bring allegations to light by communicating with external entities,
	such as the media, government, or law enforcement.
	Whistleblowing can occur in either the private sector or the public sector.</p>
	HTML;

$div_wikipedia_List_of_whistleblowers = new WikipediaContentSection();
$div_wikipedia_List_of_whistleblowers->setTitleText('List of whistleblowers');
$div_wikipedia_List_of_whistleblowers->setTitleLink('https://en.wikipedia.org/wiki/List_of_whistleblowers');
$div_wikipedia_List_of_whistleblowers->content = <<<HTML
	<p>This is a list of major whistleblowers from various countries.</p>
	HTML;


$page->parent('democracy.html');
$page->parent('justice.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_wikipedia_Whistleblower);
$page->body($div_wikipedia_List_of_whistleblowers);
