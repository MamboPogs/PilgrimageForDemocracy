<?php
$page = new Page();
$page->h1('Duverger symptom 1: divisive dualism');
$page->stars(0);

$page->preview( <<<HTML
	HTML );

//$page->snp('description', "");
//$page->snp('image', "/copyrighted/");


$r1 = $page->ref('https://thehill.com/blogs/congress-blog/politics/267222-the-two-party-system-is-destroying-america/', 'The two-party system is destroying America');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<blockquote>
		<p><strong>The two-party system is destroying America</strong></p>

		<p>Democrats and Republicans are in a death match and the American people are caught in the middle. $r1</p>
	</blockquote>

	<p>This was headline and opening sentence of an op-ed in 2016 in a major American political website.
	Politics today is extremely dualistic and antagonistic, and not only in the $USA.
	This is the first and by far the most obvious symptom of the Duverger Syndrome.</p>
	HTML;

$div_dualism_united_states = new ContentSection();
$div_dualism_united_states->content = <<<HTML
	<h3>Bi-polarisation in the United States</h3>

	<p>American politics has long been dominated by two parties: the Republican Party and the Democratic Party.</p>

	<p>The increased polarisation of politics in the United States is such that there is less and less split-ticket voting .
	After the 2020 elections, out of 100 senators, only five caucus with the party that lost the 2020 presidential race in their state.
	In the House of Representatives, only 18 Republicans are in districts that Biden won, and 5 Democrats are in districts that Trump won.
	It is increasingly the same for governors: only nine states out of fifty have governors from the "wrong" party.</p>
	HTML;


$page->parent('duverger_syndrome.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body($div_dualism_united_states);
