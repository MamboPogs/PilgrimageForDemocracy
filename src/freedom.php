<?php
$page = new Page();
$page->h1('Freedom');
$page->keywords('Freedom', 'freedom');
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$list = new ListOfPages();
$list->add('freedom_of_association.html');
$list->add('freedom_of_conscience.html');
$list->add('freedom_of_speech.html');
$print_list = $list->print();

$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>Related content</h3>

	$print_list
	HTML;

$div_wikipedia_Freedom = new WikipediaContentSection();
$div_wikipedia_Freedom->setTitleText('Freedom');
$div_wikipedia_Freedom->setTitleLink('https://en.wikipedia.org/wiki/Freedom');
$div_wikipedia_Freedom->content = <<<HTML
	<p>Freedom is the power or right to act, speak, and change as one wants without hindrance or restraint.
	Freedom is often associated with liberty and autonomy in the sense of "giving oneself one's own laws".</p>

	<p>Philosophy and religion sometimes associate freedom with free will, as distinct from predestination.</p>

	<p>In modern liberal nations, freedom is considered a right, especially freedom of speech,
	freedom of religion, and freedom of the press.</p>
	HTML;


$page->parent('primary_features_of_democracy.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body($div_list);


$page->body($div_wikipedia_Freedom);
