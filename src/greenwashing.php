<?php
$page = new Page();
$page->h1('Greenwashing');
$page->keywords('Greenwashing', 'greenwashing');
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Greenwashing = new WikipediaContentSection();
$div_wikipedia_Greenwashing->setTitleText('Greenwashing');
$div_wikipedia_Greenwashing->setTitleLink('https://en.wikipedia.org/wiki/Greenwashing');
$div_wikipedia_Greenwashing->content = <<<HTML
	<p>Greenwashing (a compound word modeled on "whitewash"), also called "green sheen",
	is a form of advertising or marketing spin in which green PR and green marketing
	are deceptively used to persuade the public that an organization's products, aims and policies are environmentally friendly.
	Companies that intentionally take up greenwashing communication strategies often do so
	in order to distance themselves from their own environmental lapses or those of their suppliers.</p>
	HTML;


$page->parent('greenwashing.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_wikipedia_Greenwashing);
