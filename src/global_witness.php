<?php
$page = new Page();
$page->h1('Global Witness');
$page->keywords('Global Witness');
$page->stars(1);

$page->preview( <<<HTML
	<p>For a more sustainable, just and equal planet,
	for forests and biodiversity to thrive, fossil fuels to stay in the ground
	and corporations to prioritise the interests of people and the planet.</p>
	HTML );

$page->snp('description', 'Protect human rights and secure the future of our planet.');
//$page->snp('image',       '/copyrighted/');



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Global Witness supports ${'environmental defenders'}.</p>

	<p>In 2021 alone, as per a Global Witness report, 200 land and ${'environmental defenders'} were killed worldwide.</p>
	HTML;



$div_Challenging_abuses_of_power_to_protect_human_rights_and_secure_the_future_of = new WebsiteContentSection();
$div_Challenging_abuses_of_power_to_protect_human_rights_and_secure_the_future_of->setTitleText('Challenging abuses of power to protect human rights and secure the future of our planet');
$div_Challenging_abuses_of_power_to_protect_human_rights_and_secure_the_future_of->setTitleLink('https://www.globalwitness.org/en/');
$div_Challenging_abuses_of_power_to_protect_human_rights_and_secure_the_future_of->content = <<<HTML
	<p>Our goal is a more sustainable, just and equal planet.
	We want forests and biodiversity to thrive, fossil fuels to stay in the ground
	and corporations to prioritise the interests of people and the planet.</p>

	<p>We want justice for those disproportionately affected by the climate crisis:
	people in the global south, indigenous communities and communities of colour, women and younger generations.
	We want corporations to respect the planet and human rights, governments to protect and listen to their citizens,
	and the online world to be free from misinformation and hate.</p>
	HTML;



$div_wikipedia_global_Witness = new WikipediaContentSection();
$div_wikipedia_global_Witness->setTitleText('Global Witness');
$div_wikipedia_global_Witness->setTitleLink('https://en.wikipedia.org/wiki/Global_Witness');
$div_wikipedia_global_Witness->content = <<<HTML
	<p>Global Witness is an international NGO established in 1993 that works to break the links
	between natural resource exploitation, conflict, $poverty, corruption, and human rights abuses worldwide.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body('environmental_defenders.html');
$page->body($div_Challenging_abuses_of_power_to_protect_human_rights_and_secure_the_future_of);
$page->body($div_wikipedia_global_Witness);
