<?php
$page = new Page();
$page->h1('Political discourse');
$page->stars(1);
$page->keywords('political discourse');

$page->snp('description', "A healthy political discourse is an important part of democratic life.");
//$page->snp('image', "/copyrighted/");

$page->preview( <<<HTML
	<p>A healthy political discourse is an important part of democratic life.</p>
	HTML );




$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Political discourse is an important part of $democratic life.
	There is a profoundly noble aspect to political discourse and dialogue in general.
	A healthy exchange of ideas is necessary in order to define the rules of our public life,
	and to develop strategies and policies for the society to progress.</p>

	<p>There is also a dark side to political discourse, a side that anyone can unfortunately easily observe today.
	This dark side has many facets: divisive discourse, fake news, disinformation, logical fallacies, hate speech, etc.</p>

	<p>Most people have more common ground than they think they do, but buy into narratives that polarize,
	and feed biases instead of understanding the full scope of the topic being discussed.</p>

	<p>As a starting point of any discussion on politics, people are invited to get the facts straight:
	people's opinion is a matter of freedom of conscience, but the facts have to be straight.</p>

	<p><strong>Note</strong>:
	<em>The purpose of this page is not to fully cover the topic of political discourse,
	but to provide an outline of this wide-ranging topic,
	with links to various articles where we can explore each aspect in more depth.</em></p>
	HTML;


$list = new ListOfPages();
$list->add('sarcasm.html');
$print_list = $list->print();

$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>Related content</h3>

	$print_list
	HTML;



$div_codeberg_Political_discourse_the_good_and_the_bad = new CodebergContentSection();
$div_codeberg_Political_discourse_the_good_and_the_bad->setTitleText('Political discourse: the good and the bad');
$div_codeberg_Political_discourse_the_good_and_the_bad->setTitleLink('https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/38');
$div_codeberg_Political_discourse_the_good_and_the_bad->content = <<<HTML
	<p>An entry page to cover all aspects, the good and the bad, of how we discuss politics today.</p>
	HTML;



$page->parent('politics.html');
$page->body($div_stub);

$page->body($div_introduction);
$page->body($div_list);

$page->body($div_codeberg_Political_discourse_the_good_and_the_bad);
