<?php
$page = new Page();
$page->h1('Sarcasm');
$page->keywords('Sarcasm', 'sarcasm');
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Sarcasm is contrary to right speech.
	Sarcasm is an unskillful and unwholesome method of humor,
	which always fails to solve any problem or improve any situation that it is mocking.</p>
	HTML;

$div_wikipedia_Sarcasm = new WikipediaContentSection();
$div_wikipedia_Sarcasm->setTitleText('Sarcasm');
$div_wikipedia_Sarcasm->setTitleLink('https://en.wikipedia.org/wiki/Sarcasm');
$div_wikipedia_Sarcasm->content = <<<HTML
	<p>Sarcasm is the caustic use of words, often in a humorous way, to mock someone or something.
	Sarcasm may employ ambivalence, although it is not necessarily ironic.
	Most noticeable in spoken word, sarcasm is mainly distinguished by the inflection with which it is spoken or,
	with an undercurrent of irony, by the extreme disproportion of the comment to the situation,
	and is largely context-dependent.</p>
	HTML;


$div_wikipedia_Poe_s_law = new WikipediaContentSection();
$div_wikipedia_Poe_s_law->setTitleText('Poe\'s law');
$div_wikipedia_Poe_s_law->setTitleLink('https://en.wikipedia.org/wiki/Poe%27s_law');
$div_wikipedia_Poe_s_law->content = <<<HTML
	<p>Poe's law is an adage of internet culture saying that, without a clear indicator of the author's intent,
	any parodic or sarcastic expression of extreme views can be mistaken by some readers for a sincere expression of those views.
	The law is frequently exploited by individuals who share genuine extremist views and,
	when faced with overwhelming criticism, deflect by insisting they were merely being satirical.</p>
	HTML;



$page->parent('political_discourse.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_wikipedia_Sarcasm);
$page->body($div_wikipedia_Poe_s_law);
