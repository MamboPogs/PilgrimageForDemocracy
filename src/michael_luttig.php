<?php
$page = new Page();
$page->h1('J. Michael Luttig');
$page->keywords('Michael Luttig');
$page->stars(0);

$page->snp('description', 'American judge and corporate lawyer.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>J. Michael Luttig is an American retired conservative judge.
	He worked both in Ronald Reagan and George H. W. Bush governments.</p>

	<p>Luttig said that "Donald Trump and his allies and supporters are a clear and present danger to American $democracy."</p>

	<p>Together with liberal legal scholar ${'Laurence Tribe'}, he argued that Donald Trump is illegible to be
	candidate in the 2024 presidential election due to the 14th amendment to the US $constitution.</p>
	HTML;

$div_wikipedia_J_Michael_Luttig = new WikipediaContentSection();
$div_wikipedia_J_Michael_Luttig->setTitleText('J Michael Luttig');
$div_wikipedia_J_Michael_Luttig->setTitleLink('https://en.wikipedia.org/wiki/J._Michael_Luttig');
$div_wikipedia_J_Michael_Luttig->content = <<<HTML
	<p>John Michael Luttig (born June 13, 1954) is an American corporate lawyer and jurist
	who was a judge of the United States Court of Appeals for the Fourth Circuit from 1991 to 2006.
	Luttig resigned from the court of appeals in 2006 to become general counsel of Boeing, a position he held until 2019.</p>
	HTML;


$page->parent('list_of_people.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_wikipedia_J_Michael_Luttig);
