<?php
$page = new Page();
$page->h1('Professionalism without Elitism');
$page->stars(1);
$page->keywords('elitism');

$page->preview( <<<HTML
	<p>How to promote the idea of professionalism in politics, without falling into the trappings of elitism?</p>
	HTML );



//$page->snp('description', "");
//$page->snp('image', "/copyrighted/");

$r1 = $page->ref('https://thebookerprizes.com/the-booker-library/features/how-the-remains-of-the-day-changed-the-way-i-think-about-england', 'How The Remains of the Day changed the way I think about England');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Politics is, or should be, a noble art.
	Lawmaking is, or should be, an honourable profession.</p>

	<p>The aim of this page is to present ways to promote the idea of professionalism in politics,
	without falling into the trappings of elitism.
	It is also to point out the negative aspects of elitism without falling into populism.</p>

	<p>Unfortunately, we see many populist candidates elected into office,
	who clearly do not understand simple notions like the rule of law, the separation of powers,
	and who apparently do not desire to take a constructive or professional approach towards their legislative or executive office.</p>

	<p>Similarly, we saw in many democratic countries strong personalities with a brazen attitude get elected into the highest office.
	We shall keep in mind that greatness does not equate to goodness.</p>

	<p>We need to restore respectability into politics, and confidence of the electorate in their elected officials.</p>

	<blockquote>
	"The voice of Stevens, the narrator of Kazuo Ishiguro’s 1989 Booker Prize-winning novel The Remains of the Day,
	is so authentic and beautifully-sustained that you could believe it had always existed.
	(...)
	Re-reading the novel in 2022 also illuminated Ishiguro’s technical achievement
	– Stevens’ voice never wavers and the narrative is structurally seamless – and its enduring relevance.
	When I read it in 2002, the hard-won progress of the post-war era, which created a more equal Britain, was being consolidated.
	Subsequent events have undermined that confidence and sown uncertainty.
	Brexit has been compared to the Suez Crisis, which happened in the year in which The Remains of the Day is set.
	At the same time, <strong>scenes involving Lord Darlington and his Nazi-sympathising allies,
	who think ‘the world’s far too complicated a place for universal suffrage and such like’,
	show that when we lose faith in democracy we enter dangerous territory</strong>."<br>
	— Max Liu, on the novel "The Remains of the Day".${r1}
	</blockquote>
	HTML;


$div_wikipedia_Populism = new WikipediaContentSection();
$div_wikipedia_Populism->setTitleText('Populism');
$div_wikipedia_Populism->setTitleLink('https://en.wikipedia.org/wiki/Populism');
$div_wikipedia_Populism->content = <<<HTML
	<p>Populism refers to a range of political stances that emphasize the idea of "the people"
	and often juxtapose this group against "the elite".
	It is frequently associated with anti-establishment and anti-political sentiment.
	The term developed in the late 19th century and has been applied to various politicians,
	parties and movements since that time, often as a pejorative.
	Within political science and other social sciences, several different definitions of populism have been employed,
	with some scholars proposing that the term be rejected altogether.</p>
	HTML;



$div_wikipedia_Elitism = new WikipediaContentSection();
$div_wikipedia_Elitism->setTitleText('Elitism');
$div_wikipedia_Elitism->setTitleLink('https://en.wikipedia.org/wiki/Elitism');
$div_wikipedia_Elitism->content = <<<HTML
	<p>Elitism is the belief or notion that individuals who form an elite—a select group of people
	perceived as having an intrinsic quality, high intellect, wealth, power, notability, special skills,
	or experience—are more likely to be constructive to society as a whole,
	and therefore deserve influence or authority greater than that of others.
	The term elitism may be used to describe a situation in which power is concentrated in the hands of a limited number of people.
	Beliefs that are in opposition to elitism include egalitarianism, anti-intellectualism, populism, and the political theory of pluralism.</p>
	HTML;


$page->parent('institutions.html');

$page->body($div_stub);

$page->body($div_introduction);
$page->body('the_remains_of_the_day.html');

$page->body($div_wikipedia_Elitism);
$page->body($div_wikipedia_Populism);
