<?php
$page = new Page();
$page->h1('Economic Community of West African States (ECOWAS)');
$page->keywords('ECOWAS');
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_ECOWAS = new WikipediaContentSection();
$div_wikipedia_ECOWAS->setTitleText('ECOWAS');
$div_wikipedia_ECOWAS->setTitleLink('https://en.wikipedia.org/wiki/ECOWAS');
$div_wikipedia_ECOWAS->content = <<<HTML
	<p>The Economic Community of West African States is
	a regional political and economic union of fifteen countries located in West Africa.
	Collectively, these countries comprise an area of 5,114,162 km2,
	and in 2019 had an estimated population of over 387 million.</p>
	HTML;


$page->parent('world.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_wikipedia_ECOWAS);
