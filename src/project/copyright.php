<?php
$page = new Page();
$page->h1('Copyright and licenses');
$page->stars(3);
$page->keywords('copyright', 'license');

$page->preview( <<<HTML
	<p>The best ideas for a mature democracy and social justice do not belong to any single person or organization.
	They belong to all of humanity.</p>
	<p>Most of the content on this website is placed in the public domain.
	You can share, copy, duplicate, republish, modify all of the most important articles,
	and add your best ideas, research, and policy proposals.</p>
	<p>There are, however, some minor exceptions for third-party material and code.
	You can check this page for details.</p>
	HTML );

$h2_introduction = new h2HeaderContent('Introduction');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The best ideas for democracy and social justice do not belong to any single person or organization.
	They belong to all of humanity.</p>

	<p>Most of the content on this website is open source and placed in the public domain.
	You can share, copy, duplicate, republish, modify all of the most important articles,
	and add your best ideas, research and policy proposals.</p>

	<p>There are, however, some exceptions for third-party material and code.
	You can check further down this page for details.</p>

	<p>You will find very little or no original ideas in this project.
	Many good ideas have been thought, spoken, written,
	published in books, articles or scholarly research,
	shared in documentaries, movies and videos, and
	shared online on numerous websites and social networks.
	</p>

	<p>Many of these good ideas would be beneficial to society and to humanity... if only they were practiced and applied.
	</p>

	<p>We believe it to be a crime against humanity for these ideas to be hidden behind paywalls, made difficultly accessible,
	buried in arcane books, forgotten, etc.</p>

	<p>It is a crime that known good policies are not being put into practice.</p>

	<p>Therefore, the Pilgrimage for Democracy and Social Justice aims to gather the best ideas, organize them, and make them accessible.
	Important secondary goals are to educate the public and take collective action to improve our nations' institutions, laws and practices.
	</p>

	<p>We believe in the value of collective intelligence and of the power of teamwork and collective action.</p>
	HTML;

$h2_open_source = new h2HeaderContent('Open source');

$div_open_source = new ContentSection();
$div_open_source->content = <<<HTML
	<p>This whole project is open source.
	The code and text of this website is published within the
	<a href="https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy">PilgrimageForDemocracy project</a>
	hosted at codeberg.org, which itself is an open-source project.
	</p>

	<p>The most efficient way to contribute to this project is to register at <a href="https://codeberg.org/">codeberg.org</a>,
	follow the PilgrimageForDemocracy project, comment on issues and, contribute via pull requests, if you know how.
	</p>
	HTML;

$h2_public_domain = new h2HeaderContent('Public domain');

$div_public_domain = new ContentSection();
$div_public_domain->content = <<<HTML
	<p>We do not claim ownership of the ideas presented in this project, nor of the text and code used to present them.
	<strong>To the exceptions noted below</strong>, we release both the text and the code into the public domain.
	</p>

	<p>Releasing the whole project into the public domain is a guarantee, a challenge, and an exercise of democracy in action.</p>

	<p>It is a <strong>guarantee</strong> because
	should the benevolent gatekeepers in charge of the Pilgrimage for Democracy disappear, die, become malevolent or simply lose interest,
	then anybody will have both the rights and the means to pick up the torch and continue the project elsewhere.
	</p>

	<p>It is a <strong>challenge</strong> to us all:
	can we build a community around this project and cooperate for the public good?
	Will we be able to rise above our differences and disagreements?
	Or will discord and division prevail?
	</p>

	<p style="margin: 0;">It is an exercise of <strong>democracy in action</strong>:</p>
	<ul style="margin: 0;">
		<li>whether benevolent or malevolent,</li>
		<li>whether with good intentions or with bad intentions,</li>
		<li>whether for the common good or for personal profit,</li>
		<li>whether enlightened or misguided,</li>
	</ul>
	<p>anybody has the same ability to copy the source and create a new website and a new community elsewhere.
	Which side of each of us shall prevail?
	</p>

	<p>In any case, together or separately, we <strong>will</strong> create something that resemble us,
	something that will reveal our character and the true nature of our souls.
	</p>
	HTML;

$h2_multimedia = new h2HeaderContent('Multimedia');

$div_multimedia = new ContentSection();
$div_multimedia->content = <<<HTML
	<p>We may use some multimedia material (photographs, illustrations, artwork, videos)
	which are copyrighted and which come under their own licenses.
	Check the copyright and licensing mentioned for each individual item.
	</p>
	HTML;

$h2_libraries = new h2HeaderContent('3rd party code libraries');

$div_jquery = new ContentSection();
$div_jquery->content = <<<HTML
	<h3>jQuery</h3>

	<p>For client-side scripting, we use jQuery, which is released under the <a href="https://github.com/jquery/jquery/blob/main/LICENSE.txt">MIT license</a>.</p>

	<ul>
	<li><a href="https://jquery.com/">jquery.com</a>, official website.</li>
	<li><a href="https://github.com/jquery/jquery">jQuery at github</a>, code repository.</li>
	</ul>
	HTML;

$div_fomantic = new ContentSection();
$div_fomantic->content = <<<HTML
	<h3>Fomantic-UI</h3>

	<p>For theming, we use Formantic-UI, which is released under the <a href="https://github.com/fomantic/Fomantic-UI/blob/develop/LICENSE.md">MIT license</a>.</p>

	<ul>
	<li><a href="https://fomantic-ui.com/">fomantic-ui.com</a>, official website.</li>
	<li><a href="https://github.com/fomantic/Fomantic-UI">formantic-UI at github</a>, code repository.</li>
	</ul>
	HTML;

$page->parent('project/index.html');
$page->body($h2_introduction);
$page->body($div_introduction);
$page->body($h2_open_source);
$page->body($div_open_source);
$page->body($h2_public_domain);
$page->body($div_public_domain);
$page->body($h2_multimedia);
$page->body($div_multimedia);
$page->body($h2_libraries);
$page->body($div_jquery);
$page->body($div_fomantic);
