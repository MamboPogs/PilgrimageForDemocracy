<?php
$page = new Page();
$page->h1('Git workflow');
$page->stars(1);
$page->keywords('Git');

$page->preview( <<<HTML
	<p>Using `git` to contribute to the Pilgrimage for Democracy project.</p>
	HTML );

$page->snp('description', "Using `git` to contribute to the Pilgrimage for Democracy project.");
$page->snp('image', "/copyrighted/git-logo.svg.1200-630.png");



$div_codeberg = new CodebergContentSection();
$div_codeberg->setTitleText('Get help at Codeberg');
$div_codeberg->setTitleLink('https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/');
$div_codeberg->content = <<<HTML
	<p>If the instructions in this page are unclear or incomplete,
	open a new issue in our issue tracker at Codeberg, and we shall assist you.</p>
	HTML;


$h2_Warning_copyright = new h2HeaderContent('Warning: copyright');

$div_public_domain = new ContentSection();
$div_public_domain->content = <<<HTML
	<p>Before submitting a <strong>pull request</strong> you must be aware of the following conditions and agree with them:</p>

	<ol>
	<li>Do not submit third-party copyrighted material.</li>
	<li>Unless agreed otherwise, the content of your pull request will enter the public domain upon being committed into the main repository.</li>
	<li>Read and agree with the content of the <a href="https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/src/branch/master/LICENSE.md">LICENSE.md</a> file.</li>
	<li>Read and agree with the $copyright page.</li>
	</ul>
	HTML;


$h2_git = new h2HeaderContent('Git');

$div_git = new ContentSection();
$div_git->content = <<<HTML
	<h3>Introduction</h3>

	<p>The purpose of this page is to help new contributors getting started with git.
	We shall introduce the main commands that are typically used during a regular workflow.</p>

	<h3>Official git resources</h3>

	<p>The official git documentation is clear and extensive.</p>

	<ul>
	<li><a href="https://git-scm.com/">Git homepage</a>, official website.</li>
	<li><a href="https://git-scm.com/doc">Git documentation</a>.</li>
	</ul>

	<p>The following sections of the official documentation may be particularly useful:</p>

	<ul>
	<li><a href="https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes">2.5 Git Basics - Working with Remotes</a></li>
	<li><a href="https://git-scm.com/book/en/v2/Git-Branching-Remote-Branches">3.5 Git Branching - Remote Branches</a></li>
	</ul>

	HTML;


$h2_git_workflow = new h2HeaderContent('Workflow');

$div_git_workflow = new ContentSection();
$div_git_workflow->content = <<<HTML
	<h3>Getting started</h3>

	<p>Create an account at Codeberg. In the examples below, we shall use the name "<em>Theo</em>" as the account name. Replace it with your own account name.</p>

	<ul>
		<li>Official Codeberg documentation: <a href="https://docs.codeberg.org/collaborating/pull-requests-and-git-flow/">Pull requests and Git flow</a>.</li>
	</ul>

	<p>Go to the <a href="https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy">official repository</a> and click the "Fork" icon in the top right corner.</p>

	<img src="/copyrighted/fork-button.png">

	<p>The following steps are to be performed in a console environment (either "<em>konsole</em>" or equivalent in Linux, or within the Windows console).
	The dollar sign "$" represents the console prompt (it could be the sign <strong>&gt;</strong> on some systems, e.g. Windows).</p>

	<p>Clone your own repository (the fork you just created). Use your own account name in place of "Theo":</p>

	<code>$ git clone https://codeberg.org/Theo/PilgrimageForDemocracy.git --origin theo</code>

	<p>Change to the code directory:</p>

	<code>$ cd PilgrimageForDemocracy</code>

	<p>Add the official repository:</p>

	<code>$ git remote add pilgrim https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy.git</code>

	<p>See that you have the remotes properly configured:</p>

	<code>$ git remote --verbose
	theo    https://codeberg.org/Theo/PilgrimageForDemocracy.git (fetch)
	theo    https://codeberg.org/Theo/PilgrimageForDemocracy.git (push)
	pilgrim https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy.git (fetch)
	pilgrim https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy.git (push)
	</code>

	<p>Fetch all the code from the official repository:</p>

	<code>$ git fetch pilgrim</code>

	<p>Check all existing branches. The default local branch should be 'master'. All available remote branches are listed as well:</p>

	<code>$ git branch --all
	* master
	remotes/theo/HEAD -> theo/master
	remotes/theo/master
	remotes/pilgrim/master
	</code>


	<p>Before editing, it is better to create a new branch for each piece of content you would like to contribute.
	Use a meaningful name for your branch, instead of the example given: '<em>my-contribution</em>'.</p>

	<code>$ git checkout -b my-contribution</code>

	<p>After having made the edits you wanted and committed them locally, you may push them to your public repository:</p>

	<code>$ git push --set-upstream theo my-contribution</code>

	<p>Remember to continuously rebase your repository with the upstream official repository.
	Do so daily or whenever you start working on the project.</p>

	<code>$ git pull --rebase pilgrim master
	</code>
	HTML;




$page->parent('project/index.html');
$page->body($div_codeberg);


$page->body($h2_git);
$page->body($div_git);


$page->body($h2_Warning_copyright);
$page->body($div_public_domain);
$page->body('project/copyright.html');


$page->body($h2_git_workflow);
$page->body($div_git_workflow);
