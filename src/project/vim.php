<?php
$page = new Page();
$page->h1('Vim');
$page->stars(0);
$page->keywords('Vim');

$page->snp('description', "Vim integration.");
//$page->snp('image', "/copyrighted/");

$page->preview( <<<HTML
	<p>For a most efficient development workflow, it is best to integrate the development scripts into your text editor.</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>For a most efficient development workflow, it is best to integrate the development scripts into your text editor.</p>

	<p>Add the following to your <strong>~/.vimrc</strong>, where <strong>add.h2</strong>, etc., are symlinks to their corresponding scripts:</p>
	<code>:noremap <F2> :r !add.h2
	:noremap <F3> :r !add.div
	:noremap <F4> :r !add.section
	</code>
	HTML;


$div_vim_website = new WebsiteContentSection();
$div_vim_website->setTitleText('vim website ');
$div_vim_website->setTitleLink('https://www.vim.org/');
$div_vim_website->content = <<<HTML
	<p>Vim website.</p>
	HTML;




$div_wikipedia_Vim = new WikipediaContentSection();
$div_wikipedia_Vim->setTitleText('Vim');
$div_wikipedia_Vim->setTitleLink('https://en.wikipedia.org/wiki/Vim');
$div_wikipedia_Vim->content = <<<HTML
	<p>Vim is a free and open-source, screen-based text editor program.
	Vim is designed for use both from a command-line interface and as a standalone application in a graphical user interface.</p>
	HTML;


$page->parent('project/index.html');

$page->body($div_introduction);
$page->body('project/development_scripts.html');
$page->body($div_vim_website);
$page->body($div_wikipedia_Vim);
