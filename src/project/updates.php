<?php
$page = new Page();
$page->h1('Updates');
$page->stars(-1);
$page->keywords('updates', 'updated');

$page->preview( <<<HTML
	<p>If you are already very familiar with the existing content of this website,
	this page makes it convenient to check all the newest updates and additions since your last visit.</p>
	HTML );

$h2_code = new h2HeaderContent('Code update');

$div_code = new ContentSection();
$div_code->content = <<<HTML
	<p>This website is open source.
	The whole source code is published within the <a href="https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy">Pilgrimage for Democracy</a> at Codeberg.
	If you are interested in following every minor change in the website,
	you can browse the project's code repository and <a href="https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/commits/branch/master">check every single commit</a>.
	</p>
	<p>See below for the most important content updates, with new articles, expanded articles and expanded sections.</p>
	HTML;

$h2_content = new h2HeaderContent('Content update');

$div_update = new ContentSection();
$div_update->content = <<<HTML
	<p>See below the list of <em>major</em> updates since your last visit:</p>
	<ul>
	HTML;

// function addUpdate(&$div_update, $date, $stars_from, $stars_to, $link, $title_en, $text_en = '')
//addUpdate($div_update, '2023-08-2', 0, 1, '/.html', '', '');
addUpdate($div_update, '2023-08-30', 0, 1, '/blasphemy.html', 'Blasphemy', '');
addUpdate($div_update, '2023-08-30', 0, 1, '/immigration.html', 'Immigration', '');
addUpdate($div_update, '2023-08-29', 0, 1, '/election_integrity.html', 'Election Integrity', '');
addUpdate($div_update, '2023-08-28', -1, -1, 'https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/commits/branch/master', '600 commits!', "Development of the $Pilgrimage continues every day! There are now 600 commits in the project's code repository, with a total of 145 articles and 200 files.");
addUpdate($div_update, '2023-08-27', -1, -1, '', '', 'All API 0 pages have now fully been upgraded to API 1!');
addUpdate($div_update, '2023-08-27', 0, 1, '/clark_cunningham.html', 'Clark D. Cunningham', '');
addUpdate($div_update, '2023-08-25', 0, 1, '/obstruction_of_justice.html', 'Obstruction of justice', '');
addUpdate($div_update, '2023-08-25', 0, 1, '/mo_gawdat.html', 'Mo Gawdat', '');
addUpdate($div_update, '2023-08-25', 0, 1, '/artificial_intelligence.html', 'Artificial intelligence', '');
addUpdate($div_update, '2023-08-24', -1, -1, '', '', 'Many new pages are added, almost daily: those are stub articles and placeholders and they are rarely mentioned in these update notices. We are laying the groundwork so that adding new content and analysis on each topic will be easy.');
addUpdate($div_update, '2023-08-24', -1, -1, '', '', 'Migrating all the old pages using API 0 to API 1 is almost completed. Out of about 100 original pages, only 14 remain to be migrated.');
addUpdate($div_update, '2023-08-24', 0, 1, '/social_justice.html', 'Social justice', '');
addUpdate($div_update, '2023-08-14', 1, 2, '/chinese_expansionism.html', 'Chinese expansionism', '');
addUpdate($div_update, '2023-08-14', -1, -1, 'https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/commits/branch/master', '500 commits!', "Development of the $Pilgrimage continues every day! There are now 500 commits in the project's code repository, with a total of 126 articles and 195 files.");
addUpdate($div_update, '2023-08-13', 0, 1, '/global_witness.html', 'Global Witness', '');
addUpdate($div_update, '2023-08-12', 0, 1, '/environmental_defenders.html', 'Environmental defenders', '');
addUpdate($div_update, '2023-08-12', 0, 0, '/colombia.html', 'Colombia', '');
addUpdate($div_update, '2023-08-12', 0, 0, '/saudi_arabia.html', 'Saudi Arabia', '');
addUpdate($div_update, '2023-08-11', 0, 0, '/ecuador.html', 'Ecuador', '');
addUpdate($div_update, '2023-08-06', 2, 2, '/transnational_authoritarianism.html', 'Transnational authoritarianism', '');
addUpdate($div_update, '2023-08-06', 0, 0, '/hong_kong.html', 'Hong Kong', '');
addUpdate($div_update, '2023-08-06', 0, 1, '/prc_china.html', "The People's Republic of China", '');
addUpdate($div_update, '2023-08-03', 0, 0, '/haiti.html', 'Haiti', '');
addUpdate($div_update, '2023-08-02', 0, 1, '/property.html', 'Property', '');
addUpdate($div_update, '2023-07-31', 1, 1, '/project/standing_on_the_shoulders_of_giants.html', 'Standing on the shoulders of giants', "Added the ${'World Future Council'} to the list of $giants.");
addUpdate($div_update, '2023-07-31', -1, -1, 'https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/commits/branch/master', '400 commits!', "Development of the $Pilgrimage continues <strong>daily</strong>! There are now 400 commits in the project's code repository, and a total of 175 files.");
addUpdate($div_update, '2023-07-28', -1, -1, '', '', <<<HTML
	'The project development has been ongoing on a daily basis, although the result is not immediately apparent within the website.
	The early architecture of the source code was not scalable, so a lot of development time has been spent on creating the API version 1,
	which is better suited to the nature of this project.
	The API 1 is now operational and almost stable.
	Each existing page must individually be upgraded from API 0 to API 1.
	A specially crafted upgrade script helps with the process. There are over 80 pages left to upgrade.
	Once the whole website has been migrated to API 1, more time will be spent on developing the actual content of the website.
	HTML);
addUpdate($div_update, '2023-07-24', 0, 2, '/world_future_council.html', 'World Future Council', '');
addUpdate($div_update, '2023-07-22', 0, 2, '/lists.html', 'Lists', '');
addUpdate($div_update, '2023-07-15', 0, 1, '/technology_and_democracy.html', 'Technology and democracy', '');
addUpdate($div_update, '2023-07-12', 0, 1, '/data_activism.html', 'Data activism', '');
addUpdate($div_update, '2023-07-11', 0, 1, '/data_pop_alliance.html', 'Data-Pop Alliance', '');
addUpdate($div_update, '2023-07-10', -1, -1, 'https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/commits/branch/master', '300 commits!', 'There are 300 commits in the project\'s code repository, 186 files having changed.');
addUpdate($div_update, '2023-07-10', 0, 2, '/richard_bluhm.html', 'Richard Bluhm', '');
addUpdate($div_update, '2023-07-06', -1, -1, '/lists.html', 'Lists and indices.', 'We are building up a list of resources, organizations, people, reports, etc. At a later stage we shall be able to dig into these resources in order to extract valuable and productive information.');
addUpdate($div_update, '2023-07-04', 0, 2, '/decentralized_autonomous_organization.html', 'Decentralized Autonomous Organization (DAO)', '');
addUpdate($div_update, '2023-07-03', 0, 1, '/taiwan.html', 'Taiwan', '');
addUpdate($div_update, '2023-07-02', 0, 1, '/political_discourse.html', 'Political discourse', '');
addUpdate($div_update, '2023-07-02', 0, 1, '/denise_dresser.html', 'Denise Dresser', '');
addUpdate($div_update, '2023-06-30', 0, 3, '/the_remains_of_the_day.html', 'The Remains of the Day', 'Novel and film');
addUpdate($div_update, '2023-06-29', 0, 1, '/maria_ressa.html', 'Maria Ressa', 'Journalist from the Philippines and Nobel Peace Prize laureate.');
addUpdate($div_update, '2023-06-29', 0, 2, '/project/codeberg.html', 'Codeberg', 'Participate');
addUpdate($div_update, '2023-06-28', 0, 1, '/ground_news.html', 'Ground News', '');
addUpdate($div_update, '2023-06-28', 1, 2, '/information_overload.html', 'Information overload', '');
addUpdate($div_update, '2023-06-27', 0, 1, '/information_overload.html', 'Information overload', '');
addUpdate($div_update, '2023-06-26', 0, 1, '/karoline_wiesner.html', 'Karoline Wiesner', '');
addUpdate($div_update, '2023-06-26', 0, 1, '/staffan_lindberg.html', 'Staffan Lindberg', '');
addUpdate($div_update, '2023-06-26', 0, 1, '/v_dem_institute.html', 'V-Dem Institute', '');
addUpdate($div_update, '2023-06-23', -1, -1, 'https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/commits/branch/master', '200 commits!', 'There are 200 commits in the project\'s code repository, 137 files having changed.');
addUpdate($div_update, '2023-06-23', 0, 1, '/constitution.html', 'Constitution', '');
addUpdate($div_update, '2023-06-20', 0, 1, '/mali.html', 'Mali', '');
addUpdate($div_update, '2023-06-19', 0, 2, '/transnational_authoritarianism.html', 'Transnational authoritarianism', '');
addUpdate($div_update, '2023-06-17', 1, 2, '/freedom_house.html', 'Freedom House', 'Policy recommendations and research methodology.');
addUpdate($div_update, '2023-06-15', 0, 1, '/philippines.html', 'Philippines', '');
addUpdate($div_update, '2023-06-14', 1, 1, '/world.html', 'Country profiles', 'Added Freedom House Internet freedom profile and index to each country.');
addUpdate($div_update, '2023-06-14', 0, 1, '/professionalism_without_elitism.html', 'Professionalism without Elitism', '');
addUpdate($div_update, '2023-06-12', 0, 0, '/lawmaking.html', 'Lawmaking', 'New stub');
addUpdate($div_update, '2023-06-11', 1, 1, '/world.html', 'Country profiles', 'Added Freedom House Index to each country.');
addUpdate($div_update, '2023-06-10', 0, 1, '/world.html', 'Country profiles', 'Added Freedom House country profiles to each country.');
addUpdate($div_update, '2023-06-08', 0, 1, '/beliefs.html', 'Beliefs', '');
addUpdate($div_update, '2023-06-07', 0, 1, '/freedom_house.html', 'Freedom House', '');
addUpdate($div_update, '2023-06-06', 3, 4, '/secondary_features_of_democracy.html', 'Secondary features of democracy — We, the Society', 'minor update');
addUpdate($div_update, '2023-06-05', 0, 1, '/project/standing_on_the_shoulders_of_giants.html', 'Standing on the shoulders of giants', '');
addUpdate($div_update, '2023-06-04', 1, 2, '/project/wikipedia.html', 'Wikipedia', 'Similarities and differences.');
addUpdate($div_update, '2023-06-02', 0, 2, '/ukraine.html', 'Ukraine', '');
addUpdate($div_update, '2023-06-01', 0, 1, '/duverger_syndrome.html', 'Duverger syndrome', 'Section outline');
addUpdate($div_update, '2023-05-31', 0, 1, '/project/wikipedia.html', 'Wikipedia', 'Similarities and differences.');
addUpdate($div_update, '2023-05-30', 0, 1, '/peaceful_resistance_in_times_of_war.html', 'Peaceful resistance in times of war', '');
addUpdate($div_update, '2023-05-29', 0, 1, '/global_issues.html', 'Global issues', '');
addUpdate($div_update, '2023-05-29', 0, 1, '/hunger.html', 'Hunger', '');
addUpdate($div_update, '2023-05-24', 0, 2, '/living_together.html', 'Living Together', '');
addUpdate($div_update, '2023-05-23', 1, 3, '/democracy_wip.html', 'Democracy — a work in progress', '');
addUpdate($div_update, '2023-05-22', 0, 1, '/democracy_wip.html', 'Democracy — a work in progress', '');
addUpdate($div_update, '2023-05-12', 0, 1, '/costa_rica.html', 'Costa Rica', '');
addUpdate($div_update, '2023-05-11', 0, 3, '/secondary_features_of_democracy.html', 'Secondary features of democracy — We, the Society', '');
addUpdate($div_update, '2023-04-15', 0, 1, '/reporters_without_borders.html', 'Reporters Without Borders', '');
addUpdate($div_update, '2023-04-12', 0, 1, '/media.html', 'Media', '');
addUpdate($div_update, '2023-04-12', 0, 2, '/foreign_influence_local_media.html', 'Foreign influence in local media landscape', '');
addUpdate($div_update, '2023-04-11', 0, 1, '/russia.html', 'Russia', '');
addUpdate($div_update, '2023-04-08', 0, 1, '/project/development_website.html', 'Development website', 'How to set up a copy of this website on your computer.');
addUpdate($div_update, '2023-04-07', 0, 1, '/project/git.html', 'Git', 'Using git to contribute to the project.');
addUpdate($div_update, '2023-03-13', 0, 1, '/georgia.html', 'Georgia', '');
addUpdate($div_update, '2023-01-28', 0, 0, '/osce.html', 'OSCE (Organization for Security and Co-operation in Europe)', '');
addUpdate($div_update, '2023-01-27', 0, 2, '/united_nations.html', 'The United Nations', '');
addUpdate($div_update, '2023-01-22', 0, 1, '/iran.html', 'Iran', 'Start with resources from wikipedia.');
addUpdate($div_update, '2022-12-26', 0, 1, '/chinese_expansionism.html', 'Chinese expansionism', '');
addUpdate($div_update, '2022-12-12', 1, 3, '/project/participate.html', 'Participate', 'Content');
addUpdate($div_update, '2022-12-08', 1, 3, '/primary_features_of_democracy.html', 'Primary features of democracy', '');
addUpdate($div_update, '2022-11-26', 0, 0, '/justice.html', 'Justice', '');
addUpdate($div_update, '2022-11-25', 0, 0, '/media.html', 'Media', '');
addUpdate($div_update, '2022-11-25', 0, 1, '/corruption.html', 'Corruption', '');
addUpdate($div_update, '2022-11-23', 0, 1, '/fair_share.html', 'Fair share', '');
addUpdate($div_update, '2022-11-20', 0, 1, '/integrity.html', 'Election integrity', '');
addUpdate($div_update, '2022-11-14', 0, 1, '/primary_features_of_democracy.html', 'Primary features of democracy', '');
addUpdate($div_update, '2022-11-13', 0, 1, '/project/participate.html', 'Participate', '');
addUpdate($div_update, '2022-11-12', 0, 3, '/project/copyright.html', 'Copyright?', '');
addUpdate($div_update, '2022-11-11', 0, 1, '/project/updates.html', 'Updates');
addUpdate($div_update, '2022-11-09', 0, 1, '/index.html', 'Front page');
addUpdate($div_update, '2022-11-05', -1, -1, '', '', 'Beginning of the project. Resgistration of the domain pildem.org.');

$div_update->content .= "</ul>";




$page->parent('project/index.html');
$page->body($h2_code);
$page->body($div_code);

$page->body($h2_content);
$page->body($div_stars);
$page->body($div_update);

