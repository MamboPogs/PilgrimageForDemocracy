<?php
$page = new Page();
$page->h1('Standing on the shoulders of giants');
$page->stars(1);
$page->keywords('giants', 'giant');

$page->preview( <<<HTML
	<p>Our nascent project is still tiny, but will achieve its goals by building up on the work of very large organizations.</p>
	HTML );

//$page->snp('description', "");
//$page->snp('image', "/copyrighted/");

$r1 = $page->ref('https://www.worldfuturecouncil.org/team-management-and-supervisory-board/', 'World Future Council: team, Management Board and Supervisory Board');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Our nascent project is still tiny, and it will achieve its goals by building up on the work of very large organizations.</p>
	HTML;

$div_Connecting_the_dots_and_filling_the_gaps = new ContentSection();
$div_Connecting_the_dots_and_filling_the_gaps->content = <<<HTML
	<h3>Connecting the dots and filling the gaps</h3>

	<p>We would be wasting our time if we were merely trying to replicate the excellent work already done
	by organizations of all kinds and which have much more financial and human resources than we do.</p>

	<p>Yet, we notice many organizations which have identical aims,
	and which work separately from each other, in some cases even in competition with one another.
	We wish to acknowledge the immense contributions made by those organizations.
	Everywhere appropriate we shall refer their work to our readers.</p>

	<p>We are connecting the dots,
	highlighting similarities in action and in purpose between organizations, between our work and theirs.</p>

	<p>At the same time, we notice some lacking perspectives and missing elements.
	Our work aims to fill the gaps.</p>
	HTML;


$h2_Giants = new h2HeaderContent('Giants');

$div_giants = new ContentSection();
$div_giants->content = <<<HTML
	<p>The following list only present the main organizations that we rely on.
	A full list of people and organizations on whose work we build our own could never be compiled.</p>

	<p>The list below is presented <strong>in alphabetical order</strong>.</p>
	HTML;



$div_Freedom_House = new ContentSection();
$div_Freedom_House->content = <<<HTML
	<h3>Freedom House</h3>

	<p>With its approx. 150 staff and it USD 50 million budget, ${'Freedom House'} truly is a giant.
	Democracy being the core concern of both Freedom House and the $Pilgrimage,
	and the notion of "<em>freedom</em>" itself being a <a href="/primary_features_of_democracy.html">core feature of democracy</a>,
	it is only natural for us to integrate as much as possible the work, data and information from Freedom House.</p>

	<p>Within each country in the $world that we cover, we already include the Freedom Index, the Internet Freedom Index
	as well as the country profile from Freedom House. We also take note of their policy recommendations,
	like for example on the topic of ${'transnational authoritarianism'}.</p>
	HTML;



$div_Wikipedia = new ContentSection();
$div_Wikipedia->content = <<<HTML
	<h3>Wikipedia</h3>

	<p>The size and the human resources of the Wikipedia project allow it to cover almost any topic in much more depth than we could ever wish to accomplish.</p>

	<p>Many of our articles prominently link to relevant Wikipedia articles, so that it saves us the time to replicate the same work.
	This way, we can focus on our own analysis, editorializing and original research,
	all of which is forbidden to do on Wikipedia.
	See the article below for further discussion on the similarities and differences between Wikipedia and our project.</p>
	HTML;

$div_World_Future_Council = new ContentSection();
$div_World_Future_Council->content = <<<HTML
	<h3>World Future Council</h3>

	<p>In many ways the ${'World Future Council'}, with its 27 members of staff $r1, is already doing what we aspire to do,
	which is to compile a list of the best policies adopted by governments across the world.
	There is a definite overlap between the policy areas covered by the WFC and those of the $Pilgrimage aims to cover.
	Thus, our website integrates resources on public policy from the World Future Council,
	as can be seen for example on the articles about ${'Costa Rica'}, the $Philippines or about $education.</p>
	HTML;



$div_wikipedia_Standing_on_the_shoulders_of_giants = new WikipediaContentSection();
$div_wikipedia_Standing_on_the_shoulders_of_giants->setTitleText('Standing on the shoulders of giants');
$div_wikipedia_Standing_on_the_shoulders_of_giants->setTitleLink('https://en.wikipedia.org/wiki/Standing_on_the_shoulders_of_giants');
$div_wikipedia_Standing_on_the_shoulders_of_giants->content = <<<HTML
	<p>The phrase "standing on the shoulders of giants" is a metaphor which means
	"using the understanding gained by major thinkers who have gone before in order to make intellectual progress".
	It is a metaphor of dwarfs standing on the shoulders of giants and expresses the meaning of "discovering truth by building on previous discoveries".
	Famously, Issac Newton wrote in a 1675 letter: "<em>if I have seen further [than others], it is by standing on the shoulders of giants.</em>"</p>
	HTML;


$page->parent('project/index.html');

$page->body($div_introduction);
$page->body($div_Connecting_the_dots_and_filling_the_gaps);

$page->body($h2_Giants);
$page->body($div_giants);

$page->body($div_Freedom_House);
$page->body('freedom_house.html');


$page->body($div_Wikipedia);
$page->body('project/wikipedia.html');

$page->body($div_World_Future_Council);
$page->body('world_future_council.html');


$page->body($div_wikipedia_Standing_on_the_shoulders_of_giants);
