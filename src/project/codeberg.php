<?php
$page = new Page();
$page->h1('Codeberg');
$page->stars(2);
$page->keywords('Codeberg');

$page->preview( <<<HTML
	<p>How to use Codeberg to participate in the project.</p>
	HTML );


$page->snp('description', "Contribute via Codeberg.");
//$page->snp('image', "/copyrighted/");


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Here is how you can participate in the $Pilgrimage project.</p>
	HTML;


$h2_Comment = new h2HeaderContent('0- Comment');

$div_comment = new ContentSection();
$div_comment->content = <<<HTML
	<p>The easiest way to contribute does not require any technical knowledge.</p>

	<p>First, <a href="https://codeberg.org/user/sing_up">create a user account at Codeberg</a>,
	then you can easily participate by commenting on <a href="https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues">open issues</a>.
	Be mindful, though: this is not a discussion forum. Comments should be on topic and help move the discussion along.
	You can create new issues to report topics or information that are relevant to the project.</p>
	HTML;


$h2_git = new h2HeaderContent('1- Contribute with git');

$div_git = new ContentSection();
$div_git->content = <<<HTML
	<p>If you are in a mind of contributing more directly, you may use $git to submit patches and pull requests.
	If you already know how to use git, it should be easy.
	If not but you are eager to learn, check the page below for your first steps.
	Let us know if you encounter any difficulties.</p>
	HTML;


$h2_Development_website = new h2HeaderContent('2- Set up a development website');

$div_devel_website = new ContentSection();
$div_devel_website->content = <<<HTML
	<p>You can contribute using $git without having set up a development website on your computer.
	But should you wish to become even more involved, and test your contributions in real time,
	you must install a web server and PHP on your own computer.
	See the guide below.</p>
	HTML;


$h2_code = new h2HeaderContent('3- Code');

$div_code = new ContentSection();
$div_code->content = <<<HTML
	<p>Lastly, having mastered the basic git work flow and having set up a development website on your own computer,
	you can code for the simple, custom CMS (Content Management System) used to power this website.
	Alternatively, you can help with CSS design and theming.
	Let us know in a Codeberg issue what you wish to do or if you have any questions.</p>
	HTML;



$page->parent('project/index.html');

$page->body($div_introduction);

$page->body($h2_Comment);
$page->body($div_comment);

$page->body($h2_git);
$page->body($div_git);
$page->body('project/git.html');

$page->body($h2_Development_website);
$page->body($div_devel_website);
$page->body('project/development_website.html');

$page->body($h2_code);
$page->body($div_code);
