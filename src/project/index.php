<?php
$page = new Page();
$page->h1('Project: Pilgrimage for Democracy and Social Justice');
$page->stars(3);
$page->keywords('project');

$page->preview( <<<HTML
	<p>All the pages introducing the different aspects of the project
	"<em>Pilgrimage for $Democracy and ${'Social justice'}</em>".</p>
	HTML );

$page->snp('description', "Description of the project, contributing tips, etc.");
$page->snp('image', "/copyrighted/marvin-meyer-SYTO3xs06fU.1200-630.jpg");


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The following pages introduce the <em>Pilgrimage for Democracy and Social Justice</em> project.</p>
	HTML;

$h2_Presentation = new h2HeaderContent('Presentation');

$h2_Community = new h2HeaderContent('Community');

$h2_Technical_aspects = new h2HeaderContent('Technical aspects');





$page->body($div_introduction);

$page->body($h2_Presentation);
$page->body('menu.html');
$page->body('project/updates.html');

$div_section_copyright = get_page('project/copyright.html');
$div_section_copyright->stars(-1);
$div_section_copyright->h1('Copyright?');
$div_section_copyright->dest('/project/copyright.html');
$page->body($div_section_copyright, array('format' => 'preview'));

$page->body($h2_Community);
$page->body('project/participate.html');
$page->body('project/standing_on_the_shoulders_of_giants.html');
$page->body('project/wikipedia.html');
$page->body('project/codeberg.html');

$page->body($h2_Technical_aspects);
$page->body('project/git.html');
$page->body('project/development_website.html');
$page->body('project/development_scripts.html');
$page->body('project/vim.html');
