<?php
$page = new Page();
$page->h1('Participate');
$page->stars(3);
$page->keywords('participate', 'contribute');

$page->preview( <<<HTML
	<p>Let's pool our resources, knowledge and skills.
	Teamwork and our collective intelligence will ensure the success of this project.</p>
	<p>Share all around you, all that which inspires you.</p>
	<p>Together, let's try to inspire people around us to sow the seeds of future peace.</p>
	HTML );

$page->snp('description', "How to contribute to the Pilgrimage for Democracy and Social Justice.");
$page->snp('image', "/copyrighted/john-schnobrich-2FPjlAyMQTA.1200-630.jpg");

$h2_community = new h2HeaderContent('Community');

$div_community = new ContentSection();
$div_community->content = <<<HTML
	<p>Something as important as democracy and freedom cannot be described as "mine" or "yours".
	They are "ours".</p>

	<p>Our fates are connected.
	We live on the planet, at the same historical time.
	Social justice is to make sure that every individual has the same opportunities
	to develop their full human and spiritual potential.</p>

	<p>A project as ambitious as this one can only be a collective project.
	By definition, democracy itself is a collective endeavour.</p>

	<p>However talented, gifted and intelligent some individuals might be,
	collective intelligence and the power of teamwork will always be greater.</p>

	<p>We are a small but growing team of dedicated individuals,
	determined to help humanity develop its fullest potential.</p>
	HTML;


$div_share = new ContentSection();
$div_share->content = <<<HTML
	<h3>Share</h3>

	<p>Share your best ideas with people around you.
	Share this website, and the articles therein that you liked the best.</p>
	HTML;


$h2_join = new h2HeaderContent('Join');

$div_join = new ContentSection();
$div_join->content = <<<HTML

	<p>With your participation, our community will be bigger, stronger, and able to achieve more.</p>

	<p>Most of the community interaction will be done within the project hosted at Codeberg.</p>

	<p>The most efficient way to contribute to this project is to register at <a href="https://codeberg.org/">codeberg.org</a>,
	follow the PilgrimageForDemocracy project, comment on issues and, if you know how, contribute via pull requests.</p>

	<p>Browse the <a href="https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues">list of open issues (tasks to do)</a> at Codeberg.
	create a new issue for each distinct topic.
	Let us know what needs to be added or modified.
	Beware: codeberg is not a chatter forum.
	Please let the discussion be focussed on the actual development of the website.</p>
	HTML;


$h2_contribute = new h2HeaderContent('Contribute');

$div_contribute = new ContentSection();
$div_contribute->content = <<<HTML
	<h3>Team work</h3>

	<p>We all have different abilities.
	We can pool our time, knowledge and skills,
	and contribute according to our areas of expertise or interest.</p>
	HTML;

$div_research = new ContentSection();
$div_research->content = <<<HTML
	<h3>Research</h3>

	<p>Many topics need to be researched. Check the issue queue at Codeberg.</p>
	HTML;

$div_multimedia = new ContentSection();
$div_multimedia->content = <<<HTML
	<h3>Illustrations and multimedia</h3>

	<p>If you have some talent in drawing some illustrations, to visually complement the existing text,
	let us know under which licence you would be willing to contribute your artwork.</p>

	<p>Most of the content here is in text form.
	At some stage, we will need help to make it more easily accessible to a larger audience,
	by creating illustrations and videos.</p>
	HTML;

$div_code = new ContentSection();
$div_code->content = <<<HTML
	<h3>Code</h3>
	<p>By design, the code is kept very simple.
	The website is pure HTML and CSS, and we use very simple PHP code for preprocessing individual pages.
	You can help improve the design, although we intentionally keep the design very low-key and simple.</p>
	HTML;

$div_git_and_pull_requests = new ContentSection();
$div_git_and_pull_requests->content = <<<HTML
	<h3>Git and pull requests</h3>

	<p>If you don't know what <strong>git</strong> and <strong>pull requests</strong> are, then this section is not for you! ;)
	However, should you wish to contribute more directly to this project, pull requests are the best way to go about it!
	Let us know if you need assistance in submitting your first patch via pull request.
	</p>

	<p>See: <a href="/project/git.html">Git workflow</a>.</p>
	HTML;

$h2_content = new h2HeaderContent('Content');

$div_content = new ContentSection();
$div_content->content = <<<HTML
	<p>We need to develop the website by adding a lot of interrelated content.
	For each item below, check the relevant sections for what already exists and what is missing.
	Also, check the issue tracker at Codeberg for specific requests.</p>

	<h3>Inform</h3>

	<p>First and foremost, we need to inform the visitors of the website,
	tell them of the issues, the challenges and possible solutions.</p>

	<h3>Teach</h3>

	<p>At some stage, we can add some content for teachers to use in their classrooms.
	If you are a teacher or educator yourself, we'd welcome your ideas for teaching material and handouts to use at school.</p>

	<h3>Technical content</h3>

	<p>Detailed policy proposals, specifications, procedures and all sorts of technical details
	that a lawmaker or an implementor would need to know.</p>

	<h3>Campaign</h3>

	<p>Campaign and outreach material, to promote this project and the ideas therein.</p>

	<h3>Research and development</h3>

	<p>There is still so much that we do not know.
	How to conduct research? What topics to research?
	What research has already been done elsewhere, that we should be aware of?
	</p>

	<h3>Crowdsourcing</h3>

	<p>No small group of individuals can know everything.
	There is a big potential for crowdsourcing information, news and data.</p>


	<h3>Fact checking</h3>

	<p>We need your help to make sure that all the information presented in this project is accurate.</p>


	<h3>Data</h3>

	<p>We need all kinds of raw data to use in our research and analysis.</p>
	HTML;




$page->parent('project/index.html');
$page->body($h2_community);
$page->body($div_community);

$page->body($h2_join);
$page->body($div_join);

$page->body($h2_contribute);
$page->body($div_contribute);
$page->body($div_share);
$page->body($div_research);
$page->body($div_multimedia);
$page->body($div_code);
$page->body($div_git_and_pull_requests);

$page->body($h2_content);
$page->body($div_content);
