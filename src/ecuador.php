<?php
$page = new CountryPage('Ecuador');
$page->h1('Ecuador');
$page->keywords('Ecuador');
$page->stars(0);

$page->preview( <<<HTML
	<p></p>
	HTML );

$page->snp('description', '18 million inhabitants.');
//$page->snp('image',       '/copyrighted/');



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Fernando Villavicencio, a prominent $anticorruption candidate to the 2023 presidential elections in Ecuador
	was assassinated in August, just prior to the election.</p>
	HTML;

$div_wikipedia_Ecuador = new WikipediaContentSection();
$div_wikipedia_Ecuador->setTitleText('Ecuador');
$div_wikipedia_Ecuador->setTitleLink('https://en.wikipedia.org/wiki/Ecuador');
$div_wikipedia_Ecuador->content = <<<HTML
	<p>The Republic of Ecuador is a country in northwestern South America,
	bordered by Colombia on the north, Peru on the east and south, and the Pacific Ocean on the west.</p>
	HTML;

$div_wikipedia_History_of_Ecuador_1990_present = new WikipediaContentSection();
$div_wikipedia_History_of_Ecuador_1990_present->setTitleText('History of Ecuador 1990 present');
$div_wikipedia_History_of_Ecuador_1990_present->setTitleLink('https://en.wikipedia.org/wiki/History_of_Ecuador_(1990–present)');
$div_wikipedia_History_of_Ecuador_1990_present->content = <<<HTML
	<p>History of Ecuador since 1990, after its return to democracy in 1979.</p>
	HTML;

$div_wikipedia_Rights_of_nature_in_Ecuador = new WikipediaContentSection();
$div_wikipedia_Rights_of_nature_in_Ecuador->setTitleText('Rights of nature in Ecuador');
$div_wikipedia_Rights_of_nature_in_Ecuador->setTitleLink('https://en.wikipedia.org/wiki/Rights_of_nature_in_Ecuador');
$div_wikipedia_Rights_of_nature_in_Ecuador->content = <<<HTML
	<p>With the adoption of a new constitution in 2008 under president Rafael Correa,
	Ecuador became the first country in the world to enshrine a set of codified Rights of Nature
	and to inform a more clarified content to those rights.</p>
	HTML;

$div_wikipedia_Politics_of_Ecuador = new WikipediaContentSection();
$div_wikipedia_Politics_of_Ecuador->setTitleText('Politics of Ecuador');
$div_wikipedia_Politics_of_Ecuador->setTitleLink('https://en.wikipedia.org/wiki/Politics_of_Ecuador');
$div_wikipedia_Politics_of_Ecuador->content = <<<HTML
	<p>The politics of Ecuador are multi-party.
	The central government polity is a quadrennially elected presidential, unicameral representative democracy.</p>
	HTML;


$page->parent('world.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_Ecuador);
$page->body($div_wikipedia_History_of_Ecuador_1990_present);
$page->body($div_wikipedia_Rights_of_nature_in_Ecuador);
$page->body($div_wikipedia_Politics_of_Ecuador);
