<?php
$page = new Page();
$page->h1('List of people');
$page->stars(0);

$page->preview( <<<HTML
	<p>
	</p>
	HTML );



//$page->snp('description', "");
//$page->snp('image', "/copyrighted/");



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>For each person that shall be featured below,
	we include only the information that is directly relevant to the project.
	In particular, we shall not cover personal information, biography, controversies, etc.
	unless such information has particular significance for the topics we aim to cover.
	To the extent that a person is notable enough to have a wikipedia article,
	we shall link to it:
	interested readers shall find more information there.</p>
	HTML;

$list = new ListOfPages();
$list->add('gar_alperovitz.html');
$list->add('anne_applebaum.html');
$list->add('richard_bluhm.html');
$list->add('clark_cunningham.html');
$list->add('denise_dresser.html');
$list->add('rolf_dobelli.html');
$list->add('mo_gawdat.html');
$list->add('staffan_lindberg.html');
$list->add('michael_luttig.html');
$list->add('maria_ressa.html');
$list->add('don_miguel_ruiz.html');
$list->add('laurence_tribe.html');
$list->add('karoline_wiesner.html');
$list->add('volodymyr_zelensky.html');
$print_list = $list->print();


$div_list = new ContentSection();

$div_list->content = <<<HTML
	<h3>list</h3>

	<p>Listed in alphabetical order by surname.</p>

	$print_list
	HTML;



$page->parent('lists.html');
$page->body($div_stub);

$page->body($div_introduction);
$page->body($div_list);
