<?php
$page = new Page();
$page->h1('Poverty');
$page->keywords('Poverty', 'poverty');
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>We are primarily concerned by extreme poverty on a $global scale.
	However, poverty is also a grave concern within rich countries like the UK or the USA.</p>
	HTML;

$list = new ListOfPages();
$list->add('richard_bluhm.html');
$print_list = $list->print();

$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>Related content</h3>

	$print_list
	HTML;



$div_wikipedia_Poverty = new WikipediaContentSection();
$div_wikipedia_Poverty->setTitleText('Poverty');
$div_wikipedia_Poverty->setTitleLink('https://en.wikipedia.org/wiki/Poverty');
$div_wikipedia_Poverty->content = <<<HTML
	<p>Poverty is a state or condition in which one lacks the financial resources and essentials for a certain standard of living.
	Poverty can have diverse social, economic, and political causes and effects.</p>
	HTML;


$page->parent('social_justice.html');
$page->body($div_stub);
$page->body($div_introduction);

$page->body($div_list);

$page->body($div_wikipedia_Poverty);
