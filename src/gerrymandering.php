<?php
$page = new Page();
$page->h1('Gerrymandering');
$page->keywords('Gerrymandering');
$page->stars(0);

$page->preview( <<<HTML
	<p></p>
	HTML );

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>In the United States, single-member districts and the practice of gerrymandering is one important factor
	in the bi-polarisation of the political environment.</p>
	HTML;

$div_wikipedia_Gerrymandering = new WikipediaContentSection();
$div_wikipedia_Gerrymandering->setTitleText('Gerrymandering');
$div_wikipedia_Gerrymandering->setTitleLink('https://en.wikipedia.org/wiki/Gerrymandering');
$div_wikipedia_Gerrymandering->content = <<<HTML
	<p>In representative democracies, gerrymandering is the political manipulation of electoral district boundaries
	with the intent to create undue advantage for a party, group, or socioeconomic class within the constituency.</p>
	HTML;

$div_wikipedia_Gerrymandering_in_the_United_States = new WikipediaContentSection();
$div_wikipedia_Gerrymandering_in_the_United_States->setTitleText('Gerrymandering in the United States');
$div_wikipedia_Gerrymandering_in_the_United_States->setTitleLink('https://en.wikipedia.org/wiki/Gerrymandering_in_the_United_States');
$div_wikipedia_Gerrymandering_in_the_United_States->content = <<<HTML
	<p>In the United States, redistricting takes place in each state about every ten years.
	Redistricting has always been regarded as a political exercise
	and in most states, it is controlled by state legislators and sometimes the governor.</p>
	HTML;


$page->parent('democracy.html');
$page->body($div_stub);
$page->body($div_introduction);

$page->body($div_wikipedia_Gerrymandering);
$page->body($div_wikipedia_Gerrymandering_in_the_United_States);
