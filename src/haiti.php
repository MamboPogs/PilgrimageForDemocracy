<?php
$page = new CountryPage('Haiti');
$page->h1('Haiti');
$page->keywords('Haiti');
$page->stars(0);

$page->preview( <<<HTML
	<p></p>
	HTML );

$page->snp('description', '11 million inhabitants.');
//$page->snp('image',       '/copyrighted/');



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Haiti is a country in dire need of help.</p>
	HTML;

$div_wikipedia_Haiti = new WikipediaContentSection();
$div_wikipedia_Haiti->setTitleText('Haiti');
$div_wikipedia_Haiti->setTitleLink('https://en.wikipedia.org/wiki/Haiti');
$div_wikipedia_Haiti->content = <<<HTML
	<p>Haiti is a country located on the island of Hispaniola in the Caribbean Sea.
	It occupies the western three-eighths of the island which it shares with the Dominican Republic.</p>
	HTML;

$div_wikipedia_Haitian_crisis_2018_present = new WikipediaContentSection();
$div_wikipedia_Haitian_crisis_2018_present->setTitleText('Haitian crisis 2018 present');
$div_wikipedia_Haitian_crisis_2018_present->setTitleLink('https://en.wikipedia.org/wiki/Haitian_crisis_(2018–present)');
$div_wikipedia_Haitian_crisis_2018_present->content = <<<HTML
	<p>Series of protests and street violence fuelled by rising energy prices,
	with armed gangs taking over streets.</p>
	HTML;

$div_wikipedia_Assassination_of_Jovenel_Moise = new WikipediaContentSection();
$div_wikipedia_Assassination_of_Jovenel_Moise->setTitleText('Assassination of Jovenel Moïse');
$div_wikipedia_Assassination_of_Jovenel_Moise->setTitleLink('https://en.wikipedia.org/wiki/Assassination_of_Jovenel_Moïse');
$div_wikipedia_Assassination_of_Jovenel_Moise->content = <<<HTML
	<p>Jovenel Moïse, the president of Haiti, was assassinated on 7 July 2021 at his residence in Port-au-Prince.
	A group of 28 foreign mercenaries, mostly Colombians, are alleged to be responsible for the killing.</p>
	HTML;

$div_wikipedia_2021_Haiti_earthquake = new WikipediaContentSection();
$div_wikipedia_2021_Haiti_earthquake->setTitleText('2021 Haiti earthquake');
$div_wikipedia_2021_Haiti_earthquake->setTitleLink('https://en.wikipedia.org/wiki/2021_Haiti_earthquake');
$div_wikipedia_2021_Haiti_earthquake->content = <<<HTML
	<p>On 14 August 2021, a magnitude 7.2 earthquake struck the Tiburon Peninsula in the Caribbean nation of Haiti.
	At least 2,248 people were confirmed killed as of 1 September 2021 and more than 12,200 injured.</p>
	HTML;

$div_wikipedia_Politics_of_Haiti = new WikipediaContentSection();
$div_wikipedia_Politics_of_Haiti->setTitleText('Politics of Haiti');
$div_wikipedia_Politics_of_Haiti->setTitleLink('https://en.wikipedia.org/wiki/Politics_of_Haiti');
$div_wikipedia_Politics_of_Haiti->content = <<<HTML
	<p>The politics of Haiti are considered historically unstable due to various coups d'état,
	regime changes, military juntas and internal conflicts.</p>
	HTML;

$div_wikipedia_Foreign_aid_to_Haiti = new WikipediaContentSection();
$div_wikipedia_Foreign_aid_to_Haiti->setTitleText('Foreign aid to Haiti');
$div_wikipedia_Foreign_aid_to_Haiti->setTitleLink('https://en.wikipedia.org/wiki/Foreign_aid_to_Haiti');
$div_wikipedia_Foreign_aid_to_Haiti->content = <<<HTML
	<p>Haiti has received billions in foreign assistance,
	yet persists as one of the poorest countries and has the lowest human development index in the Americas.
	There have been more than 15 natural disasters since 2001 including tropical storms, flooding, earthquakes and hurricanes.
	The international donor community classifies Haiti as a fragile state.
	Haiti is also considered a post-conflict state—one emerging from a recent coup d'état and civil war.</p>
	HTML;


$page->parent('world.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_Haiti);
$page->body($div_wikipedia_Haitian_crisis_2018_present);
$page->body($div_wikipedia_Assassination_of_Jovenel_Moise);
$page->body($div_wikipedia_2021_Haiti_earthquake);
$page->body($div_wikipedia_Politics_of_Haiti);
$page->body($div_wikipedia_Foreign_aid_to_Haiti);
