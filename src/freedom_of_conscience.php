<?php
$page = new Page();
$page->h1('Freedom of conscience');
$page->keywords('Freedom of conscience', 'freedom of conscience');
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Freedom of conscience is one of the most basic $freedom
	and one of the ${'primary features of democracy'}.</p>
	HTML;

$div_wikipedia_Freedom_of_thought = new WikipediaContentSection();
$div_wikipedia_Freedom_of_thought->setTitleText('Freedom of thought');
$div_wikipedia_Freedom_of_thought->setTitleLink('https://en.wikipedia.org/wiki/Freedom_of_thought');
$div_wikipedia_Freedom_of_thought->content = <<<HTML
	<p>Freedom of thought (also called freedom of conscience) is the freedom of an individual
	to hold or consider a fact, viewpoint, or thought, independent of others' viewpoints.</p>
	HTML;


$page->parent('primary_features_of_democracy.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_wikipedia_Freedom_of_thought);
