<?php
$page = new Page();
$page->h1('Corruption');
$page->stars(0);
$page->keywords('Corruption', 'corruption', 'anticorruption');


$page->snp('description', 'A malign for of cancer affecting democracy.');
//$page->snp('image', "/copyrighted/");


$page->preview( <<<HTML
	<p>The continued failure of most countries to significantly control corruption
	is contributing to a crisis in democracy around the world.</p>
	HTML );

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Corruption is one of the most malign form of cancer of democracy.
	Corruption is an obstacle to the realisation of all human rights, without exception.</p>

	<p>Corruption disproportionately impacts the poorest members of the community.</p>

	<blockquote>
	"Not a single Sustainable Development Goal will be attained, and development will remain a utopia,
	as long as corruption prevails.
	Corruption kills and destroys lives daily.
	It is a curse which will only be curbed by our human determination to promote and protect the common good."
	<br>
	- Ketakandriana Rafitoson,<br>
	Executive Director of ${'Transparency International'} Initiative Madagascar
	</blockquote>

	<p>In a televised interview on Aug. 27 2023, $Ukraine President ${'Volodymyr Zelensky'} said he has submitted a proposal to parliament
	to equate corruption with treason while martial law is in effect in Ukraine.</p>

	<p>Fernando Villavicencio, a prominent anticorruption candidate to the 2023 presidential elections in $Ecuador
	was assassinated in August, just prior to the election.</p>
	HTML;

$page->parent('institutions.html');
$page->body($div_stub);

$page->body($div_introduction);

$page->body('transparency_international.html');
