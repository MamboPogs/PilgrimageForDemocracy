<?php
$page = new Page();
$page->h1('Transnational authoritarianism');
$page->stars(2);
$page->keywords('Transnational authoritarianism', 'transnational authoritarianism', 'transnational repression', 'Transnational repression');

$page->preview( <<<HTML
	<p>Authoritarian regimes cross borders and repress refugees living in democratic countries.</p>
	HTML );



$page->snp('description', "Dictatorships pursuing refugees within free countries.");
//$page->snp('image', "/copyrighted/");



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Authoritarian regimes cross borders and repress refugees living in $democratic $countries.</p>

	<p>${'Freedom House'} has published a few reports (linked below) on the topic of transnational authoritarianism.<p>

	<p>Freedom House lists six $countries that currently operate aggressive campaigns of transnational repression:</p>

	<ul>
	<li>$Iran</li>
	<li>${"People's Republic of China"}</li>
	<li>${'Turkey'}</li>
	<li>$Rwanda</li>
	<li>${'Saudi Arabia'}</li>
	<li>${'Russia'}</li>
	</ul>
	HTML;



$div_The_People_Republic_of_China = new ContentSection();
$div_The_People_Republic_of_China->content = <<<HTML
	<h3>The People's Republic of China</h3>

	<p>The ${"People's Republic of China"} has opened secret Chinese police stations within Western countries
	is order to pursue, monitor and put pressure on Chinese refugees living abroad.</p>

	<p>The $PRC routinely harasses family members who still live in China.</p>

	<p>The authorities of ${'Hong Kong'} have started adopting similar tactics
	in order to force pro-democracy Hong Kong dissidents to return home to face recriminations.</p>
	HTML;


$div_Transnational_Repression = new FreedomHouseContentSection();
$div_Transnational_Repression->setTitleText('Report: Transnational Repression');
$div_Transnational_Repression->setTitleLink('https://freedomhouse.org/report/transnational-repression');
$div_Transnational_Repression->content = <<<HTML
	<p>2022 report on transnational repression by Freedom House with 2023 update.</p>

	<p>Critical voices that challenge authoritarian rule become voices to silence.
	Journalists and human rights defenders. Diaspora groups and family members of exiles.
	Political activists, dissidents and civil society leaders.</p>

	<p>What appear to be isolated incidents when viewed separately—an assassination here, a kidnapping there—in fact form
	a constant threat across the world that is affecting the lives of millions of people
	and changing how activists, journalists, and regular individuals go about their lives.
	Transnational repression is no longer an exceptional tool, but a normal and institutionalized practice
	for dozens of countries that seek to control their citizens abroad.</p>

	<p>The report includes:</p>

	<ul>
	<li>List of perpetrating countries.</li>
	<li>List of targeted countries.</li>
	<li>Case studies.</li>
	<li>Policy response with best and worst practices in term of security policy, migration policy, and foreign policy.</li>
	</ul>
	HTML;



$div_CECC_Statement_The_Threat_of_Transnational_Repression_from_China_and_the_U_S_Response = new FreedomHouseContentSection();
$div_CECC_Statement_The_Threat_of_Transnational_Repression_from_China_and_the_U_S_Response->setTitleText('CECC Statement: The Threat of Transnational Repression from China and the U.S. Response');
$div_CECC_Statement_The_Threat_of_Transnational_Repression_from_China_and_the_U_S_Response->setTitleLink('https://freedomhouse.org/article/cecc-statement-threat-transnational-repression-china-and-us-response');
$div_CECC_Statement_The_Threat_of_Transnational_Repression_from_China_and_the_U_S_Response->content = <<<HTML
	<p>A statement for the record from Freedom House for the Congressional-Executive Commission on China's hearing on Transnational Repression.</p>
	HTML;



$div_Transnational_Repression_Threatens_Freedom_Worldwide = new FreedomHouseContentSection();
$div_Transnational_Repression_Threatens_Freedom_Worldwide->setTitleText('Transnational Repression Threatens Freedom Worldwide');
$div_Transnational_Repression_Threatens_Freedom_Worldwide->setTitleLink('https://freedomhouse.org/article/transnational-repression-threatens-freedom-worldwide');
$div_Transnational_Repression_Threatens_Freedom_Worldwide->content = <<<HTML
	<p>[<em>2022 article</em>] If exiled dissidents can be attacked even in the strongest democracies, none of us are safe.</p>
	HTML;



$div_Policy_Recommendations_Transnational_Repression = new FreedomHouseContentSection();
$div_Policy_Recommendations_Transnational_Repression->setTitleText('Policy Recommendations: Transnational Repression');
$div_Policy_Recommendations_Transnational_Repression->setTitleLink('https://freedomhouse.org/policy-recommendations/transnational-repression');
$div_Policy_Recommendations_Transnational_Repression->content = <<<HTML
	<p>Includes recommendations for: The United States and other governments of countries that host exiles,
	targeted diaspora, civil society, UN member states, technology companies.</p>

	<p>Policy recommendations include:</p>

	<ul>
	<li>Raising awareness about the threat of transnational repression</li>
	<li>Limiting the ability of perpetrators to commit transnational repression</li>
	<li>Bringing accountability for acts of transnational repression</li>
	<li>Supporting victims of transnational repression</li>
	<li>Track transnational repression and coordinate responses</li>
	</ul>
	HTML;



$div_NEW_REPORT_More_Governments_Engaged_in_More_Transnational_Repression_during_2022 = new FreedomHouseContentSection();
$div_NEW_REPORT_More_Governments_Engaged_in_More_Transnational_Repression_during_2022->setTitleText('NEW REPORT: More Governments Engaged in More Transnational Repression during 2022');
$div_NEW_REPORT_More_Governments_Engaged_in_More_Transnational_Repression_during_2022->setTitleLink('https://freedomhouse.org/article/new-report-more-governments-engaged-more-transnational-repression-during-2022');
$div_NEW_REPORT_More_Governments_Engaged_in_More_Transnational_Repression_during_2022->content = <<<HTML
	<p>[April 6, 2023] Perpetrators from a total of 20 states were responsible
	for 79 incidents of physical transnational repression last year,
	including the first documented cases originating in Djibouti and Bangladesh.</p>
	HTML;



$div_Declaration_of_Principles_to_Combat_Transnational_Repression = new FreedomHouseContentSection();
$div_Declaration_of_Principles_to_Combat_Transnational_Repression->setTitleText('Declaration of Principles to Combat Transnational Repression');
$div_Declaration_of_Principles_to_Combat_Transnational_Repression->setTitleLink('https://freedomhouse.org/2023/summit-for-democracy-transnational-repression');
$div_Declaration_of_Principles_to_Combat_Transnational_Repression->content = <<<HTML
	<p>The Declaration of Principles to Combat Transnational Repression calls upon democratic governments
	to acknowledge and commit to addressing the increasingly prevalent phenomenon of transnational repression,
	whereby states reach across borders to harm, intimidate, and silence journalists, activists, dissidents, and diaspora communities.</p>

	<p>Transnational repression is a threat to democracy and human rights worldwide.
	It undermines the rule of law, imperils civil and political liberties, and spreads authoritarianism.
	Combatting transnational repression requires ending impunity for perpetrators, strengthening the resilience of democratic institutions,
	and protecting vulnerable groups and individuals.</p>
	HTML;



$div_wikipedia_Transnational_authoritarianism = new WikipediaContentSection();
$div_wikipedia_Transnational_authoritarianism->setTitleText('Transnational authoritarianism');
$div_wikipedia_Transnational_authoritarianism->setTitleLink('https://en.wikipedia.org/wiki/Transnational_authoritarianism');
$div_wikipedia_Transnational_authoritarianism->content = <<<HTML
	<p>Transnational authoritarianism represents any effort to prevent acts of political dissent against an authoritarian state
	by targeting one or more existing or potential members of its emigrant or diaspora communities.</p>
	HTML;

$div_wikipedia_Chinese_police_overseas_service_stations = new WikipediaContentSection();
$div_wikipedia_Chinese_police_overseas_service_stations->setTitleText('Chinese police overseas service stations');
$div_wikipedia_Chinese_police_overseas_service_stations->setTitleLink('https://en.wikipedia.org/wiki/Chinese_police_overseas_service_stations');
$div_wikipedia_Chinese_police_overseas_service_stations->content = <<<HTML
	<p>Human rights groups published reports stating that
	the Chinese government used "overseas service station" to intimidate Chinese dissidents and criminal suspects abroad
	and to pressure them to return to China. The report led to investigations of the stations by the governments of several countries.</p>
	HTML;

$div_wikipedia_List_of_Soviet_and_Russian_assassinations = new WikipediaContentSection();
$div_wikipedia_List_of_Soviet_and_Russian_assassinations->setTitleText('List of Soviet and Russian assassinations');
$div_wikipedia_List_of_Soviet_and_Russian_assassinations->setTitleLink('https://en.wikipedia.org/wiki/List_of_Soviet_and_Russian_assassinations');
$div_wikipedia_List_of_Soviet_and_Russian_assassinations->content = <<<HTML
	<p>List of people confirmed to have been assassinated by governments of the Soviet Union and Russian Federation.
	Some of the assassinations or targeted killings took place overseas.</p>
	HTML;


$page->parent('democracy_under_attack.html');
$page->body($div_stub);

$page->body($div_introduction);
$page->body($div_The_People_Republic_of_China);

$page->body($div_Transnational_Repression);
$page->body($div_Transnational_Repression_Threatens_Freedom_Worldwide);
$page->body($div_wikipedia_Transnational_authoritarianism);

$page->body($div_Declaration_of_Principles_to_Combat_Transnational_Repression);
$page->body($div_Policy_Recommendations_Transnational_Repression);
$page->body($div_NEW_REPORT_More_Governments_Engaged_in_More_Transnational_Repression_during_2022);
$page->body($div_CECC_Statement_The_Threat_of_Transnational_Repression_from_China_and_the_U_S_Response);

$page->body($div_wikipedia_Chinese_police_overseas_service_stations);
$page->body($div_wikipedia_List_of_Soviet_and_Russian_assassinations);
