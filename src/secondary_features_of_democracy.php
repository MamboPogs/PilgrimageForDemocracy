<?php
$page = new Page();
$page->h1('2: Secondary features of democracy — We, the Society');
$page->stars(4);
$page->keywords('secondary features of democracy');

$page->preview( <<<HTML
	<p>The primary features of democracy, if unmoderated and left unchecked, can lead to mob rule and the tyranny of the masses.
	The secondary features provide balance to the first ones.</p>
	<p>The secondary flaws of democracy are:
		<span class="democracy flaw">chaos</span>,
		<span class="democracy flaw">divisiveness</span>,
		<span class="democracy flaw">tribalism</span>,
		<span class="democracy flaw">party politics</span>,
		<span class="democracy flaw">blaming others</span>,
		<span class="democracy flaw">pride</span>,
		<span class="democracy flaw">exploitation</span>.</p>
	<p>The secondary virtues of democracy are:
		<span class="democracy virtue">order</span>,
		<span class="democracy virtue">solidarity</span>,
		<span class="democracy virtue">justice</span>,
		<span class="democracy virtue">compassion</span>,
		<span class="democracy virtue">humility</span>,
		<span class="democracy virtue">cooperation</span>.</p>
	HTML );

$page->snp('description', "No man is an island. Beyong individual liberties: living together as a community.");
$page->snp('image', "/copyrighted/mario-purisic-jG1z5o7NCq4.1200-630.jpg");

$r1 = $page->ref('https://en.wikipedia.org/wiki/Devotions_upon_Emergent_Occasions', 'Devotions upon Emergent Occasions');
$r2 = $page->ref('https://quoteinvestigator.com/2011/10/15/liberty-fist-nose/', 'Your Liberty To Swing Your Fist Ends Just Where My Nose Begins');
$r3 = $page->ref('https://en.wikisource.org/wiki/Meditation_XVII', 'Meditation XVII, by John Donne');
$r4 = $page->ref('https://xroads.virginia.edu/~Hyper/DETOC/1_ch15.htm', 'Unlimited power of the majority in the United States, and tis consequences');
$r5 = $page->ref('https://kyivindependent.com/dennis-soltys-ukraine-is-winning-her-great-modernization-war/', 'Dennis Soltys: Ukraine is winning her Great Modernization War');
$r6 = $page->ref('https://en.wikipedia.org/wiki/Piss_Christ', 'Piss Christ');
$r7 = $page->ref('https://en.wikipedia.org/wiki/The_Kiss_(1896_film)', 'The Kiss (1896 film)');
$r8 = $page->ref('https://www.riverdalepress.com/stories/its-all-about-you-and-everyone-around-you,75777', 'It\'s all about you and everyone around you');

$h2_limits_individual_liberty = new h2HeaderContent('Beyond individual liberties');

$div_limits_individual_liberty = new ContentSection();
$div_limits_individual_liberty->content = <<<HTML
	<p>There is no simple definition of democracy
	because it a multi-layered aggregate of interconnected concepts.</p>

	<p>In the <a href="/primary_features_of_democracy.html">primary features of democracy</a>,
	we saw the importance given to individual liberties.
	Such personal liberties shall always be the cornerstone of any modern democracy.
	They are the guarantee that individuals will have the freedom and the opportunity
	to develop their human, emotional, intellectual and spiritual potential.</p>

	<p>However, as should already be obvious to anybody reading these lines,
	individual liberties do not come without restrictions.
	There are evident reasons to limit people so that
	they are not exactly free to do anything they please.</p>

	<p>As the 17th century English poet John Donne wrote: {$r1}<p>
	<blockquote>
	No man is an island entire of itself;<br>
	every man is a piece of the continent,<br>
	a part of the main.
	</blockquote>

	<p>Indeed, we, as individuals, are not isolated from the rest of humanity,
	as if we were small islets lost in the vastness of a boundless ocean.
	It is illusory to imagine that one person's action shall never affect another person.
	Thus, unrestrained freedom is fraught with dangers.
	The consequences of one's actions upon others should be considered.</p>

	<p>It is commonly understood, according to a popular saying of unclear origin,
	that the liberty of one person ends where the liberty of the next person starts.
	A 19th century, post American Civil War version of the saying is stated thus: {$r2}</p>

	<blockquote>
	“Is not this a free country?”
	<br><br>
	“Yes, sir.”
	<br><br>
	“Have not I a right to swing my arm?”
	<br><br>
	“Yes, but your right to swing your arm leaves off where my right not to have my nose struck begins.”
	</blockquote>

	<p>This is a simple concept, easily understood and accepted by all,
	however when one delves into the details,
	things can get very complicated and subject to much argument.</p>
	HTML;

$h2_definition = new h2HeaderContent('Definitions');

$div_Do_no_harm = new ContentSection();
$div_Do_no_harm->content = <<<HTML
	<h3>Do no harm</h3>

	<p>If one wanted to summarize democracy, one could say:
	"<em><strong>Be free, but do no harm!</strong></em>"</p>

	<p>The imperative not to harm others is the one alluded to in the saying quoted above, about not striking another person's nose.
	However, the concept of "harm" is pervasive and encompasses too many different levels.
	It will be discussed further below,
	and may eventually cover many sections in this website.</p>
	HTML;

$div_A_dual_imperative = new ContentSection();
$div_A_dual_imperative->content = <<<HTML
	<h3>A dual imperative</h3>

	<p>The injunctions described in this article about the society as a whole do not replace
	those mentioned in the <a href="/primary_features_of_democracy.html">primary features of democracy</a>, which concern discrete individuals.
	They supplement them.
	A proper balance must be kept between both sets of injunctions and values.
	In many cases, it is obvious that the rights of the individual take precedence over the imperatives of the collectivity.
	In many other cases, it is obvious that the needs of the collectivity requires the reasonable sacrifice of some rights of some individuals.
	A suitable balance should be found between the two poles.
	Problems and controversy often arise in borderline situations.</p>

	<p>The problem is compounded by the fact that different people have different sensibilities,
	and while some might strongly favour personal freedom over anything else,
	others would more strongly value societal benefits.
	Democratic processes are all about finding a proper middle ground.</p>

	<p>Below, we shall consider some typical situations and key concepts where the tension between the dual imperative is at play.</p>
	HTML;

$div_not_mob_rule = new ContentSection();
$div_not_mob_rule->content = <<<HTML
	<h3>Democracy is not mob rule</h3>

	<p>There are inherent dangers to unrestrained individual freedom.</p>

	<p>History has seen numerous examples of societies sinking into lawlessness,
	where there was no functioning government to enforce laws and impose necessary restraints upon the citizens.
	Individuals could do absolutely as they pleased.
	Indeed! It was every man for himself!
	It can still be observed in the 21st century in some countries:
	the utter powerlessness of the government has allowed the rule of law to be replaced by the law of the jungle.
	Those places sank into a state of violent anarchy where armed gangs took control of the communities.</p>

	<p>In less extreme cases, people coalesce into mobs to more or less violently
	impose the will of the community upon a few individuals.
	Imagine the American Old West at the end of the 19th century.</p>

	<p>We can emphatically state that <strong>might is not right</strong>.
	Democracy is not mob rule, and even less anarchy nor the law of the jungle.
	Democratic civilization is not the <strong>tyranny of the masses</strong>.
	It is not even the survival of the fittest:
	even the less fit have a right to survive and to be given an opportunity to grow as a human being.</p>

	<p>It is obvious that there must be limits to individual liberties.</p>
	HTML;

$div_not_majority_rule = new ContentSection();
$div_not_majority_rule->content = <<<HTML
	<h3>Democracy is not majority rule</h3>

	<p>Counter-intuitively to many, democracy is not necessarily majority rule, or 50% of the votes plus one vote.
	Unconditional majority rule is simply mob rule at the ballot box!
	Every modern democracy has constitutional mechanisms to protect the rights of the individuals.</p>

	<p>Some individual rights are considered so sacred, like the freedom of conscience,
	that a simple majority vote cannot alienate them.</p>

	<p>Thus, democracies have enshrined into their constitutions some fundamental clauses
	that overrule any law that may have passed with a majority vote at the legislature.</p>
	HTML;


$div_protection_of_minorities = new ContentSection();
$div_protection_of_minorities->content = <<<HTML
	<h3>Protection of minorities</h3>

	<p>Further proofs that democracy cannot be reduced to majority rule,
	are all the legal and constitutional safeguards that modern democracies have adopted
	in order to protect minorities,
	whether ethnic, or religious or any other kind of minorities.</p>

	<p>One could be a minority of one in a large country,
	and still enjoy unalienable personal freedoms.</p>

	<p>Another type of minority worth mentioning here, are
	people with disabilities who shall have access to the same fundamental government services and governmental protection.
	They shall enjoy the same human rights to live as fulfilling and nurturing a life as possible.</p>
	HTML;


$div_private_vs_public = new ContentSection();
$div_private_vs_public->content = <<<HTML
	<h3>Private vs public</h3>

	<p>Now that we no longer consider a person as an island, but as a member of the society,
	we are obligated to make a distinction between the private sphere and the public space.</p>

	<p>In relation to this, there are two specific notions that will be expanded upon in future sections of this website:</p>

	<p>The right to <strong>privacy</strong>:
	other members of the society shall not intrude into the private sphere of one individual or of a small group of individuals, for example a family.</p>

	<p>The notion of <strong>${'private property'}</strong> is also an extremely important one to discuss,
	especially in opposition to that of <strong>${'public property'}</strong>.
	These notions are particularly significant in one specific area: <strong>$taxes</strong>.
	They will be discussed in a future section of this website.</p>
	HTML;


$div_rights_and_obligations = new ContentSection();
$div_rights_and_obligations->content = <<<HTML
	<h3>Rights and obligations</h3>

	<p>So far, we have spoken a lot about <em>rights</em>, but we would be amiss if we did not mention their counterpart: <em>obligations</em>.</p>

	<p>Obligations are justified as a way for individuals to perform actions that would be beneficial for the community or the society.</p>

	<p>Here is one simple example: in more and more countries, people have the obligation to segregate their trash.
	They must recycle what can be instead of indiscriminately throwing all their refuse together.</p>

	<p>An example of a more imposing obligation would be: conscription or compulsory military service,
	as exists or has existed in many democratic countries,
	to ensure the protection of the nation.</p>
	HTML;

$div_freedom_of_association = new ContentSection();
$div_freedom_of_association->content = <<<HTML
	<h3>Freedom of association</h3>

	<p>Since we are no longer talking about isolated individuals but about collectivities,
	it is obvious that ${'freedom of association'} is one very important secondary features of democracy.
	Our ability to associate and what we do with this freedom determines to a large extent the quality of our democracy.</p>
	HTML;


$h2_society = new h2HeaderContent('We, the Society');

$div_do_no_harm_really = new ContentSection();
$div_do_no_harm_really->content = <<<HTML
	<h3>Do no harm. Really?</h3>

	<p>It is properly understood that our fists shall not infringe upon our neighbour's nose.
	The reason why anyone would agree with this is because we are talking about direct physical harm.
	Indeed, it is commonly considered bad taste not only to hit people in the face,
	but also to stab them, to shoot them, in short: to physically hurt them in any way.</p>

	<p>The case is clear for physical harm.
	But what about more subtle ways to harm others?
	How far can we go?</p>

	<p>It is clear that it is perfectly possible to harm others even without any physical contact.
	To give a simple example that everybody can agree with:
	there can be <strong>domestic abuse</strong> even in the complete absence of physical violence.
	Similarly, <strong>bullying</strong> is a form of emotional abuse.
	All of this is clearly to be condemned.</p>

	<p>Beyond causing emotional harm,
	the law commonly defines other ways to hurt other people.
	<strong>Libel</strong> laws and <strong>defamation</strong> laws clearly address some of those.<p>

	<p>Even in the absence of verbal abuse, simple speech can cause harm.
	Careless statements can cause loss of reputation and loss of business
	for individuals and enterprises.</p>

	<p>As we move further and further away from the physical,
	the most concrete and material forms of harm,
	and towards the more abstract and immaterial,
	"harm" gets harder and harder to define.</p>

	<p>There is no definite boundary between what should clearly be
	condemnable harm, to be prohibited and punished by law,
	and what should be protected in the name of <strong>${'freedom of speech'}</strong> and individual liberties.
	The proper balance between individual freedom and the imperative not to harm others is tenuous.
	It is constantly changing according to countries, epochs, and civilizations.
	What was unacceptable before becomes uncontroversial today.
	We are no more able than anyone else to provide a definite and precise definition of where the boundary lays.</p>

	<p>To provide an example of the other extreme,
	imagine a person suing his neighbour and demands damages for the harm that the neighbour caused.
	The plaintiff claims that his personal political sensibilities were offended
	by the colour of the tie that his neighbour wore that morning!</p>

	<p>In today's polarized society, however one behaves,
	it has become too easy to inadvertently offend other people's personal sensibilities.
	A lot of arguments are fuelled by differing political sensibilities,
	differing religious sensibilities,
	by a person's <a href="social_dress_code.html">dress code</a>,
	by a person's choice of partner, etc.</p>

	<p>Consider the following events:</p>

	<ul>
	<li>Christians were scandalized by the film: "<em>The Last Temptation of Christ</em>" by famous director Martin Scorcese.
	A theatre featuring the film was set ablaze in Paris, $France.
	To this day, the film is banned in several countries.</li>

	<li>The novelist Salman Rushdie was the subject of several assassination attempts and death threats,
	including a fatwa calling for his death issued by Ayatollah Khomeini,
	because one of his novels was considered blasphemous.</li>

	<li>It is easy to imagine that a picture of a crucifix submerged in a small glass tank of the artist's urine
	created a lot of controversy, especially since the photograph actually received an award for visual arts! ${r6}</li>

	<li>Several illustrators and staff member of the French weekly "<em>Charlie Hebdo</em>" were shot dead
	in a terrorist attack by Muslim terrorists
	because the periodical had published caricatures of the Prophet Mohammed that were seen as blasphemous.</li>
	</ul>

	<p>The list of such incidents would be very long.
	In a democratic country, the photograph of a crucifix in urine, Rushdie's novels, Scorsese's film and Charlie Hebdo's caricatures
	are all covered under the principle of <strong>${'freedom of speech'}</strong>.</p>

	<p>Moral values vary in time.
	What does not vary is the amount of debates and dispute they engender.
	The very first kissing scene shot on film was a 1896 short, lasting a mere 18 seconds,
	and which simply depicts a man and a woman kissing on the lips ${r7}:
	moral leaders were outraged and said the film was utterly shocking, obscene and completely immoral.
	Today, walking completely in the nude is legal in some European countries,
	not only on nudist beaches but also within cities.</p>

	<p>Moral values vary in place.
	Even in the most liberal Muslim countries,
	a woman on a beach wearing a revealing bikini would be considered offensive.
	Meanwhile, in secular $France, it is not the state of undress that  offends, but being overdressed:
	a controversy arose because women were seen on a beach wearing a "burkini",
	a swimwear especially designed to cover the whole body,
	so that women could enjoy bathing in the sea while still conforming to Muslim dress code.
	A ban on burkini was even voted by a municipality located on the French sea shore.</p>

	<p>The consumption of alcohol is another hotly debated issue,
	and not only in 2022 in Qatar, where football fans were prohibited from drinking beer during the World Cup.
	Even the United States of America went through a period of prohibition in the early 20th century.
	In 1887, an American pro-Prohibition orator spoke thus:</p>

	<blockquote>
	The only leading argument urged by the anti-prohibitionists in this campaign for keeping open the bar-rooms, is personal liberty.
	A great man has said, "your personal liberty to swing your arm ends where my nose begins".
	A man's personal liberty to drink whisky and support barrooms ends where the rights of the family and the community begin.
	</blockquote>

	<p>The problem is the same everywhere: it is nigh impossible, if not completely so,
	to provide an absolute definition of what is "<em>harmful</em>", as opposed to merely "<em>offensive</em>".
	The boundary between the two is tenuous.
	The balance between what is acceptable, what should be tolerated, and what should be outright prohibited is
	mostly a question of personal judgement.
	The tipping point differ according to societies, according to country, and within the same country, according to time.
	There is no fixed point.
	Mores are constantly changing.</p>

	<p>It is obvious that there is no absolute guidelines about what should be accepted in a democratic society and what not.
	The point is that communities of individuals are sharing the same geographical space and chronological time,
	and they have to find ways to live together in peace and harmony, with mutual respect.</p>

	<p>Either members of the society on either extreme of each hot button issue try to impose their point of view on the other side,
	and at times even relishing in triggering the opponent, provoking discord and fuelling division within the society.</p>

	<p>Or they consensually strive to find a balance,
	with both extremes making efforts to meet somewhat closer to the middle:
	those more prone to outlandish display of their own freedom could try to curb their desires
	and behave more modestly out of respect for fellow members of the society;
	and those more conservatives and more easily offended could try to tone down their natural disgust
	and be more patient and understanding of the people with different persuasions and moral values.</p>
	HTML;


$div_individual_and_community = new ContentSection();
$div_individual_and_community->content = <<<HTML
	<h3>The individual and the community</h3>

	<p>The foundation of a democratic society are based on the individual.
	The result of the emphasis placed on individual liberties and individual rights
	is that individuals and personalities are glorified,
	much more than communities and collective achievements.</p>

	<p>In today's society, appropriately or not,
	we can observe a profound idolatry for singular individuals:
	film stars, pop stars, historical figures, leaders and role models of any kind.</p>

	<p>We have lots of reverence towards <strong>the first</strong> individual to have achieved something or performed something.
	There is a strong probability that you would know the name of the first person to walk on the moon.
	Ask around you and see how many know the right answer.
	What about the second person to walk on the moon? The third? The last person to ever walk the moon?</p>

	<p>Neil Armstrong was according to all accounts a very accomplished astronaut,
	a very professional individual who went through rigorous training to prepare for his moon landing.
	He was also a very private and modest person.
	His famous words when he first stepped on the moon
	alluded to the accomplishment of a whole society, the "<em>giant leap for mankind</em>".
	We do not diminish in any way Armstrong's personal contributions and professionalism
	by recalling that his feat was made possible by a whole nation working towards that singular goal.
	Countless engineers, scientists and anonymous workers at every level collaborated at NASA,
	funded and supported by the people of a whole country.
	All played their parts in the overall Apollo program.</p>

	<p>Similarly, we have lots of admiration for <strong>the best</strong> person in any domain, in sports, in arts,
	in science, or in any other field of achievement.
	Great minds have indeed blessed humanity with their works.</p>

	<p>Maybe it is time to realize that the greatest feats of all have been achieved by teams, communities, nations, societies...</p>

	<p>The quality of a democratic society depends largely upon
	how the members of that society behave with each other.
	Are we full of mutual recriminations or do we treat each other with respect?
	Are we solely focused on our own personal goals,
	or do we cooperate with our communities to build something greater, something better?
	Are we only preoccupied by our own welfare, or do we also act for the greater good?</p>
	HTML;

$div_social_utilitarianism = new ContentSection();
$div_social_utilitarianism->content = <<<HTML
	<h3>Social utilitarianism</h3>

	<p>Public policy is always a balancing act between protecting and respecting the rights of the individuals,
	and implementing policies that are beneficial for the whole society.</p>

	<p>The idea is not only to refrain from doing harm,
	but also to actively seek the benefit of the greatest number of, if not all, members of the society.</p>

	<p>The concept of social utilitarianism can itself be a very divisive one.
	Many people are more concerned about individual rights and individual responsibilities.
	Others, although they might agree with the notion to seek the common good,
	would fiercely argue bout about what the common good is and how to achieve it.
	Here, we can only offer some avenues for reflection.</p>

	<p>The constant question is:
	how much of individual rights is it appropriate to sacrifice in order to benefit the whole community?
	How to achieve a satisfying balance in this permanent tension
	between what is perceived to be good for individuals, and what is perceived to be beneficial for the community?</p>

	<p>For example, individual rights include the right to private property.
	Sometimes, however, it is considered beneficial for the majority to build some sort of large infrastructure,
	which would, more often than not, necessitate the expropriation of some individual from their private property.
	Every democratic country has some laws allowing for expropriation.
	Without such laws, it would be impossible to build
	new high speed rail roads, new airports nor large industrial complexes.
	So, is it admissible to expropriate people in order to build a new highway?
	One cannot reply in the absolute.
	It really depends on scores of details of the case.
	How necessary is the highway?
	How many people need to be expropriated?
	Would those people be appropriately compensated?
	As always, there will be people on both sides, for or against the proposed infrastructure.
	It is one purpose of democratic processes to resolve such conflicting interests.</p>

	<p>The debate over personal freedom is a long and very old one.
	The freedom extends to the freedom of having a lifestyle that is ultimately detrimental to oneself.
	Nothing legally prevents an individual from having an unhealthy diet,
	not exercising and becoming obese,
	smoking and drinking heavily at home,
	and thus dramatically increasing the likelihood of developing serious health complications
	and even meeting with an early death.</p>

	<p>What then to make with all the controversies that came with the Covid-19 pandemic
	and the mask-wearing and vaccination mandates that came with it?
	Many in western countries complained that their freedoms and civil liberties were being violated.
	What of it, if they wish to put their own lives in danger by not getting any vaccine nor taking any social distancing precautions,
	and taking the risk to catch this infectious disease with all the medical complications that might entail?</p>

	<p>The difference between failing to take precautions when an infectious disease spreads rapidly,
	and having an unhealthy lifestyle,
	is that during a pandemic,
	if people refuse to wear a mask and do not get vaccinated,
	they are not only putting their own life in danger,
	but they are also putting the lives of everyone else in the community in danger.
	One has to keep in mind the reproduction rate of the disease,
	and the dramatic effect that a large number of people being infected can have.
	Refusing to follow health guidelines and mandates
	is akin to swinging one's fist and not stopping where the other man's nose begins.
	Indeed, it could mean hitting him full force, with a blow that could very well be deadly. ${r8}</p>

	<p>In the United States and in western Europe,
	societies which revere above all personal freedoms,
	there were strong resistance by large sections of the society to vaccination and mask wearing.
	It is very interesting to note how differently the populations of Asia reacted
	to the pandemic and to the various health mandates.
	In Asian democracies like Japan, Taiwan (Republic of China), and South Korea,
	those same mandates did not cause any controversy.
	If anything, people were complaining that vaccines were not made available quickly enough!
	By comparing how much more the people from Asian countries willingly complied with the mandates of the health authorities,
	to what we witnessed happened in western countries,
	we can see the difference it makes when a society is more in tune with the common good,
	rather than with personal rights.</p>

	<p>Finally, when considering social utilitarianism,
	one should keep a <strong>hierarchy of needs</strong> in mind.</p>

	<p>In 2022, when Europe was weaning itself from Russian oil and gas,
	some people were tempted to complain about the increased cost of their utility bills
	to heat their homes during the winter.
	However, how does that compare to the extreme hardships that the Ukrainian people had to go through that same winter,
	during a brutal war, when the Russian army was targeting civilian energy infrastructure?</p>

	<p>There is something indecent about people having for primary concern the specifications and cost of the latest smart phones,
	when so many peoples around the world literally starve to death for lack of a bowl of rice.</p>

	<p>Being <a href="/hunger.html">deprived of food and literally starving to death</a>
	are not the same as being deprived of ice cream for desert!</p>
	HTML;


$div_opportunity = new ContentSection();
$div_opportunity->content = <<<HTML
	<h3>An opportunity</h3>

	<p>We live communal lives,
	and as we live our own free lives,
	we must respect the freedom and the rights of others.</p>

	<p>However, it would be wrong to view this imperative as a hindrance.
	Properly seen, it is rather an opportunity.
	Let's take a positive approach.</p>

	<p>Instead of considering what we <em>cannot</em>  do because it might infringe on other people's rights
	("<em>If it weren't for fear of my bloody neighbour complaining, I could play tuba at night!</em>"),
	let's consider what we <em>could</em> do, if only we harnessed the powers of our communities
	("<em>I can accomplish everything that is important to me, and so much more thanks to the cooperation of like-minded people in the neighbourhood.</em>")!</p>

	<p>Indeed, one of the most important freedoms afforded within democracies,
	is the <strong>${'freedom of association'}</strong>.
	Let's put this freedom to good use,
	and associate with people for productive and beneficial use!</p>

	<p>We must find ways to avoid the pitfalls of the <strong>Tyranny of the majority</strong>
	and let the <strong>Wisdom of the crowds</strong> flourish.</p>

	<p>The challenge is not new.
	19th century French statesman and political philosopher Alexis de Tocqueville
	dedicated a whole chapter of his 1835 book "<em>Democracy in America</em>" on the dangers of the tyranny of the majority.
	In that same chapter, he explains that:
	"<em>The moral authority of the majority is partly based upon the notion that
	there is more intelligence and wisdom in a number of men united than in a single individual</em>". ${r4}<p>

	<p>Canadian professor Dennis Soltys reminded us what Locke had to say about it, much earlier than Tocqueville:</p>

	<blockquote>
	Probably the most influential person in modern Western political thought, especially in Anglo-American thought,
	is Englishman John Locke (1632-1704).
	Locke had an exalted view of the person.
	He believed that people were intelligent, capable of understanding their own interests, the interests of others, and could resolve differences reasonably.
	People were more inclined to do good than harm; and so the main task for constitution-builders
	was to devise democratic political structures and judicial systems that would enable the better sides of the human character to be realized.
	${r5}
	</blockquote>

	<p>In a democracy, the citizens get the society that they deserve, since they are the most important actors of any public life.
	By the very etymological definition of the word "democracy", we, the people, have power.
	Let's use this power wisely, in loving compassion for our fellow human beings.
	We can do so much better than what the news report daily.</p>
	<br>
	HTML;


$div_conclusion = new ContentSection();
$div_conclusion->content = <<<HTML
	<br>
	<p>We started this presentation with some verses from a poem by John Donne.
	We shall end with the verses that follow within the same poem: ${r3}</p>

	<blockquote>
	Any man's death diminishes me,<br>
	because I am involved in mankind.<br>
	And therefore never send to know for whom the bell tolls;<br>
	it tolls for thee.
	</blockquote>

	<p>The day our global society recognizes the depth and the veracity of this statement,
	humanity would have reached an important milestone in its evolution.</p>

	<p>The untimely death of a single being who has not achieved their full human potential,
	is a tragedy for the whole society.
	For our own sake, and for the development of a better world,
	let us work hand in hand to make sure
	that no country is forgotten,
	that no single human being is left behind.
	The potential of each individual is precious;
	each has something valuable to contribute to the community.</p>

	<p>The bell tolls for us all.
	It is time we learned to live together in mutual respect and mutual cooperation.</p>
	HTML;

$list = new ListOfPages();
$list->add('blasphemy.html');
$list->add('global_issues.html');
$list->add('interpersonal_relationships.html');
$list->add('living_together.html');
$list->add('rohingya_people.html');
$print_list = $list->print();

$div_List_of_topics_related_to_the_secondary_features_of_democracy = new ContentSection();
$div_List_of_topics_related_to_the_secondary_features_of_democracy->content = <<<HTML
	<h3>List of topics related to the secondary features of democracy</h3>

	$print_list
	HTML;





$page->parent('primary_features_of_democracy.html');

$page->body($h2_limits_individual_liberty);
$page->body($div_limits_individual_liberty);

$page->body($h2_definition);
$page->body($div_Do_no_harm);
$page->body($div_A_dual_imperative);
$page->body($div_not_mob_rule);
$page->body($div_not_majority_rule);
$page->body($div_protection_of_minorities);
$page->body($div_private_vs_public);
$page->body($div_rights_and_obligations);
$page->body($div_freedom_of_association);


$page->body($h2_society);
$page->body($div_do_no_harm_really);
$page->body($div_individual_and_community);
$page->body($div_social_utilitarianism);
$page->body($div_opportunity);
$page->body($div_conclusion);


$page->body('tertiary_features_of_democracy.html');

$page->body($div_List_of_topics_related_to_the_secondary_features_of_democracy);
