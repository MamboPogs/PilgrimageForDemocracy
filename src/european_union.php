<?php
$page = new Page();
$page->h1('European Union');
$page->keywords('European Union', 'EU', 'Europe');
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_European_Union = new WikipediaContentSection();
$div_wikipedia_European_Union->setTitleText('European Union');
$div_wikipedia_European_Union->setTitleLink('https://en.wikipedia.org/wiki/European_Union');
$div_wikipedia_European_Union->content = <<<HTML
	<p>The European Union (EU) is a supranational political and economic union of 27 member states
	that are located primarily in Europe.
	The union has a total area of 4,233,255 km2 and an estimated total population of over 448 million.
	The EU has often been described as a sui generis political entity (without precedent or comparison)
	combining the characteristics of both a federation and a confederation.</p>
	HTML;


$page->parent('world.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_wikipedia_European_Union);
