<?php
$page = new Page();
$page->h1('Beliefs');
$page->stars(1);
$page->keywords('Beliefs', 'belief', 'beliefs');

$page->snp('description', "Beliefs from the point of view of democracy and social justice.");
//$page->snp('image', "/copyrighted/");

$page->preview( <<<HTML
	<p>Beliefs from the point of view of democracy and social justice.</p>
	HTML );




$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>This page shall discuss beliefs from the point of view of democracy and social justice.</p>

	<p>We discuss beliefs of any kind, whether religious, political or any other.</p>
	HTML;


$h2_Beliefs_and_the_first_principles_of_democracy = new h2HeaderContent('Beliefs and the first principles of democracy');

$div_Freedom_of_conscience = new ContentSection();
$div_Freedom_of_conscience->content = <<<HTML
	<h3>Freedom of conscience</h3>

	<p><strong>Freedom of conscience</strong> is one of the major tenets of democracy,
	according to which anyone is absolutely free to believe what they want.</p>

	<p>The belief that one holds may be false, and it may even be harmful to oneself,
	but one is still free to hold onto it, in respect of <strong>personal responsibility</strong> and <strong>personal liberty</strong>.</p>
	HTML;



$h2_Beliefs_and_the_second_principles_of_democracy = new h2HeaderContent('Beliefs and the second principles of democracy');


$div_Sharing_with_respect = new ContentSection();
$div_Sharing_with_respect->content = <<<HTML
	<h3>Sharing with respect</h3>

	<p>When one cherishes or feel strongly about some specific beliefs, it is natural to want to share it with other people in the community.
	However, and it is especially true for religious beliefs, one cannot impose one's beliefs onto others.
	When one communicates one's beliefs with other, one shall do so with respect,
	including respecting the possibility that other parties may not be interested or may have contradictory beliefs.</p>
	HTML;


$div_Harmful_wrong_beliefs = new ContentSection();
$div_Harmful_wrong_beliefs->content = <<<HTML
	<h3>Harmful wrong beliefs</h3>

	<p>A mature democracy shall have a way to deal with some strongly held beliefs that are both wrong, and harmful to others and to society in general.</p>

	<p>One good example of such is the widely held belief that the 2020 US presidential elections
	was massively fraudulent and that Donald J. Trump was somehow the legitimate president elect.
	These largely debunked claims have nonetheless been the cause of major social and political troubles in the US,
	putting at risk the very survival of American democracy.</p>
	HTML;




$div_wikipedia_Belief = new WikipediaContentSection();
$div_wikipedia_Belief->setTitleText('Belief');
$div_wikipedia_Belief->setTitleLink('https://en.wikipedia.org/wiki/Belief');
$div_wikipedia_Belief->content = <<<HTML
	<p>Beliefs are the subject of various important philosophical debates.
	Notable examples include:
	"What is the rational way to revise one's beliefs when presented with various sorts of evidence?",
	"Is the content of our beliefs entirely determined by our mental states?", or
	"Do the relevant facts have any bearing on our beliefs?"</p>
	HTML;

$page->parent('primary_features_of_democracy.html');
$page->body($div_stub);
$page->body($div_introduction);

$page->body($h2_Beliefs_and_the_first_principles_of_democracy);
$page->body($div_Freedom_of_conscience);

$page->body($h2_Beliefs_and_the_second_principles_of_democracy);
$page->body($div_Sharing_with_respect);
$page->body($div_Harmful_wrong_beliefs);
$page->body('secondary_features_of_democracy.html');

$page->body($div_wikipedia_Belief);
