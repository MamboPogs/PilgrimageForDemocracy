<?php
$page = new Page();
$page->h1('Cyberwarfare');
$page->stars(0);
$page->keywords('Cyberwarfare', 'cyberwarfare');

//$page->snp('description', "");
//$page->snp('image', "/copyrighted/");

$page->preview( <<<HTML
	<p>Both authoritarian regimes and democratic countries have active cyberwarfare units.</p>
	HTML );

$r1 = $page->ref('https://www.reuters.com/world/europe/russian-hackers-lured-embassy-workers-ukraine-with-an-ad-cheap-bmw-2023-07-12/', 'Russian hackers lured diplomats in Ukraine with cheap BMW ad');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Cyberwarfare is state-organized cybercrime targeting other states.</p>
	HTML;

$div_in_the_news = new ContentSection();
$div_in_the_news->content = <<<HTML
	<h3>In the news</h3>

	<p>There is no way to know the real extent of cyberwarfare between countries not officially at war,
	but who are strategic and ideological opponents.
	Now and then, there appears some stories in the news of active cyberwarfare between states.</p>

	<ul>
		<li>In April 2023, hackers suspected of working for Russia's foreign intelligence agency
		attempted to hack into the computers of foreign diplomats working at embassies in Kyiv, Ukraine,
		using an ad for a used BMW car.</li>
	</ul>
	HTML;



$div_wikipedia_Cyberwarfare = new WikipediaContentSection();
$div_wikipedia_Cyberwarfare->setTitleText('Cyberwarfare');
$div_wikipedia_Cyberwarfare->setTitleLink('https://en.wikipedia.org/wiki/Cyberwarfare');
$div_wikipedia_Cyberwarfare->content = <<<HTML
	<p>Many countries including the United States, United Kingdom, Russia, China, Israel, Iran, and North Korea
	have active cyber capabilities for offensive and defensive operations.
	As states explore the use of cyber operations and combine capabilities, the likelihood of physical confrontation
	and violence playing out as a result of, or part of, a cyber operation is increased.</p>
	HTML;

$div_wikipedia_Cyberwarfare_by_China = new WikipediaContentSection();
$div_wikipedia_Cyberwarfare_by_China->setTitleText('Cyberwarfare by China');
$div_wikipedia_Cyberwarfare_by_China->setTitleLink('https://en.wikipedia.org/wiki/Cyberwarfare_by_China');
$div_wikipedia_Cyberwarfare_by_China->content = <<<HTML
	<p>In 2017, Foreign Policy provided an estimated range for China's "hacker army" personnel, anywhere from 50,000 to 100,000 individuals.</p>
	HTML;

$div_wikipedia_Cyberwarfare_by_Russia = new WikipediaContentSection();
$div_wikipedia_Cyberwarfare_by_Russia->setTitleText('Cyberwarfare by Russia');
$div_wikipedia_Cyberwarfare_by_Russia->setTitleLink('https://en.wikipedia.org/wiki/Cyberwarfare_by_Russia');
$div_wikipedia_Cyberwarfare_by_Russia->content = <<<HTML
	<p>Cyberwarfare by Russia includes denial of service attacks, hacker attacks, dissemination of disinformation and propaganda,
	participation of state-sponsored teams in political blogs, internet surveillance using SORM technology, persecution of cyber-dissidents and other active measures.</p>
	HTML;



$page->parent('democracy_under_attack.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body($div_in_the_news);

$page->body($div_wikipedia_Cyberwarfare);
$page->body($div_wikipedia_Cyberwarfare_by_China);
$page->body($div_wikipedia_Cyberwarfare_by_Russia);
