<?php
$page = new Page();
$page->h1('Democracy');
$page->stars(2);
$page->keywords('democracy', 'Democracy', 'democratic', 'Democratic', 'Democracies', 'democracies');

$page->preview( <<<HTML
	<p>
	</p>
	HTML );



//$snp['description'] = "";
//$snp['image'] = "/copyrighted/";



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Global democracy is the ultimate goal of the $Pilgrimage.</p>
	HTML;


$list = new ListOfPages();
$list->add('accountability.html');
$list->add('capital_punishment.html');
$list->add('conflict_resolution.html');
$list->add('data_activism.html');
$list->add('decentralized_autonomous_organization.html');
$list->add('democracy_wip.html');
$list->add('disfranchisement.html');
$list->add('election_integrity.html');
$list->add('freedom_of_association.html');
$list->add('freedom_of_speech.html');
$list->add('gerrymandering.html');
$list->add('institutions.html');
$list->add('oecd.html');
$list->add('technology_and_democracy.html');
$list->add('types_of_democracy.html');
$list->add('voter_suppression.html');
$list->add('whistleblowers.html');
$list->add('world.html');
$print_list = $list->print();

$div_other_democracy_topics = new ContentSection();
$div_other_democracy_topics->content = <<<HTML
	<h3>Other democracy related topics</h3>

	$print_list

	<p>See also the $OECD's "Reinforcing Democracy Initiative".</p>
	HTML;



$div_wikipedia_Democracy = new WikipediaContentSection();
$div_wikipedia_Democracy->setTitleText('Democracy');
$div_wikipedia_Democracy->setTitleLink('https://en.wikipedia.org/wiki/Democracy');
$div_wikipedia_Democracy->content = <<<HTML
	<p>Democracy is a form of government in which the people have the authority to deliberate and decide legislation ("direct democracy"),
	or to choose governing officials to do so ("representative democracy").
	Who is considered part of "the people" and how authority is shared among or delegated by the people has changed over time
	and at different rates in different countries.
	Features of democracy often include freedom of assembly, association, property rights, freedom of religion and speech, citizenship,
	consent of the governed, voting rights, freedom from unwarranted governmental deprivation of the right to life and liberty, and minority rights.</p>
	HTML;

$page->parent('menu.html');
$page->body($div_introduction);

$page->body('primary_features_of_democracy.html');
$page->body('secondary_features_of_democracy.html');
$page->body('tertiary_features_of_democracy.html');

include 'section/features_of_democracy.php';
$page->body($div_section_what_is_democracy_quaternary_feature);
$page->body($div_section_what_is_democracy_quinary_feature);


$page->body($div_other_democracy_topics);


$page->body($div_wikipedia_Democracy);
