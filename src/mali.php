<?php
$page = new CountryPage('Mali');
$page->h1('Mali');
$page->stars(1);
$page->keywords('Mali');

$page->preview( <<<HTML
	<p>
	</p>
	HTML );

$page->snp('description', '21 million inhabitants');
//$page->snp('image', "/copyrighted/");

$r1 = $page->ref('https://www.dw.com/en/mali-set-for-constitutional-referendum/a-65912018', 'Mali set for constitutional referendum');
$r2 = $page->ref('https://www.dw.com/en/mali-counts-votes-in-referendum-on-new-constitution/a-65953014', 'Mali counts votes in referendum on new constitution');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$div_2023_Referendum = new ContentSection();
$div_2023_Referendum->content = <<<HTML
	<h3>2023 Referendum</h3>

	<p>In 2023 June 18, Mali held a referendum on a new $constitution. ${r1}
	The proposed constitution expands the power of the ruling military junta
	who claims it wants to pave the way to a return to civilian rule. ${r2}.</p>

	<p>The referendum is supposed to pave the way for a general election by February 2024.
	It is a milestone in the transition of Mali from military rule to democracy.</p>

	<p>The Coalition for Citizen Election Observation (COCEM) is observing the election, and reported some irregularities.</p>

	<p>Election observer group MODELE reported the closing of dozens of polling stations due to security issues.</p>

	<p>Some of the provisions in the new constitution include:</p>

	<ul>
	<li>Strengthen the power of the president to appoint or sack the prime minister and ministers.</li>
	<li>Strengthen the power of the president to dissolve the parliament.</li>
	<li>The power of the parliament will consequentially be somewhat diminished.</li>
	<li>Remove French as Mali's official language.</li>
	<li>Set Mali as an "independent, sovereign, unitary, indivisible, democratic, secular and social republic".</li>
	</ul>
	HTML;


$div_wikipedia_History_of_Mali = new WikipediaContentSection();
$div_wikipedia_History_of_Mali->setTitleText('History of Mali');
$div_wikipedia_History_of_Mali->setTitleLink('https://en.wikipedia.org/wiki/History_of_Mali');
$div_wikipedia_History_of_Mali->content = <<<HTML
	<p>Following the withdrawal of Senegal from the federation in August 1960,
	the former Sudanese Republic became the Republic of Mali on 22 September 1960.</p>
	HTML;

$div_wikipedia_Politics_of_Mali = new WikipediaContentSection();
$div_wikipedia_Politics_of_Mali->setTitleText('Politics of Mali');
$div_wikipedia_Politics_of_Mali->setTitleLink('https://en.wikipedia.org/wiki/Politics_of_Mali');
$div_wikipedia_Politics_of_Mali->content = <<<HTML
	<p>Until the military coups of 2012, the politics of Mali took place in a framework of a semi-presidential representative democratic republic.</p>

	<p>Further coups in 2020 and 2021 hampered Mali's return to democratic regime.</p>
	HTML;

$div_wikipedia_Human_rights_in_Mali = new WikipediaContentSection();
$div_wikipedia_Human_rights_in_Mali->setTitleText('Human rights in Mali');
$div_wikipedia_Human_rights_in_Mali->setTitleLink('https://en.wikipedia.org/wiki/Human_rights_in_Mali');
$div_wikipedia_Human_rights_in_Mali->content = <<<HTML
	<p>Men play a dominant role in society, and women continue to suffer from widespread discrimination and domestic violence.
	Child labour and trafficking in children as forced labour remain serious problems.</p>
	HTML;

$div_wikipedia_Childrens_rights_in_Mali = new WikipediaContentSection();
$div_wikipedia_Childrens_rights_in_Mali->setTitleText('Children\'s rights in Mali');
$div_wikipedia_Childrens_rights_in_Mali->setTitleLink('https://en.wikipedia.org/wiki/Children%27s_rights_in_Mali');
$div_wikipedia_Childrens_rights_in_Mali->content = <<<HTML
	<p>Children's rights in Mali are secured by several laws designed to protect children and provide for their welfare.
	However, this is the official account, based on laws that have only a status on paper.</p>
	HTML;

$div_wikipedia_Women_in_Mali = new WikipediaContentSection();
$div_wikipedia_Women_in_Mali->setTitleText('Women in Mali');
$div_wikipedia_Women_in_Mali->setTitleLink('https://en.wikipedia.org/wiki/Women_in_Mali');
$div_wikipedia_Women_in_Mali->content = <<<HTML
	<p>Contemporary problems faced by women in Mali include high rate of violence against women, child marriage and female genital mutilation.</p>
	HTML;

$div_wikipedia_Human_trafficking_in_Mali = new WikipediaContentSection();
$div_wikipedia_Human_trafficking_in_Mali->setTitleText('Human trafficking in Mali');
$div_wikipedia_Human_trafficking_in_Mali->setTitleLink('https://en.wikipedia.org/wiki/Human_trafficking_in_Mali');
$div_wikipedia_Human_trafficking_in_Mali->content = <<<HTML
	<p>Mali is a source, transit, and destination country for men, women, and children subjected to trafficking in persons,
	specifically forced labor and, to a lesser extent, forced prostitution.</p>
	HTML;


$page->parent('world.html');

$page->body($div_stub);
//$page->body($div_introduction);
$page->body($div_2023_Referendum);

$page->body('Country indices');

$page->body($div_wikipedia_History_of_Mali);
$page->body($div_wikipedia_Politics_of_Mali);
$page->body($div_wikipedia_Human_rights_in_Mali);
$page->body($div_wikipedia_Childrens_rights_in_Mali);
$page->body($div_wikipedia_Women_in_Mali);
$page->body($div_wikipedia_Human_trafficking_in_Mali);
