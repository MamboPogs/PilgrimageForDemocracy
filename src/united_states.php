<?php
$page = new CountryPage('United States');
$page->h1('United States of America');
$page->keywords('United States', 'America', 'USA', 'American');
$page->stars(0);

$page->snp('description', '333 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_United_States = new WikipediaContentSection();
$div_wikipedia_United_States->setTitleText('United States');
$div_wikipedia_United_States->setTitleLink('https://en.wikipedia.org/wiki/United_States');
$div_wikipedia_United_States->content = <<<HTML
	<p>The United States of America (U.S.A. or USA), commonly known as the United States (U.S. or US) or America,
	is a country primarily located in North America and consisting of 50 states, a federal district,
	five major unincorporated territories, nine Minor Outlying Islands, and 326 Indian reservations.</p>
	HTML;

$div_wikipedia_Constitution_of_the_United_States = new WikipediaContentSection();
$div_wikipedia_Constitution_of_the_United_States->setTitleText('Constitution of the United States');
$div_wikipedia_Constitution_of_the_United_States->setTitleLink('https://en.wikipedia.org/wiki/Constitution_of_the_United_States');
$div_wikipedia_Constitution_of_the_United_States->content = <<<HTML
	<p>The Constitution of the United States is the supreme law of the United States of America.
	It superseded the Articles of Confederation, the nation's first constitution, in 1789.</p>
	HTML;

$div_wikipedia_Politics_of_the_United_States = new WikipediaContentSection();
$div_wikipedia_Politics_of_the_United_States->setTitleText('Politics of the United States');
$div_wikipedia_Politics_of_the_United_States->setTitleLink('https://en.wikipedia.org/wiki/Politics_of_the_United_States');
$div_wikipedia_Politics_of_the_United_States->content = <<<HTML
	<p>In the United States, politics function within a framework of a constitutional federal republic
	and presidential system, with three distinct branches that share powers:
	the U.S. Congress which forms the legislative branch, a bicameral legislative body
	comprising the House of Representatives and the Senate;
	the executive branch, which is headed by the president of the United States,
	who serves as the country's head of state and government;
	and the judicial branch, composed of the Supreme Court and lower federal courts, and which exercises judicial power.</p>
	HTML;

$div_wikipedia_Human_rights_in_the_United_States = new WikipediaContentSection();
$div_wikipedia_Human_rights_in_the_United_States->setTitleText('Human rights in the United States');
$div_wikipedia_Human_rights_in_the_United_States->setTitleLink('https://en.wikipedia.org/wiki/Human_rights_in_the_United_States');
$div_wikipedia_Human_rights_in_the_United_States->content = <<<HTML
	<p>In the United States, human rights comprise a series of rights which are
	legally protected by the Constitution of the United States (particularly the Bill of Rights),
	state constitutions, treaty and customary international law, legislation enacted by Congress and state legislatures,
	and state referendums and citizen's initiatives.
	The Federal Government has, through a ratified constitution, guaranteed unalienable rights to its citizens
	and (to some degree) non-citizens.</p>
	HTML;

$div_wikipedia_Elections_in_the_United_States = new WikipediaContentSection();
$div_wikipedia_Elections_in_the_United_States->setTitleText('Elections in the United States');
$div_wikipedia_Elections_in_the_United_States->setTitleLink('https://en.wikipedia.org/wiki/Elections_in_the_United_States');
$div_wikipedia_Elections_in_the_United_States->content = <<<HTML
	<p>In the politics of the United States, elections are held for government officials
	at the federal, state, and local levels.</p>
	HTML;

$div_wikipedia_Voter_identification_laws_in_the_United_States = new WikipediaContentSection();
$div_wikipedia_Voter_identification_laws_in_the_United_States->setTitleText('Voter identification laws in the United States');
$div_wikipedia_Voter_identification_laws_in_the_United_States->setTitleLink('https://en.wikipedia.org/wiki/Voter_identification_laws_in_the_United_States');
$div_wikipedia_Voter_identification_laws_in_the_United_States->content = <<<HTML
	<p>Voter ID laws in the United States are laws that require a person
	to provide some form of official identification before they are permitted to register to vote,
	receive a ballot for an election, or to actually vote in elections in the United States.</p>
	HTML;

$div_wikipedia_Electronic_voting_in_the_United_States = new WikipediaContentSection();
$div_wikipedia_Electronic_voting_in_the_United_States->setTitleText('Electronic voting in the United States');
$div_wikipedia_Electronic_voting_in_the_United_States->setTitleLink('https://en.wikipedia.org/wiki/Electronic_voting_in_the_United_States');
$div_wikipedia_Electronic_voting_in_the_United_States->content = <<<HTML
	<p>Electronic voting in the United States involves several types of machines:
	touchscreens for voters to mark choices, scanners to read paper ballots, scanners to verify signatures on envelopes of absentee ballots,
	and web servers to display tallies to the public.</p>
	HTML;

$div_wikipedia_Election_Day_United_States = new WikipediaContentSection();
$div_wikipedia_Election_Day_United_States->setTitleText('Election Day United States');
$div_wikipedia_Election_Day_United_States->setTitleLink('https://en.wikipedia.org/wiki/Election_Day_(United_States)');
$div_wikipedia_Election_Day_United_States->content = <<<HTML
	<p>Election Day in the United States is the annual day for general elections of federal public officials.
	It is statutorily set by the U.S. government as "the Tuesday next after the first Monday in November",
	i.e. the Tuesday that occurs within November 2 to November 8.</p>
	HTML;

$div_wikipedia_Voter_suppression_in_the_United_States = new WikipediaContentSection();
$div_wikipedia_Voter_suppression_in_the_United_States->setTitleText('Voter suppression in the United States');
$div_wikipedia_Voter_suppression_in_the_United_States->setTitleLink('https://en.wikipedia.org/wiki/Voter_suppression_in_the_United_States');
$div_wikipedia_Voter_suppression_in_the_United_States->content = <<<HTML
	<p>Voter suppression in the United States consists of various legal and illegal efforts
	to prevent eligible citizens from exercising their right to vote.
	Such voter suppression efforts vary by state, local government, precinct, and election.
	Voter suppression has historically been used for racial, economic, gender, age and disability discrimination.</p>
	HTML;


$page->parent('world.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_United_States);
$page->body($div_wikipedia_Constitution_of_the_United_States);
$page->body($div_wikipedia_Politics_of_the_United_States);
$page->body($div_wikipedia_Human_rights_in_the_United_States);
$page->body($div_wikipedia_Elections_in_the_United_States);
$page->body($div_wikipedia_Voter_identification_laws_in_the_United_States);
$page->body($div_wikipedia_Electronic_voting_in_the_United_States);
$page->body($div_wikipedia_Election_Day_United_States);
$page->body($div_wikipedia_Voter_suppression_in_the_United_States);
