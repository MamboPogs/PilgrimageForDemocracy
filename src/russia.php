<?php
$page = new CountryPage('Russia');
$page->h1('Russia');
$page->stars(1);
$page->keywords('Russia');

$page->preview( <<<HTML
	<p>Russia is one of the major threat to global peace and democracy.</p>
	HTML );

$page->snp('description', '145 million inhabitants.');
$page->snp('image', "/copyrighted/natalya-letunova-DLclPZyS_bs.1200-630.jpg");


$div_codeberg = new CodebergContentSection();
$div_codeberg->setTitleText('Improve the article on Russia');
$div_codeberg->setTitleLink('https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/27');
$div_codeberg->content = <<<HTML
	<p>What should be our priority to develop this article about Russia?</p>
	HTML;

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Russia is one of the major threat to global peace and democracy.</p>

	<p>Vladimir Putin changed the $constitution of Russia so that he could remain in power for life.</p>

	<p>Putin's Russia is a major <a href="/democracy_under_attack.html">threat to democracy</a>
	and is waging a war on $Ukraine.</p>
	HTML;


$div_wikipedia_human_rights_russia = new WikipediaContentSection();
$div_wikipedia_human_rights_russia->setTitleText('Human rights in Russia');
$div_wikipedia_human_rights_russia->setTitleLink('https://en.wikipedia.org/wiki/Human_rights_in_Russia');
$div_wikipedia_human_rights_russia->content = <<<HTML
	<p>Human rights violations in Russia have routinely been criticized
	by international organizations and independent domestic media outlets.</p>
	HTML;

$div_wikipedia_russian_war_crimes = new WikipediaContentSection();
$div_wikipedia_russian_war_crimes->setTitleText('Russian war crimes');
$div_wikipedia_russian_war_crimes->setTitleLink('https://en.wikipedia.org/wiki/Russian_war_crimes');
$div_wikipedia_russian_war_crimes->content = <<<HTML
	<p>Russian war crimes since 1991 are the violations of the law of war,
	including the Hague Conventions of 1899 and 1907 and the Geneva Conventions,
	consisting of war crimes, crimes against humanity, and the crime of genocide,
	which the official armed and paramilitary forces of the Russian Federation
	have been accused of committing since the dissolution of the Soviet Union.</p>
	HTML;

$div_wikipedia_memorial_russia = new WikipediaContentSection();
$div_wikipedia_memorial_russia->setTitleText('Memorial (society)');
$div_wikipedia_memorial_russia->setTitleLink('https://en.wikipedia.org/wiki/Memorial_(society)');
$div_wikipedia_memorial_russia->content = <<<HTML
	<p>Memorial is an international human rights organisation,
	founded in Russia during the fall of the Soviet Union
	to study and examine the human rights violations and other crimes committed under Joseph Stalin's reign.</p>
	HTML;

$div_wikipedia_russian_war_crimes_ukraine = new WikipediaContentSection();
$div_wikipedia_russian_war_crimes_ukraine->setTitleText('War crimes in the Russian invasion of Ukraine');
$div_wikipedia_russian_war_crimes_ukraine->setTitleLink('https://en.wikipedia.org/wiki/War_crimes_in_the_Russian_invasion_of_Ukraine');
$div_wikipedia_russian_war_crimes_ukraine->content = <<<HTML
	<p>Since the beginning of the Russian invasion of Ukraine in 2022,
	Russian authorities and armed forces have committed multiple war crimes
	in the form of deliberate attacks against civilian targets, massacres of civilians,
	torture and rape of women and children,[4][5] and indiscriminate attacks in densely populated areas.</p>
	HTML;

$div_wikipedia_russia_traumazone = new WikipediaContentSection();
$div_wikipedia_russia_traumazone->setTitleText('Russia 1985–1999: TraumaZone');
$div_wikipedia_russia_traumazone->setTitleLink('https://en.wikipedia.org/wiki/Russia_1985%E2%80%931999:_TraumaZone');
$div_wikipedia_russia_traumazone->content = <<<HTML
	<p>Using stock footage shot by the BBC,
	the series chronicles the collapse of the Soviet Union, the rise of capitalist Russia and its oligarchs,
	and the effects of this on Russian people of all levels of society, leading to the rise to power of Vladimir Putin.</p>
	HTML;


$div_wikipedia_ICC_russia_ukraine = new WikipediaContentSection();
$div_wikipedia_ICC_russia_ukraine->setTitleText('International Criminal Court investigation in Ukraine');
$div_wikipedia_ICC_russia_ukraine->setTitleLink('https://en.wikipedia.org/wiki/International_Criminal_Court_investigation_in_Ukraine');
$div_wikipedia_ICC_russia_ukraine->content = <<<HTML
	<p>The International Criminal Court investigation in Ukraine
	is an ongoing investigation by the Prosecutor of the International Criminal Court (ICC)
	into war crimes and crimes against humanity that may have occurred since 21 November 2013,
	during the Russo-Ukrainian War, including the 2014 annexation of Crimea by Russia, the war in Donbas and the 2022 Russian invasion of Ukraine.</p>
	HTML;

$div_wikipedia_legality_russian_war_ukraine = new WikipediaContentSection();
$div_wikipedia_legality_russian_war_ukraine->setTitleText('Legality of the Russian invasion of Ukraine');
$div_wikipedia_legality_russian_war_ukraine->setTitleLink('https://en.wikipedia.org/wiki/Legality_of_the_Russian_invasion_of_Ukraine');
$div_wikipedia_legality_russian_war_ukraine->content = <<<HTML
	<p>The Russian invasion of Ukraine violated international law (including the Charter of the United Nations).
	The invasion has also been called a crime of aggression under international criminal law.</p>
	HTML;

$div_wikipedia_censorship_in_russia = new WikipediaContentSection();
$div_wikipedia_censorship_in_russia->setTitleText('Censorship in the Russian Federation');
$div_wikipedia_censorship_in_russia->setTitleLink('https://en.wikipedia.org/wiki/Censorship_in_the_Russian_Federation');
$div_wikipedia_censorship_in_russia->content = <<<HTML
	<p>Censorship is controlled by the Russian government and by civil society in the Russian Federation,
	applying to the content and the diffusion of information
	with the aim of limiting or preventing the dissemination of ideas and information that the Russian state considers to be a danger.</p>
	HTML;



$page->parent('world.html');
$page->body($div_stub);
$page->body($div_codeberg);
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_human_rights_russia);
$page->body($div_wikipedia_censorship_in_russia);
$page->body($div_wikipedia_russian_war_crimes);
$page->body($div_wikipedia_russian_war_crimes_ukraine);
$page->body($div_wikipedia_ICC_russia_ukraine);
$page->body($div_wikipedia_legality_russian_war_ukraine);
$page->body($div_wikipedia_russia_traumazone);
$page->body($div_wikipedia_memorial_russia);
