<?php
$page = new CountryPage('Denmark');
$page->h1('Denmark');
$page->keywords('Denmark');
$page->stars(0);

$page->snp('description', '5.9 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>In 2023, Denmark proposed a bill that could see ban on Quran burnings,
	aimed at ending the provocative stunts.
	See discussion on the article about $blasphemy.</p>
	HTML;

$div_wikipedia_Denmark = new WikipediaContentSection();
$div_wikipedia_Denmark->setTitleText('Denmark');
$div_wikipedia_Denmark->setTitleLink('https://en.wikipedia.org/wiki/Denmark');
$div_wikipedia_Denmark->content = <<<HTML
	<p>Denmark is a Nordic country in Northern Europe.
	It is the metropolitan part of and the most populous constituent of the Kingdom of Denmark,
	a constitutionally unitary state that includes the autonomous territories of the Faroe Islands and Greenland in the North Atlantic Ocean.</p>
	HTML;

$div_wikipedia_Politics_of_Denmark = new WikipediaContentSection();
$div_wikipedia_Politics_of_Denmark->setTitleText('Politics of Denmark');
$div_wikipedia_Politics_of_Denmark->setTitleLink('https://en.wikipedia.org/wiki/Politics_of_Denmark');
$div_wikipedia_Politics_of_Denmark->content = <<<HTML
	<p>The politics of Denmark take place within the framework of a parliamentary representative democracy,
	a constitutional monarchy and a decentralised unitary state in which the monarch of Denmark, Queen Margrethe II, is the head of state.
	Denmark is a nation state.
	Danish politics and governance are characterized by a common striving for broad consensus on important issues,
	within both the political community and society as a whole.</p>
	HTML;

$div_wikipedia_Taxation_in_Denmark = new WikipediaContentSection();
$div_wikipedia_Taxation_in_Denmark->setTitleText('Taxation in Denmark');
$div_wikipedia_Taxation_in_Denmark->setTitleLink('https://en.wikipedia.org/wiki/Taxation_in_Denmark');
$div_wikipedia_Taxation_in_Denmark->content = <<<HTML
	<p>Taxation in Denmark consists of a comprehensive system of direct and indirect taxes.
	Ever since the income tax was introduced in Denmark via a fundamental tax reform in 1903,
	it has been a fundamental pillar in the Danish tax system.
	Today various personal and corporate income taxes yield around two thirds of the total Danish tax revenues,
	indirect taxes being responsible for the last third.
	The state personal income tax is a progressive tax while the municipal income tax is a proportional tax above a certain income level.</p>
	HTML;


$page->parent('world.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_Denmark);
$page->body($div_wikipedia_Politics_of_Denmark);
$page->body($div_wikipedia_Taxation_in_Denmark);
