<?php
$page = new Page();
$page->h1('Voter suppression');
$page->keywords('Voter suppression');
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_Voter_suppression_in_the_USA = new ContentSection();
$div_Voter_suppression_in_the_USA->content = <<<HTML
	<h3>Voter suppression in the $USA</h3>

	<p>The conservative Texas Supreme Court has ruled that Harris County must abolish elections office.</p>

	<p>In June 2023, Texas Republican Governor Greg Abbott signed a law passed by the Republican legislature
	that would abolish the County elections administrator in counties in Texas with more than 4 million residents
	which means that the law applies to just one County in Texas Harris County,
	home to 5 million people, 60 percent of whom are Black or Hispanic and 56 percent of whom voted for Joe Biden in 2020.
	The republican-controlled Texas Supreme Court ruled that Harris County has to disband its Elections office by September 1st
	and give the state Republican Secretary of State oversight of the County's elections.
	Harris county is scheduled to argue against the Law's constitutionality to this Texas Supreme Court at the end of November.
	Harris's County attorney says it's illegal for Texas Republicans to pass a law that applies only to one County
	so this Republican passed law might be illegal
	but Harris County has to dismantle its elections administrator's office ahead of the Texas Supreme court hearing.</p>
	HTML;

$div_wikipedia_Voter_suppression = new WikipediaContentSection();
$div_wikipedia_Voter_suppression->setTitleText('Voter suppression');
$div_wikipedia_Voter_suppression->setTitleLink('https://en.wikipedia.org/wiki/Voter_suppression');
$div_wikipedia_Voter_suppression->content = <<<HTML
	<p>Voter suppression is a strategy used to influence the outcome of an election
	by discouraging or preventing specific groups of people from voting.
	It is distinguished from political campaigning in that campaigning attempts to change likely voting behavior
	by changing the opinions of potential voters through persuasion and organization,
	activating otherwise inactive voters, or registering new supporters.
	Voter suppression, instead, attempts to gain an advantage by reducing the turnout of certain voters.
	Suppression is an anti-democratic tactic associated with authoritarianism.</p>
	HTML;


$page->parent('democracy.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_Voter_suppression_in_the_USA);
$page->body($div_wikipedia_Voter_suppression);
