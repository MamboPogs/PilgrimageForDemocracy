<?php
$page = new Page();
$page->h1('Media');
$page->stars(1);
$page->keywords('Media');

$page->preview( <<<HTML
	<p>A good media environment is critical for a healthy and stable democracy.</p>

	<p>The quality of our knowledge of public matters is
	commensurate with the quality of the media that deliver us the information
	upon which we rely to create our own opinion of what is right and what is wrong, whom to vote for or against, etc.</p>
	HTML );

$page->snp('description', "Media and democracy");
$page->snp('image', "/copyrighted/brotin-biswas-518543.1200-630.jpg");

$h2_introduction = new h2HeaderContent('Introduction');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The quality of our knowledge of public matters is commensurate with the quality of the media that deliver us the information
	upon which we rely to create our own opinion of what is right and what is wrong, whom to vote for or against, etc.</p>

	<p>On one side, we support free speech, the plurality of opinions and systemic checks and balances,
	with the media acting as a counterweight to political and economic powers.<p>

	<p>On the other side, we must find remedies against the excesses of unrestrained free speech, fake news, manipulative media,
	money politics, and media controlled by powerful vested interests.</p>

	<p>We must find a way to strike a balance between these apparently contradictory injunctions.</p>

	<p>How can we, collectively, become media-savvy, foster reliable and trustworthy media, etc.?</p>
	HTML;

$div_Framing_and_narrative = new ContentSection();
$div_Framing_and_narrative->content = <<<HTML
	<h3>Framing and narrative</h3>

	<p>
	The same news item can be framed very differently, depending on the point of view.
	A random incident can be entirely dismissed by one side of the political divide,
	and blown out of proportion by the other.
	Although we are already aware that media and political operatives use such framing and intently crafted narratives,
	we are still often unconsciously subject to their effects.
	We have to renew our efforts to be on guards against the divisive effect media framing can have.
	We have to teach younger generations how to intelligently consume news.</p>
	HTML;


$list = new ListOfPages();
$list->add('foreign_influence_local_media.html');
$list->add('reporters_without_borders.html');
$list->add('information_overload.html');
$list->add('ground_news.html');
$list->add('social_networks.html');
$print_list = $list->print();

$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>List or related pages</h3>

	$print_list
	HTML;



$div_wikipedia_media_democracy = new WikipediaContentSection();
$div_wikipedia_media_democracy->setTitleText('Media democracy');
$div_wikipedia_media_democracy->setTitleLink('https://en.wikipedia.org/wiki/Media_democracy');
$div_wikipedia_media_democracy->content = <<<HTML
	<p>Media democracy is a democratic approach to media studies that advocates for the reform of mass media
	to strengthen public service broadcasting and develop participation in alternative media and citizen journalism
	in order to create a mass media system that informs and empowers all members of society and enhances democratic values.</p>
	HTML;


$div_wikipedia_social_media_politics = new WikipediaContentSection();
$div_wikipedia_social_media_politics->setTitleText('Social media use in politics');
$div_wikipedia_social_media_politics->setTitleLink('https://en.wikipedia.org/wiki/Social_media_use_in_politics');
$div_wikipedia_social_media_politics->content = <<<HTML
	<p>Social media use in politics refers to the use of online social media platforms in political processes and activities.</p>
	HTML;


$div_wikipedia_center_media_democracy = new WikipediaContentSection();
$div_wikipedia_center_media_democracy->setTitleText('Center for Media and Democracy');
$div_wikipedia_center_media_democracy->setTitleLink('https://en.wikipedia.org/wiki/Center_for_Media_and_Democracy');
$div_wikipedia_center_media_democracy->content = <<<HTML
	<p>The Center for Media and Democracy (CMD) is a progressive nonprofit watchdog and advocacy organization based in Madison, Wisconsin.</p>
	HTML;


$div_wikipedia_media_ethics = new WikipediaContentSection();
$div_wikipedia_media_ethics->setTitleText('Media ethics');
$div_wikipedia_media_ethics->setTitleLink('https://en.wikipedia.org/wiki/Media_ethics');
$div_wikipedia_media_ethics->content = <<<HTML
	<p>Media ethics is the subdivision dealing with the specific ethical principles and standards of media, including broadcast media, film, theatre, the arts, print media and the internet.
	Media ethics promotes and defends values such as a universal respect for life and the rule of law and legality.</p>
	HTML;


$div_wikipedia_schuman_center_media_democracy = new WikipediaContentSection();
$div_wikipedia_schuman_center_media_democracy->setTitleText('Schumann Center for Media and Democracy');
$div_wikipedia_schuman_center_media_democracy->setTitleLink('https://en.wikipedia.org/wiki/Schumann_Center_for_Media_and_Democracy');
$div_wikipedia_schuman_center_media_democracy->content = <<<HTML
	<p>The Schumann Center for Media and Democracy was established in 1961, by Florence Ford and John J. Schumann Jr.
	The foundation states that its purpose is to renew the democratic process through cooperative acts of citizenship, especially as they apply to governance, and the environment.</p>
	HTML;





$page->parent('menu.html');
$page->body($div_stub);

$page->body($h2_introduction);
$page->body($div_introduction);
$page->body($div_Framing_and_narrative);

$page->body($div_list);
$page->body('foreign_influence_local_media.html');
$page->body('reporters_without_borders.html');

$page->body('information_overload.html');

$page->body($div_wikipedia_media_democracy);
$page->body($div_wikipedia_social_media_politics);
$page->body($div_wikipedia_media_ethics);
$page->body($div_wikipedia_center_media_democracy);
$page->body($div_wikipedia_schuman_center_media_democracy);
