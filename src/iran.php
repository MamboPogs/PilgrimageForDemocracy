<?php
$page = new CountryPage('Iran');
$page->h1('Iran');
$page->stars(1);
$page->keywords('Iran');

$page->preview( <<<HTML
	<p>Imposition of Islamic law, a continuing economic crisis, lack of freedom of expression,
	violation of women's rights, brutality carried out during protests, internet cutoffs,
	and the killing of Mahsa Amini were some of the reasons for the start of civil protests in Iran in 2021-2022.</p>

	<p>From Cyrus the Great to today, the Iranian people have however a long history and deep democratic aspirations.</p>
	HTML );

$page->snp('description', "87 million inhabitants.");
$page->snp('image', "/copyrighted/morteza-f-shojaei-Yef1y6MuLVU.1200-630.jpg");


$div_codeberg = new CodebergContentSection();
$div_codeberg->setTitleText('Democracy and Human Rights in Iran');
$div_codeberg->setTitleLink('https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/15');
$div_codeberg->content = <<<HTML
	<p>Let us know how to improve this page.</p>
	HTML;



$h2_iran_today = new h2HeaderContent('Iran today');


$div_intro = new ContentSection();
$div_intro->content = <<<HTML
	<p>Imposition of Islamic law, a continuing economic crisis, lack of freedom of expression,
	violation of women's rights, brutality carried out during protests, internet cutoffs,
	and the killing of Mahsa Amini were some of the reasons for the start of civil protests in Iran in 2021-2022.</p>

	<p>Iran is a perpetrator of ${'transnational authoritarianism'}.</p>

	<p>From Cyrus the Great to today, the Iranian people have however a long history and deep democratic aspirations.</p>
	HTML;


$h2_iran_democracy = new h2HeaderContent('Iran and democracy');

$h2_iran_human_rights = new h2HeaderContent('Iran and human rights');




$div_wikipedia_human_rights_islamic_republic_iran = new WikipediaContentSection();
$div_wikipedia_human_rights_islamic_republic_iran->setTitleText('Human rights in the Islamic Republic of Iran');
$div_wikipedia_human_rights_islamic_republic_iran->setTitleLink('https://en.wikipedia.org/wiki/Human_rights_in_the_Islamic_Republic_of_Iran');
$div_wikipedia_human_rights_islamic_republic_iran->content = <<<HTML
	<p>This article is only about human rights in Iran, specifically since 1979.</p>
	HTML;

$div_wikipedia_human_rights_in_iran = new WikipediaContentSection();
$div_wikipedia_human_rights_in_iran->setTitleText('Human rights in Iran');
$div_wikipedia_human_rights_in_iran->setTitleLink('https://en.wikipedia.org/wiki/Human_rights_in_Iran');
$div_wikipedia_human_rights_in_iran->content = <<<HTML
	<p>Human rights in Iran during
	the Imperial Pahlavi dynasty (1925 to 1979),
	and the Islamic Republic (since 1979).
	</p>
	HTML;



$div_wikipedia_history_democracy_classical_iran = new WikipediaContentSection();
$div_wikipedia_history_democracy_classical_iran->setTitleText('History of democracy in classical Iran');
$div_wikipedia_history_democracy_classical_iran->setTitleLink('https://en.wikipedia.org/wiki/History_of_democracy_in_classical_Iran');
$div_wikipedia_history_democracy_classical_iran->content = <<<HTML
	<p>Covers Zoroastrianism,
	the Medes (around 11th century BCE),
	the Achaemenid (9th century BCE),
	and the Arsacid Empire (3rd century BCE).
	</p>
	HTML;



$div_wikipedia_iran_human_rights_org = new WikipediaContentSection();
$div_wikipedia_iran_human_rights_org->setTitleText('Iran Human Rights');
$div_wikipedia_iran_human_rights_org->setTitleLink('https://en.wikipedia.org/wiki/Iran_Human_Rights');
$div_wikipedia_iran_human_rights_org->content = <<<HTML
	<p>Iran Human Rights (IHR) (Persian: سازمان حقوق بشر ایران)
	is a non-profit international non-governmental organization focused on human rights in Iran.
	Founded in 2005, it is a non-partisan and politically independent organisation based in Oslo, Norway.
	</p>

	<p>Official website: <a href="https://www.iranhr.net/en/">iranhr.net - Iran Human Rights</a>.</p>
	HTML;


$div_wikipedia_islam_and_democracy = new WikipediaContentSection();
$div_wikipedia_islam_and_democracy->setTitleText('Islam and democracy');
$div_wikipedia_islam_and_democracy->setTitleLink('https://en.wikipedia.org/wiki/Islam_and_democracy');
$div_wikipedia_islam_and_democracy->content = <<<HTML
	<p>There exist several perspectives on the relationship between Islam and democracy
	among Islamic political theorists, the general Muslim public, and Western authors.</p>

	<p>Polls indicate that majorities in the Muslim world desire a political model where
	democratic institutions and values can coexist with the values and principles of Islam, seeing no contradiction between the two.</p>
	HTML;


$div_wikipedia_winds_of_change = new WikipediaContentSection();
$div_wikipedia_winds_of_change->setTitleText('Winds of Change: The Future of Democracy in Iran');
$div_wikipedia_winds_of_change->setTitleLink('https://en.wikipedia.org/wiki/Winds_of_Change:_The_Future_of_Democracy_in_Iran');
$div_wikipedia_winds_of_change->content = <<<HTML
	<p><em>Winds of Change: The Future of Democracy in Iran</em> is a book written by Prince Reza Pahlavi II, Crown Prince of Iran.
	He advocates the principles of freedom, democracy and human rights for his countrymen.</p>
	HTML;


$div_wikipedia_children_rights_iran = new WikipediaContentSection();
$div_wikipedia_children_rights_iran->setTitleText('Childrens\' Rights in Iran');
$div_wikipedia_children_rights_iran->setTitleLink('https://en.wikipedia.org/wiki/Childrens%27_Rights_in_Iran');
$div_wikipedia_children_rights_iran->content = <<<HTML
	<p>Despite having signed the UN Convention on the Rights of the Child (CRC) in 1991 and ratified it in 1994,
	Iran has not upheld its obligations under the treaty.</p>
	HTML;


$div_wikipedia_defenders_human_rights = new WikipediaContentSection();
$div_wikipedia_defenders_human_rights->setTitleText('Defenders of Human Rights Center');
$div_wikipedia_defenders_human_rights->setTitleLink('https://en.wikipedia.org/wiki/Defenders_of_Human_Rights_Center');
$div_wikipedia_defenders_human_rights->content = <<<HTML
	<p>The Defenders of Human Rights Center is an Iranian human rights organization.
	Based in Tehran, the organization was founded in 2001 and
	has actively defended the rights of women, political prisoners and minorities in Iran.</p>

	<p>Official website: <a href="https://www.humanrights-ir.org/">humanrights-ir.org - Defenders of Human Rights Center</a>.</p>
	HTML;


$div_wikipedia_foundation_democracy_iran = new WikipediaContentSection();
$div_wikipedia_foundation_democracy_iran->setTitleText('Foundation for Democracy in Iran');
$div_wikipedia_foundation_democracy_iran->setTitleLink('https://en.wikipedia.org/wiki/Foundation_for_Democracy_in_Iran');
$div_wikipedia_foundation_democracy_iran->content = <<<HTML
	<p>The Foundation for Democracy in Iran is a private, non-profit organization established in 1995
	with grants from the National Endowment for Democracy (NED), to promote regime change in Iran.</p>

	<p>Official website: <a href="http://www.iran.org/">iran.org - Foundation for Democracy in Iran</a>.</p>
	HTML;




$div_wikipedia_2021_2023_iranian_protests = new WikipediaContentSection();
$div_wikipedia_2021_2023_iranian_protests->setTitleText('2021–2023 Iranian protests');
$div_wikipedia_2021_2023_iranian_protests->setTitleLink('https://en.wikipedia.org/wiki/2021%E2%80%932023_Iranian_protests');
$div_wikipedia_2021_2023_iranian_protests->content = <<<HTML
	<p>Imposition of Islamic law, a continuing economic crisis, lack of freedom of expression, violation of women's rights,
	brutality carried out during protests, internet cutoffs, and the killing of Mahsa Amini
	were some of the reasons for the start of civil protests in Iran in 2021-2022.</p>
	HTML;

$div_wikipedia_mahsa_amini_protests = new WikipediaContentSection();
$div_wikipedia_mahsa_amini_protests->setTitleText('Mahsa Amini protests');
$div_wikipedia_mahsa_amini_protests->setTitleLink('https://en.wikipedia.org/wiki/Mahsa_Amini_protests');
$div_wikipedia_mahsa_amini_protests->content = <<<HTML
	<p>Civil unrest and protests against the government of Iran
	associated with the death in police custody of Mahsa Amini (Persian: مهسا امینی)
	began on 16 September 2022 and are ongoing in 2023.</p>
	HTML;





$page->parent('world.html');
$page->body($div_stub);
$page->body($div_codeberg);

$page->body($h2_iran_today);
$page->body($div_intro);
$page->body('Country indices');
$page->body($div_wikipedia_human_rights_islamic_republic_iran);
$page->body($div_wikipedia_2021_2023_iranian_protests);
$page->body($div_wikipedia_mahsa_amini_protests);


$page->body($h2_iran_human_rights);
$page->body($div_wikipedia_human_rights_in_iran);
$page->body($div_wikipedia_children_rights_iran);
$page->body($div_wikipedia_iran_human_rights_org);
$page->body($div_wikipedia_defenders_human_rights);

$page->body($h2_iran_democracy);
$page->body($div_wikipedia_history_democracy_classical_iran);
$page->body($div_wikipedia_islam_and_democracy);
$page->body($div_wikipedia_winds_of_change);
$page->body($div_wikipedia_foundation_democracy_iran);
