<?php
$page = new Page();
$page->h1('Denise Dresser');
$page->stars(1);
$page->keywords('Denise Dresser');


$page->snp('description', "Mexican writer and university professor");
//$page->snp('image', "/copyrighted/");

$page->preview( <<<HTML
	<p>
	</p>
	HTML );

$r1 = $page->ref('https://www.denisedresser.com/en/biografia', 'Denise Dresser: Biography');
$r2 = $page->ref('https://t.me/United24media/10689', 'Political scientist and prominent columnist Denise Dresser (Mexico) came to Ukraine in June 2023');
$r3 = $page->ref('https://www.youtube.com/watch?v=2Rg-ucdhTTs&ab_channel=NM%C3%A1s', 'Denise Dresser narra su experiencia tras su visita a Zelenski en Ucrania - Es la Hora de Opinar');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Denise Dresser is a Mexican writer, and university professor.</p>

	<p>She was recently awarded the Legion of Honor by the French government
	for her work on democracy, justice, gender equality and human rights. ${r1}</p>

	<p>Dresser visited Ukraine in June 2023 to show her support,
	and stated that her home country of Mexico could learn from Ukraine,
	in their determination to protect their democracy. ${r2}${r3}</p>

	<p>She quoted ${'Anne Applebaum'} as saying:
	"<em>People too often think that democracy is like water from the tap:
	you open it; it comes out; and it will always be there.</em>".</p>

	<p>In the same interview, Dresser said:</p>

	<blockquote>
	There is a wave of democratic backsliding in Latin America.
	You should be worried when presidents try to take control of courts,
	when the media is silenced,
	when journalists are assassinated.
	(...)
	Democracy can die very slowly
	and the suddenly, the water starts to become only a drip.
	</blockquote>

	HTML;

$div_media_appearances = new ContentSection();
$div_media_appearances->content = <<<HTML
	<h3>Media appearances</h3>

	<p>Denise Dresser has made numerous media appearances and given many speeches in Spanish.
	Here is a video where she speaks in English:</p>

	<ul>
		<li><a href="https://www.youtube.com/watch?v=cDR2K1syllA&ab_channel=UniversityofCaliforniaTelevision%28UCTV%29">
			Mexico Under The New PRI: The Good The Bad and The Ugly with Denise Dresser</a></li>
	</ul>
	HTML;


$div_Denise_Dresser_website = new WebsiteContentSection();
$div_Denise_Dresser_website->setTitleText('Denise Dresser website ');
$div_Denise_Dresser_website->setTitleLink('https://www.denisedresser.com/en');
$div_Denise_Dresser_website->content = <<<HTML
	HTML;




$div_wikipedia_Denise_Dresser = new WikipediaContentSection();
$div_wikipedia_Denise_Dresser->setTitleText('Denise Dresser');
$div_wikipedia_Denise_Dresser->setTitleLink('https://en.wikipedia.org/wiki/Denise_Dresser');
$div_wikipedia_Denise_Dresser->content = <<<HTML
	<p>Denise Eugenia Dresser Guerra is a Mexican writer, and university professor.
	She is currently a faculty member of the Department of Political Science at the Instituto Tecnológico Autónomo de México.</p>
	HTML;

$page->parent('list_of_people.html');
$page->body($div_stub);

$page->body($div_introduction);
$page->body($div_media_appearances);

$page->body($div_Denise_Dresser_website);
$page->body($div_wikipedia_Denise_Dresser);
