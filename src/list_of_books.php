<?php
$page = new Page();
$page->h1('List of books');
$page->stars(0);
$page->keywords('books', 'book');

$page->snp('description', "Books about democracy, social justice, history, global issues...");
//$page->snp('image', "/copyrighted/");


$page->preview( <<<HTML
	<p>
	</p>
	HTML );


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Books about democracy, social justice, history, global issues...</p>
	HTML;

$list = new ListOfPages();
$list->add('the_art_of_thinking_clearly.html');
$list->add('the_four_agreements.html');
$list->add('the_remains_of_the_day.html');
$list->add('twilight_of_democracy.html');
$print_list = $list->print();


$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>List</h3>

	$print_list
	HTML;

$page->parent('lists.html');

$page->body($div_introduction);
$page->body($div_list);

// Featured
$page->body('the_remains_of_the_day.html');
