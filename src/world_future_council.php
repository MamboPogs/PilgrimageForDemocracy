<?php
$page = new Page();
$page->h1('World Future Council');
$page->keywords('World Future Council', 'WFC');
$page->stars(2);

$page->preview( <<<HTML
	<p>The World Future Council (WFC) is a German non-profit foundation..
	It works to pass on a healthy and sustainable planet with just and peaceful societies to future generations.</p>
	HTML );


$page->snp('description', 'Policy solutions for a sustainable and peaceful future.');
//$page->snp('image',       '/copyrighted/');

$r1 = $page->ref('https://www.worldfuturecouncil.org/future-policy-award/', 'worldfuturecouncil.org: Future Policy Award');
$r2 = $page->ref('https://www.worldfuturecouncil.org/how-does-the-future-policy-award-work/#7-principles', '7 principles for future-just lawmaking');
$r3 = $page->ref('https://www.futurepolicy.org/award/', 'futurepolicy.org: Future Policy Award');
$r4 = $page->ref('https://www.futurepolicy.org/future-just-lawmaking-methodology/', 'Future-Just Lawmaking Principles');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The World Future Council (WFC) is a non-profit foundation
	working towards a healthy and sustainable planet with just and peaceful societies – now and in the future.
	To achieve this, they find and disseminate effective solutions for climate protection, child protection, sustainable ecosystems and much more.</p>

	<p>Their projects cover the following topics:</p>
	<ul>
		<li>Future Generations</li>
		<li>Rights of Children and Youth</li>
		<li>Energy & Just Development</li>
		<li>Ecosystems & Livelihoods</li>
		<li>Peace & Disarmament</li>
	</ul>
	HTML;



$div_wpc_World_Future_Council_website = new WorldPolicyCouncilContentSection();
$div_wpc_World_Future_Council_website->setTitleText('World Future Council');
$div_wpc_World_Future_Council_website->setTitleLink('https://www.worldfuturecouncil.org/');
$div_wpc_World_Future_Council_website->content = <<<HTML
	<p>The World Future Council official website.</p>
	HTML;


$div_Future_Policy_Award = new ContentSection();
$div_Future_Policy_Award->content = <<<HTML
	<h3>Future Policy Award</h3>

	<p>Since 2009, the World Future Council has been honouring effective policies
	that promote better living conditions for current and future generations
	with the Future Policy Award (FPA), known as the Oscar on best policies.
	Each time, the Award is dedicated to a different, pressing topic. $r1 $r3</p>

	<p>The WFC has developed a methodology for future-just lawmaking
	that supports legislators in the conception, revision and evaluation of policies.
	The methodology is based on the seven principles for sustainable development
	presented by the International Law Association and adopted at the 2002 Earth Summit: $r2 $r4</p>

	<ol>
		<li>Sustainable use of natural resources</li>
		<li>Equity and $poverty eradication</li>
		<li>Precautionary approach to human health, natural resources and ecosystems</li>
		<li>Public participation, access to information and justice</li>
		<li>Good governance and human security</li>
		<li>Integration and interrelationship</li>
		<li>Common but differentiated responsibilities</li>
	</ol>
	HTML;


$div_Future_Policy = new ContentSection();
$div_Future_Policy->content = <<<HTML
	<h3>Future Policy</h3>

	<p>The World Future Council (WFC) operates the futurepolicy.org website.</p>

	<p>The WFC is one of the $giants on whose shoulders the $Pilgrimage stands,
	We integrate resources on public policy from the World Future Council into this website,
	as can be seen for example on the articles about ${'Costa Rica'}, the $Philippines or about $education.</p>
	HTML;



$div_wpc_Future_Policy_org = new FuturePolicyContentSection();
$div_wpc_Future_Policy_org->setTitleText('Future Policy.org ');
$div_wpc_Future_Policy_org->setTitleLink('https://www.futurepolicy.org/');
$div_wpc_Future_Policy_org->content = <<<HTML
	<p>FuturePolicy.org is an online database designed for forward-thinking policy-makers,
	to simplify the sharing of existing and proven policy solutions
	to tackle the world’s most fundamental and urgent problems.</p>
	HTML;



$div_wikipedia_World_Future_Council = new WikipediaContentSection();
$div_wikipedia_World_Future_Council->setTitleText('World Future Council');
$div_wikipedia_World_Future_Council->setTitleLink('https://en.wikipedia.org/wiki/World_Future_Council');
$div_wikipedia_World_Future_Council->content = <<<HTML
	<p>The World Future Council (WFC) is a German non-profit foundation..
	It works to pass on a healthy and sustainable planet with just and peaceful societies to future generations.</p>
	HTML;


$page->parent('list_of_organisations.html');

$page->body($div_stub);

$page->body($div_introduction);
$page->body($div_wpc_World_Future_Council_website);

$page->body($div_Future_Policy_Award);

$page->body($div_Future_Policy);
$page->body($div_wpc_Future_Policy_org);


$page->body($div_wikipedia_World_Future_Council);
$page->body('institutions.html');
