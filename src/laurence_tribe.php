<?php
$page = new Page();
$page->h1('Laurence Tribe');
$page->keywords('Laurence Tribe');
$page->stars(0);

$page->snp('description', 'American constitutional legal scholar.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Laurence Tribe is an American legal scholar, specializing in $constitutional law.
	He is a co-founder of the ${'American Constitution Society'}.</p>

	<p>Tribe, together with conservative judge ${'Michael Luttig'}, argues that Donald Trump is illegible to be candidate
	in the 2024 presidential elections because of the 14th amendment of the US $constitution.</p>
	HTML;

$div_wikipedia_Laurence_Tribe = new WikipediaContentSection();
$div_wikipedia_Laurence_Tribe->setTitleText('Laurence Tribe');
$div_wikipedia_Laurence_Tribe->setTitleLink('https://en.wikipedia.org/wiki/Laurence_Tribe');
$div_wikipedia_Laurence_Tribe->content = <<<HTML
	<p>Laurence Henry Tribe is an American legal scholar who is a University Professor Emeritus at Harvard University.
	He previously served as the Carl M. Loeb University Professor at Harvard Law School.</p>

	<p>A constitutional law scholar, Tribe is co-founder of the American Constitution Society.
	He is also the author of American Constitutional Law (1978), a major treatise in that field,
	and has argued before the United States Supreme Court 36 times.
	Tribe was elected to the American Philosophical Society in 2010.</p>
	HTML;


$page->parent('list_of_people.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_wikipedia_Laurence_Tribe);
