<?php
$page = new Page();
$page->h1('Economic, social and cultural rights');
$page->keywords('rights', 'right');
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$list = new ListOfPages();
$list->add('democracy.html');
$list->add('housing.html');
$list->add('social_justice.html');
$print_list = $list->print();

$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>Related content</h3>

	$print_list
	HTML;

$div_wikipedia_Economic_social_and_cultural_rights = new WikipediaContentSection();
$div_wikipedia_Economic_social_and_cultural_rights->setTitleText('Economic social and cultural rights');
$div_wikipedia_Economic_social_and_cultural_rights->setTitleLink('https://en.wikipedia.org/wiki/Economic,_social_and_cultural_rights');
$div_wikipedia_Economic_social_and_cultural_rights->content = <<<HTML
	<p>Economic, social and cultural rights (ESCR) are socio-economic human rights,
	such as the right to education, right to housing, right to an adequate standard of living,
	right to health, victims' rights and the right to science and culture.</p>
	HTML;


$page->parent('social_justice.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body($div_list);


$page->body($div_wikipedia_Economic_social_and_cultural_rights);
