<?php
$page = new Page();
$page->h1('List of indices');
$page->stars(0);

$page->preview( <<<HTML
	<p>All kinds of rating and indices about democracy, peace, freedom, justice etc.
	published by a variety of organisations.</p>
	HTML );


$page->snp('description', "Indices about democracy, peace, freedom, justice, etc.");
//$page->snp('image', "/copyrighted/");



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Here are all kinds of rating and indices about democracy, peace, freedom, justice etc.
	published by a variety of organisations.</p>
	HTML;

$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>List</h3>

	<ul>
	<li>${'Democracy Index'}: for each country in the $world, we display the Democracy Index published by the Economist Intelligence Unit.</li>
	<li>${'Freedom House'}: for each country in the $world, we display the Freedom Index and the Internet freedom index published by the Freedom House.</li>
	<li>${'Global Peace Index'}, published by the ${'Institute for Economics and Peace'}.</li>
	<li>${'Press Freedom Index'}, published by ${'Reporters Without Borders'}.</li>
	<li>${'Corruption Perception Index'}, publish by ${'Transparency International'}.</li>
	</ul>
	HTML;

$list = new ListOfPages();
$list->add('economist_democracy_index.html');
$list->add('freedom_house.html');
$list->add('global_peace_index.html');
$list->add('reporters_without_borders.html');
$list->add('world_happiness_report.html');
$print_list = $list->print();

$div_list_pages = new ContentSection();
$div_list_pages->content = <<<HTML
	$print_list
	HTML;



$div_wikipedia_Democracy_indices = new WikipediaContentSection();
$div_wikipedia_Democracy_indices->setTitleText('Democracy indices');
$div_wikipedia_Democracy_indices->setTitleLink('https://en.wikipedia.org/wiki/Democracy_indices');
$div_wikipedia_Democracy_indices->content = <<<HTML
	<p>Democracy indices are quantitative and comparative assessments of the state of democracy
	for different countries according to various definitions of democracy.</p>
	HTML;

$div_wikipedia_List_of_freedom_indices = new WikipediaContentSection();
$div_wikipedia_List_of_freedom_indices->setTitleText('List of freedom indices');
$div_wikipedia_List_of_freedom_indices->setTitleLink('https://en.wikipedia.org/wiki/List_of_freedom_indices');
$div_wikipedia_List_of_freedom_indices->content = <<<HTML
	<p>This article contains a list of freedom indices produced by several non-governmental organizations
	that publish and maintain assessments of the state of freedom in the world, according to their own various definitions of the term.</p>
	HTML;

$page->parent('lists.html');

$page->body($div_introduction);
$page->body($div_list);
$page->body($div_list_pages);

$page->body($div_wikipedia_Democracy_indices);
$page->body($div_wikipedia_List_of_freedom_indices);
