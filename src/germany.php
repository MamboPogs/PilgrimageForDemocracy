<?php
$page = new CountryPage('Germany');
$page->h1('Germany');
$page->stars(0);
$page->keywords('Germany');

$page->snp('description', '84 million inhabitants.');
$page->snp('image', "/copyrighted/");

$page->preview( <<<HTML
	HTML );


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	HTML;


$page->parent('world.html');
$page->body($div_stub);
$page->body('Country indices');
$page->body($div_introduction);
