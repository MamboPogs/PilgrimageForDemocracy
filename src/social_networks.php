<?php
$page = new Page();
$page->h1('Social networks');
$page->keywords('Social networks', 'social networks');
$page->stars(0);

$page->snp('description', 'Social networks and democracy.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The negative impact that social networks have had on our democracies is hard to overstate,
	between their impact on Brexit, the election of Donald Trump, etc..</p>

	<p>Facebook has even been accused of having contributed, by lack of moderation, to the genocide of the ${'Rohingya people'}.<p>
	HTML;

$div_wikipedia_Social_network = new WikipediaContentSection();
$div_wikipedia_Social_network->setTitleText('Social network');
$div_wikipedia_Social_network->setTitleLink('https://en.wikipedia.org/wiki/Social_network');
$div_wikipedia_Social_network->content = <<<HTML
	<p>A social network is a social structure made up of a set of social actors (such as individuals or organizations),
	sets of dyadic ties, and other social interactions between actors.</p>
	HTML;


$div_wikipedia_Social_networking_service = new WikipediaContentSection();
$div_wikipedia_Social_networking_service->setTitleText('Social networking service');
$div_wikipedia_Social_networking_service->setTitleLink('https://en.wikipedia.org/wiki/Social_networking_service');
$div_wikipedia_Social_networking_service->content = <<<HTML
	<p>A social networking service or SNS (sometimes called a social networking site)
	is a type of online social media platform which people use to build social networks or social relationships
	with other people who share similar personal or career content, interests, activities, backgrounds or real-life connections.</p>
	HTML;




$div_wikipedia_Social_media = new WikipediaContentSection();
$div_wikipedia_Social_media->setTitleText('Social media');
$div_wikipedia_Social_media->setTitleLink('https://en.wikipedia.org/wiki/Social_media');
$div_wikipedia_Social_media->content = <<<HTML
	<p>Social media are interactive technologies that facilitate the creation and sharing of information, ideas, interests,
	and other forms of expression through virtual communities and networks.</p>
	HTML;



$div_wikipedia_Sociology_of_the_Internet = new WikipediaContentSection();
$div_wikipedia_Sociology_of_the_Internet->setTitleText('Sociology of the Internet');
$div_wikipedia_Sociology_of_the_Internet->setTitleLink('https://en.wikipedia.org/wiki/Sociology_of_the_Internet');
$div_wikipedia_Sociology_of_the_Internet->content = <<<HTML
	<p>The sociology of the Internet involves the application of sociological theory
	and method to the Internet as a source of information and communication.
	The overlapping field of digital sociology focuses on understanding the use of digital media
	as part of everyday life,
	and how these various technologies contribute to patterns of human behavior, social relationships, and concepts of the self.
	Sociologists are concerned with the social implications of the technology;
	new social networks, virtual communities and ways of interaction that have arisen,
	as well as issues related to cyber crime.</p>
	HTML;



$page->parent('media.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_wikipedia_Social_network);
$page->body($div_wikipedia_Social_networking_service);
$page->body($div_wikipedia_Social_media);
$page->body($div_wikipedia_Sociology_of_the_Internet);
