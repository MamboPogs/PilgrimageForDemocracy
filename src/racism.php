<?php
$page = new Page();
$page->h1('Racism');
$page->keywords('Racism', 'racism', 'racist', 'Racist');
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Racism is unfortunately prevalent throughout the world.</p>

	<p>While interracial problems in the USA and other Western countries are well known,
	${'Human Rights Watch'} also found an increasing prevalence of racist content targeting Black people
	on $China's social media platforms.</p>

	<p>Race and $religion are the subject of numerous discrimination.
	One should not use either to explain phenomenons that can be explained by other means.</p>
	HTML;

$div_wikipedia_Racism = new WikipediaContentSection();
$div_wikipedia_Racism->setTitleText('Racism');
$div_wikipedia_Racism->setTitleLink('https://en.wikipedia.org/wiki/Racism');
$div_wikipedia_Racism->content = <<<HTML
	<p>Racism is discrimination and prejudice towards people based on their race or ethnicity.
	Racism can be present in social actions, practices, or political systems (e.g. apartheid)
	that support the expression of prejudice or aversion in discriminatory practices.</p>
	HTML;


$page->parent('social_justice.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_wikipedia_Racism);
