<?php
$page = new Page();
$page->h1('One country, two systems');
$page->keywords('One country, two systems', 'one country, two systems');
$page->stars(0);

$page->preview( <<<HTML
	<p>Anti-democratic policy of the ${'Chinese Communist Party'},
	applied in ${'Hong Kong'} and meant as a strategy to take over $Taiwan.</p>
	HTML );

$page->snp('description', 'Anti-democratic policy of the Chinese Communist Party.');
//$page->snp('image',       '/copyrighted/');



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Hong Kong was supposed to be the showcase for the ${"People's Republic of China"}'s policy
	of ${'one country, two systems'}, which the PRC wanted to apply to $Taiwan (Republic of China).
	However, the crackdown of the 2019 pro-democracy protests showed the complete failure of the intrinsically undemocratic policy.</p>
	HTML;


$div_taiwan_is_not_hong_kong_one_country_two_systems = new WebsiteContentSection();
$div_taiwan_is_not_hong_kong_one_country_two_systems->setTitleText('Why Taiwan Is Not Hong Kong: A Review of the PRC\'s "One Country Two Systems" Model for Reunification with Taiwan');
$div_taiwan_is_not_hong_kong_one_country_two_systems->setTitleLink('https://digitalcommons.law.uw.edu/wilj/vol6/iss3/2/');
$div_taiwan_is_not_hong_kong_one_country_two_systems->content = <<<HTML
	<p>This article critically examines the "One Country Two Systems" model (OCTS) developed by the People's Republic of China (PRC)
	for achieving the reunification of Taiwan.
	The model is in many respects the same as that already applied in Hong Kong.
	The PRC promises that under OCTS, the Taiwanese will enjoy a "high degree of autonomy",
	be "masters in their own house" and maintain their way of life.
	However, in contrast to the people of Hong Kong, who have never enjoyed full democracy,
	the Taiwanese have achieved a much greater degree of autonomy and accountability than is possible under OCTS.
	The OCTS model cannot herefore deliver what it promises.
	The Article demonstrates this by comparing OCTS as elaborated in the Basic Law of Hong Kong
	with the current constitutional and political arrangements in Taiwan.</p>
	HTML;


$div_wikipedia_One_country_two_systems = new WikipediaContentSection();
$div_wikipedia_One_country_two_systems->setTitleText('One country two systems');
$div_wikipedia_One_country_two_systems->setTitleLink('https://en.wikipedia.org/wiki/One_country,_two_systems');
$div_wikipedia_One_country_two_systems->content = <<<HTML
	<p>"One country, two systems" was a constitutional principle of the People's Republic of China (PRC)
	describing the governance of the special administrative regions of Hong Kong and Macau.</p>

	<p>The "one country, two systems" principle has also been proposed by the PRC government for Taiwan,
	but the government of the Republic of China has refused this suggestion.
	It has also been previously claimed that the system was originally designed for Taiwan in order for it to be unified with the PRC.</p>
	HTML;

$div_wikipedia_2019_2020_Hong_Kong_protests = new WikipediaContentSection();
$div_wikipedia_2019_2020_Hong_Kong_protests->setTitleText('2019 2020 Hong Kong protests');
$div_wikipedia_2019_2020_Hong_Kong_protests->setTitleLink('https://en.wikipedia.org/wiki/2019–2020_Hong_Kong_protests');
$div_wikipedia_2019_2020_Hong_Kong_protests->content = <<<HTML
	<p>The 2019–2020 Hong Kong protests were a series of demonstrations
	against the Hong Kong government's introduction of a bill to amend the Fugitive Offenders Ordinance in regard to extradition.
	It was the largest series of demonstrations in the history of Hong Kong.</p>
	HTML;

$div_wikipedia_2019_Hong_Kong_extradition_bill = new WikipediaContentSection();
$div_wikipedia_2019_Hong_Kong_extradition_bill->setTitleText('2019 Hong Kong extradition bill');
$div_wikipedia_2019_Hong_Kong_extradition_bill->setTitleLink('https://en.wikipedia.org/wiki/2019_Hong_Kong_extradition_bill');
$div_wikipedia_2019_Hong_Kong_extradition_bill->content = <<<HTML
	<p>The bill was proposed by the Hong Kong government in February 2019
	to establish a mechanism for transfers of fugitives not only for Taiwan, but also for Mainland China and Macau,
	which are currently excluded in the existing laws.</p>
	HTML;

$div_wikipedia_Political_status_of_Taiwan = new WikipediaContentSection();
$div_wikipedia_Political_status_of_Taiwan->setTitleText('Political status of Taiwan');
$div_wikipedia_Political_status_of_Taiwan->setTitleLink('https://en.wikipedia.org/wiki/Political_status_of_Taiwan');
$div_wikipedia_Political_status_of_Taiwan->content = <<<HTML
	<p>The controversy surrounding the political status of Taiwan or the Taiwan issue is a result of World War II,
	the second phase of the Chinese Civil War (1945–1949), and the Cold War.
	The basic issue hinges on whom the islands of Taiwan, Penghu, Kinmen, and Matsu should be administered by.</p>
	HTML;


$page->parent('prc_china.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_taiwan_is_not_hong_kong_one_country_two_systems);
$page->body('taiwan.html');

$page->body($div_wikipedia_One_country_two_systems);
$page->body($div_wikipedia_2019_2020_Hong_Kong_protests);
$page->body($div_wikipedia_2019_Hong_Kong_extradition_bill);
$page->body($div_wikipedia_Political_status_of_Taiwan);
