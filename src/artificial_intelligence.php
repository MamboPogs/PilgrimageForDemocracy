<?php
$page = new Page();
$page->h1('Artificial intelligence');
$page->keywords('Artificial intelligence', 'artificial intelligence', 'AI');
$page->stars(1);

$page->snp('description', 'What impact can AI have on democracy and social justice?');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>${'Mo Gawdat'}, former chief business officer for Google X, suggested to $tax
	Artificial intelligence businesses at 98% to support the people who are going to be displaced by AI,
	and to deal with the economic and social consequences of the technology.</p>

	<p>Check the $OECD's policy observatory on artificial intelligence (featured below).</p>
	HTML;

$list = new ListOfPages();
$list->add('mo_gawdat.html');
$print_list = $list->print();

$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>Related topics</h3>

	$print_list
	HTML;


$div_Will_Robots_Automate_Your_Job_Away = new WebsiteContentSection();
$div_Will_Robots_Automate_Your_Job_Away->setTitleText('Will Robots Automate Your Job Away? Full Employment, Basic Income, and Economic Democracy');
$div_Will_Robots_Automate_Your_Job_Away->setTitleLink('https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3044448');
$div_Will_Robots_Automate_Your_Job_Away->content = <<<HTML
	<p>Will the internet, robotics and artificial intelligence mean a ‘jobless future’?
	A recent narrative, endorsed by prominent tech-billionaires, says we face mass unemployment, and we need a basic income.
	In contrast, this article shows why the law can achieve full employment with fair incomes, and holidays with pay.
	Universal human rights, including the right to ‘share in scientific advancement and its benefits’, set the proper guiding principles.
	Three distinct views of the causes of unemployment are that it is a ‘natural’ phenomenon, that technology may propel it,
	or that it is social and legal choice: to let capital owners restrict investment in jobs.
	Only the third view has any credible evidence to support it.
	Technology may create redundancies, but unemployment is an entirely social phenomenon.
	After World War Two, 42% of UK jobs were redundant but social policy maintained full employment, and it can be done again.
	This said, transition to new technology, when markets are left alone, can be exceedingly slow:
	a staggering 88% of American horses lost their jobs after the Model T Ford, but only over 45 years.
	Taking lessons from history, it is clear that unemployment is driven by inequality of wealth and of votes in the economy.
	To uphold human rights, governments should reprogramme the law, for full employment, fair incomes and reduced working time, on a living planet.
	Robot owners will not automate your job away, if we defend economic democracy.</p>
	HTML;


$div_Ex_Google_Officer_Finally_Speaks_Out_On_The_Dangers_Of_AI = new WebsiteContentSection();
$div_Ex_Google_Officer_Finally_Speaks_Out_On_The_Dangers_Of_AI->setTitleText('Ex-Google Officer Finally Speaks Out On The Dangers Of AI ');
$div_Ex_Google_Officer_Finally_Speaks_Out_On_The_Dangers_Of_AI->setTitleLink('https://www.youtube.com/watch?v=bk-nQ7HF6k4');
$div_Ex_Google_Officer_Finally_Speaks_Out_On_The_Dangers_Of_AI->content = <<<HTML
	<p>In this new episode Steven sits down with the Egyptian entrepreneur and writer, Mo Gawdat.</p>
	HTML;



$div_OECD_AI_Policy_Observatory = new WebsiteContentSection();
$div_OECD_AI_Policy_Observatory->setTitleText('OECD: AI Policy Observatory ');
$div_OECD_AI_Policy_Observatory->setTitleLink('https://www.oecd.org/digital/artificial-intelligence/');
$div_OECD_AI_Policy_Observatory->content = <<<HTML
	<p>Artificial intelligence (AI) is transforming every aspect of our lives.
	It influences how we work and play.
	It promises to help solve global challenges like climate change and access to quality medical care.
	Yet AI also brings real challenges for governments and citizens alike.</p>

	<p>As it permeates economies and societies, what sort of policy and $institutional frameworks should guide AI design and use,
	and how can we ensure that it benefits society as a whole?</p>

	<p>The $OECD supports governments by measuring and analysing the economic and $social impacts of AI technologies and applications,
	and engaging with all stakeholders to identify good practices for public policy.</p>
	HTML;



$div_wikipedia_Artificial_intelligence = new WikipediaContentSection();
$div_wikipedia_Artificial_intelligence->setTitleText('Artificial intelligence');
$div_wikipedia_Artificial_intelligence->setTitleLink('https://en.wikipedia.org/wiki/Artificial_intelligence');
$div_wikipedia_Artificial_intelligence->content = <<<HTML
	<p>Artificial intelligence (AI) is the intelligence of machines or software,
	as opposed to the intelligence of human beings or animals.
	AI applications include advanced web search engines (e.g., Google Search),
	recommendation systems (used by YouTube, Amazon, and Netflix),
	understanding human speech (such as Siri and Alexa),
	self-driving cars (e.g., Waymo),
	generative or creative tools (ChatGPT and AI art),
	and competing at the highest level in strategic games (such as chess and Go).</p>
	HTML;


$page->parent('technology_and_democracy.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body($div_list);


$page->body($div_Will_Robots_Automate_Your_Job_Away);
$page->body($div_Ex_Google_Officer_Finally_Speaks_Out_On_The_Dangers_Of_AI);
$page->body($div_OECD_AI_Policy_Observatory);

$page->body($div_wikipedia_Artificial_intelligence);
