<?php
$page = new Page();
$page->h1('Green New Deal Network');
$page->keywords('Green New Deal Network');
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>See also: ${'Green New Deal'}.</p>
	HTML;



$div_Green_New_Deal_Network_website = new WebsiteContentSection();
$div_Green_New_Deal_Network_website->setTitleText('Green New Deal Network website ');
$div_Green_New_Deal_Network_website->setTitleLink('https://www.greennewdealnetwork.org/');
$div_Green_New_Deal_Network_website->content = <<<HTML
	<p>The Green New Deal Network is a 50-state campaign with a national table of 14 organizations:
	Center for Popular Democracy, Climate Justice Alliance, Grassroots Global Justice Alliance, Greenpeace,
	Indigenous Environmental Network, Indivisible, Movement for Black Lives, MoveOn, People’s Action,
	Right To The City Alliance, Service Employees International Union, Sunrise Movement, US Climate Action Network,
	and the Working Families Party.</p>
	HTML;

$page->body($div_Green_New_Deal_Network_website);

$page->parent('menu.html');
$page->body($div_stub);
$page->body($div_introduction);


