<?php
$page = new Page();
$page->h1('Volodymyr Zelensky');
$page->keywords('Volodymyr Zelensky', 'Zelensky');
$page->stars(0);


$page->snp('description', 'President of Ukraine.');
//$page->snp('image',       '/copyrighted/');


$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Volodymyr Zelensky is the president of $Ukraine and a hero of Ukrainian freedom and $democracy.</p>

	<p>In a televised interview on Aug. 27 2023, President Zelensky said he has submitted a proposal to parliament
	to equate $corruption with treason while martial law is in effect in Ukraine.</p>
	HTML;

$div_wikipedia_Volodymyr_Zelenskyy = new WikipediaContentSection();
$div_wikipedia_Volodymyr_Zelenskyy->setTitleText('Volodymyr Zelenskyy');
$div_wikipedia_Volodymyr_Zelenskyy->setTitleLink('https://en.wikipedia.org/wiki/Volodymyr_Zelenskyy');
$div_wikipedia_Volodymyr_Zelenskyy->content = <<<HTML
	<p>Volodymyr Zelenskyy (born 25 January 1978) is a Ukrainian politician and former comedian and actor
	who is the sixth and current president of Ukraine since 2019.</p>
	HTML;


$page->parent('list_of_people.html');
$page->parent('ukraine.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_wikipedia_Volodymyr_Zelenskyy);
