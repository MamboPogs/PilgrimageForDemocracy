<?php
$page = new Page();
$page->h1('Justice');
$page->stars(0);
$page->keywords('Justice', 'justice', 'judiciary');

$page->preview( <<<HTML
	<p>A strong judiciary is important in any democracy,
	as it can balance the powers of the executive and of the legislative.</p>
	HTML );

$list = new ListOfPages();
$list->add('bail.html');
$list->add('criminal_justice.html');
$list->add('obstruction_of_justice.html');
$list->add('whistleblowers.html');
$print_list = $list->print();

$h2_introduction = new h2HeaderContent('Introduction');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>A strong judiciary is important in any democracy,
	as it can balance the powers of the executive and of the legislative.</p>

	<p>Topics that can be covered are:
	judge appointments and terms,
	independence of the judges and the judiciary,
	as well as the potential corruption of the system and of individual judges.</p>

	$print_list
	HTML;



$div_wikipedia_Justice = new WikipediaContentSection();
$div_wikipedia_Justice->setTitleText('Justice');
$div_wikipedia_Justice->setTitleLink('https://en.wikipedia.org/wiki/Justice');
$div_wikipedia_Justice->content = <<<HTML
	<p>To achieve justice, individuals should receive that which they deserve,
	with the interpretation of what "deserve" means, in turn, drawing on numerous viewpoints and perspectives,
	including fields like ethics, rationality, law, religion, equity and fairness.</p>
	HTML;



$page->parent('institutions.html');
$page->body($div_stub);
$page->body($h2_introduction);
$page->body($div_introduction);

$page->body($div_wikipedia_Justice);
