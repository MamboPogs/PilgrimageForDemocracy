<?php
$page = new Page();
$page->h1('Obstruction of justice');
$page->keywords('Obstruction of justice', 'obstruction of justice', 'obstruction');
$page->stars(1);

$page->snp('description', 'Obstruction in breach of the separation of powers.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );


$r1 = $page->ref('https://www.youtube.com/watch?v=6p1NTpJQm00', 'D.A. Fani Willis Arrested Trump: See how Georgia GOP is trying to remove her');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>In this article, we shall not focus on obstruction of justice by common criminals (See the Wikipedia article on that topic),
	but on cases where obstruction of justice is or may be conducted by officials from other branches of government,
	with disregard for the ${'separation of powers'}.</p>
	HTML;


$div_Georgia_election_racketeering_prosecution = new ContentSection();
$div_Georgia_election_racketeering_prosecution->content = <<<HTML
	<h3>Georgia election racketeering prosecution</h3>

	<p>As district attorney for Fulton County, Georgia, Fani Willis is prosecuting Donald Trump
	for "knowingly and willfully [joining] a conspiracy to unlawfully change the outcome" of the 2020 U.S. presidential election in Georgia.</p>

	<p>Republican officials are making moves against Fani Willis, even attempting to have her removed,
	in order to prevent the trial against Trump in Georgia to take place.</p>

	<p>The US House Judiciary Committee has launched an investigation into Fulton Country District Attorney Fani Willis
	question her "motivation for prosecuting former president Donald Trump".</p>

	<p>Also, the Republican-led legislature of the State of Georgia voted a new law, SB 92, giving them the power to remove Fani Willis from office.
	The law was signed by Republican Governor Brian Kemp in May 2023.
	The start date of the law was moved up to October 1st of the same year,
	just in time to be used to interfere with the now ongoing prosecution of Donald Trump.
	Indeed, a Republican state senator in Georgia has announced that as soon as this new "remove the prosecutor" law goes into effect,
	he is going to start proceedings in the Georgia's legislature to use this new power to remove funny Willis from her job. $r1
	The law specifies particular reasons that a district attorney could be removed.
	${'Clark Cunningham'}, professor of law at Georgia State University College, stated that
	"willful misconduct in office" would be one basis used by Republicans to remove a district attorney, or
	"conduct prejudicial to the administration of justice".
	It would then be the job of a commission to adjudicate on the actual removal of the attorney.
	Commission members would all be appointed by Republican office holders.</p>
	HTML;



$div_wikipedia_Georgia_election_racketeering_prosecution = new WikipediaContentSection();
$div_wikipedia_Georgia_election_racketeering_prosecution->setTitleText('Georgia election racketeering prosecution');
$div_wikipedia_Georgia_election_racketeering_prosecution->setTitleLink('https://en.wikipedia.org/wiki/Georgia_election_racketeering_prosecution');
$div_wikipedia_Georgia_election_racketeering_prosecution->content = <<<HTML
	<p>The State of Georgia v. Donald J. Trump, et al. is a criminal case
	against Donald Trump, the 45th president of the United States, and 18 co-defendants.
	The prosecution alleges that Trump led a "criminal racketeering enterprise",
	in which he and all other defendants "knowingly and willfully joined a conspiracy
	to unlawfully change the outcome" of the 2020 U.S. presidential election in Georgia.
	All defendants are charged with one count of violating Georgia's Racketeer Influenced and Corrupt Organizations (RICO) statute,
	which allows for a penalty of five to twenty years in prison.</p>
	HTML;



$div_wikipedia_2020_Georgia_election_investigation = new WikipediaContentSection();
$div_wikipedia_2020_Georgia_election_investigation->setTitleText('2020 Georgia election investigation');
$div_wikipedia_2020_Georgia_election_investigation->setTitleLink('https://en.wikipedia.org/wiki/2020_Georgia_election_investigation');
$div_wikipedia_2020_Georgia_election_investigation->content = <<<HTML
	<p>The 2020 Georgia election investigation refers to an ongoing criminal investigation
	into alleged efforts by then-president Donald Trump and his allies
	to overturn the certified victory of Democratic candidate Joe Biden
	and award the state's electoral college votes to Trump.</p>
	HTML;



$div_wikipedia_Obstruction_of_justice = new WikipediaContentSection();
$div_wikipedia_Obstruction_of_justice->setTitleText('Obstruction of justice');
$div_wikipedia_Obstruction_of_justice->setTitleLink('https://en.wikipedia.org/wiki/Obstruction_of_justice');
$div_wikipedia_Obstruction_of_justice->content = <<<HTML
	<p>Obstruction is a broad crime that may include acts such as perjury,
	making false statements to officials, witness tampering, jury tampering, destruction of evidence, and many others.
	Obstruction also applies to overt coercion of court or government officials via the means of threats or actual physical harm,
	and also applying to deliberate sedition against a court official to undermine the appearance of legitimate authority.</p>
	HTML;


$page->parent('justice.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_Georgia_election_racketeering_prosecution);
$page->body($div_wikipedia_2020_Georgia_election_investigation);
$page->body($div_wikipedia_Georgia_election_racketeering_prosecution);



$page->body($div_wikipedia_Obstruction_of_justice);
