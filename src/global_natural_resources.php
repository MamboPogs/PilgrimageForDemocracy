<?php
$page = new Page();
$page->h1('Global Natural Resources');
$page->stars(0);
$page->keywords('natural resources', 'Natural resources');

$page->preview( <<<HTML
	<p>Sharing the limited resources of a finite world.</p>
	HTML );

$page->snp('description', "Sharing the limited resources of a finite world.");
$page->snp('image', "/copyrighted/anne-nygard-v5mEr9CfG18.1200-630.jpg");


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>We have no other choice but to learn how to share the limited resources of a finite world.</p>

	<p>Many conflicts could be avoided if we used our natural resources sustainably.</p>

	<p>In 2008, $Ecuador became the first country in the world to enshrine a set of codified Rights of Nature.</p>
	HTML;



$div_wikipedia_Resource_war = new WikipediaContentSection();
$div_wikipedia_Resource_war->setTitleText('Resource war');
$div_wikipedia_Resource_war->setTitleLink('https://en.wikipedia.org/wiki/Resource_war');
$div_wikipedia_Resource_war->content = <<<HTML
	<p>In a resource war, there is typically a nation or group that controls the resource
	and an aggressor that wishes to seize control over said resource.
	This power dynamic between nations has been a significant underlying factor in conflicts since the late 19th century.</p>
	HTML;

$div_wikipedia_Non_renewable_resource = new WikipediaContentSection();
$div_wikipedia_Non_renewable_resource->setTitleText('Non-renewable resource');
$div_wikipedia_Non_renewable_resource->setTitleLink('https://en.wikipedia.org/wiki/Non-renewable_resource');
$div_wikipedia_Non_renewable_resource->content = <<<HTML
	<p>Earth minerals and metal ores, fossil fuels (coal, petroleum, natural gas)
	and groundwater in certain aquifers are all considered non-renewable resources.</p>
	HTML;

$div_wikipedia_Water_conflict = new WikipediaContentSection();
$div_wikipedia_Water_conflict->setTitleText('Water conflict');
$div_wikipedia_Water_conflict->setTitleLink('https://en.wikipedia.org/wiki/Water_conflict');
$div_wikipedia_Water_conflict->content = <<<HTML
	<p>Water has long been a source of tension and one of the causes for conflicts.
	Water conflicts arise for several reasons, including territorial disputes, a fight for resources, and strategic advantage.</p>
	HTML;


$page->parent('global_issues.html');
$page->parent('environment.html');
$page->body($div_stub);

$page->body($div_introduction);
$page->body('renewable_energy.html');
$page->body('environmental_defenders.html');

$page->body($div_wikipedia_Resource_war);
$page->body($div_wikipedia_Non_renewable_resource);
$page->body($div_wikipedia_Water_conflict);
