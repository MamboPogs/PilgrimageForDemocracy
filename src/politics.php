<?php
$page = new Page();
$page->h1('Politics');
$page->stars(0);
$page->keywords('Politics', 'politics', 'political');

//$page->snp('description', "");
//$page->snp('image', "/copyrighted/");

$page->preview( <<<HTML
	<p>
	</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Politics has a bad reputation.
	However, properly seen and properly practised, politics is a noble art.</p>

	<p>We, humanity, share the same geographical space (our only planet), and the same chronological time.
	Politics is the art of elaborating a framework thanks to which we can live in peace and harmony.</p>
	HTML;



$div_wikipedia_Politics = new WikipediaContentSection();
$div_wikipedia_Politics->setTitleText('Politics');
$div_wikipedia_Politics->setTitleLink('https://en.wikipedia.org/wiki/Politics');
$div_wikipedia_Politics->content = <<<HTML
	<p>Politics is the set of activities that are associated with making decisions in groups, or other forms of power relations among individuals,
	such as the distribution of resources or status. The branch of social science that studies politics and government is referred to as political science.</p>
	HTML;

$div_wikipedia_Political_science = new WikipediaContentSection();
$div_wikipedia_Political_science->setTitleText('Political science');
$div_wikipedia_Political_science->setTitleLink('https://en.wikipedia.org/wiki/Political_science');
$div_wikipedia_Political_science->content = <<<HTML
	<p>Political science is the scientific study of politics.
	It is a social science dealing with systems of governance and power, and the analysis of political activities,
	political institutions, political thought and behavior, and associated constitutions and laws.</p>
	HTML;

$div_wikipedia_List_of_political_theorists = new WikipediaContentSection();
$div_wikipedia_List_of_political_theorists->setTitleText('List of political theorists');
$div_wikipedia_List_of_political_theorists->setTitleLink('https://en.wikipedia.org/wiki/List_of_political_theorists');
$div_wikipedia_List_of_political_theorists->content = <<<HTML
	<p>A political theorist is someone who engages in constructing or evaluating political theory, including political philosophy.
	Theorists may be academics or independent scholars. Here is a list of the most notable political theorists.</p>
	HTML;


$page->parent('institutions.html');
$page->body($div_stub);
$page->body($div_introduction);

$page->body('political_discourse.html');

$page->body($div_wikipedia_Politics);
$page->body($div_wikipedia_Political_science);
$page->body($div_wikipedia_List_of_political_theorists);
