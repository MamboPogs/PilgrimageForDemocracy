<?php
$page = new Page();
$page->h1('Richard Bluhm');
$page->stars(2);
$page->keywords('Richard Bluhm');


$page->snp('description', "Professor of macroeconomics and digital transformation");
//$page->snp('image', "/copyrighted/");

$page->preview( <<<HTML
	<p>
	</p>
	HTML );

$r1 = $page->ref('https://www.ivr.uni-stuttgart.de/en/institute/team/Bluhm-00001/', 'University of Stuttgart: Richard Bluhm.');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Richard Bluhm is a professor of macroeconomics and digital transformation
	at the Institute of Economics and Law, University of Stuttgart.</p>
	HTML;


$div_Research = new ContentSection();
$div_Research->content = <<<HTML
	<h3>Research</h3>

	<p>The following research papers may be of interest to the $Pilgrimage: $r1</p>

	<ul>
	<li>Ethnofederalism and ethnic voting</li>
	<li>Connective financing: Chinese infrastructure projects and the diffusion of economic activity in developing countries </li>
	<li>Local majorities: How administrative divisions shape comparative development</li>
	<li>Fueling conflict? (De)escalation and bilateral aid.</li>
	<li>Do Weak Institutions Prolong Crises? On the Identification, Characteristics, and Duration of Declines during Economic Slumps.</li>
	<li>$Poverty accounting.</li>
	<li>Poor Trends: The Pace of Poverty Reduction after the Millennium Development Agenda1</li>
	<li>Ethnic Divisions, Political Institutions and the Duration of Declines-A Political Economy Theory of Delayed Recovery</li>
	<li>The Pace of Poverty Reduction: A fractional response approach</li>
	<li>Multidimensional Poverty in Senegal–an assessment of multidimensional poverty, poverty dynamics and patterns of deprivations, mimeographed</li>
	</ul>
	HTML;


$div_Richard_Bluhm_website = new WebsiteContentSection();
$div_Richard_Bluhm_website->setTitleText('Richard Bluhm website ');
$div_Richard_Bluhm_website->setTitleLink('https://www.richard-bluhm.com/');
$div_Richard_Bluhm_website->content = <<<HTML
	<p>His research is at the intersection of macroeconomics, development/ political economics, and urban/ regional economics.
	Some of the larger questions I try to answer in my research are:
	Why is economic activity more concentrated in some places than others?
	Do politics and international linkages influence the growth of cities?
	How do ethnic politics and the geography of ethnic groups interact with economic growth?
	Which political factors influence the dominance of some ethnic groups over others and how do these factors shape regional development?</p>
	HTML;



$div_Richard_Bluhm = new WebsiteContentSection();
$div_Richard_Bluhm->setTitleText(' Richard Bluhm');
$div_Richard_Bluhm->setTitleLink('https://www.ivr.uni-stuttgart.de/en/institute/team/Bluhm-00001/');
$div_Richard_Bluhm->content = <<<HTML
	<p>Richard Bluhm's page at the University of Stuttgart.</p>
	HTML;



$div_googlescholar_Richard_Bluhm = new GoogleScholarContentSection();
$div_googlescholar_Richard_Bluhm->setTitleText('Richard Bluhm');
$div_googlescholar_Richard_Bluhm->setTitleLink('https://scholar.google.com/citations?user=c7on_IAAAAAJ&hl=en');
$div_googlescholar_Richard_Bluhm->content = <<<HTML
	<p>University of Stuttgart, Institute of Economics and Law.</p>
	HTML;



$page->parent('list_of_people.html');
$page->body($div_stub);

$page->body($div_introduction);
$page->body($div_Research);

$page->body($div_Richard_Bluhm_website);
$page->body($div_Richard_Bluhm);
$page->body($div_googlescholar_Richard_Bluhm);
