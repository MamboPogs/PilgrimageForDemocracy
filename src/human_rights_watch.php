<?php
$page = new Page();
$page->h1('Human Rights Watch');
$page->keywords('Human Rights Watch', 'HRW');
$page->stars(0);

$page->snp('description', 'Conducting research and advocacy on human rights.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p>HRW conducts research and advocacy on human rights.</p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Human Rights Watch conducts research and advocacy on human rights.</p>
	HTML;



$div_Trending_Human_Rights_Watch_Defending_Human_Rights_Worldwide = new WebsiteContentSection();
$div_Trending_Human_Rights_Watch_Defending_Human_Rights_Worldwide->setTitleText('Trending //Human Rights Watch | Defending Human Rights Worldwide');
$div_Trending_Human_Rights_Watch_Defending_Human_Rights_Worldwide->setTitleLink('https://www.hrw.org/');
$div_Trending_Human_Rights_Watch_Defending_Human_Rights_Worldwide->content = <<<HTML
	<p>Human Rights Watch investigates and reports on abuses happening in all corners of the world.
	We are roughly 550 plus people of 70-plus nationalities who are country experts, lawyers, journalists, and others
	who work to protect the most at risk, from vulnerable minorities and civilians in wartime, to refugees and children in need.
	We direct our advocacy towards governments, armed groups and businesses, pushing them to change or enforce their laws, policies and practices.
	To ensure our independence, we refuse government funding and carefully review all donations
	to ensure that they are consistent with our policies, mission, and values.
	We partner with organizations large and small across the globe to protect embattled activists
	and to help hold abusers to account and bring justice to victims.</p>
	HTML;




$div_wikipedia_Human_Rights_Watch = new WikipediaContentSection();
$div_wikipedia_Human_Rights_Watch->setTitleText('Human Rights Watch');
$div_wikipedia_Human_Rights_Watch->setTitleLink('https://en.wikipedia.org/wiki/Human_Rights_Watch');
$div_wikipedia_Human_Rights_Watch->content = <<<HTML
	<p>Human Rights Watch (HRW) is an international non-governmental organization headquartered in New York City
	that conducts research and advocacy on human rights.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_Trending_Human_Rights_Watch_Defending_Human_Rights_Worldwide);
$page->body($div_wikipedia_Human_Rights_Watch);
