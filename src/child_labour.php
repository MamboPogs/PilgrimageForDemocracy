<?php
$page = new Page();
$page->h1('Child labour');
$page->keywords('child labour');
$page->stars(0);

$page->preview( <<<HTML
	<p></p>
	HTML );

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;




$div_wikipedia_Child_labour = new WikipediaContentSection();
$div_wikipedia_Child_labour->setTitleText('Child labour');
$div_wikipedia_Child_labour->setTitleLink('https://en.wikipedia.org/wiki/Child_labour');
$div_wikipedia_Child_labour->content = <<<HTML
	<p>Child labour refers to the exploitation of children through any form of work that deprives them of their childhood,
	interferes with their ability to attend regular school, or is mentally, physically, socially and morally harmful.
	Such exploitation is prohibited by legislation worldwide,
	although these laws do not consider all work by children as child labour, as exceptions are made for some types of work by children.</p>
	HTML;


$page->body($div_stub);
$page->body($div_introduction);
$page->body($div_wikipedia_Child_labour);
$page->body('education.html');
