<?php
$page = new Page();
$page->h1('Global Peace Index');
$page->stars(0);
$page->keywords('Global Peace Index');

$page->preview( <<<HTML
	<p>The Global Peace Index (GPI), published by the <a href='/institute_for_economics_and_peace.html'>IEP</a>, measures the relative peacefulness of each country.</p>
	HTML );


$page->snp('description', "Published by the IEP, the GPI measures the relative peacefulness of each country.");
//$page->snp('image', "/copyrighted/");



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The Global Peace Index is published by the ${'Institute for Economics and Peace'}.</p>
	HTML;



$div_Global_Peace_Index_map_and_data = new WebsiteContentSection();
$div_Global_Peace_Index_map_and_data->setTitleText('Global Peace Index: map and data ');
$div_Global_Peace_Index_map_and_data->setTitleLink('https://www.visionofhumanity.org/maps/');
$div_Global_Peace_Index_map_and_data->content = <<<HTML
	<p>The overall GPI score is a composite index measuring the peacefulness of countries
	made up of 23 quantitative and qualitative indicators each weighted on a scale of 1-5.
	The lower the score the more peaceful the country.</p>
	HTML;



$div_wikipedia_Global_Peace_Index = new WikipediaContentSection();
$div_wikipedia_Global_Peace_Index->setTitleText('Global Peace Index');
$div_wikipedia_Global_Peace_Index->setTitleLink('https://en.wikipedia.org/wiki/Global_Peace_Index');
$div_wikipedia_Global_Peace_Index->content = <<<HTML
	<p>Global Peace Index (GPI) is a report produced by the Institute for Economics & Peace (IEP)
	which measures the relative position of nations' and regions' peacefulness.</p>
	HTML;

$page->parent('list_of_indices.html');
$page->body($div_stub);

$page->body($div_introduction);
$page->body($div_Global_Peace_Index_map_and_data);

$page->body($div_wikipedia_Global_Peace_Index);
