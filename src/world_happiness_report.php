<?php
$page = new Page();
$page->h1('World Happiness Report');
$page->keywords('World Happiness Report');
$page->stars(0);

$page->snp('description', 'Rankings of national happiness.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$div_World_Happiness_Report_2023 = new WebsiteContentSection();
$div_World_Happiness_Report_2023->setTitleText('World Happiness Report');
$div_World_Happiness_Report_2023->setTitleLink('https://worldhappiness.report/');
$div_World_Happiness_Report_2023->content = <<<HTML
	<p>It has been over ten years since the first World Happiness Report was published.
	And it is exactly ten years since the United Nations General Assembly adopted Resolution 66/281,
	proclaiming 20 March to be observed annually as International Day of Happiness.
	Since then, more and more people have come to believe that our success as
	countries should be judged by the happiness of our people.
	There is also a growing consensus about how happiness should be measured.
	This consensus means that national happiness can now become an operational objective for governments.</p>
	HTML;


$div_wikipedia_World_Happiness_Report = new WikipediaContentSection();
$div_wikipedia_World_Happiness_Report->setTitleText('World Happiness Report');
$div_wikipedia_World_Happiness_Report->setTitleLink('https://en.wikipedia.org/wiki/World_Happiness_Report');
$div_wikipedia_World_Happiness_Report->content = <<<HTML
	<p>The World Happiness Report is a publication that contains articles and rankings of national happiness,
	based on respondent ratings of their own lives,
	which the report also correlates with various (quality of) life factors.</p>
	HTML;


$page->parent('list_of_indices.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_World_Happiness_Report_2023);
$page->body($div_wikipedia_World_Happiness_Report);
