<?php
$page = new Page();
$page->h1('Democracy and social justice in the world.');
$page->stars(1);
$page->keywords('world', 'countries', 'country', 'global');

$page->preview( <<<HTML
	<p>Although we are only getting started, we aim to progressively extend our coverage of countries around the world.</p>
	HTML );

$list = new ListOfPages();
$list->add('brics.html');
$list->add('ecowas.html');
$list->add('european_union.html');
$list->add('oecd.html');
$list->add('osce.html');
$list->add('united_nations.html');
$print_list = $list->print();


$div_International_organisations = new ContentSection();
$div_International_organisations->content = <<<HTML
	<h3>International organisations</h3>
	$print_list
	HTML;


$list = new ListOfPages();
$list->add('australia.html');
$list->add('prc_china.html');
$list->add('colombia.html');
$list->add('costa_rica.html');
$list->add('denmark.html');
$list->add('ecuador.html');
$list->add('france.html');
$list->add('gabon.html');
$list->add('germany.html');
$list->add('georgia.html');
$list->add('haiti.html');
$list->add('hong_kong.html');
$list->add('iran.html');
$list->add('liberia.html');
$list->add('mali.html');
$list->add('myanmar.html');
$list->add('niger.html');
$list->add('pakistan.html');
$list->add('philippines.html');
$list->add('russia.html');
$list->add('rwanda.html');
$list->add('saudi_arabia.html');
$list->add('somalia.html');
$list->add('sweden.html');
$list->add('taiwan.html');
$list->add('tunisia.html');
$list->add('turkey.html');
$list->add('ukraine.html');
$list->add('united_states.html');
$list->add('vietnam.html');
$print_list = $list->print();


$div_countries = new ContentSection();
$div_countries->content = <<<HTML
	<h3>Countries</h3>
	$print_list
	HTML;


$page->parent('lists.html');


$page->body($div_International_organisations);
$page->body($div_countries);
