<?php
$page = new Page();
$page->h1('BRICS');
$page->keywords('BRICS');
$page->stars(0);

$page->snp('description', 'Brazil, Russia, India, China, and South Africa.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>BRICS is a grouping of the world economies of Brazil, $Russia, India, $China, and South Africa.</p>

	<p>At the 2023 BRICS summit, it was announced that Saudi Arabia, $Iran, Argentina, Egypt, Ethiopia and the United Arab Emirates
	would join the alliance. Other countries, like $Tunisia, are also interested in joining.</p>

	<p>We call for an economically just world, a free world where $democratic rule is the norm.
	It is only fair that economic alliances are formed to compete with the West European / North American economic hegemony.
	However, the democratic and freedom indices of the countries within the BRICS alliance are very low in average.
	We have to watch out to what extent undemocratic $countries will continue to use their economic might
	to promote their authoritarian agenda.</p>

	<p>Part of the talks within BRICS is the possible creation of a "BRICS currency" that would challenge the hegemony of the US dollar.</p>
	HTML;

$list = new ListOfPages();
$list->add('prc_china.html');
$list->add('russia.html');
$print_list = $list->print();

$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>List</h3>

	$print_list
	HTML;

$div_wikipedia_BRICS = new WikipediaContentSection();
$div_wikipedia_BRICS->setTitleText('BRICS');
$div_wikipedia_BRICS->setTitleLink('https://en.wikipedia.org/wiki/BRICS');
$div_wikipedia_BRICS->content = <<<HTML
	<p>The BRICS have a combined area of 39,746,220 km2
	and an estimated total population of about 3.21 billion,
	or about 26.7% of the world's land surface and 41.5% of the global population.
	Brazil, Russia, India, and China are among the world's ten largest countries by population, area, and GDP (PPP),
	and the latter three are widely considered to be current or emerging superpowers.</p>
	HTML;


$page->parent('world.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_list);
$page->body($div_wikipedia_BRICS);
