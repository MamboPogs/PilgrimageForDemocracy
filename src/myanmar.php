<?php
$page = new CountryPage('Myanmar');
$page->h1('Myanmar');
$page->keywords('Myanmar', 'Burma');
$page->stars(0);

$page->snp('description', '57 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>See also: ${'Rohingya people'}.</p>
	HTML;

$div_wikipedia_Myanmar = new WikipediaContentSection();
$div_wikipedia_Myanmar->setTitleText('Myanmar');
$div_wikipedia_Myanmar->setTitleLink('https://en.wikipedia.org/wiki/Myanmar');
$div_wikipedia_Myanmar->content = <<<HTML
	<p>Myanmar, officially the Republic of the Union of Myanmar, also known as Burma (the official name until 1989), is a country in Southeast Asia.</p>
	HTML;

$div_wikipedia_Human_rights_in_Myanmar = new WikipediaContentSection();
$div_wikipedia_Human_rights_in_Myanmar->setTitleText('Human rights in Myanmar');
$div_wikipedia_Human_rights_in_Myanmar->setTitleLink('https://en.wikipedia.org/wiki/Human_rights_in_Myanmar');
$div_wikipedia_Human_rights_in_Myanmar->content = <<<HTML
	<p>Human rights in Myanmar under its military regime have long been regarded as among the worst in the world.</p>
	HTML;

$div_wikipedia_Myanmar_National_Human_Rights_Commission = new WikipediaContentSection();
$div_wikipedia_Myanmar_National_Human_Rights_Commission->setTitleText('Myanmar National Human Rights Commission');
$div_wikipedia_Myanmar_National_Human_Rights_Commission->setTitleLink('https://en.wikipedia.org/wiki/Myanmar_National_Human_Rights_Commission');
$div_wikipedia_Myanmar_National_Human_Rights_Commission->content = <<<HTML
	<p>The Myanmar National Human Rights Commission is the independent national human rights commission of Myanmar,
	consisting of 11 retired bureaucrats and academics.
	Analysts have questioned the panel's will and ability to challenge the government,
	but the commission has challenged the President's claims that there are no political prisoners in Myanmar,
	calling for all political prisoners' release and amnesty.</p>
	HTML;

$div_wikipedia_Internal_conflict_in_Myanmar = new WikipediaContentSection();
$div_wikipedia_Internal_conflict_in_Myanmar->setTitleText('Internal conflict in Myanmar');
$div_wikipedia_Internal_conflict_in_Myanmar->setTitleLink('https://en.wikipedia.org/wiki/Internal_conflict_in_Myanmar');
$div_wikipedia_Internal_conflict_in_Myanmar->content = <<<HTML
	<p>Insurgencies have been ongoing in Myanmar since 1948, the year the country, then known as Burma,
	gained independence from the United Kingdom.
	The conflict has largely been ethnic-based, with several ethnic armed groups fighting Myanmar's armed forces, the Tatmadaw, for self-determination.
	Despite numerous ceasefires and the creation of autonomous self-administered zones in 2008,
	many armed groups continue to call for independence, increased autonomy, or the federalisation of the country.
	The conflict is the world's longest ongoing civil war, having spanned more than seven decades.</p>
	HTML;

$div_wikipedia_Sex_trafficking_in_Myanmar = new WikipediaContentSection();
$div_wikipedia_Sex_trafficking_in_Myanmar->setTitleText('Sex trafficking in Myanmar');
$div_wikipedia_Sex_trafficking_in_Myanmar->setTitleLink('https://en.wikipedia.org/wiki/Sex_trafficking_in_Myanmar');
$div_wikipedia_Sex_trafficking_in_Myanmar->content = <<<HTML
	<p>Sex trafficking in Myanmar is human trafficking for the purpose of sexual exploitation and slavery
	that occurs in the Republic of the Union of Myanmar.
	Myanmar is primarily a source and transit country for sexually trafficked persons.</p>
	HTML;

$div_wikipedia_2013_Myanmar_anti_Muslim_riots = new WikipediaContentSection();
$div_wikipedia_2013_Myanmar_anti_Muslim_riots->setTitleText('2013 Myanmar anti Muslim riots');
$div_wikipedia_2013_Myanmar_anti_Muslim_riots->setTitleLink('https://en.wikipedia.org/wiki/2013_Myanmar_anti-Muslim_riots');
$div_wikipedia_2013_Myanmar_anti_Muslim_riots->content = <<<HTML
	<p>The 2013 Myanmar anti-Muslim riots were a series of conflicts in various cities throughout central and eastern Myanmar (Burma).</p>
	HTML;

$div_wikipedia_Rohingya_conflict = new WikipediaContentSection();
$div_wikipedia_Rohingya_conflict->setTitleText('Rohingya conflict');
$div_wikipedia_Rohingya_conflict->setTitleLink('https://en.wikipedia.org/wiki/Rohingya_conflict');
$div_wikipedia_Rohingya_conflict->content = <<<HTML
	<p>The Rohingya conflict is an ongoing conflict in the northern part of Myanmar's Rakhine State (formerly known as Arakan),
	characterised by sectarian violence between the Rohingya Muslim and Rakhine Buddhist communities,
	a military crackdown on Rohingya civilians by Myanmar's security forces, and militant attacks by Rohingya insurgents.</p>
	HTML;

$div_wikipedia_Rohingya_genocide = new WikipediaContentSection();
$div_wikipedia_Rohingya_genocide->setTitleText('Rohingya genocide');
$div_wikipedia_Rohingya_genocide->setTitleLink('https://en.wikipedia.org/wiki/Rohingya_genocide');
$div_wikipedia_Rohingya_genocide->content = <<<HTML
	<p>The Rohingya genocide is a series of ongoing persecutions and killings of the Muslim Rohingya people by the military of Myanmar.
	The genocide has consisted of two phases to date:
	the first was a military crackdown that occurred from October 2016 to January 2017, and the second has been occurring since August 2017.
	The crisis forced over a million Rohingya to flee to other countries.
	Most fled to Bangladesh, resulting in the creation of the world's largest refugee camp,
	while others escaped to India, Thailand, Malaysia, and other parts of South and Southeast Asia, where they continue to face persecution.
	Many other countries refer to the events as "ethnic cleansing".</p>
	HTML;


$page->parent('world.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_Myanmar);
$page->body($div_wikipedia_Human_rights_in_Myanmar);
$page->body($div_wikipedia_Myanmar_National_Human_Rights_Commission);
$page->body($div_wikipedia_Internal_conflict_in_Myanmar);
$page->body($div_wikipedia_Sex_trafficking_in_Myanmar);
$page->body($div_wikipedia_2013_Myanmar_anti_Muslim_riots);
$page->body($div_wikipedia_Rohingya_conflict);
$page->body($div_wikipedia_Rohingya_genocide);
