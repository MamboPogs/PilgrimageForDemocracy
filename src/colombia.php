<?php
$page = new CountryPage('Colombia');
$page->h1('Colombia');
$page->keywords('Colombia');
$page->stars(0);

$page->preview( <<<HTML
	<p></p>
	HTML );

$page->snp('description', '49 million inhabitants.');
//$page->snp('image',       '/copyrighted/');



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>As of September 2021, 611 ${'environmental defenders'} had already been assassinated in Colombia
	since the signing of the 2016 Peace Deal,
	according to the Colombian Institute of Studies for Development and Peace (Indepaz).
	Of those 611 people, 332 were Indigenous.</p>
	HTML;

$div_wikipedia_Colombia = new WikipediaContentSection();
$div_wikipedia_Colombia->setTitleText('Colombia');
$div_wikipedia_Colombia->setTitleLink('https://en.wikipedia.org/wiki/Colombia');
$div_wikipedia_Colombia->content = <<<HTML
	<p>The Republic of Colombia is a country mostly in South America with insular regions in North America.
	The Colombian mainland is bordered by the Caribbean Sea to the north, Venezuela to the east and northeast,
	Brazil to the southeast, Ecuador and Peru to the south and southwest, the Pacific Ocean to the west, and Panama to the northwest.</p>
	HTML;

$div_wikipedia_Crime_in_Colombia = new WikipediaContentSection();
$div_wikipedia_Crime_in_Colombia->setTitleText('Crime in Colombia');
$div_wikipedia_Crime_in_Colombia->setTitleLink('https://en.wikipedia.org/wiki/Crime_in_Colombia');
$div_wikipedia_Crime_in_Colombia->content = <<<HTML
	<p>Colombia has a high crime rate due to being a center for the cultivation and trafficking of cocaine.</p>
	HTML;

$div_wikipedia_Freedom_of_religion_in_Colombia = new WikipediaContentSection();
$div_wikipedia_Freedom_of_religion_in_Colombia->setTitleText('Freedom of religion in Colombia');
$div_wikipedia_Freedom_of_religion_in_Colombia->setTitleLink('https://en.wikipedia.org/wiki/Freedom_of_religion_in_Colombia');
$div_wikipedia_Freedom_of_religion_in_Colombia->content = <<<HTML
	<p>Freedom of religion in Colombia is enforced by the State and well tolerated in the Colombian culture.</p>
	HTML;

$div_wikipedia_Colombian_conflict = new WikipediaContentSection();
$div_wikipedia_Colombian_conflict->setTitleText('Colombian conflict');
$div_wikipedia_Colombian_conflict->setTitleLink('https://en.wikipedia.org/wiki/Colombian_conflict');
$div_wikipedia_Colombian_conflict->content = <<<HTML
	<p>The Colombian conflict began in the mid-1960s and is a low-intensity asymmetric war between Colombian governments,
	paramilitary groups, crime syndicates,
	and left-wing guerrillas such as the Revolutionary Armed Forces of Colombia (FARC), and the National Liberation Army (ELN),
	fighting each other to increase their influence in Colombian territory.
	Two of the most important international actors that have contributed to the Colombian conflict are multinational companies and the United States.</p>
	HTML;

$div_wikipedia_Colombian_peace_process = new WikipediaContentSection();
$div_wikipedia_Colombian_peace_process->setTitleText('Colombian peace process');
$div_wikipedia_Colombian_peace_process->setTitleLink('https://en.wikipedia.org/wiki/Colombian_peace_process');
$div_wikipedia_Colombian_peace_process->content = <<<HTML
	<p>The Colombian peace process is the peace process between the Colombian government of President Juan Manuel Santos
	and the Revolutionary Armed Forces of Colombia (FARC–EP)
	to bring an end to the Colombian conflict,
	which eventually led to the Peace Agreements between the Colombian Government of Juan Manuel Santos and FARC-EP.</p>
	HTML;


$page->parent('world.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_Colombia);
$page->body($div_wikipedia_Crime_in_Colombia);
$page->body($div_wikipedia_Freedom_of_religion_in_Colombia);
$page->body($div_wikipedia_Colombian_conflict);
$page->body($div_wikipedia_Colombian_peace_process);
