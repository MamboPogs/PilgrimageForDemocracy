<?php
$page = new Page();
$page->h1('List of movies');
$page->stars(0);
$page->keywords('movies');

$page->preview( <<<HTML
	<p>
	</p>
	HTML );


$page->snp('description', "Some movies worth watching...");
//$page->snp('image', "/copyrighted/");


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Some movies which are related to the themes discussed in the $Pilgrimage.</p>
	HTML;


$div_codeberg_List_of_documentaries_and_movies = new CodebergContentSection();
$div_codeberg_List_of_documentaries_and_movies->setTitleText('List of documentaries and movies');
$div_codeberg_List_of_documentaries_and_movies->setTitleLink('https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/3');
$div_codeberg_List_of_documentaries_and_movies->content = <<<HTML
	<p>list of relevant documentaries and movies.</p>
	HTML;

$list = new ListOfPages();
$list->add('the_remains_of_the_day.html');
$print_list = $list->print();


$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>list</h3>

	$print_list
	HTML;


$page->parent('lists.html');

$page->body($div_introduction);
$page->body($div_codeberg_List_of_documentaries_and_movies);
$page->body($div_list);

// Featured
$page->body('the_remains_of_the_day.html');
