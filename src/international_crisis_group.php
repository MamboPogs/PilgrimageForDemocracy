<?php
$page = new Page();
$page->h1('International Crisis Group');
$page->keywords('International Crisis Group', 'ICG');
$page->stars(0);

$page->snp('description', 'Conducting research and analysis on global crises.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;


$div_International_Crisis_Group_website = new WebsiteContentSection();
$div_International_Crisis_Group_website->setTitleText('International Crisis Group website ');
$div_International_Crisis_Group_website->setTitleLink('https://www.crisisgroup.org/');
$div_International_Crisis_Group_website->content = <<<HTML
	<p>The International Crisis Group is an independent organisation
	working to prevent wars and shape policies that will build a more peaceful world.</p>

	<p>Crisis Group sounds the alarm to prevent deadly conflict.
	We build support for the good governance and inclusive politics that enable societies to flourish.
	We engage directly with a range of conflict actors to seek and share information,
	and to encourage intelligent action for peace.</p>

	<p>Our work is urgently needed as the world is confronted with both new and chronic existing conflicts,
	each of which has devastating humanitarian, social and economic costs.
	Efforts to resolve conflicts are complicated by the profound shift in geopolitics,
	as well as the increasing prominence of non-state actors ranging from religious militants to criminal gangs.</p>
	HTML;


$div_CrisisWatch = new WebsiteContentSection();
$div_CrisisWatch->setTitleText('CrisisWatch ');
$div_CrisisWatch->setTitleLink('https://www.crisisgroup.org/crisiswatch');
$div_CrisisWatch->content = <<<HTML
	<p>CrisisWatch is our early warning and global conflict tracker, a tool designed to help decision-makers
	prevent deadly violence by keeping them up-to-date with developments in over 70 conflicts and crises,
	identifying trends and alerting them to risks of escalation and opportunities to advance peace.</p>
	HTML;




$div_wikipedia_International_Crisis_Group = new WikipediaContentSection();
$div_wikipedia_International_Crisis_Group->setTitleText('International Crisis Group');
$div_wikipedia_International_Crisis_Group->setTitleLink('https://en.wikipedia.org/wiki/International_Crisis_Group');
$div_wikipedia_International_Crisis_Group->content = <<<HTML
	<p>The International Crisis Group (ICG; also known as the Crisis Group) is
	a transnational non-profit, non-governmental organisation founded in 1995.
	It is a think tank, used by policymakers and academics, conducting research and analysis on global crises.
	ICG has described itself as "working to prevent wars and shape policies that will build a more peaceful world".</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->body($div_stub);
$page->body($div_introduction);

$page->body($div_International_Crisis_Group_website);
$page->body($div_CrisisWatch);

$page->body($div_wikipedia_International_Crisis_Group);
