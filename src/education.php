<?php
$page = new Page();
$page->h1('Education');
$page->keywords('education', 'Education');
$page->stars(0);

$page->preview( <<<HTML
	<p></p>
	HTML );

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$r1 = $page->ref('https://www.youtube.com/watch?v=aISXCw0Pi94&ab_channel=TED', 'Molly Wright: How every child can thrive by five | TED');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>This page aims to explore the role of education in a democratic society.
	Education or indoctrination?</p>

	<p>The ${'World Future Council'} has identified Finland's education policy as an example to learn from.</p>

	<p>Early education (children below the age of five $r1), is a critical stage in a child's development.
	Our society should strive to create the conditions for young children to be well taken take of,
	regardless of financial and social status.</p>
	HTML;



$div_wpc_Finland_s_Basic_Education_Act_General_Education_Policy = new FuturePolicyContentSection();
$div_wpc_Finland_s_Basic_Education_Act_General_Education_Policy->setTitleText('Finland’s Basic Education Act &#038; General Education Policy');
$div_wpc_Finland_s_Basic_Education_Act_General_Education_Policy->setTitleLink('https://www.futurepolicy.org/rightsofchildren/finlands-basic-education-act/');
$div_wpc_Finland_s_Basic_Education_Act_General_Education_Policy->content = <<<HTML
	<p>Education has been a national priority in Finland for over three decades,
	with the country developing a unique holistic approach that continues to evolve and has produced significant results;
	often being hailed as a world-class education system. One of the basic principles of Finnish education is that
	all people must have equal access to high-quality education and training.
	The same opportunities for education are available to all citizens irrespective of their ethnic origin, age, wealth, language, or location.
	The basic right to education and culture is recorded in the Constitution,
	while education is free at all levels from pre-primary to higher education.
	Key elements of Finnish education policy include quality, efficiency, equity, well-being and internationalization.
	Geared to promote the effectiveness of the Finnish welfare society, education policy is built on the lifelong learning principle.
	Education is also seen as an end in itself. Recent reforms aim to further develop schools as learning communities,
	emphasizing the joy of learning and a collaborative atmosphere,
	as well as promoting student autonomy in studying and in school life.
	Finland’s holistic and trust-based education system produces excellent results,
	ranked near the top in reading, maths, and science as well as in overall child well-being levels.</p>
	HTML;



$div_wikipedia_Education = new WikipediaContentSection();
$div_wikipedia_Education->setTitleText('Education');
$div_wikipedia_Education->setTitleLink('https://en.wikipedia.org/wiki/Education');
$div_wikipedia_Education->content = <<<HTML
	<p>Education is the transmission of knowledge, skills, and character traits.
	There are many debates about its precise definition, for example, about which aims it tries to achieve.
	A further issue is whether part of the meaning of education is that the change in the student is an improvement.
	Some researchers stress the role of critical thinking to distinguish education from indoctrination.</p>
	HTML;

$page->parent('global_issues.html');
$page->body($div_stub);
$page->body($div_introduction);

$page->body($div_wpc_Finland_s_Basic_Education_Act_General_Education_Policy);
$page->body($div_wikipedia_Education);
$page->body('child_labour.html');
