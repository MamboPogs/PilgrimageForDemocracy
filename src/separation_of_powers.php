<?php
$page = new Page();
$page->h1('Separation of powers');
$page->keywords('Separation of powers', 'separation of powers');
$page->stars(0);

$page->preview( <<<HTML
	<p></p>
	HTML );

$page->snp('description', 'A cornerstone of democratic institutions.');
//$page->snp('image',       '/copyrighted/');



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Separation of powers in a cornerstone of $democratic $institutions.</p>

	<p>We shall pay special attention of attempts at ${'obstruction of justice'}
	by one branch (legislative or executive) over the $judiciary.</p>
	HTML;




$div_wikipedia_Separation_of_powers = new WikipediaContentSection();
$div_wikipedia_Separation_of_powers->setTitleText('Separation of powers');
$div_wikipedia_Separation_of_powers->setTitleLink('https://en.wikipedia.org/wiki/Separation_of_powers');
$div_wikipedia_Separation_of_powers->content = <<<HTML
	<p>Separation of powers refers to the division of a state's government into "branches",
	each with separate, independent powers and responsibilities,
	so that the powers of one branch are not in conflict with those of the other branches.</p>
	HTML;


$page->parent('institutions.html');

$page->body($div_stub);
$page->body($div_introduction);
$page->body($div_wikipedia_Separation_of_powers);
