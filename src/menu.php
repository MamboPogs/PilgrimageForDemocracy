<?php
$page = new Page();
include('section/all.php');

$page->h1('Menu');
$page->stars(-1);
$page->keywords('menu');

$page->preview( <<<HTML
	<p>There are so many topics to cover!
	Check the growing listing of sections and articles that are already published.
	You can also preview some of the topics that we plan to develop in the next few months and years.</p>
	<p>The menu provides a convenient entry point for new and returning visitors.</p>
	HTML );


$h2_what_is_democracy = new h2HeaderContent('What is democracy?');

$div_what_is_democracy = new ContentSection();
$div_what_is_democracy->content = <<<HTML
	<h3>Definition of Democracy</h3>

	<p>Before we can even start discussing the ways our democracy can be improved,
	we must agree on a common sense definition of the term.</p>

	<p>Searching how countless thinkers and scholars define the word "democracy",
	we can't fail to notice that there is no simple, standard definition.
	Similarly, if we were to ask common people on the street to define democracy, we would get a variety of different answers
	and we would be no closer to having an exact definition of the term.</p>

	<p>However, by putting together all the possible answers and definitions,
	we can highlight the intrinsic qualities of democracies.
	A definite series of features stand out,
	with each one deriving from the previously enumerated ones.</p>

	<p>It is, after all, critical to gain a clear understanding of the defining features of a democracy,
	because it provides a bedrock upon which to build all of that which will be discussed afterwards,
	a standard that we can use to gauge our democratic achievements.</p>
	HTML;


$h2_priorities_avoid = new h2HeaderContent('Challenges');

$div_priorities_avoid = new ContentSection();
$div_priorities_avoid->content = <<<HTML
	<p>This section explores the main challenges in established democracies.</p>
	HTML;

$div_tweed_syndrome = new ContentSection();
$div_tweed_syndrome->content = <<<HTML
	<h3>2: Tweed Syndrome</h3>
	HTML;

$h2_priorities_build = new h2HeaderContent('Building democracy');

$div_election_method = new ContentSection();
$div_election_method->content = <<<HTML
	<h3>1: Good election method</h3>
	HTML;

$div_build_good_media = new ContentSection();
$div_build_good_media->content = <<<HTML
	<h3>3: Good media environment</h3>
	HTML;


$h2_taxes = new h2HeaderContent('Taxes');

$h2_media = new h2HeaderContent('Media');


$div_media = new ContentSection();
$div_media->content = <<<HTML
	<h3><a href="/media.html">Media</a></h3>
	HTML;


$h2_institutions = new h2HeaderContent('Institutions');

$h2_justice = new h2HeaderContent('Justice');


$h2_Right_speech_and_constructive_discourse = new h2HeaderContent('Right speech and constructive discourse');

$h2_fair_share = new h2HeaderContent('Fair share');


$h2_democracy_world = new h2HeaderContent('Democracy and social justice in the world');


$h2_Indices = new h2HeaderContent('Indices');

$page->body($div_stars);

$page->body($h2_what_is_democracy);
$page->body($div_what_is_democracy);
$page->body('primary_features_of_democracy.html');
$page->body('secondary_features_of_democracy.html');
$page->body('tertiary_features_of_democracy.html');
$page->body($div_section_what_is_democracy_quaternary_feature);
$page->body($div_section_what_is_democracy_quinary_feature);
$page->body('democracy_wip.html');
$page->body($div_section_democracy_soloist_choir);
$page->body($div_section_democracy_saints_and_little_devils);

$page->body($h2_priorities_avoid);
$page->body($div_priorities_avoid);
$page->body('duverger_syndrome.html');
$page->body($div_tweed_syndrome);
$page->body('democracy_under_attack.html');

$page->body($h2_priorities_build);
$page->body($div_election_method);
$page->body('election_integrity.html');


$page->body($h2_media);
$page->body('media.html');
$page->body('foreign_influence_local_media.html');

$page->body($h2_institutions);
$page->body('institutions.html');
$page->body('corruption.html');

$page->body($h2_justice);
$page->body('justice.html');
$page->body('social_justice.html');

$page->body($h2_taxes);
$page->body('taxes.html');


$page->body($h2_Right_speech_and_constructive_discourse);
$page->body('freedom_of_speech.html');
$page->body('political_discourse.html');


$page->body($h2_fair_share);
$page->body('fair_share.html');

$page->body($h2_democracy_world);
$page->body('world.html');
$page->body('united_nations.html');

$page->body($h2_Indices);
$page->body('lists.html');
