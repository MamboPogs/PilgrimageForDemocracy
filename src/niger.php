<?php
$page = new CountryPage('Niger');
$page->h1('Niger');
$page->keywords('Niger');
$page->stars(0);

$page->snp('description', '25.4 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Niger = new WikipediaContentSection();
$div_wikipedia_Niger->setTitleText('Niger');
$div_wikipedia_Niger->setTitleLink('https://en.wikipedia.org/wiki/Niger');
$div_wikipedia_Niger->content = <<<HTML
	<p>Niger or the Niger, officially the Republic of the Niger, is a landlocked country in West Africa.
	It is a unitary state bordered by Libya to the northeast, Chad to the east, Nigeria to the south,
	Benin and Burkina Faso to the southwest, Mali to the west, and Algeria to the northwest.</p>
	HTML;

$div_wikipedia_2023_Nigerien_crisis = new WikipediaContentSection();
$div_wikipedia_2023_Nigerien_crisis->setTitleText('2023 Nigerien crisis');
$div_wikipedia_2023_Nigerien_crisis->setTitleLink('https://en.wikipedia.org/wiki/2023_Nigerien_crisis');
$div_wikipedia_2023_Nigerien_crisis->content = <<<HTML
	<p>On 26 July 2023, a coup d'état occurred in the Republic of the Niger,
	in which the country's presidential guard removed and detained President Mohamed Bazoum.
	Presidential guard commander General Abdourahamane Tchiani proclaimed himself the leader of a military junta
	shortly after confirming the coup to be a success.</p>
	HTML;


$page->parent('world.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_Niger);
$page->body($div_wikipedia_2023_Nigerien_crisis);
