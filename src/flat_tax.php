<?php
$page = new Page();
$page->h1('Flat tax');
$page->stars(0);

$page->preview( <<<HTML
	<p>A tax with a single rate.</p>
	HTML );

$page->snp('description', 'A tax with a single rate');
//$page->snp('image', '/copyrighted/');



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>A flat tax, whereby everybody is taxed with the same rate regardless of wealth or income,
	would not solve any of the major problem of current tax systems.
	It would not be taxing was ought to be taxed,
	nor would it solve the problem of unfair wealth inequality.</p>
	HTML;




$div_wikipedia_Flat_tax = new WikipediaContentSection();
$div_wikipedia_Flat_tax->setTitleText('Flat tax');
$div_wikipedia_Flat_tax->setTitleLink('https://en.wikipedia.org/wiki/Flat_tax');
$div_wikipedia_Flat_tax->content = <<<HTML
	<p>A flat tax (short for flat-rate tax) is a tax with a single rate on the taxable amount,
	after accounting for any deductions or exemptions from the tax base.</p>
	HTML;

$page->body('taxes.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body($div_wikipedia_Flat_tax);
