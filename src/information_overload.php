<?php
$page = new Page();
$page->h1('Information overload');
$page->stars(2);
$page->keywords('Information overload', 'information overload');


//$page->snp('description', "");
//$page->snp('image', "/copyrighted/");

$page->preview( <<<HTML
	<p>Often, we consume too much information for our own good.</p>
	HTML );

$r1 = $page->ref('https://www.theguardian.com/media/2013/apr/12/news-is-bad-rolf-dobelli', 'News is bad for you – and giving up reading it will make you happier');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>As citizens and members of the electorate, it is primordial to be informed on important issues.
	However, in this interconnected world, many people consume too much news, which have detrimental effects on many aspects of our lives.</p>

	<p>${'Rolf Dobelli'}, author of "The Art of Thinking Clearly" and of "Stop Reading the News", argues that: ${r1}</p>
	<ul>
		<li>News misleads.</li>
		<li>News is irrelevant.</li>
		<li>News has no explanatory power.</li>
		<li>News is toxic to your body.</li>
		<li>News increases cognitive errors.</li>
		<li>News inhibits thinking.</li>
		<li>News works like a drug.</li>
		<li>News wastes time.</li>
		<li>News makes us passive.</li>
		<li>News kills creativity.</li>
	</ul>

	<p>To a large extent, all of the above is true.
	The $Pilgrimage aims to provide information in ways that avoids those caveats.
	The first step is to recognise the cause, symptoms and effects of information overload, remedy them,
	and then learn to how to get a healthy dose of information, in a way that is productive and constructive.</p>
	HTML;

$div_balance = new ContentSection();

$div_balance->content = <<<HTML
	<h3>Seeking a balanced approach</h3>

	<p>News is valuable because it provides insight into broader social trends, global events and different perspectives,
	even if it is not inherently related to an individual's daily life.
	The challenge is finding a balance between informing with maintained information and avoiding information overload.
	One of the limitations of news is that it tends to focus on telling facts and events without addressing their root causes and complexities.
	This can hinder a deeper understanding of complex issues, as they often lack comprehensive analysis and relevance.</p>

	<p>Additionally, constant exposure to negative news can have psychological effects, which can increase stress, anxiety, and a pessimistic world view.
	Still, it's important to recognize that the impact of news consumption varies from person to person and that
	some people may be more resilient to negative effects while others are susceptible.</p>

	<p>Working on a message can lead to cognitive errors such as confirmation bias and availability heuristics.
	Such biases arise when individuals rely solely on readily available information without seeking diverse perspectives or conducting in-depth analyses.
	$Dobelli explains that recognizing these biases can help mitigate their impact.</p>

	<p>News consumption can inhibit critical thinking by presenting ready-made narratives,
	but it also acts as a catalyst for critical consideration and questioning.
	Ultimately, how much news encourages critical thinking depends not only on content
	but also on the individual's approach and willingness to take multiple perspectives.</p>

	<p>Comparing the addictiveness of news consumption to the addictiveness of drugs may be an oversimplification,
	but the constant availability of news on various media platforms coupled with the release of dopamine
	associated with the retention of information can produce habit-forming behaviours that lead to over-consumption.
	Often making it even more difficult to differentiate between engagement farming, bias, truth, and falsehoods masquerading as truth.
	To maintain a healthy balance in consumption, it's important to be aware of
	time spent consuming and interacting with the news alongside other activities and priorities.</p>

	<p>Relying solely on the news for information often shows us selective events
	that can discourage active engagement and critical thinking, and can foster passivity.
	To combat this, it's helpful to seek out different sources of information, participate in discussions,and explore different perspectives.
	News consumption provides individuals with different perspectives and new information,
	but excessive consumption can limit the time and mental space available for introspection and creative thinking.
	Setting aside time to think and be active outside of the news encourages creativity and helps us understand the world more holistically.</p>

	<p>So we establish that News can be a complex and multifaceted source of information.
	On the other hand, it can also be misleading due to factors such as sensationalism, prejudice and incomplete or false information.
	To address this, it is important to critically assess the sources and claims presented in news articles.
	This is time-consuming and can be difficult.
	The $Pilgrimage aims to contribute by providing an open platform of referenced sources and researched thoughts.</p>
	HTML;





$div_wikipedia_Information_overload = new WikipediaContentSection();
$div_wikipedia_Information_overload->setTitleText('Information overload');
$div_wikipedia_Information_overload->setTitleLink('https://en.wikipedia.org/wiki/Information_overload');
$div_wikipedia_Information_overload->content = <<<HTML
	<p>In the age of connective digital technologies, informatics, the Internet culture (or the digital culture),
	information overload is associated with over-exposure, excessive viewing of information, and input abundance of information and data.</p>
	HTML;

$div_wikipedia_Information_explosion = new WikipediaContentSection();
$div_wikipedia_Information_explosion->setTitleText('Information explosion');
$div_wikipedia_Information_explosion->setTitleLink('https://en.wikipedia.org/wiki/Information_explosion');
$div_wikipedia_Information_explosion->content = <<<HTML
	<p>The information explosion is the rapid increase in the amount of published information or data and the effects of this abundance.
	As the amount of available data grows, the problem of managing the information becomes more difficult, which can lead to information overload.</p>
	HTML;

$page->parent('media.html');
$page->body($div_stub);

$page->body($div_introduction);
$page->body($div_balance);

$page->body($div_wikipedia_Information_explosion);
$page->body($div_wikipedia_Information_overload);
