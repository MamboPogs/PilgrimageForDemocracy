<?php
$page = new Page();
$page->h1('Refugees');
$page->keywords('Refugees', 'refugees', 'refugee');
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>See also: ${'refugee camps'}.</p>
	HTML;

$div_wikipedia_Refugee = new WikipediaContentSection();
$div_wikipedia_Refugee->setTitleText('Refugee');
$div_wikipedia_Refugee->setTitleLink('https://en.wikipedia.org/wiki/Refugee');
$div_wikipedia_Refugee->content = <<<HTML
	<p>A refugee, conventionally speaking, is a person who has lost the protection of their $country of origin
	and who cannot or is unwilling to return there due to well-founded fear of persecution.
	Such a person may be called an asylum seeker until granted refugee status
	by the contracting state or the ${'United Nations High Commissioner for Refugees'} ($UNHCR)
	if they formally make a claim for asylum.</p>
	HTML;


$page->parent('social_justice.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_wikipedia_Refugee);
