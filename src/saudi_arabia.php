<?php
$page = new CountryPage('Saudi Arabia');
$page->h1('Saudi Arabia');
$page->keywords('Saudi Arabia');
$page->stars(0);

$page->preview( <<<HTML
	<p></p>
	HTML );

$page->snp('description', '32 million inhabitants.');
//$page->snp('image',       '/copyrighted/');



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Saudi Arabia is a perpetrator of ${'transnational authoritarianism'}.</p>
	HTML;

$div_wikipedia_Saudi_Arabia = new WikipediaContentSection();
$div_wikipedia_Saudi_Arabia->setTitleText('Saudi Arabia');
$div_wikipedia_Saudi_Arabia->setTitleLink('https://en.wikipedia.org/wiki/Saudi_Arabia');
$div_wikipedia_Saudi_Arabia->content = <<<HTML
	<p>Saudi Arabia is a country in West Asia.
	It covers the bulk of the Arabian Peninsula, and has a land area of about 2150000 km2 (830000 sq mi), making it the fifth-largest country in Asia.</p>
	HTML;

$div_wikipedia_Human_rights_in_Saudi_Arabia = new WikipediaContentSection();
$div_wikipedia_Human_rights_in_Saudi_Arabia->setTitleText('Human rights in Saudi Arabia');
$div_wikipedia_Human_rights_in_Saudi_Arabia->setTitleLink('https://en.wikipedia.org/wiki/Human_rights_in_Saudi_Arabia');
$div_wikipedia_Human_rights_in_Saudi_Arabia->content = <<<HTML
	<p>Human rights in Saudi Arabia are a topic of concern and controversy.
	The Saudi government, which mandates both Muslim and non-Muslim observance of Islamic law under the absolute rule of the House of Saud,
	has been accused of and denounced by various international organizations and governments for violating human rights within the country.</p>
	HTML;

$div_wikipedia_Freedom_of_religion_in_Saudi_Arabia = new WikipediaContentSection();
$div_wikipedia_Freedom_of_religion_in_Saudi_Arabia->setTitleText('Freedom of religion in Saudi Arabia');
$div_wikipedia_Freedom_of_religion_in_Saudi_Arabia->setTitleLink('https://en.wikipedia.org/wiki/Freedom_of_religion_in_Saudi_Arabia');
$div_wikipedia_Freedom_of_religion_in_Saudi_Arabia->content = <<<HTML
	<p>The Kingdom of Saudi Arabia is an Islamic absolute monarchy
	in which Sunni Islam is the official state religion based on firm Sharia law.</p>
	HTML;

$div_wikipedia_Capital_punishment_in_Saudi_Arabia = new WikipediaContentSection();
$div_wikipedia_Capital_punishment_in_Saudi_Arabia->setTitleText('Capital punishment in Saudi Arabia');
$div_wikipedia_Capital_punishment_in_Saudi_Arabia->setTitleLink('https://en.wikipedia.org/wiki/Capital_punishment_in_Saudi_Arabia');
$div_wikipedia_Capital_punishment_in_Saudi_Arabia->content = <<<HTML
	<p>Capital punishment in Saudi Arabia is a legal penalty.
	Death sentences are almost exclusively based on the system of judicial sentencing discretion (tazir),
	following the classical principle of avoiding Sharia-prescribed (hudud) penalties when possible.</p>
	HTML;


$page->parent('world.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_Saudi_Arabia);
$page->body($div_wikipedia_Human_rights_in_Saudi_Arabia);
$page->body($div_wikipedia_Freedom_of_religion_in_Saudi_Arabia);
$page->body($div_wikipedia_Capital_punishment_in_Saudi_Arabia);
