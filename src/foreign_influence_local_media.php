<?php
$page = new Page();
$page->h1('Foreign influence in local media landscape');
$page->stars(2);

$page->preview( <<<HTML
	<p>For good or for evil, openly or covertly, countries routinely influence each other's media landscape.
	Some countries have adopted legislation to control, regular, curtail or restrict foreign influence.</p>
	HTML );

$page->snp('description', "Should foreign influence in local media be restricted?");
$page->snp('image', "/copyrighted/killian-cartignies-lJk004r27no.1200-630.jpg");


$r1 = $page->ref('https://www.indexoncensorship.org/2021/02/bbc-banned-in-mainland-china/', 'BBC banned in mainland China');
$r2 = $page->ref('https://www.cnbc.com/2021/02/16/china-blocks-bbc-world-news-after-uk-revokes-license-of-cgtn.html', 'Why China banned the BBC, and why it matters');
$r3 = $page->ref('https://www.aljazeera.com/news/2023/4/12/algeria-moves-to-further-curb-press-freedom-with-new-law', 'Algeria law ‘controlling media’ close to passing');
$r4 = $page->ref('https://www.jeuneafrique.com/1433048/politique/liberte-de-la-presse-nouveau-tour-de-vis-en-algerie/', 'En Algérie, des journalistes toujours plus sous pression');

$h2_introduction = new h2HeaderContent('Introduction');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>No country is exempt from foreign influence in their local media environment.
	As shown below, a few countries have enacted legislation in order to identify and monitor foreign agents.</p>

	<p>Russia has used its "<em>foreign agent law</em>" to further its autocratic agenda,
	for example against the organization and Nobel Peace Prize laureate <em>Memorial</em>.
	The people from $Georgia have successfully ralied against a similar law that was discussed in their country.
	At the same time, democratic countries like the $USA and $Australia have their own foreign agent laws.</p>


	<p>The BBC would be as much a foreign agent in Russia as RT (Russia Today) would be in Britain.
	Yet, from the point of view of our democratic values,
	we cannot put the influence of these two media conglomerates at the same level.</p>

	<p>When considering the influence that foreign agents, possibily backed by foreign governments,
	have on a country's local media landscape,
	one must take the following into account.</p>

	<p>Is the attempt at influence <strong>overt</strong> or <strong>covert</strong>?
	For example, authoritarian regimes are known to use troll farms to covertly influence elections in democratic countries.</p>


	<p>Is the influence <strong>benign</strong> or <strong>malignent</strong> in intent?
	Taiwan, a democratic jewel in Asia, has to resort to some ad-hoc legislation
	in order to curb the nefarious influence that the Chinese Communist Party would like to exert
	in the Taiwan media market.</p>

	<p>Is the foreign influence in opposition to the <strong>national interest</strong>?
	But then one should consider who defines what the national interest is and how.
	For example, the interest of the interests of the Iran autocratic regime
	may not be in alignment with the interests of the Iranian people.</p>

	<p>In view of the above, should democratic countries adopt legislation to identify and control foreign influence?
	Do the US and Australian legislation on foreign agents play a legitimate role in a democracy?
	Is the letter of the law acceptable?
	Does the way the law is enforced congruent with democratic principles?</p>

	<p>Maybe the problem is not that the influence is of foreign origin,
	but of the autocratic nature of some of the countries trying to exert influence.
	It may be preferable to judge local and foreign media by the same standards.
	However, those standards still remain to be defined, within the boundaries of freedom of expression.</p>
	HTML;

$h2_legislation = new h2HeaderContent('Legislation');


$div_codeberg = new CodebergContentSection();
$div_codeberg->setTitleText('Ban of Russian media in the EU and other countries');
$div_codeberg->setTitleLink('https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/28');
$div_codeberg->content = <<<HTML
	<p>What existing legislation was used to justify the ban of Russian media outlets and Russian propaganda sources?</p>
	HTML;

$div_foreign_agents = new ContentSection();
$div_foreign_agents->content = <<<HTML
	<p>Some countries have adopted legislation to control, regular, curtail or restrict foreign influence in their national media landscape.</p>
	HTML;



$div_foreign_agent_russia = new ContentSection();
$div_foreign_agent_russia->content = <<<HTML
	<h3>Russia</h3>

	<p>Russia's foreign agent laws has been used as a tool of autocratic suppression of democratic opposition to the regime.</p>
	HTML;

$div_foreign_agent_georgia = new ContentSection();
$div_foreign_agent_georgia->content = <<<HTML
	<h3>Georgia</h3>

	<p>The people of Georgia demonstrated in 2023 in opposition of a foreign agent law similar to the one in use in Russia.
	The demonstrations successfully defeated the bill which was ultimately withdrawn.</p>

	<p>See also:</p>
	<ul>
		<li><a href="/georgia.html">Georgia</a></li>
	</ul>
	HTML;

$div_foreign_agent_prc = new ContentSection();
$div_foreign_agent_prc->content = <<<HTML
	<h3>People's Republic of China</h3>

	<p>The People's Republic of China is known to routinely curtail the freedom of media from democratic countries.
	In 2021, China banned the BBC. {$r1}{$r2}</p>
	HTML;


$div_foreign_agent_taiwan = new ContentSection();
$div_foreign_agent_taiwan->content = <<<HTML
	<h3>Taiwan (Republic of China)</h3>

	<p>Taiwan must contend with pervasive aggression from the People's Republic of China,
	and for that reason must take a proactive stance against Chinese influence.</p>
	HTML;


$div_foreign_agent_usa = new ContentSection();
$div_foreign_agent_usa->content = <<<HTML
	<h3>United States</h3>

	<p>The United States and Australia are some of the democratic countries which have explicit foreign agent laws.</p>
	HTML;



$div_wikipedia_foreign_agent = new WikipediaContentSection();
$div_wikipedia_foreign_agent->setTitleText('Foreign agent');
$div_wikipedia_foreign_agent->setTitleLink('https://en.wikipedia.org/wiki/Foreign_agent');
$div_wikipedia_foreign_agent->content = <<<HTML
	<p>Some countries have formal procedures to legalize the activities of foreign agents acting overtly.
	Laws covering foreign agents vary widely from country to country, and selective enforcement may prevail within countries, based on perceived national interest.</p>
	HTML;

$div_wikipedia_foreign_agents_registration_act = new WikipediaContentSection();
$div_wikipedia_foreign_agents_registration_act->setTitleText('Foreign Agents Registration Act');
$div_wikipedia_foreign_agents_registration_act->setTitleLink('https://en.wikipedia.org/wiki/Foreign_Agents_Registration_Act');
$div_wikipedia_foreign_agents_registration_act->content = <<<HTML
	<p>The Foreign Agents Registration Act is a United States law that imposes public disclosure obligations on persons representing foreign interests.
	It requires "foreign agents" to register with the Department of Justice (DOJ) and disclose their activities.</p>
	HTML;

$div_wikipedia_foreign_influence_transparency_scheme_act_2018 = new WikipediaContentSection();
$div_wikipedia_foreign_influence_transparency_scheme_act_2018->setTitleText('Foreign Influence Transparency Scheme Act 2018');
$div_wikipedia_foreign_influence_transparency_scheme_act_2018->setTitleLink('https://en.wikipedia.org/wiki/Foreign_Influence_Transparency_Scheme_Act_2018');
$div_wikipedia_foreign_influence_transparency_scheme_act_2018->content = <<<HTML
	<p>The Foreign Influence Transparency Scheme Act 2018 is an Australian statute that creates a registration scheme for foreign agents in Australia.</p>
	HTML;

$div_wikipedia_russian_foreign_agent_law = new WikipediaContentSection();
$div_wikipedia_russian_foreign_agent_law->setTitleText('Russian foreign agent law');
$div_wikipedia_russian_foreign_agent_law->setTitleLink('https://en.wikipedia.org/wiki/Russian_foreign_agent_law');
$div_wikipedia_russian_foreign_agent_law->content = <<<HTML
	<p>The Russian foreign agent law requires anyone who receives "support" from outside Russia or is under "influence" from outside Russia
	to register and declare themselves as "foreign agents".
	Once registered, they are subject to additional audits
	and are obliged to mark all their publications with a 24-word disclaimer saying that they are being distributed by a "foreign agent".</p>
	HTML;




$div_foreign_agent_algeria = new ContentSection();
$div_foreign_agent_algeria->content = <<<HTML
	<h3>Algeria</h3>

	<p>The Algerian parliament is voting on a new media law forcing journalists to reveal their sources,
	banning media organisations from receiving foreign funds.
	Reporters Without Borders has decried this bill and worries about the adverse effect on journalism in Algeria. {$r3}{$r4}</p>
	HTML;


$page->parent('media.html');
$page->body($div_stub);

$page->body($h2_introduction);
$page->body($div_introduction);

$page->body($h2_legislation);

$page->body($div_foreign_agents);
$page->body($div_wikipedia_foreign_agent);


$page->body($div_foreign_agent_russia);
$page->body($div_wikipedia_russian_foreign_agent_law);
$page->body($div_codeberg);

$page->body($div_foreign_agent_georgia);

$page->body($div_foreign_agent_prc);

$page->body($div_foreign_agent_taiwan);

$page->body($div_foreign_agent_usa);
$page->body($div_wikipedia_foreign_agents_registration_act);
$page->body($div_wikipedia_foreign_influence_transparency_scheme_act_2018);

$page->body($div_foreign_agent_algeria);
