<?php
$page = new Page();
$page->h1('V-Dem Institute');
$page->stars(0);
$page->keywords('V-Dem Institute');

$page->preview( <<<HTML
	<p>Varieties of Democracy (V-Dem) is a unique approach to conceptualizing and measuring democracy.</p>
	HTML );


$page->snp('description', "Conceptualizing and measuring democracy.");
//$page->snp('image', "/copyrighted/");



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The V-Dem Institute (Varieties of Democracy) is a research institute
	founded by Professor ${'Staffan Lindberg'}.
	It studies the qualities of government, and evaluates countries according to seven different qualities of democracy:</p>
	<ul>
		<li>consensual democracy</li>
		<li>deliberative democracy</li>
		<li>egalitarian democracy</li>
		<li>electoral democracy</li>
		<li>liberal democracy</li>
		<li>majoritarian democracy</li>
		<li>participatory democracy</li>
	</ul>

	<p>Researchers working with the V-Dem Institute include: ${'Karoline Wiesner'}
	who co-authored the research paper: ${'The Hidden Dimension in Democracy'}.</p>
	HTML;



$div_The_V_Dem_Dataset = new WebsiteContentSection();
$div_The_V_Dem_Dataset->setTitleText('The V-Dem Dataset');
$div_The_V_Dem_Dataset->setTitleLink('https://www.v-dem.net/data/the-v-dem-dataset/');
$div_The_V_Dem_Dataset->content = <<<HTML
	<p>The V-Dem Dataset includes the world's most comprehensive and detailed democracy ratings.
	The latest version of the dataset and associated reference documents can be downloaded free of charge.
	NOTE: this only includes data of countries that have some form of democracy.</p>
	HTML;



$div_Varieties_of_Democracy_V_Dem = new WebsiteContentSection();
$div_Varieties_of_Democracy_V_Dem->setTitleText('Varieties of Democracy (V-Dem)');
$div_Varieties_of_Democracy_V_Dem->setTitleLink('https://v-dem.net/');
$div_Varieties_of_Democracy_V_Dem->content = <<<HTML
	<p>Varieties of Democracy (V-Dem) is a unique approach to conceptualizing and measuring democracy.
	V-Dem distinguishes between five high-level principles of democracy:
	electoral, liberal, participatory, deliberative, and egalitarian, and collects data to measure these principles.</p>
	HTML;



$div_codeberg_V_dem = new CodebergContentSection();
$div_codeberg_V_dem->setTitleText('V-dem');
$div_codeberg_V_dem->setTitleLink('https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/35');
$div_codeberg_V_dem->content = <<<HTML
	<p>The V-Dem institute has lots of content: which parts of their research can we use illustrate our articles?</p>
	HTML;



$div_wikipedia_V_Dem_Institute = new WikipediaContentSection();
$div_wikipedia_V_Dem_Institute->setTitleText('V Dem Institute');
$div_wikipedia_V_Dem_Institute->setTitleLink('https://en.wikipedia.org/wiki/V-Dem_Institute');
$div_wikipedia_V_Dem_Institute->content = <<<HTML
	<p>The V-Dem Institute publishes a number of high-profile datasets that describe qualities of different governments,
	annually published and publicly available for free.</p>
	HTML;

$div_wikipedia_Democracy_indices_V_Dem = new WikipediaContentSection();
$div_wikipedia_Democracy_indices_V_Dem->setTitleText('Democracy indices V Dem');
$div_wikipedia_Democracy_indices_V_Dem->setTitleLink('https://en.wikipedia.org/wiki/Democracy_indices_(V-Dem)');
$div_wikipedia_Democracy_indices_V_Dem->content = <<<HTML
	<p>The Democracy Indices by V-Dem are a dataset that describe qualities of different governments.
	Datasets include information on hundreds of indicator variables describing all aspects of government,
	especially on the quality of democracy, inclusivity, and other economic indicators.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->body($div_stub);

$page->body($div_introduction);
$page->body($div_Varieties_of_Democracy_V_Dem);
$page->body($div_codeberg_V_dem);
$page->body($div_The_V_Dem_Dataset);
$page->body('types_of_democracy.html');
$page->body($div_wikipedia_V_Dem_Institute);
$page->body($div_wikipedia_Democracy_indices_V_Dem);
