<?php
$page = new Page();
$page->h1('Don Miguel Ruiz');
$page->keywords('Don Miguel Ruiz');
$page->stars(0);

$page->snp('description', 'Mexican author.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Don Miguel Ruiz is the author of ${'The Four Agreements'}.</p>
	HTML;

$div_wikipedia_Don_Miguel_Ruiz = new WikipediaContentSection();
$div_wikipedia_Don_Miguel_Ruiz->setTitleText('Don Miguel Ruiz');
$div_wikipedia_Don_Miguel_Ruiz->setTitleLink('https://en.wikipedia.org/wiki/Don_Miguel_Ruiz');
$div_wikipedia_Don_Miguel_Ruiz->content = <<<HTML
	<p>Miguel Ángel Ruiz Macías (born 1952) is a Mexican author of Toltec spiritual and neoshamanistic texts.</p>
	HTML;


$page->parent('list_of_people.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_wikipedia_Don_Miguel_Ruiz);
