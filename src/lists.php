<?php
$page = new Page();
$page->h1('Lists and topics');
$page->stars(2);

$page->preview( <<<HTML
	<p>List of lists and topical entry pages.</p>
	HTML );

$page->snp('description', 'An entrance into the rabbit hole...');
//$page->snp('image', '/copyrighted/');

$h2_Lists = new h2HeaderContent('Lists');


$div_lists = new ContentSection();
$div_lists->content = <<<HTML
	<p>List of lists.</p>
	HTML;


$h2_Topics = new h2HeaderContent('Topics');

$div_topics = new ContentSection();
$div_topics->content = <<<HTML
	<p>Here are the main topical entry pages.</p>
	HTML;




$page->body('menu.html');

$page->body($h2_Lists);
$page->body($div_lists);
$page->body('list_of_books.html');
$page->body('list_of_indices.html');
$page->body('list_of_movies.html');
$page->body('list_of_organisations.html');
$page->body('list_of_people.html');
$page->body('list_of_research_and_reports.html');
$page->body('world.html');

$page->body($h2_Topics);
$page->body($div_topics);
$page->body('democracy.html');
$page->body('social_justice.html');
$page->body('environment.html');
$page->body('politics.html');
$page->body('political_discourse.html');
$page->body('religion.html');
$page->body('global_issues.html');
