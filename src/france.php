<?php
$page = new CountryPage('France');
$page->h1('France');
$page->keywords('France');
$page->stars(0);

$page->snp('description', '68 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_France = new WikipediaContentSection();
$div_wikipedia_France->setTitleText('France');
$div_wikipedia_France->setTitleLink('https://en.wikipedia.org/wiki/France');
$div_wikipedia_France->content = <<<HTML
	<p>France, officially the French Republic, is a country located primarily in Western Europe.
	It also includes overseas regions and territories in the Americas and the Atlantic, Pacific and Indian Oceans,
	giving it one of the largest discontiguous exclusive economic zones in the world.</p>
	HTML;

$div_wikipedia_French_ban_on_face_covering = new WikipediaContentSection();
$div_wikipedia_French_ban_on_face_covering->setTitleText('French ban on face covering');
$div_wikipedia_French_ban_on_face_covering->setTitleLink('https://en.wikipedia.org/wiki/French_ban_on_face_covering');
$div_wikipedia_French_ban_on_face_covering->content = <<<HTML
	<p>The French ban on face covering is an act of parliament passed by the Senate of France on 14 September 2010,
	resulting in the ban on the wearing of face-covering headgear, including masks, helmets, balaclavas, niqābs
	and other veils covering the face in public places, except under specified circumstances.</p>
	HTML;

$div_wikipedia_French_law_on_secularity_and_conspicuous_religious_symbols_in_schools = new WikipediaContentSection();
$div_wikipedia_French_law_on_secularity_and_conspicuous_religious_symbols_in_schools->setTitleText('French law on secularity and conspicuous religious symbols in schools');
$div_wikipedia_French_law_on_secularity_and_conspicuous_religious_symbols_in_schools->setTitleLink('https://en.wikipedia.org/wiki/French_law_on_secularity_and_conspicuous_religious_symbols_in_schools');
$div_wikipedia_French_law_on_secularity_and_conspicuous_religious_symbols_in_schools->content = <<<HTML
	<p>The French law on secularity and conspicuous religious symbols in schools
	bans wearing conspicuous religious symbols in French public primary and secondary schools.
	The law is an amendment to the French Code of Education that expands principles founded in existing French law,
	especially the constitutional requirement of laïcité: the separation of state and religious activities.</p>
	HTML;

$div_wikipedia_Calais_Jungle = new WikipediaContentSection();
$div_wikipedia_Calais_Jungle->setTitleText('Calais Jungle');
$div_wikipedia_Calais_Jungle->setTitleLink('https://en.wikipedia.org/wiki/Calais_Jungle');
$div_wikipedia_Calais_Jungle->content = <<<HTML
	<p>The Calais Jungle (known officially as Camp de la Lande) was
	a refugee and immigrant encampment in the vicinity of Calais, France that existed from January 2015 to October 2016.
	There had been other camps known as "jungles" in previous years,
	but this particular shanty town drew global media attention during the peak of the European migrant crisis in 2015, when its population grew rapidly.
	Migrants stayed at the camp while they attempted to enter the United Kingdom,
	or while they waited for their French asylum claims to be processed.</p>
	HTML;


$page->parent('world.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_France);
$page->body($div_wikipedia_French_ban_on_face_covering);
$page->body($div_wikipedia_French_law_on_secularity_and_conspicuous_religious_symbols_in_schools);
$page->body($div_wikipedia_Calais_Jungle);
