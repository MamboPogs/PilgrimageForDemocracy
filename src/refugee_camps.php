<?php
$page = new Page();
$page->h1('Refugee camps');
$page->keywords('Refugee camps', 'refugee camp', 'refugee camps');
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );


$r1 = $page->ref('https://www.aljazeera.com/news/longform/2023/8/25/what-is-life-like-inside-the-worlds-largest-refugee-camp', 'What is life like inside the world’s biggest refugee camp?');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Most $refugee camps are administered by the ${'United Nations High Commissioner for Refugees'}.</p>

	<p>See also $right to $housing and the ${'Rohingya people'} $r1.</p>
	HTML;

$div_wikipedia_Refugee_camp = new WikipediaContentSection();
$div_wikipedia_Refugee_camp->setTitleText('Refugee camp');
$div_wikipedia_Refugee_camp->setTitleLink('https://en.wikipedia.org/wiki/Refugee_camp');
$div_wikipedia_Refugee_camp->content = <<<HTML
	<p>A refugee camp is a temporary settlement built to receive refugees and people in refugee-like situations.
	Refugee camps usually accommodate displaced people who have fled their home country,
	but camps are also made for internally displaced people.
	Usually, refugees seek asylum after they have escaped war in their home countries,
	but some camps also house environmental and economic migrants.
	Camps with over a hundred thousand people are common, but as of 2012, the average-sized camp housed around 11,400.
	They are usually built and run by a government, the United Nations,
	international organizations (such as the International Committee of the Red Cross), or non-governmental organization.
	Unofficial refugee camps, such as Idomeni in Greece or the Calais jungle in France,
	are where refugees are largely left without the support of governments or international organizations.</p>
	HTML;


$page->parent('social_justice.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_wikipedia_Refugee_camp);
