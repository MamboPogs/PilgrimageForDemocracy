<?php
$page = new Page();
$page->h1('Immigration');
$page->keywords('Immigration', 'immigration');
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Massive, illegal immigration comes with a huge cost in terms of human suffering.
	The scale of immigration is driven by lack of $democracy and basic human $rights in the $countries of origin,
	and by the lack of ${'social justice'} on a $global scale,
	with the destination countries, like the $USA or $Europe being vastly wealthier
	than the countries migrants and $refugees originate from.
	All of these factors create a massive imbalance on a global scale, which drives migratory forces.</p>

	<p>The ideal situation can easily be stated but reaching that situation is another matter.
	Given the ability to live safely, in good conditions, free of repression and extreme economic hardship,
	people would naturally prefer to stay close to home and family.</p>

	<p>However, creating the conditions for such an idea situation is a tall order.
	How do we create global democracy?
	How do we create a just international trading framework wherein each country gets its fair share
	and is able to provide food, $housing and security to its people?</p>

	<p>Creating an ever stronger barrier between rich countries and poor countries,
	between the haves and the have-nots, is the politically convenient solution,
	and in the short term there does not seem to be any way around it.
	It is however the source of much human suffering and it cannot be seen as an appropriate, humane, long term solution.</p>
	HTML;


$div_Immigration_to_Europe = new ContentSection();
$div_Immigration_to_Europe->content = <<<HTML
	<h3>Immigration to Europe</h3>

	<p>The ${'European Union'} is trying to prevent migrants from ever reaching its shores.</p>

	<p>In 2023, the $EU offered $Tunisia 100 million euros for border management, search and rescue,
	and returns “rooted in respect for human rights” to address migration,
	as part of a larger 1 billion euros loan package.</p>
	HTML;


$div_wikipedia_Immigration = new WikipediaContentSection();
$div_wikipedia_Immigration->setTitleText('Immigration');
$div_wikipedia_Immigration->setTitleLink('https://en.wikipedia.org/wiki/Immigration');
$div_wikipedia_Immigration->content = <<<HTML
	<p>Immigration is the international movement of people
	to a destination country of which they are not natives or where they do not possess citizenship
	in order to settle as permanent residents or naturalized citizens.
	Commuters, tourists, and other short-term stays in a destination country
	do not fall under the definition of immigration or migration;
	seasonal labour immigration is sometimes included, however.</p>
	HTML;

$div_wikipedia_International_migration = new WikipediaContentSection();
$div_wikipedia_International_migration->setTitleText('International migration');
$div_wikipedia_International_migration->setTitleLink('https://en.wikipedia.org/wiki/International_migration');
$div_wikipedia_International_migration->content = <<<HTML
	<p>International migration occurs when people cross state boundaries
	and stay in the host state for some minimum length of the time.
	Migration occurs for many reasons.
	Many people leave their home countries in order to look for economic opportunities in another country.
	Others migrate to be with family members who have migrated or because of political conditions in their countries.
	Education is another reason for international migration, as students pursue their studies abroad,
	although this migration is often temporary, with a return to the home country after the studies are completed.</p>
	HTML;

$div_wikipedia_Human_migration = new WikipediaContentSection();
$div_wikipedia_Human_migration->setTitleText('Human migration');
$div_wikipedia_Human_migration->setTitleLink('https://en.wikipedia.org/wiki/Human_migration');
$div_wikipedia_Human_migration->content = <<<HTML
	<p>Human migration is the movement of people from one place to another
	with intentions of settling, permanently or temporarily, at a new location (geographic region).</p>
	HTML;


$page->parent('social_justice.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body($div_Immigration_to_Europe);


$page->body($div_wikipedia_Immigration);
$page->body($div_wikipedia_International_migration);
$page->body($div_wikipedia_Human_migration);
