<?php
$page = new Page();
$page->h1('United Nations');
$page->stars(2);
$page->keywords('United Nations', 'UN');

$page->preview( <<<HTML
	<p>The United Nations has a mandate to maintain peace among nations.</p>
	HTML );

$r1 = $page->ref('https://www.un.org/securitycouncil/content/voting-system', 'Voting System at the United Nations Security Council');
$r2 = $page->ref('https://www.securitycouncilreport.org/about-security-council-report', 'About Security Council Report');
$r3 = $page->ref('https://www.securitycouncilreport.org/monthly-forecast/2023-01/rule-of-law.php', 'Security Council report - Thematic issue: Rule of Law');
$r4 = $page->ref('https://www.securitycouncilreport.org/monthly-forecast/2014-04/ukraine.php', 'April 2014 Monthly Forecast: Ukraine');
$r5 = $page->ref('https://www.securitycouncilreport.org/monthly-forecast/2014-04/in_hindsight_obligatory_abstentions.php', 'In Hindsight: Obligatory Abstentions');
$r6 = $page->ref('https://www.securitycouncilreport.org/monthly-forecast/2014-04/in_hindsight_obligatory_abstentions.php', 'In Hindsight: Obligatory Abstentions');


$list = new ListOfPages();
$list->add('united_nations_high_commissioner_for_refugees.html');
$print_list = $list->print();

$div_List_of_UN_Agencies = new ContentSection();
$div_List_of_UN_Agencies->content = <<<HTML
	<h3>List of UN Agencies</h3>

	$print_list
	HTML;


$h2_UN_democracy_human_rights = new h2HeaderContent('The United Nations: Human Rights and Democracy');


$div_UN_democracy_human_rights = new ContentSection();
$div_UN_democracy_human_rights->content = <<<HTML
	<h3>An undemocratic institution</h3>

	<p>The United Nations's mandates include many aspects relevant to democracy and social justice.
	However, the UN itself is not a democratic body.
	Its member states include all existing dictatorships and authoritarian regimes.
	The individual participants of the UN General Assembly are not elected ambassadors named by the member countries.
	The budget of the UN is also contributed by the member states, whether democratic countries or not.
	The top two contributors to the budget for the period 2019–2021 are:
	the United States (22%) and the People's Republic of China (12%).</p>

	<p>Two of the five permanent members of the UN Security Council are authoritarian regimes,
	namely: the People's Republic of China, and the Russian Federation.</p>
	HTML;


$div_UN_programs = new ContentSection();
$div_UN_programs->content = <<<HTML
	<h3>UN programs</h3>

	<p>The following UN programs are of particular interest (Wikipedia):</p>

	<ul>
	<li><a href="https://en.wikipedia.org/wiki/International_Court_of_Justice">International Court of Justice</a></li>
	<li><a href="https://en.wikipedia.org/wiki/United_Nations_Commission_on_Human_Rights">United Nations Commission on Human Rights</a></li>
	<li><a href="https://en.wikipedia.org/wiki/World_Conference_on_Human_Rights">World Conference on Human Rights</a></li>
	<li><a href="https://en.wikipedia.org/wiki/United_Nations_Human_Rights_Council">United Nations Human Rights Council</a></li>
	<li><a href="https://en.wikipedia.org/wiki/Convention_on_the_Rights_of_the_Child">onvention on the Rights of the Child</a></li>
	<li><a href="https://en.wikipedia.org/wiki/Convention_on_the_Elimination_of_All_Forms_of_Discrimination_Against_Women"
	                                          >Convention on the Elimination of All Forms of Discrimination Against_Women</a></li>
	</ul>

	HTML;


$div_UN_security_council_veto_abstention = new ContentSection();
$div_UN_security_council_veto_abstention->id('veto-abstention');
$div_UN_security_council_veto_abstention->content = <<<HTML
	<h3>Security Council: veto and obligation to abstain</h3>

	<p>The most important body within the UN is the Security Council,
	which is composed of 15 member countries, 5 of which are permanent members.
	The Voting system at the United Nations Security Council grants the five permanent members the right to veto any decision:</p>

	<blockquote>
	<p><strong>Vote and Majority Required</strong></p>

	<p>Article 27 of the UN Charter states that:</p>

	<ol>
	<li>Each member of the Security Council shall have one vote.</li>
	<li>Decisions of the Security Council on procedural matters shall be made by an affirmative vote of nine members.</li>
	<li>Decisions of the Security Council on all other matters shall be made by an affirmative vote of nine members
	including the concurring votes of the permanent members;
	<strong>provided that, in decisions under Chapter VI and under paragraph 3 of Article 52, a party to a dispute shall abstain from voting</strong>.</li>
	</ol>

	<p><strong>The Right to Veto</strong></p>

	<p>The creators of the United Nations Charter conceived that five countries
	— $China, $France, the Union of Soviet Socialist Republics (USSR) [which was succeeded in 1990 by the Russian Federation],
	the United Kingdom and the United States —,
	because of their key roles in the establishment of the United Nations,
	would continue to play important roles in maintaining international peace and security.</p>

	<p>They were granted the special status of Permanent Member States at the Security Council,
	along with a special voting power known as the "right to veto".
	It was agreed by the drafters that if any one of the five permanent members cast a negative vote in the 15-member Security Council,
	the resolution or decision would not be approved.</p>

	<p>All five permanent members have exercised the right of veto at one time or another.
	If a permanent member does not fully agree with a proposed resolution but does not wish to cast a veto, they may choose to abstain,
	thus allowing the resolution to be adopted if it obtains the required number of nine favourable votes. {$r1}</p>
	</blockquote>

	<p>We have here a double problem.</p>

	<p>Firstly, two of the five permanent members are authoritarian regimes.
	All permanent members exercise their veto powers according to their own interests,
	but in the case of Russia and China, it can be expected to be done against the interests of peace and democracy.</p>

	<p>Secondly, despite the provision highlighted above,
	the Security Council members do <em>not</em> abstain from voting on issues in which they are parties.
	Thus, The People's Republic of China has constantly voted against any representation of Taiwan at the UN.
	And Russia's veto power has prevented the UN from adopting any meaningful resolution against Russia's war in Ukraine.</p>

	<p>The problem is compounded by the fact that the democratic members of the Security Council
	do not seem to be willing to impose the obligation of abstention.
	In 2014, following Russia's veto on a resolution about the situation in Ukraine,
	the <a href="#security-council-report">Security Council Report</a> published
	an article titled <q>In Hindsight: Obligatory Abstentions</q>, which provides a good summary of the use of the veto,
	and of the failure of the Security Council to enforce the obligation to abstain for members who are parties to a given conflict.
	The Security Council Report writes:</p>

	<blockquote>
	<p>Article 27(3) of the UN Charter not only enshrines the veto power of permanent members,
	but also institutes a limitation of this power through the principle of obligatory abstentions.
	In providing that “in decisions under Chapter VI, and under paragraph 3 of Article 52,
	a party to a dispute shall abstain from voting”, the Charter seeks to ensure that
	a Council member “should not be allowed to be party, judge and jury at the same time”.</p>

	<p>Although obligatory abstentions are a compromise, slightly tempering the scope of the veto,
	they apply in equal measure to permanent and non-permanent members:
	any member of the Security Council may be required to abstain from voting on a decision on which it is a party to the dispute. (...)</p>

	<p>The practice of the Security Council, and its members, in terms of raising and <strong>complying with Article 27(3) abstentions,
	has been inconsistent since 1946, and basically inexistent since 17 April 2000</strong> (...).
	With the exception of the UK in 1947, <strong>permanent members have never shown an interest in raising the matter,
	and non-permanent members have only done so sporadically.</strong></p>

	<p>(...)</p>

	<p>Obligatory abstentions are rare.
	There have been only six Council members that have abstained from voting in the Council,
	or else cast an abstention, explicitly or implicitly acknowledging Article 27(3).</p>
	</blockquote>

	<p>The article goes over all historical uses of the abstention under Article 27(3).
	It praises both Pakistan and India as a notable example of countries abiding with the spirit of the obligatory abstention
	in all the disputes between their two countries, at times when one or the other was at the Security Council.
	The Security Council Report concludes:</p>

	<blockquote>
	<p>As the last Article 27(3) abstention dates back to 23 June 1960,
	and the most recent reference to the spirit of the provision in a Council meeting dates back to 13 May 2003,
	it seems that <strong>Council members have little appetite for reviving this restriction.</strong>
	In practical terms, disregard by parties to a dispute that are non-permanent members has limited effects,
	as the adoption of a decision cannot be prevented if it enjoys nine affirmative votes.
	In the case of permanent members, however, if the other Council members forego Article 27(3) when applicable,
	nothing stands in the way of the permanent member to veto a decision under Chapter VI on a dispute to which it is a party.</p>

	<p>Negligence in the application of Article 27(3) abstentions risks not only reducing the provision to <li><a href="https://en.wikipedia.org/wiki/Desuetude">desuetude</a></li>,
	but also enlarging the scope for the use of the veto.</p>

	<p>The recent veto by Russia on a draft resolution under Chapter VI on the situation in Ukraine without discussion on Article 27(3)
	seems to confirm that <strong>Council members generally do not see any compelling interest in bringing the provision back to life.
	Obligatory abstentions seem to have vanished under a tacit agreement.</strong></p>
	</blockquote>

	<p>In summary, democratic countries within the Security Council
	are as much to blame for failing to impose the obligation of abstention.
	The UN now faces an uphill battle if it wants to gain back some of its lost credibility,
	and its power to maintain peace, and hopefully uphold democracy.</p>
	HTML;


$div_wikipedia_UN_security_council_veto_power = new WikipediaContentSection();
$div_wikipedia_UN_security_council_veto_power->setTitleText('United Nations Security Council veto power');
$div_wikipedia_UN_security_council_veto_power->setTitleLink('https://en.wikipedia.org/wiki/United_Nations_Security_Council_veto_power');
$div_wikipedia_UN_security_council_veto_power->content = <<<HTML
	<p>The United Nations Security Council veto power is the power of the five permanent members of the UN Security Council
	(China, France, Russia, the United Kingdom, and the United States)
	to veto any "substantive" resolution.
	Critics say that the veto is the most undemocratic element of the UN,
	as well as the main cause of inaction on war crimes and crimes against humanity,
	as it effectively prevents UN action against the permanent members and their allies.
	</p>

	<p>See also the following Wikipedia articles:</p>

	<ul>
	<li><a href="https://en.wikipedia.org/wiki/Reform_of_the_United_Nations">Reform of the United Nations</a></li>
	<li><a href="https://en.wikipedia.org/wiki/Reform_of_the_United_Nations_Security_Council">Reform of the United Nations Security Council</a></li>
	</ul>
	HTML;


$div_security_council_report = new ContentSection();
$div_security_council_report->id('security-council-report');
$div_security_council_report->content = <<<HTML
	<h3><a href="https://www.securitycouncilreport.org/">securitycouncilreport.org - Security Council Report</a></h3>

	<p>The Security Council Report's mission is to advance the transparency and effectiveness of the UN Security Council.
	They make available timely, balanced, high-quality information about the activities of the Council and its subsidiary bodies.
	They are founded by 25 countries, mostly democracies, and not including any of the Security Council's permanent members. {$r2}</p>

	<p>They provide further insight into the workings of the Security Council with their section "<em>What's In Blue</em>".
	The text is printed in blue when the Security Council approaches the final stage of negotiating a draft resolution.</p>
	<ul>
	<li><a href="https://www.securitycouncilreport.org/whatsinblue">What's In Blue</a></li>
	</ul>
	HTML;




$h2_UN_taiwan = new h2HeaderContent("Representation of Taiwan (Republic of China) in the United Nation");


$div_UN_taiwan = new ContentSection();
$div_UN_taiwan->content = <<<HTML
	<p>In addition to the Wikipedia articles listed further below,
	the following articles provide more contextual information about Taiwan:</p>

	<ul>
	<li><a href="https://en.wikipedia.org/wiki/Political_status_of_Taiwan">Political status of Taiwan</a></li>
	<li><a href="https://en.wikipedia.org/wiki/Taiwan,_China">Taiwan, China</a></li>
	<li><a href="https://en.wikipedia.org/wiki/Taiwan_independence_movement">Taiwan independence movement</a></li>
	<li><a href="https://en.wikipedia.org/wiki/United_Nations_General_Assembly_observers">United Nations General Assembly observers</a></li>
	</ul>
	HTML;

$div_wikipedia_china_UN = new WikipediaContentSection();
$div_wikipedia_china_UN->setTitleText('China and the United Nations');
$div_wikipedia_china_UN->setTitleLink('https://en.wikipedia.org/wiki/China_and_the_United_Nations');
$div_wikipedia_china_UN->content = <<<HTML
	<p>This Wikipedia article covers the periods when China was represented at the UN
	by the Republic of China (1945-1971)
	and by the People's Republic of China (1971-present).
	</p>
	<p>It includes a section about the ongoing efforts to reintroduce Taiwan to the UN.
	Taiwan (ROC)'s full membership needs the approval of the UN Security Council, which the PRC holds a veto.</p>
	HTML;

$div_wikipedia_UNGA_resolution_2758 = new WikipediaContentSection();
$div_wikipedia_UNGA_resolution_2758->setTitleText('United Nations General Assembly Resolution 2758');
$div_wikipedia_UNGA_resolution_2758->setTitleLink('https://en.wikipedia.org/wiki/United_Nations_General_Assembly_Resolution_2758');
$div_wikipedia_UNGA_resolution_2758->content = <<<HTML
	<p>The resolution, passed on 25 October 1971,
	recognized the People's Republic of China (PRC) as "the only legitimate representative of China to the United Nations"
	and removed "the representatives of Chiang Kai-shek" (referring to Republic of China (ROC)) from the United Nations.</p>

	<p>The resolution left the issue of Taiwan's representation unresolved in a practical sense.
	The ROC government continues to hold de facto control over Taiwan and other islands.</p>
	HTML;

$div_wikipedia_taiwan_UN_membership_referendum = new WikipediaContentSection();
$div_wikipedia_taiwan_UN_membership_referendum->setTitleText('2008 Taiwanese United Nations membership referendum');
$div_wikipedia_taiwan_UN_membership_referendum->setTitleLink('https://en.wikipedia.org/wiki/2008_Taiwanese_United_Nations_membership_referendum');
$div_wikipedia_taiwan_UN_membership_referendum->content = <<<HTML
	<p>Two referendums on United Nations membership applications were held in Taiwan on 22 March 2008, the same day as the presidential elections.
	Large majorities voted in favour of both proposals, but the necessary quorum was not reached.
	China protested and Western powers were perceived to cave in to pressure from the Chinese Communist Party.</p>
	HTML;




$h2_UN_russia_war_ukraine = new h2HeaderContent("The United Nation and Russia's war in Ukraine");


$div_UN_russia_war_ukraine = new ContentSection();
$div_UN_russia_war_ukraine->content = <<<HTML
	<h3>A double failure</h3>

	<p>The United Nations was established just after World War II, in 1946, to promote peace among nations.
	In that regard, the UN had a similar mandate to its predecessor, the League of Nations.
	The League of Nations was disbanded after its failure to prevent World War II, and for its utter powerlessness during the war.
	Today, the UN is coming under similar criticism, notably from Ukraine's president Zelensky, for its inability to prevent Russia's war in Ukraine.</p>

	<p>As noted above:
	"<em>in decisions under Chapter VI, and under paragraph 3 of Article 52, a party to a dispute shall abstain from voting</em>".
	Despite this provision, Russia has not abstained from voting on matters related to its invasion of Ukraine,
	and the Security Council has failed to pass any meaningful resolutions to prevent or end the war.</p>

	<p>Already in 2014, after Russia invaded Crimea and incited a war in the Donbas,
	The Security Council tried to pass a draft resolution that <q>reaffirmed the sovereignty, unity and territorial integrity of Ukraine</q>.
	The resolution did not pass not so much because Russia vetoed it,
	but because all of the other Security Council members failed to point out that Russia had an obligation to abstain from the vote (<em>emphasis added</em>):</p>

	<blockquote>
	<p>Russia was able to veto the Chapter VI draft resolution
	as <strong>no challenges were raised</strong> as to whether or not it had to abstain as a party to a dispute,
	as envisaged in Article 27(3) of the UN Charter.</p>

	<p>(...)</p>

	<p>The single attempt at a decision was a weakly worded draft resolution
	which failed to mention Russia and made no reference to the strategic port of Sevastopol.
	As Council members were aware in advance that Russia would veto the draft,
	<strong>it is surprising that there was no previous discussion of whether or not it was a party to the dispute
	and whether it had to abstain from voting in accordance with Article 27(3)</strong>.
	Ultimately, the Council <strong>could have decided</strong>, with at least nine affirmative votes,
	<strong>to challenge Russia and oblige it to abstain from voting on the Chapter VI draft resolution.</strong>
	{$r4}</p>
	</blockquote>

	<p>The <a href="#security-council-report">Security Council Report</a> further states in 2014:</p>

	<blockquote>
	<p>Negligence in the application of Article 27(3) abstentions risks not only reducing the provision to desuetude,
	but also enlarging the scope for the use of the veto.</p>

	<p>The recent veto by Russia on a draft resolution under Chapter VI on the situation in Ukraine without discussion on Article 27(3)
	seems to confirm that <strong>Council members in general do not see any compelling interest in bringing the provision back to life.
	Obligatory abstentions seem to have vanished under a tacit agreement.</strong> {$r5}</p>
	</blockquote>

	<p>Thus, Russia's invasion of Ukraine represents a double failure of the United Nations as a whole,
	and of the UN Security Council in particular.
	First, there is the obvious failure to prevent the war.
	Secondly, there is the failure to prevent a party to the war from imposing its veto,
	depriving the UN of any real means to take action against the aggressor.</p>

	<p>The question should be asked to all members of the Security Council,
	and especially to those who now support Ukraine:
	why didn't they raise Article 27(3) and demand that Russia abstains on any UNSC vote on Ukraine?</p>

	<p>Another important actor that must be mentioned here: the People's Republic of China.
	The same document states:</p>

	<blockquote>
	China may well become the “arbiter” on future Council outcomes on Ukraine.
	If Russia is cornered into an Article 27(3) abstention,
	China could eventually decide to veto any future Chapter VI draft resolution(s) out of solidarity with Russia.
	Doing so, however, would undermine the respect for territorial integrity and national sovereignty
	that China has upheld as core principles of its foreign policy.
	</blockquote>

	<p>For the Chinese Communist Party, <q>territorial integrity and national sovereignty</q> include Taiwan.
	China has the explicit policy to bring Taiwan under its control, by force if necessary.
	Any resolution about Ukraine that binds Russia has a good chance to be held against China about Taiwan in the future.
	The PRC already considers Taiwan as part of its territory (despite historical facts to the contrary).
	Thus, even if Russia had been forced to abstain from voting,
	China may very well have vetoed the resolution in view of its own territorial claims.</p>

	<h3>United Nations resolutions</h3>

	<p>United Nations resolutions on Russia's war in Ukraine (Wikipedia):</p>
	<ul>
	<li><a href="https://en.wikipedia.org/wiki/United_Nations_Security_Council_Resolution_2623">United Nations Security Council Resolution 2623</a></li>
	<li><a href="https://en.wikipedia.org/wiki/United_Nations_General_Assembly_Resolution_ES-11/1">United Nations General Assembly Resolution ES-11/1</a></li>
	<li><a href="https://en.wikipedia.org/wiki/United_Nations_General_Assembly_Resolution_ES-11/2">United Nations General Assembly Resolution ES-11/2</a></li>
	<li><a href="https://en.wikipedia.org/wiki/United_Nations_General_Assembly_Resolution_ES-11/3">United Nations General Assembly Resolution ES-11/3</a></li>
	<li><a href="https://en.wikipedia.org/wiki/United_Nations_General_Assembly_Resolution_ES-11/4">United Nations General Assembly Resolution ES-11/4</a></li>
	</ul>

	<p>The United Nations itself provides a fact sheet about the UN's dealing with Russia's war in Ukraine:</p>
	<ul>
	<li><a href="https://unric.org/en/the-un-and-the-war-in-ukraine-key-information/">The UN and the war in Ukraine: key information</a></li>
	</ul>
	<p>Different UN bodies are engaged within Ukraine:</p>
	<ul>
	<li>UN Development Programme: <a href="https://www.undp.org/war-ukraine">UNDP's response: War in Ukraine</a></li>
	<li>UN Office of the High Commissioner for Human Rights: <a href="https://www.ohchr.org/en/countries/ukraine">OHCHR: Ukraine</a></li>
	<li>UN Office of the High Commissioner for Human Rights: <a href="https://www.ohchr.org/en/countries/ukraine/our-presence">UHCHR: UN Human Rights in Ukraine</a></li>
	</ul>

	<p>The <a href="#security-council-report">Security Council Report</a> provides the following summary
	about the UN's handling of Russia's war in Ukraine, as of early 2023: {$r3}</p>

	<blockquote>
	<p>In relation to Ukraine, on 26 February 2022,
	Ukraine instituted proceedings against Russia before the ICJ, which rendered provisional measures on 16 March,
	ordering Russia to immediately suspend the military operations it commenced on 24 February 2022 in Ukrainian territory, among other matters.</p>

	<p>On 2 March, ICC Prosecutor Karim Asad Ahmad Khan announced that he had decided to proceed
	with an active investigation into the situation in Ukraine after receiving referrals from 39 ICC States Parties.</p>

	<p>After holding an “urgent debate on the situation of human rights in Ukraine stemming from the Russian aggression” on 3 and 4 March,
	the Human Rights Council (HRC) established the Independent International Commission of Inquiry (COI) on Ukraine.
	The COI’s responsibilities include investigating
	“all alleged violations and abuses of human rights and violations of international humanitarian law, and related crimes
	in the context of aggression against Ukraine by the Russian Federation”,
	as well as preserving information, documentation and evidence of such violations and abuses in view of any future legal proceedings.</p>

	<p>Ukraine is also advocating for creating a special international tribunal to prosecute high-level Russian officials for the crime of aggression.</p>

	<p>On 14 November 2022, the General Assembly adopted a resolution recommending the creation of a register
	to document damages “caused by internationally wrongful acts of [Russia] in or against Ukraine”.
	The register is also intended to promote and coordinate evidence gathering.</p>
	</blockquote>

	<p>The <a href="#security-council-report">Security Council Report</a> has a list of publications about Ukraine:</p>
	<ul>
	<li><a href="https://www.securitycouncilreport.org/ukraine/">SCR: Ukraine</a></li>
	</ul>
	HTML;


$div_wikipedia_legality_russian_invasion_ukraine = new WikipediaContentSection();
$div_wikipedia_legality_russian_invasion_ukraine->setTitleText('Legality of the 2022 Russian invasion of Ukraine');
$div_wikipedia_legality_russian_invasion_ukraine->setTitleLink('https://en.wikipedia.org/wiki/Legality_of_the_2022_Russian_invasion_of_Ukraine');
$div_wikipedia_legality_russian_invasion_ukraine->content = <<<HTML
	<p>This article includes the responses at the United Nations as well as at the International Criminal Court.</p>
	HTML;



$div_codeberg = new CodebergContentSection();
$div_codeberg->setTitleText('The United Nations: Human Rights and Democracy');
$div_codeberg->setTitleLink('https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/17');
$div_codeberg->content = <<<HTML
	<p>Improve this page about the United Nations,
	its role in maintaining peace, upholding Human Rights and promoting democracy,
	the existence or absence of its mandate to do so, and its successes and failures in doing so.</p>
	HTML;

$div_wikipedia_united_nations = new WikipediaContentSection();
$div_wikipedia_united_nations->setTitleText('United Nations');
$div_wikipedia_united_nations->setTitleLink('https://en.wikipedia.org/wiki/United_Nations');
$div_wikipedia_united_nations->content = <<<HTML
	<p>The stated purposes of the United Nations (UN) are to maintain international peace and security,
	develop friendly relations among nations, achieve international cooperation, and be a centre for harmonizing the actions of nations.</p>
	HTML;


$div_wikipedia_UN_democracy_fund = new WikipediaContentSection();
$div_wikipedia_UN_democracy_fund->setTitleText('United Nations Democracy Fund');
$div_wikipedia_UN_democracy_fund->setTitleLink('https://en.wikipedia.org/wiki/United_Nations_Democracy_Fund');
$div_wikipedia_UN_democracy_fund->content = <<<HTML
	<p>The United Nations Democracy Fund (UNDEF) was created by UN Secretary-General Kofi A. Annan in 2005
	as a United Nations General Trust Fund to support democratization efforts worldwide.</p>

	<p>UNDEF supports projects that strengthen the voice of civil society, promote human rights,
	and encourage the participation of all groups in democratic processes.</p>
	HTML;


$div_wikipedia_league_of_nations = new WikipediaContentSection();
$div_wikipedia_league_of_nations->setTitleText('League of Nations');
$div_wikipedia_league_of_nations->setTitleLink('https://en.wikipedia.org/wiki/League_of_Nations');
$div_wikipedia_league_of_nations->content = <<<HTML
	<p>The League of Nations (1920–1946) was the first worldwide intergovernmental organisation
	whose principal mission was to maintain world peace.
	The League's failure to prevent in any way World War II and its other perceived weaknesses led to its demise.
	It was succeeded by the United Nations.</p>
	HTML;


$page->parent('world.html');
$page->body($div_codeberg);

$page->body($div_List_of_UN_Agencies);

$page->body($h2_UN_democracy_human_rights);
$page->body($div_UN_democracy_human_rights);
$page->body($div_wikipedia_united_nations);
$page->body($div_UN_programs);
$page->body($div_wikipedia_UN_democracy_fund);
$page->body($div_UN_security_council_veto_abstention);
$page->body($div_wikipedia_UN_security_council_veto_power);
$page->body($div_security_council_report);

$page->body($h2_UN_taiwan);
$page->body($div_UN_taiwan);
$page->body($div_wikipedia_china_UN);
$page->body($div_wikipedia_UNGA_resolution_2758);
$page->body($div_wikipedia_taiwan_UN_membership_referendum);

$page->body($h2_UN_russia_war_ukraine);
$page->body($div_UN_russia_war_ukraine);
$page->body($div_wikipedia_legality_russian_invasion_ukraine);
$page->body($div_wikipedia_league_of_nations);
