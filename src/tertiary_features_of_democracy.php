<?php
$page = new Page();
$page->h1('3: Tertiary feature of democracy &mdash; the Professionals');
$page->stars(0);

$page->preview( <<<HTML
	<p>The primary and secondary levels laid out the theoretical foundations, and stated the core values of democracies.
	The tertiary features of democracy are more about the concrete steps to make it work.
	We shall explore the core institutions, as well as the professionalism and practicalities required.</p>

	<p>The tertiary flaws of democracy are:
		<span class="democracy flaw">incompetence</span>,
		<span class="democracy flaw">corruption</span>,
		<span class="democracy flaw">cronyism</span>.</p>
	<p>The tertiary virtues of democracy are:
		<span class="democracy virtue">competence</span>,
		<span class="democracy virtue">professionalism</span>,
		<span class="democracy virtue">integrity</span>,
		<span class="democracy virtue">public service</span>.</p>
	HTML );


$page->snp('description', "Making living together work.");
//$page->snp('image', "/copyrighted/");



$h2_Making_it_work = new h2HeaderContent('Making it work');


$div_What_side_to_drive_in_England = new ContentSection();
$div_What_side_to_drive_in_England->content = <<<HTML
	<h3>What side to drive in England?</h3>
	HTML;




$h2_institutions = new h2HeaderContent('Institutions');

$div_institutions = new ContentSection();
$div_institutions->content = <<<HTML
	<h3></h3>
	<p>
	</p>
	HTML;


$h2_professionals = new h2HeaderContent('We, the Professionals');

$div_professionals = new ContentSection();
$div_professionals->content = <<<HTML
	<h3></h3>
	<p>
	</p>
	HTML;


$page->parent('primary_features_of_democracy.html');
$page->parent('secondary_features_of_democracy.html');

$page->body($h2_Making_it_work);
$page->body($div_What_side_to_drive_in_England);

$page->body($h2_institutions);
$page->body($div_institutions);

$page->body($h2_professionals);
$page->body($div_professionals);

include('section/features_of_democracy.php');
$page->body($div_section_what_is_democracy_quaternary_feature);
