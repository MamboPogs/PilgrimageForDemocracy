<?php
$page = new CountryPage('Rwanda');
$page->h1('Rwanda');
$page->keywords('Rwanda');
$page->stars(0);

$page->preview( <<<HTML
	<p></p>
	HTML );

$page->snp('description', '13 million inhabitants.');
//$page->snp('image',       '/copyrighted/');



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Rwanda is a perpetrator of ${'transnational authoritarianism'}.</p>
	HTML;

$div_wikipedia_Rwanda = new WikipediaContentSection();
$div_wikipedia_Rwanda->setTitleText('Rwanda');
$div_wikipedia_Rwanda->setTitleLink('https://en.wikipedia.org/wiki/Rwanda#');
$div_wikipedia_Rwanda->content = <<<HTML
	<p>Located a few degrees south of the Equator, Rwanda is bordered by Uganda, Tanzania, Burundi, and the Democratic Republic of the Congo.</p>
	HTML;

$div_wikipedia_Human_rights_in_Rwanda = new WikipediaContentSection();
$div_wikipedia_Human_rights_in_Rwanda->setTitleText('Human rights in Rwanda');
$div_wikipedia_Human_rights_in_Rwanda->setTitleLink('https://en.wikipedia.org/wiki/Human_rights_in_Rwanda');
$div_wikipedia_Human_rights_in_Rwanda->content = <<<HTML
	<p>Human rights in Rwanda have been violated on a grand scale.
	The greatest violation is the Rwandan genocide of Tutsi in 1994.
	The post-genocide government is also responsible for grave violations of human rights.</p>
	HTML;

$div_wikipedia_History_of_Rwanda = new WikipediaContentSection();
$div_wikipedia_History_of_Rwanda->setTitleText('History of Rwanda');
$div_wikipedia_History_of_Rwanda->setTitleLink('https://en.wikipedia.org/wiki/History_of_Rwanda');
$div_wikipedia_History_of_Rwanda->content = <<<HTML
	<p>Rwanda today struggles to heal and rebuild, showing signs of rapid economic development,
	but with growing international concern about the decline of human rights within the country.</p>
	HTML;


$page->parent('world.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_Rwanda);
$page->body($div_wikipedia_Human_rights_in_Rwanda);
$page->body($div_wikipedia_History_of_Rwanda);
