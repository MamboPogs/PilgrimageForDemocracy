<?php
$page = new Page();
$page->h1('Election integrity');
$page->stars(1);
$page->keywords('Election integrity', 'election integrity');

$page->preview( <<<HTML
	HTML );


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>In an age when there is so much defiance over the electoral process,
	in a society where populist candidates thrive on casting doubts about the integrity of the vote count,
	it is critical that we establish an election protocol beyond any doubt.</p>

	<p>The goal is not so much to rely on experts and election officials who would assure us that massive voter fraud did not occur,
	or indeed, that it is almost impossible.
	The goal is to create such a foolproof system that any well-intentioned voter can witness the integrity of the entire process with their own eyes.
	The chain of custody of cast ballots should be under constant voter supervision.</p>

	<p>This way, should anyone believe that some elections were rigged,
	they could be simply invited to stay the whole day at the polling station,
	and witness first the whole electoral process and vote count from beginning to end.
	Any doubt in their mind could then easily be dispelled.</p>

	<p>The purpose of this section is to clearly establish the protocol and conditions for a foolproof election,
	that any concerned voter could safely and responsibly supervise.</p>

	<p>We should document all the major pitfalls to avoid, and the proper procedure to establish.</p>
	HTML;

$div_Contested_election = new ContentSection();
$div_Contested_election->content = <<<HTML
	<h3>Contested election</h3>

	<p>A comprehensive list of contested election would be very long, and near impossible to establish.
	Here are a few examples of recent elections that have been contested.</p>

	<ul>
		<li>Certainly, the most famous example worldwide of a contested election is the 2020 $American presidential election:
			Donald Trump up to today falsely claims that the elections were rigged.</li>
		<li>In $Gabon, as soon as the results of the 2023 presidential election were announced,
			re-electing Ali Bongo for a third term, the military contested the result and staged a coup,
			putting an abrupt end to the long-running Bongo dynasty.</li>
	</ul>
	HTML;


$div_integrity = new ContentSection();
$div_integrity->content = <<<HTML
	<h3>Towards confidence in electoral integrity</h3>

	<p>Our recommendations would tend towards an electoral system which
	increases popular confidence in the integrity of the electoral process,
	while avoiding obvious pitfalls such as electoral $disfranchisement.</p>

	<p><strong>Single day voting on election day</strong> can be considered in the $USA.
	Indeed, it is the norm is many $democratic $countries.
	If so, that day should be a national holiday, so that by default most workers would have the day free to vote.
	Also, for those professions and services that cannot take a day off (e.g. nursing, emergency services, etc.),
	workers should have a guaranteed right to have their individual work schedule adjusted so that
	they have time to leave work, go to the polling station, queue, vote and come back to work.
	In addition, voting should be a guaranteed speedy process, not necessitating more than a given amount of time,
	so that citizens do not have to stand in line for hours on end.
	Thus, there should be enough polling stations, open from early to late, and properly staff,
	so that people can quickly vote and go on with their private lives or go back to work.</p>

	<p><strong>Paper ballots</strong> that can be manually recounted according to a clearly established procedure
	preventing in doubt ever forming in anybody's mind who cares enough to observe the process.
	There should be a clear established procedure for ballot chain of custody.</p>

	<p><strong>Government issued ID matching the voter file</strong>: this is another reasonable request
	which is the norm in most democratic countries ($France, ${'Republic of China'}, etc...),
	but its possible implementation in the $USA should be subject to much scrutiny in the details of the law.
	It would require government issued ID to be issued free of charge, regardless of socio-economic status.
	Moreover, such IDs much be pro-actively issued by the government at birth or when the individual reaches legal voting age,
	as is done in other countries.</p>
	HTML;

$h2_voting_machines = new h2HeaderContent('Voting machines');

$div_voting_machines = new ContentSection();
$div_voting_machines->content = <<<HTML
	<p>It is acceptable to use voting machines and scanners to assist in the electoral process and during the vote count,
	provided that there is a complete paper trail, and that the voters themselves can verify the voting by manually recounting votes in whole or batches.</p>

	<p>The goal is not only to ensure the complete integrity of the election,
	but also to gain the trust of every voter, regardless of their understanding of modern technologies.
	For voters who are having doubts but who can reasonably be persuaded, there is a difference between telling them:
	"Trust us, this machine counts everything accurately",
	and allowing them to see for themselves the votes being counted.</p>

	<p>Elections that are completely automated,
	like voting online or Blockchain-based voting systems,
	may certainly have their uses,
	but for important elections like presidential or legislative elections,
	we should require a method with a voter-verifiable paper trail.	</p>

	<p>The purpose of this section will be to fully document the minimum required protocol for machine-assisted elections,
	the acceptable use and prohibited use of voting machines, scanners, tabulators, etc., during an election.</p>
	HTML;

$page->parent('democracy.html');
$page->body($div_stub);

$page->body($div_introduction);
$page->body($div_Contested_election);
$page->body($div_integrity);

$page->body('disfranchisement.html');

$page->body($h2_voting_machines);
$page->body($div_voting_machines);
