<?php
$page = new Page();
$page->h1('Freedom of association');
$page->keywords('Freedom of association', 'freedom of association');
$page->stars(0);

$page->preview( <<<HTML
	<p></p>
	HTML );

$page->snp('description', 'The right of people to collectively express, promote, pursue and/or defend common interests.');
//$page->snp('image',       '/copyrighted/');



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Freedom of association is one of the ${'secondary features of democracy'}.</p>
	HTML;




$div_wikipedia_Freedom_of_association = new WikipediaContentSection();
$div_wikipedia_Freedom_of_association->setTitleText('Freedom of association');
$div_wikipedia_Freedom_of_association->setTitleLink('https://en.wikipedia.org/wiki/Freedom_of_association');
$div_wikipedia_Freedom_of_association->content = <<<HTML
	<p>Freedom of association encompasses both an individual's right to join or leave groups voluntarily,
	the right of the group to take collective action to pursue the interests of its members,
	and the right of an association to accept or decline membership based on certain criteria.
	It can be described as the right of a person coming together with other individuals
	to collectively express, promote, pursue and/or defend common interests.</p>
	HTML;


$page->parent('democracy.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body('secondary_features_of_democracy.html');
$page->body($div_wikipedia_Freedom_of_association);
