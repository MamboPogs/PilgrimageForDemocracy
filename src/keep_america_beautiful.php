<?php
$page = new Page();
$page->h1('Keep America Beautiful');
$page->keywords('Keep America Beautiful');
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Check the Wikipedia article about $greenwashing and corporate sponsorship.</p>

	<p>We need a comprehensive solution where litter is cleaned,
	and causes of littering must be solved upstream.</p>
	HTML;

$div_wikipedia_Keep_America_Beautiful = new WikipediaContentSection();
$div_wikipedia_Keep_America_Beautiful->setTitleText('Keep America Beautiful');
$div_wikipedia_Keep_America_Beautiful->setTitleLink('https://en.wikipedia.org/wiki/Keep_America_Beautiful');
$div_wikipedia_Keep_America_Beautiful->content = <<<HTML
	<p>Keep America Beautiful is a nonprofit organization founded in 1953.
	It is the largest community improvement organization in the United States,
	with more than 700 state and community-based affiliate organizations and more than 1,000 partner organizations.</p>

	<p>Keep America Beautiful aims to end littering, to improve recycling, and to beautify American communities.
	The organization's narrow focus on littering and recycling has been criticized as greenwashing
	in that it diverts responsibility away from corporations and industries.</p>
	HTML;


$page->parent('environment.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_wikipedia_Keep_America_Beautiful);
