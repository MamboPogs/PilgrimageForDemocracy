<?php
$page = new Page();
$page->h1('Institutions');
$page->stars(0);
$page->keywords('Institutions', 'institutions', 'institutional');


$page->snp('description', "The pillars of democracy");
$page->snp('image', "/copyrighted/colin-lloyd-_JEiyOfC2y8.1200-630.jpg");


$page->preview( <<<HTML
	<p>Healthy institutions are the most critical parts to safeguard democracy.</p>
	HTML );


$div_institutions = new ContentSection();
$div_institutions->content = <<<HTML
	<p>Healthy institutions are the most critical parts to safeguard democracy.</p>

	<p>Topics to be covered include:</p>

	<ul>
	<li>Branches of powers</li>
	<li>Separation of powers</li>
	<li>Electoral process and electoral institutions</li>
	<li>Corruption</li>
	<li>Judicial independence</li>
	<li>Accountability</li>
	</ul>

	<p>It is often said that with great power comes great responsibility.
	We have to make sure not to disconnect power and responsibility.
	(<em>The legal troubles of Donald Trump come to mind as an example...</em>)</p>
	HTML;


$list = new ListOfPages();
$list->add('constitution.html');
$list->add('politics.html');
$list->add('corruption.html');
$list->add('lawmaking.html');
$list->add('justice.html');
$list->add('list_of_organisations.html');
$list->add('professionalism_without_elitism.html');
$list->add('separation_of_powers.html');
$print_list = $list->print();

$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>Related content</h3>

	$print_list
	HTML;


$page->parent('menu.html');
$page->body($div_stub);
$page->body($div_institutions);
$page->body($div_list);
