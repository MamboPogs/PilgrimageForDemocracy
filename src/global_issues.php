<?php
$page = new Page();
$page->h1('Global Issues');
$page->stars(1);

$page->preview( <<<HTML
	<p>We live in an interconnected world, with issues affecting the whole of humanity.</p>
	HTML );

$page->snp('description', "We live in an interconnected world, with issues affecting the whole of humanity.");
$page->snp('image', "/copyrighted/ben-white-gEKMstKfZ6w.1200-630.jpg");


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	We live in an interconnected world, with issues affecting the whole of humanity.
	HTML;


$page->parent('lists.html');
$page->body($div_stub);
$page->body($div_introduction);

$page->body('education.html');
$page->body('hunger.html');
$page->body('global_natural_resources.html');
$page->body('peaceful_resistance_in_times_of_war.html');
$page->body('living_together.html');
$page->body('secondary_features_of_democracy.html');
