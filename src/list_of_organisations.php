<?php
$page = new Page();
$page->h1('List of organisations');
$page->stars(0);
$page->keywords('organisations', 'organisation');

$page->preview( <<<HTML
	<p>Organisation related to peace, democracy or social justice.</p>
	HTML );


$page->snp('description', "Working for peace, democracy and social justice.");
//$page->snp('image', "/copyrighted/");



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Below is a list of organisations that are covered in this website so far.</p>
	HTML;

$list = new ListOfPages();
$list->add('american_constitution_society.html');
$list->add('data_pop_alliance.html');
$list->add('food_and_agriculture_organization.html');
$list->add('freedom_house.html');
$list->add('global_witness.html');
$list->add('green_new_deal_network.html');
$list->add('human_rights_watch.html');
$list->add('institute_for_economics_and_peace.html');
$list->add('international_crisis_group.html');
$list->add('international_monetary_fund.html');
$list->add('keep_america_beautiful.html');
$list->add('oecd.html');
$list->add('reporters_without_borders.html');
$list->add('transparency_international.html');
$list->add('united_nations_high_commissioner_for_refugees.html');
$list->add('v_dem_institute.html');
$list->add('vision_of_humanity.html');
$list->add('world_future_council.html');
$print_list = $list->print();


$div_list = new ContentSection();

$div_list->content = <<<HTML
	<h3>list</h3>

	<p>Listed in alphabetical order.</p>

	$print_list
	HTML;



$page->parent('lists.html');

$page->body($div_introduction);
$page->body($div_stars);
$page->body($div_list);
$page->body('institutions.html');
