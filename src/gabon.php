<?php
$page = new CountryPage('Gabon');
$page->h1('Gabon');
$page->keywords('Gabon');
$page->stars(0);

$page->snp('description', '2.4 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Gabon President Ali Bongo, who had served two consecutive seven-year terms, was standing for a third term in 2023.
	His family has led Gabon for 56 years.</p>

	<p>As soon as the results of the 2023 presidential election were announced,
	re-electing Ali Bongo for a third term, the military contested the result and staged a coup,
	putting an abrupt end to the Bongo dynasty.</p>
	HTML;

$div_wikipedia_Gabon = new WikipediaContentSection();
$div_wikipedia_Gabon->setTitleText('Gabon');
$div_wikipedia_Gabon->setTitleLink('https://en.wikipedia.org/wiki/Gabon');
$div_wikipedia_Gabon->content = <<<HTML
	<p>Since its independence from $France in August 1960, the sovereign state of Gabon has had three presidents.
	In the 1990s, it introduced a multi-party system and a $democratic $constitution
	that aimed for a more transparent electoral process and reformed some governmental institutions.
	Despite this, the Gabonese Democratic Party (PDG) remains the dominant party.</p>
	HTML;

$div_wikipedia_Politics_of_Gabon = new WikipediaContentSection();
$div_wikipedia_Politics_of_Gabon->setTitleText('Politics of Gabon');
$div_wikipedia_Politics_of_Gabon->setTitleLink('https://en.wikipedia.org/wiki/Politics_of_Gabon');
$div_wikipedia_Politics_of_Gabon->content = <<<HTML
	<p>In March 1991 a new constitution was adopted.
	Among its provisions are a Western-style bill of rights, the creation of the National Council of Democracy
	that also oversees the guarantee of those rights and a governmental advisory board
	which deals with economic and social issues.
	Multi-party legislative elections were held in 1990-91 even though opposition parties had not been declared formally legal.</p>
	HTML;


$page->parent('world.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_Gabon);
$page->body($div_wikipedia_Politics_of_Gabon);
