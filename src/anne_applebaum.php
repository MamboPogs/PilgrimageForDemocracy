<?php
$page = new Page();
$page->h1('Anne Applebaum');
$page->stars(0);
$page->keywords('Anne Applebaum');

$page->preview( <<<HTML
	<p>
	</p>
	HTML );


$page->snp('description', "Journalist and historian, Applebaum wrote about the history of Communism");
//$page->snp('image', "/copyrighted/");

$r1 = $page->ref('https://t.me/United24media/10689', 'Political scientist and prominent columnist Denise Dresser (Mexico) came to Ukraine in June 2023');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Anne Applebaum is a journalist and historian.</p>

	<p>She is the author of ${'Twilight of Democracy'}.</p>

	<p>Applebaum was quoted by ${'Denise Dresser'} as having said: ${r1}</p>

	<blockquote>People too often think that democracy is like water from the tap:
	you open it; it comes out; and it will always be there.</blockquote>

	HTML;



$div_wikipedia_Anne_Applebaum = new WikipediaContentSection();
$div_wikipedia_Anne_Applebaum->setTitleText('Anne Applebaum');
$div_wikipedia_Anne_Applebaum->setTitleLink('https://en.wikipedia.org/wiki/Anne_Applebaum');
$div_wikipedia_Anne_Applebaum->content = <<<HTML
	<p>Anne Applebaum is an American Polish journalist and historian.
	She has written extensively about the history of Communism and the development of civil society in Central and Eastern Europe.</p>
	HTML;

$div_wikipedia_Gulag_A_History = new WikipediaContentSection();
$div_wikipedia_Gulag_A_History->setTitleText('Gulag A History');
$div_wikipedia_Gulag_A_History->setTitleLink('https://en.wikipedia.org/wiki/Gulag:_A_History');
$div_wikipedia_Gulag_A_History->content = <<<HTML
	<p>"Gulag: A History", also published as "Gulag: A History of the Soviet Camps", is a non-fiction book covering the history of the Soviet Gulag system.
	The book charts the history of the Gulag organization; from its beginnings under Vladimir Lenin and the Solovki prison camp...</p>
	HTML;

$page->parent('list_of_people.html');
$page->body($div_stub);

$page->body($div_introduction);

$page->body('twilight_of_democracy.html');

$page->body($div_wikipedia_Anne_Applebaum);
$page->body($div_wikipedia_Gulag_A_History);
