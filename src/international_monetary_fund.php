<?php
$page = new Page();
$page->h1('International Monetary Fund (IMF)');
$page->keywords('International Monetary Fund', 'IMF');
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_International_Monetary_Fund = new WikipediaContentSection();
$div_wikipedia_International_Monetary_Fund->setTitleText('International Monetary Fund');
$div_wikipedia_International_Monetary_Fund->setTitleLink('https://en.wikipedia.org/wiki/International_Monetary_Fund');
$div_wikipedia_International_Monetary_Fund->content = <<<HTML
	<p>The International Monetary Fund (IMF) is a major financial agency of the United Nations,
	and an international financial institution, headquartered in Washington, D.C., consisting of 190 countries.
	Its stated mission is "working to foster global monetary cooperation, secure financial stability, facilitate international trade,
	promote high employment and sustainable economic growth, and reduce poverty around the world."</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_wikipedia_International_Monetary_Fund);
