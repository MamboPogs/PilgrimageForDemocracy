<?php
$page = new Page();
$page->h1('Chinese expansionism');
$page->stars(2);
$page->keywords('Chinese expansionism', 'nine-dash line');

$page->preview( <<<HTML
	<p>The People's Republic of China has territorial disputes with most of its neighbors.
	The PRC government has a long term policy to gain control over territories it considers its own.</p>
	HTML );

$r1 = $page->ref('https://www.taipeitimes.com/News/front/archives/2022/12/22/2003791155', 'Manila frets over China’s island activity');
$r2 = $page->ref('https://en.wikipedia.org/wiki/Barbie_(film)', 'Wikipedia: Barbie (film)');
$r3 = $page->ref('https://en.wikipedia.org/wiki/BRP_Sierra_Madre', 'BRP Sierra Madre');

$h2_introduction = new h2HeaderContent('');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML

	<p>The <a href="/prc_china.html">People's Republic of China</a> has territorial disputes with most of its neighbors.
	There are unresolved conflicting territorial claims between China
	and practically all of its terrestrial and maritime neighbours.
	The PRC government has a long-term policy to gain control over territories it considers its own.</p>

	<p>It is commonly known that PRC claims Taiwan as its own
	as well as all the islands currently controlled by the Republic of China (Taiwan).<p>

	<p>Another bold territorial claim is represented by a map of the South China sea
	on which are drawn nine dashes encompassing most of the sea,
	putting the PRC in conflict with all of its maritime neighbours.</p>
	HTML;

$h2_taiwan = new h2HeaderContent('China (PRC) and Taiwan (ROC)');


$h2_nine_dash_line = new h2HeaderContent('The nine-dash line');

$div_nine_dash_line_intro = new ContentSection();
$div_nine_dash_line_intro->content = <<<HTML
	<p>The so-called "nine-dash line" is a map of the South China Sea released by the People's Republic of China
	on which a rough outline is drawn with nine dashes encompassing most of the South China Sea
	representing the PRC's maritime claims over the area.
	See the Wikipedia article for more details.</p>
	HTML;

$div_nine_dash_line_in_movies = new ContentSection();
$div_nine_dash_line_in_movies->content = <<<HTML
	<h3>The nine-dash line in movies</h3>

	<p>The $PRC is using all subliminal and not-so subliminal methods at its disposal to push forward its propaganda.
	Many Western movies have been subject to controversies for including images or scenes that agree with the propaganda of the Chinese Communist Party.</p>

	<p>The latest such movie is <strong>Barbie</strong>, a live action film featuring the Mattel Barbie doll,
	in which a child-like drawing of the world includes a nine-dash line alongside what would be the coast of China.
	As a result, the movie has been banned in Vietnam, and there were calls for it to be banned in the $Philippines as well. $r2
	The movie producers are claiming that no political statement was meant by the inclusion of the nine dashes.
	A better question would be: by which process and under which influence did the nine dashes find their way into the map depicted in the movie?
	Mattel's Barbie dolls are made in China.</p>
	HTML;


$h2_philippines = new h2HeaderContent('The PRC and the Philippines');


$div_philippines = new ContentSection();
$div_philippines->content = <<<HTML
	<p>The ${"People's Republic of China"}'s claims over the South China Sea has put it
	at odds with the $Philippines.
	In recent years, conflicts between the two countries have increased.</p>
	HTML;


$div_Spratly_Islands = new ContentSection();
$div_Spratly_Islands->content = <<<HTML
	<h3>Spratly Islands</h3>


	<p>For the last few years, the People's Republic of China has been building military runways and facilities
	on otherwise unhabitable islets parts of the  Spratly Islands.
	This way, the PRC is able to build advanced military bases,
	and boost its claims to the South China Sea.</p>

	<p>In December 2022, news satellite images from US officials show new land formations in the Spratly Islands,
	with Chinese vessels seen working there. The Philippine Ministry of Foreign Affairs said that
	they were "seriously concerned, as such activities contravene the
	Declaration of Conduct on the South China Sea’s undertaking on self-restraint and the 2016 Arbitral Award." {$r1}</p>
	HTML;


$div_Ayungin_Shoal = new ContentSection();
$div_Ayungin_Shoal->content = <<<HTML
	<h3>Ayungin Shoal</h3>

	<p>Ayungin Shoal, also known as Second Thomas Shoal, is an atoll in the Spratly Island,
	located in the exclusive economic zone of the Philippines.
	The atoll is currently militarily occupied by the Philippines.</p>

	<p>In the late 1990, the Philippines grounded a warship on the shoal$r3
	and since then China has been demanding for it to be removed.
	In 2023, the Chinese Coastguard even fired water cannons at a Philippine ship
	en route to resupply the garrison stationed on the shoal.</p>
	HTML;



$div_wikipedia_Spratly_Islands_dispute = new WikipediaContentSection();
$div_wikipedia_Spratly_Islands_dispute->setTitleText('Spratly Islands dispute');
$div_wikipedia_Spratly_Islands_dispute->setTitleLink('https://en.wikipedia.org/wiki/Spratly_Islands_dispute');
$div_wikipedia_Spratly_Islands_dispute->content = <<<HTML
	HTML;



$div_wikipedia_Second_Thomas_Shoal = new WikipediaContentSection();
$div_wikipedia_Second_Thomas_Shoal->setTitleText('Second Thomas Shoal');
$div_wikipedia_Second_Thomas_Shoal->setTitleLink('https://en.wikipedia.org/wiki/Second_Thomas_Shoal');
$div_wikipedia_Second_Thomas_Shoal->content = <<<HTML
	HTML;



$div_wikipedia_prc_philippines = new WikipediaContentSection();
$div_wikipedia_prc_philippines->setTitleText('Philippines v. China');
$div_wikipedia_prc_philippines->setTitleLink('https://en.wikipedia.org/wiki/Philippines_v._China');
$div_wikipedia_prc_philippines->content = <<<HTML
	<p>In 2016, an arbitration under the <em>United Nations Convention on Law of the Sea (UNCLOS)</em> ruled in favour of the Philippines
	in a maritime border dispute against the PRC.
	China issued a statement stating it would not abide by the arbitral tribunal's decision and that it will "ignore the ruling".</p>
	HTML;




$div_wikipedia = new WikipediaContentSection();
$div_wikipedia->setTitleText('Chinese expansionism');
$div_wikipedia->setTitleLink('https://en.wikipedia.org/wiki/Chinese_expansionism');
$div_wikipedia->content = <<<HTML
	<p>This wikipedia article includes historical occurrences of Chinese expansionism.
	There is a list of disputed islands in the South China Sea.</p>
	HTML;

$div_wikipedia_9_dash_line = new WikipediaContentSection();
$div_wikipedia_9_dash_line->setTitleText('Nine-dash line');
$div_wikipedia_9_dash_line->setTitleLink('https://en.wikipedia.org/wiki/Nine-dash_line');
$div_wikipedia_9_dash_line->content = <<<HTML
	<p>Today, the People's Republic of China still claims
	territories encompassed by nine dashes drawn on a map of the South China Sea.
	These claims puts the PRC on a collision course with all its maritime neighbors.</p>
	HTML;

$div_wikipedia_south_china_sea = new WikipediaContentSection();
$div_wikipedia_south_china_sea->setTitleText('Territorial disputes in the South China Sea');
$div_wikipedia_south_china_sea->setTitleLink('https://en.wikipedia.org/wiki/Territorial_disputes_in_the_South_China_Sea');
$div_wikipedia_south_china_sea->content = <<<HTML
	<p>Territorial disputes in the South China Sea involve conflicting island and maritime claims in the region by several sovereign states,
	namely Brunei, the People's Republic of China (PRC), Taiwan (Republic of China/ROC), Indonesia, Malaysia, the Philippines, and Vietnam.</p>
	HTML;


$div_codeberg = new CodebergContentSection();
$div_codeberg->setTitleText('Research the People\'s Republic of China\'s territorial claims');
$div_codeberg->setTitleLink('https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/11');
$div_codeberg->content = <<<HTML
	<p>Make list of current territorial claims and how they affect the relationship between the PRC and its neighbors.</p>
	HTML;




$page->parent('prc_china.html');
$page->body($div_stub);

$page->body($div_introduction);

$page->body($h2_taiwan);
$page->body('china_and_taiwan.html');

$page->body($h2_nine_dash_line);
$page->body($div_nine_dash_line_intro);
$page->body($div_nine_dash_line_in_movies);
$page->body($div_wikipedia_9_dash_line);



$page->body($h2_philippines);
$page->body($div_philippines);

$page->body($div_Spratly_Islands);
$page->body($div_Ayungin_Shoal);
$page->body($div_wikipedia_Spratly_Islands_dispute);
$page->body($div_wikipedia_Second_Thomas_Shoal);
$page->body($div_wikipedia_prc_philippines);




$page->body($div_codeberg);
$page->body($div_wikipedia);
$page->body($div_wikipedia_south_china_sea);
