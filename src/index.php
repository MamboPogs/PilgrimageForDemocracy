<?php
$page = new Page();

$page->h1('Pilgrimage for Democracy and Social Justice');
$page->stars(4);

$page->preview( <<<HTML
	HTML );

$div_quote_purpose = new ContentSection();
$div_quote_purpose->content = <<<HTML
	<blockquote>
	"<strong>A pilgrim is a wanderer with a purpose.</strong>
	A pilgrimage can be to a place — that's the best-known kind — but it can also be for a thing.
	Mine is for peace, and that is why I am a Peace Pilgrim."
	<br>
	— Peace Pilgrim (1908 – 1981)
	</blockquote>
	HTML;

$div_democracy_and_social_justice = new ContentSection();
$div_democracy_and_social_justice->content = <<<HTML
	<h3>Democracy and social justice are two critical topics for our times</h3>

	<p>There cannot be true, lasting peace without democracy.
	<br>There cannot be true, lasting peace without social justice.
	</p>

	<p>The latest electronic gadgets, fancy cars, big houses, fashionable clothes, etc., are not critical necessities.
	But simple vegetables or a simple bowl of rice are essential items for life.
	<br><strong>A world where many have much too much
	while too many still struggle to survive with the barest necessities of life,
	cannot achieve global peace.</strong>
	</p>

	<p>Democracy and social justice are the two sides of the same coin.
	</p>
	HTML;

$div_improve_ourselves_institutions_leaders = new ContentSection();
$div_improve_ourselves_institutions_leaders->content = <<<HTML
	<blockquote>
	"<strong>In order for the world to become peaceful, people must become more peaceful</strong>.
	Among mature people war would not be a problem — it would be impossible.
	In their immaturity people want, at the same time, peace and the things which make war.
	However, people can mature just as children grow up.
	Yes, our institutions and our leaders reflect our immaturity,
	but <strong>as we mature we will elect better leaders and set up better institutions</strong>.
	It always comes back to the thing so many of us wish to avoid: working to improve ourselves.
	<br>
	— Peace Pilgrim (1908 – 1981), Harmonious Principles for Human Living.
	</blockquote>

	<br>

	<blockquote>
	"<strong>No man is an island entire of itself</strong>;<br>
	every man is a piece of the continent,<br>
	a part of the main;<br>
	<br>
	if a clod be washed away by the sea,<br>
	Europe is the less, as well as if a promontory were,<br>
	as well as any manner of thy friends or of thine own were;</br>
	<br>
	<strong>any man's death diminishes me,<br>
	because I am involved in mankind.<br>
	And therefore never send to know for whom the bell tolls;<br>
	it tolls for thee.</strong>"
	<br>
	— John Donne (1571 – 1631), English poet.
	</blockquote>
	HTML;

$h2_menu = new h2HeaderContent('Come on in!');


$page->body($div_quote_purpose);
$page->body($div_democracy_and_social_justice);
$page->body($div_improve_ourselves_institutions_leaders);

$page->body($h2_menu);
$page->body('menu.html');
$page->body('project/participate.html');

$div_section_copyright = get_page('project/copyright.html');
$div_section_copyright->stars(-1);
$div_section_copyright->h1('Copyright?');
$div_section_copyright->dest('/project/copyright.html');
$page->body($div_section_copyright, array('format' => 'preview'));

$page->body('project/updates.html');
