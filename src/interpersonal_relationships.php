<?php
$page = new Page();
$page->h1('Interpersonal relationships');
$page->keywords('Interpersonal relationships', 'relationships', 'interpersonal relationships');
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Interpersonal relationships are part of the ${'secondary features of democracy'}.</p>
	HTML;

$list = new ListOfPages();
$list->add('living_together.html');
$list->add('the_four_agreements.html');
$print_list = $list->print();

$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>Related topics</h3>

	$print_list
	HTML;

$div_wikipedia_Interpersonal_relationship = new WikipediaContentSection();
$div_wikipedia_Interpersonal_relationship->setTitleText('Interpersonal relationship');
$div_wikipedia_Interpersonal_relationship->setTitleLink('https://en.wikipedia.org/wiki/Interpersonal_relationship');
$div_wikipedia_Interpersonal_relationship->content = <<<HTML
	<p>The main themes or trends of the interpersonal relations are:
	family, kinship, friendship, love, marriage, business, employment, clubs, neighborhoods, ethical values, support and solidarity.
	Interpersonal relations may be regulated by law, custom, or mutual agreement, and form the basis of social groups and societies.</p>
	HTML;


$page->parent('secondary_features_of_democracy.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body($div_list);


$page->body($div_wikipedia_Interpersonal_relationship);
