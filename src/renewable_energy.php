<?php
$page = new Page();
$page->h1('Renewable energy');
$page->keywords('Renewable energy', 'renewable energy');
$page->stars(0);

$page->preview( <<<HTML
	<p></p>
	HTML );

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;




$div_wpc_Renewable_energy_and_just_development = new WorldPolicyCouncilContentSection();
$div_wpc_Renewable_energy_and_just_development->setTitleText('Renewable energy and just development ');
$div_wpc_Renewable_energy_and_just_development->setTitleLink('https://www.worldfuturecouncil.org/energy-and-just-development/');
$div_wpc_Renewable_energy_and_just_development->content = <<<HTML
	<p>International agreements such as the Paris Climate Agreement or the 2030 Agenda signal the political will
	for the necessary transition to renewable energies and sustainability,
	but they are not sufficient: National energy and climate targets are not ambitious enough.
	Well-founded political solutions and measures for expansion of renewable energy and energy savings
	and for freeing up the necessary financial resources are insufficient.
	Political measures to achieve a socially just transition that includes everyone are missing.
	Therefore, our goal is to help national decision-makers to convert international agreements into national measures.</p>
	HTML;




$div_wikipedia_Renewable_energy = new WikipediaContentSection();
$div_wikipedia_Renewable_energy->setTitleText('Renewable energy');
$div_wikipedia_Renewable_energy->setTitleLink('https://en.wikipedia.org/wiki/Renewable_energy');
$div_wikipedia_Renewable_energy->content = <<<HTML
	<p>Renewable resources include sunlight, wind, the movement of water, and geothermal heat.
	Although most renewable energy sources are sustainable, some are not.</p>
	HTML;


$page->parent('global_natural_resources.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body($div_wpc_Renewable_energy_and_just_development);
$page->body($div_wikipedia_Renewable_energy);
