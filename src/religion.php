<?php
$page = new Page();
$page->h1('Religion');
$page->keywords('Religion', 'religion', 'Religions', 'religions');
$page->stars(0);

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );

$r1 = $page->ref('https://en.wikipedia.org/wiki/List_of_religious_populations', 'List of religious populations');
$r2 = $page->ref('https://www.pewresearch.org/religion/2022/12/21/key-findings-from-the-global-religious-futures-project/', 'Key Findings From the Global Religious Futures Project');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>According to a study by the Pew Research Center,
	about seven billion people are adherent of some form of religion,
	leaving about 1.2 billion secular/nonreligious/agnostic/atheist people
	out of over 8 billion Earth population.</p>

	<p>Freedom of conscience, and thus religious freedom, is one of the most important tenets of $democracy,
	and one of the ${'primary features of democracy'}.</p>

	<p>For these reasons, religious tolerance and interfaith dialogue are critical topics to achieve global peace and democracy.</p>
	HTML;

$list = new ListOfPages();
$list->add('beliefs.html');
$list->add('blasphemy.html');
$print_list = $list->print();

$div_list = new ContentSection();
$div_list->content = <<<HTML
	<h3>Related content</h3>

	$print_list
	HTML;


$div_wikipedia_Religion = new WikipediaContentSection();
$div_wikipedia_Religion->setTitleText('Religion');
$div_wikipedia_Religion->setTitleLink('https://en.wikipedia.org/wiki/Religion');
$div_wikipedia_Religion->content = <<<HTML
	<p>Religion is a range of social-cultural systems, including designated behaviors and practices,
	morals, beliefs, worldviews, texts, sanctified places, prophecies, ethics, or organizations,
	that generally relate humanity to supernatural, transcendental, and spiritual elements—although
	there is no scholarly consensus over what precisely constitutes a religion.</p>
	HTML;


$page->parent('lists.html');
$page->body($div_stub);
$page->body($div_introduction);

$page->body($div_list);

$page->body($div_wikipedia_Religion);
