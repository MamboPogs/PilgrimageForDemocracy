<?php
$page = new Page();
$page->h1('Conflict resolution');
$page->stars(0);
$page->keywords('Conflict resolution', 'conflict resolution');

$page->preview( <<<HTML
	<p></p>
	HTML );

$page->snp('description', 'Solving problems and actively restoring peace and harmony.');
//$page->snp('image',       '/copyrighted/');



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>$Democracy contains mechanisms for resolving conflicts.</p>
	<p>The mere fact that in a democracy one has the liberty to openly talk about problems within the society and the way it is run
	differentiates democracies from dictatorships.</p>
	HTML;

$div_wikipedia_Conflict_resolution = new WikipediaContentSection();
$div_wikipedia_Conflict_resolution->setTitleText('Conflict resolution');
$div_wikipedia_Conflict_resolution->setTitleLink('https://en.wikipedia.org/wiki/Conflict_resolution');
$div_wikipedia_Conflict_resolution->content = <<<HTML
	<p>Conflict resolution is conceptualized as the methods and processes involved in facilitating the peaceful ending of conflict and retribution.
	Committed group members attempt to resolve group conflicts by actively communicating information
	about their conflicting motives or ideologies to the rest of group and by engaging in collective negotiation.</p>
	HTML;


$page->parent('democracy.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body($div_wikipedia_Conflict_resolution);
