<?php
$page = new Page();
$page->h1('Rohingya people');
$page->keywords('Rohingya people', 'Rohingya');
$page->stars(0);

$page->snp('description', 'A stateless ethnic group following Islam.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );


$r1 = $page->ref('https://www.aljazeera.com/news/longform/2023/8/25/what-is-life-like-inside-the-worlds-largest-refugee-camp', 'What is life like inside the world’s biggest refugee camp?');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The Rohingya people are a stateless ethnic group who predominantly follow Islam.
	Before the Rohingya genocide in 2017, when over 740,000 fled to Bangladesh, an estimated 1.4 million Rohingya lived in $Myanmar.</p>

	<p>Today, many Rohingya people live in ${'refugee camps'} in Bangladesh $r1.</p>

	<p>Facebook (${'social networks'}) has been accused of having contributed, by lack of moderation, to the genocide of the Rohingya people.<p>
	HTML;


$div_U_N_calls_for_Myanmar_generals_to_be_tried_for_genocide_blames_Facebook_for = new WebsiteContentSection();
$div_U_N_calls_for_Myanmar_generals_to_be_tried_for_genocide_blames_Facebook_for->setTitleText('U.N. calls for Myanmar generals to be tried for genocide, blames Facebook');
$div_U_N_calls_for_Myanmar_generals_to_be_tried_for_genocide_blames_Facebook_for->setTitleLink('https://www.reuters.com/article/us-myanmar-rohingya-un/myanmar-generals-had-genocidal-intent-against-rohingya-must-face-justice-u-n-idUSKCN1LC0KN');
$div_U_N_calls_for_Myanmar_generals_to_be_tried_for_genocide_blames_Facebook_for->content = <<<HTML
	<p>Myanmar’s military carried out mass killings and gang rapes of Muslim Rohingya with “genocidal intent”,
	and the commander-in-chief and five generals should be prosecuted for the gravest crimes under international law,
	United Nations investigators said.</p>

	<p>(...) A report by investigators was the first time the United Nations has explicitly called for Myanmar officials
	to face genocide charges over their campaign against the Rohingya, and is likely to deepen the country’s isolation.</p>

	<p>(...) The report also could serve as a major catalyst for change in how
	the world’s big social media companies handle hate speech in parts of the world
	where they have limited direct presence but their platforms command huge influence.
	The investigators sharply criticized Facebook,
	which has become Myanmar’s dominant social media network despite having no employees there,
	for letting its platform be used to incite violence and hatred.</p>
	HTML;



$div_wikipedia_Rohingya_people = new WikipediaContentSection();
$div_wikipedia_Rohingya_people->setTitleText('Rohingya people');
$div_wikipedia_Rohingya_people->setTitleLink('https://en.wikipedia.org/wiki/Rohingya_people');
$div_wikipedia_Rohingya_people->content = <<<HTML
	<p>The Rohingya people are a stateless Indo-Aryan ethnic group who predominantly follow Islam and reside in Rakhine State, Myanmar.
	Before the Rohingya genocide in 2017, when over 740,000 fled to Bangladesh, an estimated 1.4 million Rohingya lived in Myanmar.</p>
	HTML;

$div_wikipedia_2012_Rakhine_State_riots = new WikipediaContentSection();
$div_wikipedia_2012_Rakhine_State_riots->setTitleText('2012 Rakhine State riots');
$div_wikipedia_2012_Rakhine_State_riots->setTitleLink('https://en.wikipedia.org/wiki/2012_Rakhine_State_riots');
$div_wikipedia_2012_Rakhine_State_riots->content = <<<HTML
	<p>The 2012 Rakhine State riots were a series of conflicts primarily between ethnic Rakhine Buddhists and Rohingya Muslims
	in northern Rakhine State, Myanmar, though by October Muslims of all ethnicities had begun to be targeted.</p>
	HTML;

$div_wikipedia_Rohingya_genocide = new WikipediaContentSection();
$div_wikipedia_Rohingya_genocide->setTitleText('Rohingya genocide');
$div_wikipedia_Rohingya_genocide->setTitleLink('https://en.wikipedia.org/wiki/Rohingya_genocide');
$div_wikipedia_Rohingya_genocide->content = <<<HTML
	<p>The Rohingya genocide is a series of ongoing persecutions and killings of the Muslim Rohingya people by the military of Myanmar.</p>
	HTML;

$div_wikipedia_Rohingya_conflict = new WikipediaContentSection();
$div_wikipedia_Rohingya_conflict->setTitleText('Rohingya conflict');
$div_wikipedia_Rohingya_conflict->setTitleLink('https://en.wikipedia.org/wiki/Rohingya_conflict');
$div_wikipedia_Rohingya_conflict->content = <<<HTML
	<p>The Rohingya conflict is an ongoing conflict in the northern part of Myanmar's Rakhine State,
	characterised by sectarian violence between the Rohingya Muslim and Rakhine Buddhist communities,
	a military crackdown on Rohingya civilians by Myanmar's security forces,
	and militant attacks by Rohingya insurgents in Buthidaung, Maungdaw, and Rathedaung Townships, which border Bangladesh.</p>
	HTML;

$div_wikipedia_2015_Rohingya_refugee_crisis = new WikipediaContentSection();
$div_wikipedia_2015_Rohingya_refugee_crisis->setTitleText('2015 Rohingya refugee crisis');
$div_wikipedia_2015_Rohingya_refugee_crisis->setTitleLink('https://en.wikipedia.org/wiki/2015_Rohingya_refugee_crisis');
$div_wikipedia_2015_Rohingya_refugee_crisis->content = <<<HTML
	<p>In 2015, hundreds of thousands of Rohingya people were forcibly displaced from their villages and IDP camps in Rakhine State, Myanmar,
	due to sectarian violence. Nearly one million fled to neighbouring Bangladesh and some travelled to Southeast Asian countries.</p>
	HTML;


$page->parent('secondary_features_of_democracy.html');
$page->body($div_stub);
$page->body($div_introduction);

$page->body($div_U_N_calls_for_Myanmar_generals_to_be_tried_for_genocide_blames_Facebook_for);

$page->body($div_wikipedia_Rohingya_people);
$page->body($div_wikipedia_2012_Rakhine_State_riots);
$page->body($div_wikipedia_Rohingya_genocide);
$page->body($div_wikipedia_Rohingya_conflict);
$page->body($div_wikipedia_2015_Rohingya_refugee_crisis);
