<?php
$page = new CountryPage('Somalia');
$page->h1('Somalia');
$page->keywords('Somalia');
$page->stars(0);

$page->snp('description', '12,7 million inhabitants.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;

$div_wikipedia_Somalia = new WikipediaContentSection();
$div_wikipedia_Somalia->setTitleText('Somalia');
$div_wikipedia_Somalia->setTitleLink('https://en.wikipedia.org/wiki/Somalia');
$div_wikipedia_Somalia->content = <<<HTML
	<p>Somalia, officially the Federal Republic of Somalia, is a country in the Horn of Africa.
	The country is bordered by Ethiopia to the west, Djibouti to the northwest, the Gulf of Aden to the north,
	the Indian Ocean to the east, and Kenya to the southwest.</p>
	HTML;

$div_wikipedia_Human_rights_in_Somalia = new WikipediaContentSection();
$div_wikipedia_Human_rights_in_Somalia->setTitleText('Human rights in Somalia');
$div_wikipedia_Human_rights_in_Somalia->setTitleLink('https://en.wikipedia.org/wiki/Human_rights_in_Somalia');
$div_wikipedia_Human_rights_in_Somalia->content = <<<HTML
	<p>Human rights in Somalia throughout the late 20th-century and early 21st-century were considered dire,
	but have gradually improved over the following years.
	Human rights are guaranteed in the Federal Constitution, which was adopted in August 2012.
	They fall under the Ministry of Human Rights established in August 2013.
	The central authorities concurrently inaugurated a National Human Rights Day, endorsed an official Human Rights Roadmap,
	and completed Somalia's first National Gender Policy.</p>
	HTML;


$page->parent('world.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body('Country indices');

$page->body($div_wikipedia_Somalia);
$page->body($div_wikipedia_Human_rights_in_Somalia);
