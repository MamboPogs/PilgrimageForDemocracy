<?php
$page = new Page();
$page->h1('Hunger');
$page->stars(1);
$page->keywords('hunger', 'Hunger');

$page->preview( <<<HTML
	<p>How do we reconcile food waste with famine?</p>
	HTML );

$page->snp('description', 'How do we reconcile food waste with famine?');
$page->snp('image', '/copyrighted/mingwei-dong-s1bmsm6VcLE.1200-630.jpg');

$r1 = $page->ref('https://www.taipeitimes.com/News/world/archives/2023/05/30/2003800688', 'UN issues hunger alert for Haiti, Sahel and Sudan');

$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>How do we reconcile food waste with famine?</p>

	<p>More than 800 million people live in hunger.
	Ten percent of the world's population does not have enough food, the highest number in more than a decade.</p>

	<p>The scope of famine is a sign that basic injustice subsists.</p>

	<p>In 2023, the ${'Food and Agriculture Organization'} and World Food Programme
	said that Afghanistan, Nigeria, Somalia, South Sudan and Yemen remain at the highest alert level,
	for food insecurity, requiring “urgent” action from the international community.
	$Haiti, the Sahel and Sudan rank among the UN’s highest alert areas. ${r1}</p>
	HTML;


$div_codeberg_Hunger_what_could_systemic_cause_be = new CodebergContentSection();
$div_codeberg_Hunger_what_could_systemic_cause_be->setTitleText('Hunger: what could systemic cause be?');
$div_codeberg_Hunger_what_could_systemic_cause_be->setTitleLink('https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/36');
$div_codeberg_Hunger_what_could_systemic_cause_be->content = <<<HTML
	<p>It would be most interesting if our project could highlight
	the work of people who might be writing about systemic problems (in global trade, institutions, etc)
	that may be the root cause for such misery.</p>
	HTML;



$div_wikipedia_Famine = new WikipediaContentSection();
$div_wikipedia_Famine->setTitleText('Famine');
$div_wikipedia_Famine->setTitleLink('https://en.wikipedia.org/wiki/Famine');
$div_wikipedia_Famine->content = <<<HTML
	<p>A famine can be caused by several factors
	including war, natural disasters, crop failure, widespread $poverty, an economic catastrophe or government policies.</p>
	HTML;

$div_wikipedia_Starvation = new WikipediaContentSection();
$div_wikipedia_Starvation->setTitleText('Starvation');
$div_wikipedia_Starvation->setTitleLink('https://en.wikipedia.org/wiki/Starvation');
$div_wikipedia_Starvation->content = <<<HTML
	<p>According to the World Health Organization (WHO),
	hunger is the single gravest threat to the world's public health.
	The WHO also states that malnutrition is by far the biggest contributor to child mortality.
	Undernutrition is a contributory factor in the death of 3.1 million children under five every year.</p>
	HTML;

$div_wikipedia_Food_security = new WikipediaContentSection();
$div_wikipedia_Food_security->setTitleText('Food security');
$div_wikipedia_Food_security->setTitleLink('https://en.wikipedia.org/wiki/Food_security');
$div_wikipedia_Food_security->content = <<<HTML
	<p>The first World Food Summit, held in 1996, stated that
	food security "exists when all people, at all times, have physical and economic access to sufficient,
	safe and nutritious food to meet their dietary needs and food preferences for an active and healthy life."</p>
	HTML;

$div_wikipedia_Food_loss_and_waste = new WikipediaContentSection();
$div_wikipedia_Food_loss_and_waste->setTitleText('Food loss and waste');
$div_wikipedia_Food_loss_and_waste->setTitleLink('https://en.wikipedia.org/wiki/Food_loss_and_waste');
$div_wikipedia_Food_loss_and_waste->content = <<<HTML
	<p>Overall, about one-third of the world's food is thrown away.
	An analysis estimated that global food waste was 931 million tonnes of food waste
	(about 121 kg per capita) across three sectors:
	61 percent from households, 26 percent from food service and 13 percent from retail.</p>
	HTML;

$div_wikipedia_Right_to_food = new WikipediaContentSection();
$div_wikipedia_Right_to_food->setTitleText('Right to food');
$div_wikipedia_Right_to_food->setTitleLink('https://en.wikipedia.org/wiki/Right_to_food');
$div_wikipedia_Right_to_food->content = <<<HTML
	<p>At the 1996 World Food Summit, governments reaffirmed the right to food
	and committed themselves to halve the number of hungry and malnourished from 840 to 420 million by 2015.
	However, the number has increased over the past years, reaching an infamous record in 2009
	of more than 1 billion undernourished people worldwide.</p>
	HTML;

$page->parent('global_issues.html');
$page->body($div_stub);

$page->body($div_introduction);
$page->body($div_codeberg_Hunger_what_could_systemic_cause_be);

$page->body('food_and_agriculture_organization.html');

$page->body($div_wikipedia_Famine);
$page->body($div_wikipedia_Starvation);
$page->body($div_wikipedia_Food_security);
$page->body($div_wikipedia_Food_loss_and_waste);
$page->body($div_wikipedia_Right_to_food);
