<?php
$page = new Page();
$page->h1('Criminal justice');
$page->keywords('Criminal justice', 'criminal justice');
$page->stars(0);

$page->preview( <<<HTML
	<p></p>
	HTML );

$page->snp('description', 'Protecting, preventing, rehabilitating.');
//$page->snp('image',       '/copyrighted/');



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Criminal $justice should pursue the following goals, in order of importance:</p>

	<ol>
		<li>Protect society from dangerous criminals (by locking them up, etc.).</li>
		<li>Considering criminals as diseased individuals,
		provide the opportunity to heal them so they can become integrated, productive members of society.</li>
		<li>Give proportional punishment for a given crime.</li>
	</ol>

	<p>The $Pilgrimage is against both capital punishment and life imprisonment.</p>

	<p>Convicted and imprisoned criminals should be treated with fairness and human decency,
	and given the opportunity to rehabilitate themselves and prepare for their life back in society.</p>

	<p>Terry A. Kupers, a forensic psychiatrist and professor at the Wright Institute,
	argues against solitary confinement behind bars, as it fosters more prison violence
	and ultimately poses increased risks for society.</p>
	HTML;

$div_wikipedia_Criminal_justice = new WikipediaContentSection();
$div_wikipedia_Criminal_justice->setTitleText('Criminal justice');
$div_wikipedia_Criminal_justice->setTitleLink('https://en.wikipedia.org/wiki/Criminal_justice');
$div_wikipedia_Criminal_justice->content = <<<HTML
	<p>Criminal justice is the delivery of justice to those who have been accused of committing crimes.</p>
	HTML;

$div_wikipedia_Rehabilitation_penology = new WikipediaContentSection();
$div_wikipedia_Rehabilitation_penology->setTitleText('Rehabilitation penology');
$div_wikipedia_Rehabilitation_penology->setTitleLink('https://en.wikipedia.org/wiki/Rehabilitation_(penology)');
$div_wikipedia_Rehabilitation_penology->content = <<<HTML
	<p>Rehabilitation is the process of re-educating and preparing those who have committed a crime, to re-enter society.
	The goal is to address all of the underlying root causes of crime
	in order to ensure inmates will be able to live a crime-free lifestyle once they are released from prison.</p>
	HTML;

$div_wikipedia_Imprisonment_for_public_protection = new WikipediaContentSection();
$div_wikipedia_Imprisonment_for_public_protection->setTitleText('Imprisonment for public protection');
$div_wikipedia_Imprisonment_for_public_protection->setTitleLink('https://en.wikipedia.org/wiki/Imprisonment_for_public_protection');
$div_wikipedia_Imprisonment_for_public_protection->content = <<<HTML
	<p>Imprisonment for public protection was intended to protect the public against criminals
	whose crimes were not serious enough to merit a normal life sentence
	but who were regarded as too dangerous to be released when the term of their original sentence had expired.</p>
	HTML;

$div_wikipedia_Prison = new WikipediaContentSection();
$div_wikipedia_Prison->setTitleText('Prison');
$div_wikipedia_Prison->setTitleLink('https://en.wikipedia.org/wiki/Prison');
$div_wikipedia_Prison->content = <<<HTML
	<p>A prison is a facility in which convicted criminals are confined involuntarily and denied a variety of freedoms
	under the authority of the state as punishment for various crimes. Authorities most commonly use prisons within a criminal-justice system.</p>
	HTML;

$div_wikipedia_life_imprisonment = new WikipediaContentSection();
$div_wikipedia_life_imprisonment->setTitleText('Life imprisonment');
$div_wikipedia_life_imprisonment->setTitleLink('https://en.wikipedia.org/wiki/Life_imprisonment');
$div_wikipedia_life_imprisonment->content = <<<HTML
	<p>Most countries have an effective live sentence, lasting until the convict's natural death.
	Portugal was the first country to abolish life imprisonment in 1884.
	In Norway, the maximum sentence is 21 years.</p>
	HTML;

$div_codeberg = new CodebergContentSection();
$div_codeberg->setTitleText('Nepal: What is the maximum duration of a life sentence?');
$div_codeberg->setTitleLink('https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/14');
$div_codeberg->content = <<<HTML
	<p>Different sources give the life sentence in Nepal as 20 years, 25 years, or an effective life sentence.</p>
	HTML;


$page->parent('justice.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_wikipedia_Criminal_justice);
$page->body($div_wikipedia_Rehabilitation_penology);
$page->body($div_wikipedia_Imprisonment_for_public_protection);
$page->body($div_wikipedia_Prison);
$page->body($div_codeberg);
$page->body($div_wikipedia_life_imprisonment);
