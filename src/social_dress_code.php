<?php
$page = new Page();
$page->h1('Social dress code');
$page->keywords('social dress code');
$page->stars(0);

$page->preview( <<<HTML
	<p></p>
	HTML );

//$page->snp('description', '');
//$page->snp('image',       '/copyrighted/');



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The social dress code is the often unwritten standard which tells people the state of dress or undress they can adopt in public.</p>

	<p>This topic is relevant as it touches on politics, public policies, religious sensibilities and is the subject of much conflict in today's society.</p>

	<p>One important aspect to consider is that there is not one standard, absolute dress code.
	What is acceptable or not varies dramatically throughout time and space.</p>

	<p>Within a single country, we can see the Overton window shifting to one side or the other, often slowly over time, sometimes dramatically.</p>

	<p>See also: $blasphemy.</p>
	HTML;





$page->parent('secondary_features_of_democracy.html');
$page->body($div_stub);
$page->body($div_introduction);
