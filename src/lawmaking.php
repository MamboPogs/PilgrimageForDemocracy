<?php
$page = new Page();
$page->h1('Lawmaking');
$page->stars(0);
$page->keywords('lawmaking');


//$page->snp('description', "");
//$page->snp('image', "/copyrighted/");


$page->preview( <<<HTML
	<p>
	</p>
	HTML );




$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>
	</p>
	HTML;



$div_wikipedia_Lawmaking = new WikipediaContentSection();
$div_wikipedia_Lawmaking->setTitleText('Lawmaking');
$div_wikipedia_Lawmaking->setTitleLink('https://en.wikipedia.org/wiki/Lawmaking');
$div_wikipedia_Lawmaking->content = <<<HTML
	<p>Lawmaking is the process of crafting legislation. In its purest sense, it is the basis of governance.
	Lawmaking in modern democracies is the work of legislatures, which exist at the local, regional, and national levels
	and make such laws as are appropriate to their level, and binding over those under their jurisdictions.</p>
	HTML;

$div_wikipedia_Legislation = new WikipediaContentSection();
$div_wikipedia_Legislation->setTitleText('Legislation');
$div_wikipedia_Legislation->setTitleLink('https://en.wikipedia.org/wiki/Legislation');
$div_wikipedia_Legislation->content = <<<HTML
	<p>Legislation is the process or result of enrolling, enacting, or promulgating laws
	by a legislature, parliament, or analogous governing body.</p>
	HTML;

$div_wikipedia_Legislature = new WikipediaContentSection();
$div_wikipedia_Legislature->setTitleText('Legislature');
$div_wikipedia_Legislature->setTitleLink('https://en.wikipedia.org/wiki/Legislature');
$div_wikipedia_Legislature->content = <<<HTML
	<p>A legislature is an assembly with the authority to make laws for a political entity such as a country or city.
	They are often contrasted with the executive and judicial powers of government.</p>
	HTML;

$div_wikipedia_Rule_of_law = new WikipediaContentSection();
$div_wikipedia_Rule_of_law->setTitleText('Rule of law');
$div_wikipedia_Rule_of_law->setTitleLink('https://en.wikipedia.org/wiki/Rule_of_law');
$div_wikipedia_Rule_of_law->content = <<<HTML
	<p>The rule of law is a political ideal that all citizens and institutions within a country, state, or community
	are accountable to the same laws, including lawmakers and leaders.</p>
	HTML;

$div_wikipedia_Deliberative_democracy = new WikipediaContentSection();
$div_wikipedia_Deliberative_democracy->setTitleText('Deliberative democracy');
$div_wikipedia_Deliberative_democracy->setTitleLink('https://en.wikipedia.org/wiki/Deliberative_democracy');
$div_wikipedia_Deliberative_democracy->content = <<<HTML
	<p>Deliberative democracy or discursive democracy is a form of democracy in which deliberation is central to decision-making.
	It often adopts elements of both consensus decision-making and majority rule.
	Deliberative democracy differs from traditional democratic theory in that authentic deliberation, not mere voting,
	is the primary source of legitimacy for the law.
	Deliberative democracy is closely related to consultative democracy, in which public consultation with citizens is central to democratic processes.</p>
	HTML;


$div_wikipedia_Agonism = new WikipediaContentSection();
$div_wikipedia_Agonism->setTitleText('Agonism');
$div_wikipedia_Agonism->setTitleLink('https://en.wikipedia.org/wiki/Agonism');
$div_wikipedia_Agonism->content = <<<HTML
	<p>Agonism (from Greek ἀγών agon, "struggle") is a political and social theory
	that emphasizes the potentially positive aspects of certain forms of conflict.
	It accepts a permanent place for such conflict in the political sphere,
	but seeks to show how individuals might accept and channel this conflict positively.
	Agonists are especially concerned with debates about democracy, and the role that conflict plays in different conceptions of it.</p>
	HTML;


$page->parent('institutions.html');
$page->body($div_stub);

$page->body($div_introduction);


$page->body($div_wikipedia_Lawmaking);
$page->body($div_wikipedia_Rule_of_law);
$page->body($div_wikipedia_Legislature);
$page->body($div_wikipedia_Legislation);
$page->body($div_wikipedia_Deliberative_democracy);
$page->body($div_wikipedia_Agonism);
