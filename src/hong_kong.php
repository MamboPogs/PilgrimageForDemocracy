<?php
$page = new CountryPage('Hong Kong');
$page->h1('Hong Kong');
$page->keywords('Hong Kong', 'HK');
$page->stars(0);

$page->preview( <<<HTML
	<p></p>
	HTML );

$page->snp('description', '7.3 million inhabitants.');
//$page->snp('image',       '/copyrighted/');



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Hong Kong was supposed to be the showcase for the ${"People's Republic of China"}'s policy
	of ${'one country, two systems'}, which the PRC wanted to apply to $Taiwan (Republic of China).
	However, the crackdown of the 2019 pro-democracy protests showed the complete failure of the intrinsically undemocratic policy.</p>

	<p>Since 2019, the government of Hong Kong has increasingly been using the authoritarian methods of the $PRC,
	and became one of the perpetrator of ${'transnational authoritarianism'},
	pursuing abroad pro-democracy activists and harassing their relatives who still live in Hong Kong.</p>
	HTML;

$div_wikipedia_Hong_Kong = new WikipediaContentSection();
$div_wikipedia_Hong_Kong->setTitleText('Hong Kong');
$div_wikipedia_Hong_Kong->setTitleLink('https://en.wikipedia.org/wiki/Hong_Kong');
$div_wikipedia_Hong_Kong->content = <<<HTML
	<p>Hong Kong, officially the Hong Kong Special Administrative Region of the People's Republic of China,
	is a city and a special administrative region in China.</p>
	HTML;

$div_wikipedia_2019_2020_Hong_Kong_protests = new WikipediaContentSection();
$div_wikipedia_2019_2020_Hong_Kong_protests->setTitleText('2019 2020 Hong Kong protests');
$div_wikipedia_2019_2020_Hong_Kong_protests->setTitleLink('https://en.wikipedia.org/wiki/2019–2020_Hong_Kong_protests');
$div_wikipedia_2019_2020_Hong_Kong_protests->content = <<<HTML
	<p>The 2019–2020 Hong Kong protests were a series of demonstrations
	against the Hong Kong government's introduction of a bill to amend the Fugitive Offenders Ordinance in regard to extradition.
	It was the largest series of demonstrations in the history of Hong Kong.</p>
	HTML;

$div_wikipedia_Democratic_development_in_Hong_Kong = new WikipediaContentSection();
$div_wikipedia_Democratic_development_in_Hong_Kong->setTitleText('Democratic development in Hong Kong');
$div_wikipedia_Democratic_development_in_Hong_Kong->setTitleLink('https://en.wikipedia.org/wiki/Democratic_development_in_Hong_Kong');
$div_wikipedia_Democratic_development_in_Hong_Kong->content = <<<HTML
	<p>Democratic development in Hong Kong has been a major issue since its transfer of sovereignty to the People's Republic of China in 1997.
	The one country, two systems principle allows Hong Kong to enjoy high autonomy in all areas
	besides foreign relations and defence, which are responsibilities of the central government.</p>
	HTML;

$div_wikipedia_Hong_Kong_Mainland_China_conflict = new WikipediaContentSection();
$div_wikipedia_Hong_Kong_Mainland_China_conflict->setTitleText('Hong Kong Mainland China conflict');
$div_wikipedia_Hong_Kong_Mainland_China_conflict->setTitleLink('https://en.wikipedia.org/wiki/Hong_Kong–Mainland_China_conflict');
$div_wikipedia_Hong_Kong_Mainland_China_conflict->content = <<<HTML
	<p>Relations between people in Hong Kong and mainland China have been relatively tense since the early 2000s.
	Various factors have contributed, including different interpretations of the "one country, two systems" principle.
	There is increased interference from the government of China and its ruling Chinese Communist Party (CCP) in Hong Kong's internal affairs.</p>
	HTML;


$page->parent('world.html');
$page->body($div_stub);
$page->body($div_introduction);
$page->body('one_country_two_systems.html');
$page->body('Country indices');

$page->body($div_wikipedia_Hong_Kong);
$page->body($div_wikipedia_2019_2020_Hong_Kong_protests);
$page->body($div_wikipedia_Democratic_development_in_Hong_Kong);
$page->body($div_wikipedia_Hong_Kong_Mainland_China_conflict);
