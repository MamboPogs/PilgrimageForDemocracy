<?php
$page = new Page();
$page->h1('American Constitution Society');
$page->keywords('American Constitution Society');
$page->stars(0);

$page->snp('description', 'Facilitating debate of progressive public policy ideas.');
//$page->snp('image',       '/copyrighted/');

$page->preview( <<<HTML
	<p></p>
	HTML );



$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>The American Constitution Society facilitates discussion and debate of progressive public policy ideas and issues.</p>

	<p>It was founded in 2001 by ${'Laurence Tribe'}.</p>

	<p>See also: $constitution and $institutions.</p>
	HTML;

$div_wikipedia_American_Constitution_Society = new WikipediaContentSection();
$div_wikipedia_American_Constitution_Society->setTitleText('American Constitution Society');
$div_wikipedia_American_Constitution_Society->setTitleLink('https://en.wikipedia.org/wiki/American_Constitution_Society');
$div_wikipedia_American_Constitution_Society->content = <<<HTML
	<p>The American Constitution Society (ACS) is a progressive legal organization.
	ACS was created as a counterweight to, and is modeled after, the Federalist Society,
	and is often described as its progressive counterpart.</p>

	<p>Founded in 2001 following the U.S. Supreme Court decision Bush v. Gore, ACS is headquartered in Washington, D.C.
	The organization promotes and facilitates discussion and debate of progressive public policy ideas and issues,
	providing forums for legal scholars, lawmakers, judges, lawyers, public policy advocates, law students, and members of the media.</p>
	HTML;


$page->parent('list_of_organisations.html');
$page->body($div_stub);
$page->body($div_introduction);


$page->body($div_wikipedia_American_Constitution_Society);
