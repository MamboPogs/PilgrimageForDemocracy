<?php
$page = new Page();
$page->h1('Freedom House');
$page->stars(2);
$page->keywords('Freedom House');

$page->preview( <<<HTML
	<p>Freedom House is a non-profit organization group in Washington, D.C.
	advocating democracy, political freedom, and human rights.</p>
	HTML );

$page->snp('description', "Advocating democracy, political freedom, and human rights.");
//$page->snp('image', "/copyrighted/");



$h2_Introduction = new h2HeaderContent('Introduction');


$div_introduction = new ContentSection();
$div_introduction->content = <<<HTML
	<p>Freedom House is a non-profit organization based in Washington, D.C. and founded in 1941.
	Eleanor Roosevelt, the wife President Franklin D. Roosevelt,
	served as its first honorary chairperson, alongside Wendell Willkie.</p>

	<p>Freedom house aims to be a "<em>voice for democracy and freedom around the world</em>".
	It produces the following reports:</p>
	<ul>
		<li>Freedom in the World</li>
		<li>Freedom of the Press</li>
		<li>Freedom on the Net</li>
	</ul>

	<p>It is mostly funded by the United States government (in 2016, 86% of its funding came from grants from the US government).</p>

	<p>Freedom House is one of the $giants on whose shoulder stands the $Pilgrimage.</p>

	<p>For each country in the $world, we display the Freedom Index and the Internet Freedom Index published by the Freedom House.</p>
	HTML;



$div_Expanding_Freedom_and_Democracy = new FreedomHouseContentSection();
$div_Expanding_Freedom_and_Democracy->setTitleText('Expanding Freedom and Democracy');
$div_Expanding_Freedom_and_Democracy->setTitleLink('https://freedomhouse.org/');
$div_Expanding_Freedom_and_Democracy->content = <<<HTML
	<p>Home page of Freedom House.</p>
	HTML;


$h2_Policy_recommendations = new h2HeaderContent('Policy recommendations');

$div_policy_recommendations = new ContentSection();
$div_policy_recommendations->content = <<<HTML
	<p>Freedom House is making policy recommendations on relevant topics, including ${'transnational authoritarianism'}.</p>
	HTML;



$div_Policy_Recommendations = new FreedomHouseContentSection();
$div_Policy_Recommendations->setTitleText('Policy Recommendations');
$div_Policy_Recommendations->setTitleLink('https://freedomhouse.org/report/nations-transit/2023/war-deepens-regional-divide/policy-recommendations');
$div_Policy_Recommendations->content = <<<HTML
	<p>To counter the spread of antidemocratic practices in Europe and Eurasia,
	democratic countries—especially the United States and European Union (EU) member states—should consider pursuing the following policy priorities.</p>

	<ul>
	<h3>Help Ukraine win</h3>
		<li>Defer to Ukrainians regarding the terms of victory and peace.</li>
		<li>Recommit to respecting the territorial integrity of all countries and condemn the violation of Ukraine’s sovereignty.</li>
		<li>Respond swiftly to requests for assistance from Ukraine’s government and civil society.</li>
		<li>Support the establishment of a special tribunal to prosecute Russian leaders for the crime of aggression.</li>
		<li>Crack down on efforts to evade sanctions against Russian entities.</li>
	<br>
	<h3>Renew commitments to democratic reform</h3>
		<li>Reinvigorate the EU accession process and related democratic benchmarks in the Western Balkans.
			<ul>
			<li>Expand accession-related reporting and consultation.</li>
			<li>Prioritize greater economic integration and investment in candidate countries.</li>
			<li>Caution against alternative trade agreements that undercut the EU process.</li>
			<li>Fund initiatives that address institutional problems with an impact on daily life.</li>
			<li>Foster nongovernmental and subnational initiatives that support democracy and human rights.</li>
			<li>Improve coordination on sanctions designations.</li>
			</ul>
		</li>
		<li>Take decisive action to end attacks on democracy and the rule of law within the EU.</li>
		<li>Seize opportunities to push for reform in Central Asia.
			<ul>
			<li>Increase diplomatic engagement with Central Asian governments.</li>
			<li>Increase the use of conditioned aid and trade to reduce the region’s dependence on Moscow and Beijing.</li>
			<li>Increase funding for programs that support nongovernmental democratic actors.</li>
			</ul>
		</li>
	<br>
	<h3>Support human rights defenders.</h3>
		<li>Provide financial support and protection to front-line activists and journalists.
			<ul>
			<li>Provide direct services and care to activists under duress.</li>
			<li>Create a special visa category for human rights defenders facing imminent danger.</li>
			<li>Limit the unintended impact on activists of sanctions against their home countries.</li>
			<li>Assist independent media serving citizens in authoritarian countries.</li>
			</ul>
		</li>
		<li>Seek accountability for human rights abuses in the region.
			<ul>
			<li>Use targeted sanctions as part of a comprehensive strategy of accountability for human rights abusers and corrupt officials.</li>
			<li>Leverage multilateral institutions to support collective responses and documentation.</li>
			</ul>
		</li>
		<li>Advocate for the immediate and unconditional release of political prisoners.</li>
		<li>Combat the proliferation of transnational repression in Eurasia.
			<ul>
			<li>Endorse Freedom House’s Declaration of Principles to Combat Transnational Repression.</li>
			<li>Establish a mechanism to track domestic incidents of transnational repression.</li>
			<li>Develop a plan to spread awareness of transnational repression across state agencies.</li>
			<li>Apply added vetting to arrest and extradition requests from authoritarian states.</li>
			<li>Use sanctions and diplomatic tools to hold individual perpetrators accountable.</li>
			<li>Strictly regulate technology that could enable transnational repression.</li>
			<li>Support efforts to document incidents inside the authoritarian states of Eurasia.</li>
			</ul>
		</li>
	</ul>
	HTML;



$h2_Research_methodology = new h2HeaderContent('Research methodology');


$div_Evaluation_criteria = new ContentSection();
$div_Evaluation_criteria->content = <<<HTML
	<h3>Evaluation criteria</h3>

	<p>Under the two main headings of "<strong>Political Rights</strong>" and "<strong>Civil Liberties</strong>",
	Freedom House evaluates every country by scoring the answers to dozens of questions organized into seven main topics.
	See the full outline below.</p>

	<p>The Pilgrimage for Democracy shall evaluate their methodology and estimate to what extent
	Freedom House covers each of our five levels of democracy.</p>
	HTML;



$div_codeberg = new CodebergContentSection();
$div_codeberg->setTitleText('Freedom House and the 5 levels of democracy');
$div_codeberg->setTitleLink('https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/33');
$div_codeberg->content = <<<HTML
	<p>We shall review the main content produced by Freedom House and see how they fit within our 5 levels of democracy.</p>
	HTML;



$div_freedom_house_data = new FreedomHouseContentSection();
$div_freedom_house_data->setTitleText('Freedom in the World data set');
$div_freedom_house_data->setTitleLink('https://freedomhouse.org/report/freedom-world#Data');
$div_freedom_house_data->content = <<<HTML
	<p>The Freedom in the World report is composed of numerical ratings and supporting descriptive texts for 195 countries and 15 territories.
	External analysts assess 210 countries and territories, using a combination of on-the-ground research,
	consultations with local contacts, and information from news articles, nongovernmental organizations, governments, and a variety of other sources.</p>

	<p>For each country and territory, Freedom in the World analyzes the electoral process, political pluralism and participation,
	the functioning of the government, freedom of expression and of belief, associational and organizational rights,
	the rule of law, and personal autonomy and individual rights.</p>
	HTML;


$div_wikipedia_Freedom_House = new WikipediaContentSection();
$div_wikipedia_Freedom_House->setTitleText('Freedom House');
$div_wikipedia_Freedom_House->setTitleLink('https://en.wikipedia.org/wiki/Freedom_House');
$div_wikipedia_Freedom_House->content = <<<HTML
	<p>Freedom House is a non-profit organization group in Washington, D.C.
	It is best known for political advocacy surrounding issues of democracy, political freedom, and human rights.
	Freedom House was founded in October 1941, and Wendell Willkie and Eleanor Roosevelt served as its first honorary chairpersons.</p>
	HTML;


$div_Marking_50_Years_in_the_Struggle_for_Democracy = new FreedomHouseContentSection();
$div_Marking_50_Years_in_the_Struggle_for_Democracy->setTitleText('Marking 50 Years in the Struggle for Democracy');
$div_Marking_50_Years_in_the_Struggle_for_Democracy->setTitleLink('https://freedomhouse.org/report/freedom-world/2023/marking-50-years');
$div_Marking_50_Years_in_the_Struggle_for_Democracy->content = <<<HTML
	<p>The 2023 edition of Freedom in the World is the 50th in this series of annual comparative reports.
	More than anything else, five decades of Freedom in the World reports demonstrate that the demand for freedom is universal.</p>

	<ul>
	<li>Global freedom declined for the 17th consecutive year.</li>
	<li>The struggle for democracy may be approaching a turning point.</li>
	<li>Infringement on freedom of expression has long been a key driver of global democratic decline.</li>
	<li>The fight for freedom persists across decades.</li>
	</ul>
	HTML;



$div_Freedom_in_the_World_Research_Methodology = new FreedomHouseContentSection();
$div_Freedom_in_the_World_Research_Methodology->setTitleText('Freedom in the World Research Methodology');
$div_Freedom_in_the_World_Research_Methodology->setTitleLink('https://freedomhouse.org/reports/freedom-world/freedom-world-research-methodology');
$div_Freedom_in_the_World_Research_Methodology->content = <<<HTML
	<p>The report’s methodology is derived in large measure from the Universal Declaration of Human Rights, adopted by the UN General Assembly in 1948.</p>

	<ul>
	<h3>Political Rights</h3>
	<li><strong>A. Electoral process</strong>
		<ul>
		<li>A1.      Was the current head of government or other chief national authority elected through free and fair elections?</li>
		<li>A2.      Were the current national legislative representatives elected through free and fair elections?</li>
		<li>A3.      Are the electoral laws and framework fair, and are they implemented impartially by the relevant election management bodies?</li>
		</ul>
	</li>
	<li><strong>B. Political pluralism and participation</strong>
		<ul>
		<li>B1.      Do the people have the right to organize in different political parties or other competitive political groupings of their choice, and is the system free of undue obstacles to the rise and fall of these competing parties or groupings?</li>
		<li>B2.      Is there a realistic opportunity for the opposition to increase its support or gain power through elections?</li>
		<li>B3.      Are the people’s political choices free from domination by forces that are external to the political sphere, or by political forces that employ extrapolitical means?</li>
		<li>B4.      Do various segments of the population (including ethnic, racial, religious, gender, LGBT+, and other relevant groups) have full political rights and electoral opportunities?</li>
		</ul>
	</li>
	<li><strong>C. Functioning of government</strong>
		<ul>
		<li>C1.      Do the freely elected head of government and national legislative representatives determine the policies of the government?</li>
		<li>C2.      Are safeguards against official corruption strong and effective?</li>
		<li>C3.      Does the government operate with openness and transparency?</li>
		<li>C4. Is the government or occupying power deliberately changing the ethnic composition of a country or territory so as to destroy a culture or tip the political balance in favor of another group?</li>
		</ul>
	</li>
	<br>
	<h3>Civil Liberties</h3>
	<li><strong>D. Freedom of expression and belief</strong>
		<ul>
		<li>D1.     Are there free and independent media?</li>
		<li>D2.      Are individuals free to practice and express their religious faith or nonbelief in public and private?</li>
		<li>D3.      Is there academic freedom, and is the educational system free from extensive political indoctrination?</li>
		<li>D4.      Are individuals free to express their personal views on political or other sensitive topics without fear of surveillance or retribution?</li>
		</ul>
	</li>
	<li><strong>E. Associational and organizational rights</strong>
		<ul>
		<li>E1.      Is there freedom of assembly?</li>
		<li>E2.      Is there freedom for nongovernmental organizations, particularly those that are engaged in human rights– and governance-related work?</li>
		<li>E3.      Is there freedom for trade unions and similar professional or labor organizations?</li>
		</ul>
	</li>
	<li><strong>F. Rule of Law</strong>
		<ul>
		<li>F1.       Is there an independent judiciary?</li>
		<li>F2.       Does due process prevail in civil and criminal matters?</li>
		<li>F3.       Is there protection from the illegitimate use of physical force and freedom from war and insurgencies?</li>
		<li>F4.       Do laws, policies, and practices guarantee equal treatment of various segments of the population?</li>
		<li></li>
		</ul>
	</li>
	<li><strong>G. Personal autonomy and individual rights</strong>
		<ul>
		<li>G1.      Do individuals enjoy freedom of movement, including the ability to change their place of residence, employment, or education?</li>
		<li>G2.      Are individuals able to exercise the right to own property and establish private businesses without undue interference from state or nonstate actors?</li>
		<li>G3.      Do individuals enjoy personal social freedoms, including choice of marriage partner and size of family, protection from domestic violence, and control over appearance?</li>
		<li>G4.      Do individuals enjoy equality of opportunity and freedom from economic exploitation?</li>
		</ul>
	</li>
	</ul>
	HTML;









$page->parent('list_of_indices.html');
$page->parent('list_of_organisations.html');

$page->body($h2_Introduction);
$page->body($div_introduction);
$page->body($div_Expanding_Freedom_and_Democracy);
$page->body($div_wikipedia_Freedom_House);

$page->body($h2_Policy_recommendations);
$page->body($div_policy_recommendations);
$page->body($div_Policy_Recommendations);


$page->body($h2_Research_methodology);
$page->body($div_Evaluation_criteria);
$page->body($div_codeberg);
$page->body($div_Freedom_in_the_World_Research_Methodology);

$page->body($div_Marking_50_Years_in_the_Struggle_for_Democracy);
$page->body($div_freedom_house_data);
