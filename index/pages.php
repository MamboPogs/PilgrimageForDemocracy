<?php


$preprocess_index_src = array(
	'src/accountability.php' => array(
		'dest'	 => 'http/accountability.html',
		'API'	 => '1',
	),
	'src/american_constitution_society.php' => array(
		'dest'	 => 'http/american_constitution_society.html',
		'API'	 => '1',
	),
	'src/anne_applebaum.php' => array(
		'dest'	 => 'http/anne_applebaum.html',
		'API'	 => '1',
	),
	'src/artificial_intelligence.php' => array(
		'dest'	 => 'http/artificial_intelligence.html',
		'API'	 => '1',
	),
	'src/australia.php' => array(
		'dest'	 => 'http/australia.html',
		'API'	 => '1',
	),
	'src/bail.php' => array(
		'dest'	 => 'http/bail.html',
		'API'	 => '1',
	),
	'src/beliefs.php' => array(
		'dest'	 => 'http/beliefs.html',
		'API'	 => '1',
	),
	'src/blasphemy.php' => array(
		'dest'	 => 'http/blasphemy.html',
		'API'	 => '1',
	),
	'src/brics.php' => array(
		'dest'	 => 'http/brics.html',
		'API'	 => '1',
	),
	'src/capital_punishment.php' => array(
		'dest'	 => 'http/capital_punishment.html',
		'API'	 => '1',
	),
	'src/child_labour.php' => array(
		'dest'	 => 'http/child_labour.html',
		'API'	 => '1',
	),
	'src/china_and_taiwan.php' => array(
		'dest'	 => 'http/china_and_taiwan.html',
		'API'	 => '1',
	),
	'src/chinese_expansionism.php' => array(
		'dest'	 => 'http/chinese_expansionism.html',
		'API'	 => '1',
	),
	'src/clark_cunningham.php' => array(
		'dest'	 => 'http/clark_cunningham.html',
		'API'	 => '1',
	),
	'src/colombia.php' => array(
		'dest'	 => 'http/colombia.html',
		'API'	 => '1',
	),
	'src/conflict_resolution.php' => array(
		'dest'	 => 'http/conflict_resolution.html',
		'API'	 => '1',
	),
	'src/constitution.php' => array(
		'dest'	 => 'http/constitution.html',
		'API'	 => '1',
	),
	'src/constitution_of_the_philippines.php' => array(
		'dest'	 => 'http/constitution_of_the_philippines.html',
		'API'	 => '1',
	),
	'src/corruption.php' => array(
		'dest'	 => 'http/corruption.html',
		'API'	 => '1',
	),
	'src/costa_rica.php' => array(
		'dest'	 => 'http/costa_rica.html',
		'API'	 => '1',
	),
	'src/criminal_justice.php' => array(
		'dest'	 => 'http/criminal_justice.html',
		'API'	 => '1',
	),
	'src/cyberwarfare.php' => array(
		'dest'	 => 'http/cyberwarfare.html',
		'API'	 => '1',
	),
	'src/data_activism.php' => array(
		'dest'	 => 'http/data_activism.html',
		'API'	 => '1',
	),
	'src/data_pop_alliance.php' => array(
		'dest'	 => 'http/data_pop_alliance.html',
		'API'	 => '1',
	),
	'src/decentralized_autonomous_organization.php' => array(
		'dest'	 => 'http/decentralized_autonomous_organization.html',
		'API'	 => '1',
	),
	'src/democracy.php' => array(
		'dest'	 => 'http/democracy.html',
		'API'	 => '1',
	),
	'src/democracy_under_attack.php' => array(
		'dest'	 => 'http/democracy_under_attack.html',
		'API'	 => '1',
	),
	'src/democracy_wip.php' => array(
		'dest'	 => 'http/democracy_wip.html',
		'API'	 => '1',
	),
	'src/denise_dresser.php' => array(
		'dest'	 => 'http/denise_dresser.html',
		'API'	 => '1',
	),
	'src/denmark.php' => array(
		'dest'	 => 'http/denmark.html',
		'API'	 => '1',
	),
	'src/disfranchisement.php' => array(
		'dest'	 => 'http/disfranchisement.html',
		'API'	 => '1',
	),
	'src/don_miguel_ruiz.php' => array(
		'dest'	 => 'http/don_miguel_ruiz.html',
		'API'	 => '1',
	),
	'src/duverger_france_alternance_danger.php' => array(
		'dest'	 => 'http/duverger_france_alternance_danger.html',
		'API'	 => '1',
	),
	'src/duverger_law.php' => array(
		'dest'	 => 'http/duverger_law.html',
		'API'	 => '1',
	),
	'src/duverger_syndrome.php' => array(
		'dest'	 => 'http/duverger_syndrome.html',
		'API'	 => '1',
	),
	'src/duverger_syndrome_alternance.php' => array(
		'dest'	 => 'http/duverger_syndrome_alternance.html',
		'API'	 => '1',
	),
	'src/duverger_syndrome_duality.php' => array(
		'dest'	 => 'http/duverger_syndrome_duality.html',
		'API'	 => '1',
	),
	'src/duverger_syndrome_lack_choice.php' => array(
		'dest'	 => 'http/duverger_syndrome_lack_choice.html',
		'API'	 => '1',
	),
	'src/duverger_syndrome_negative_campaigning.php' => array(
		'dest'	 => 'http/duverger_syndrome_negative_campaigning.html',
		'API'	 => '1',
	),
	'src/duverger_syndrome_party_politics.php' => array(
		'dest'	 => 'http/duverger_syndrome_party_politics.html',
		'API'	 => '1',
	),
	'src/duverger_taiwan_2000_2004.php' => array(
		'dest'	 => 'http/duverger_taiwan_2000_2004.html',
		'API'	 => '1',
	),
	'src/duverger_usa_19_century.php' => array(
		'dest'	 => 'http/duverger_usa_19_century.html',
		'API'	 => '1',
	),
	'src/economic_social_and_cultural_rights.php' => array(
		'dest'	 => 'http/economic_social_and_cultural_rights.html',
		'API'	 => '1',
	),
	'src/economist_democracy_index.php' => array(
		'dest'	 => 'http/economist_democracy_index.html',
		'API'	 => '1',
	),
	'src/ecowas.php' => array(
		'dest'	 => 'http/ecowas.html',
		'API'	 => '1',
	),
	'src/ecuador.php' => array(
		'dest'	 => 'http/ecuador.html',
		'API'	 => '1',
	),
	'src/education.php' => array(
		'dest'	 => 'http/education.html',
		'API'	 => '1',
	),
	'src/election_integrity.php' => array(
		'dest'	 => 'http/election_integrity.html',
		'API'	 => '1',
	),
	'src/environment.php' => array(
		'dest'	 => 'http/environment.html',
		'API'	 => '1',
	),
	'src/environmental_defenders.php' => array(
		'dest'	 => 'http/environmental_defenders.html',
		'API'	 => '1',
	),
	'src/european_union.php' => array(
		'dest'	 => 'http/european_union.html',
		'API'	 => '1',
	),
	'src/fair_share.php' => array(
		'dest'	 => 'http/fair_share.html',
		'API'	 => '1',
	),
	'src/flat_tax.php' => array(
		'dest'	 => 'http/flat_tax.html',
		'API'	 => '1',
	),
	'src/food_and_agriculture_organization.php' => array(
		'dest'	 => 'http/food_and_agriculture_organization.html',
		'API'	 => '1',
	),
	'src/foreign_influence_local_media.php' => array(
		'dest'	 => 'http/foreign_influence_local_media.html',
		'API'	 => '1',
	),
	'src/france.php' => array(
		'dest'	 => 'http/france.html',
		'API'	 => '1',
	),
	'src/freedom.php' => array(
		'dest'	 => 'http/freedom.html',
		'API'	 => '1',
	),
	'src/freedom_house.php' => array(
		'dest'	 => 'http/freedom_house.html',
		'API'	 => '1',
	),
	'src/freedom_of_association.php' => array(
		'dest'	 => 'http/freedom_of_association.html',
		'API'	 => '1',
	),
	'src/freedom_of_conscience.php' => array(
		'dest'	 => 'http/freedom_of_conscience.html',
		'API'	 => '1',
	),
	'src/freedom_of_speech.php' => array(
		'dest'	 => 'http/freedom_of_speech.html',
		'API'	 => '1',
	),
	'src/gabon.php' => array(
		'dest'	 => 'http/gabon.html',
		'API'	 => '1',
	),
	'src/gar_alperovitz.php' => array(
		'dest'	 => 'http/gar_alperovitz.html',
		'API'	 => '1',
	),
	'src/georgia.php' => array(
		'dest'	 => 'http/georgia.html',
		'API'	 => '1',
	),
	'src/germany.php' => array(
		'dest'	 => 'http/germany.html',
		'API'	 => '1',
	),
	'src/gerrymandering.php' => array(
		'dest'	 => 'http/gerrymandering.html',
		'API'	 => '1',
	),
	'src/global_issues.php' => array(
		'dest'	 => 'http/global_issues.html',
		'API'	 => '1',
	),
	'src/global_natural_resources.php' => array(
		'dest'	 => 'http/global_natural_resources.html',
		'API'	 => '1',
	),
	'src/global_peace_index.php' => array(
		'dest'	 => 'http/global_peace_index.html',
		'API'	 => '1',
	),
	'src/global_witness.php' => array(
		'dest'	 => 'http/global_witness.html',
		'API'	 => '1',
	),
	'src/green_new_deal.php' => array(
		'dest'	 => 'http/green_new_deal.html',
		'API'	 => '1',
	),
	'src/green_new_deal_network.php' => array(
		'dest'	 => 'http/green_new_deal_network.html',
		'API'	 => '1',
	),
	'src/greenwashing.php' => array(
		'dest'	 => 'http/greenwashing.html',
		'API'	 => '1',
	),
	'src/ground_news.php' => array(
		'dest'	 => 'http/ground_news.html',
		'API'	 => '1',
	),
	'src/haiti.php' => array(
		'dest'	 => 'http/haiti.html',
		'API'	 => '1',
	),
	'src/hong_kong.php' => array(
		'dest'	 => 'http/hong_kong.html',
		'API'	 => '1',
	),
	'src/housing.php' => array(
		'dest'	 => 'http/housing.html',
		'API'	 => '1',
	),
	'src/human_rights_watch.php' => array(
		'dest'	 => 'http/human_rights_watch.html',
		'API'	 => '1',
	),
	'src/hunger.php' => array(
		'dest'	 => 'http/hunger.html',
		'API'	 => '1',
	),
	'src/immigration.php' => array(
		'dest'	 => 'http/immigration.html',
		'API'	 => '1',
	),
	'src/index.php' => array(
		'dest'	 => 'http/index.html',
		'API'	 => '1',
	),
	'src/information_overload.php' => array(
		'dest'	 => 'http/information_overload.html',
		'API'	 => '1',
	),
	'src/institute_for_economics_and_peace.php' => array(
		'dest'	 => 'http/institute_for_economics_and_peace.html',
		'API'	 => '1',
	),
	'src/institutions.php' => array(
		'dest'	 => 'http/institutions.html',
		'API'	 => '1',
	),
	'src/international_crisis_group.php' => array(
		'dest'	 => 'http/international_crisis_group.html',
		'API'	 => '1',
	),
	'src/international_monetary_fund.php' => array(
		'dest'	 => 'http/international_monetary_fund.html',
		'API'	 => '1',
	),
	'src/interpersonal_relationships.php' => array(
		'dest'	 => 'http/interpersonal_relationships.html',
		'API'	 => '1',
	),
	'src/iran.php' => array(
		'dest'	 => 'http/iran.html',
		'API'	 => '1',
	),
	'src/justice.php' => array(
		'dest'	 => 'http/justice.html',
		'API'	 => '1',
	),
	'src/karoline_wiesner.php' => array(
		'dest'	 => 'http/karoline_wiesner.html',
		'API'	 => '1',
	),
	'src/keep_america_beautiful.php' => array(
		'dest'	 => 'http/keep_america_beautiful.html',
		'API'	 => '1',
	),
	'src/laurence_tribe.php' => array(
		'dest'	 => 'http/laurence_tribe.html',
		'API'	 => '1',
	),
	'src/lawmaking.php' => array(
		'dest'	 => 'http/lawmaking.html',
		'API'	 => '1',
	),
	'src/liberia.php' => array(
		'dest'	 => 'http/liberia.html',
		'API'	 => '1',
	),
	'src/list_of_books.php' => array(
		'dest'	 => 'http/list_of_books.html',
		'API'	 => '1',
	),
	'src/list_of_indices.php' => array(
		'dest'	 => 'http/list_of_indices.html',
		'API'	 => '1',
	),
	'src/list_of_movies.php' => array(
		'dest'	 => 'http/list_of_movies.html',
		'API'	 => '1',
	),
	'src/list_of_organisations.php' => array(
		'dest'	 => 'http/list_of_organisations.html',
		'API'	 => '1',
	),
	'src/list_of_people.php' => array(
		'dest'	 => 'http/list_of_people.html',
		'API'	 => '1',
	),
	'src/list_of_research_and_reports.php' => array(
		'dest'	 => 'http/list_of_research_and_reports.html',
		'API'	 => '1',
	),
	'src/lists.php' => array(
		'dest'	 => 'http/lists.html',
		'API'	 => '1',
	),
	'src/living_together.php' => array(
		'dest'	 => 'http/living_together.html',
		'API'	 => '1',
	),
	'src/mali.php' => array(
		'dest'	 => 'http/mali.html',
		'API'	 => '1',
	),
	'src/maria_ressa.php' => array(
		'dest'	 => 'http/maria_ressa.html',
		'API'	 => '1',
	),
	'src/media.php' => array(
		'dest'	 => 'http/media.html',
		'API'	 => '1',
	),
	'src/menu.php' => array(
		'dest'	 => 'http/menu.html',
		'API'	 => '1',
	),
	'src/michael_luttig.php' => array(
		'dest'	 => 'http/michael_luttig.html',
		'API'	 => '1',
	),
	'src/mo_gawdat.php' => array(
		'dest'	 => 'http/mo_gawdat.html',
		'API'	 => '1',
	),
	'src/myanmar.php' => array(
		'dest'	 => 'http/myanmar.html',
		'API'	 => '1',
	),
	'src/niger.php' => array(
		'dest'	 => 'http/niger.html',
		'API'	 => '1',
	),
	'src/obstruction_of_justice.php' => array(
		'dest'	 => 'http/obstruction_of_justice.html',
		'API'	 => '1',
	),
	'src/oecd.php' => array(
		'dest'	 => 'http/oecd.html',
		'API'	 => '1',
	),
	'src/one_country_two_systems.php' => array(
		'dest'	 => 'http/one_country_two_systems.html',
		'API'	 => '1',
	),
	'src/osce.php' => array(
		'dest'	 => 'http/osce.html',
		'API'	 => '1',
	),
	'src/pakistan.php' => array(
		'dest'	 => 'http/pakistan.html',
		'API'	 => '1',
	),
	'src/peaceful_resistance_in_times_of_war.php' => array(
		'dest'	 => 'http/peaceful_resistance_in_times_of_war.html',
		'API'	 => '1',
	),
	'src/philippines.php' => array(
		'dest'	 => 'http/philippines.html',
		'API'	 => '1',
	),
	'src/political_discourse.php' => array(
		'dest'	 => 'http/political_discourse.html',
		'API'	 => '1',
	),
	'src/politics.php' => array(
		'dest'	 => 'http/politics.html',
		'API'	 => '1',
	),
	'src/poverty.php' => array(
		'dest'	 => 'http/poverty.html',
		'API'	 => '1',
	),
	'src/prc_china.php' => array(
		'dest'	 => 'http/prc_china.html',
		'API'	 => '1',
	),
	'src/primary_features_of_democracy.php' => array(
		'dest'	 => 'http/primary_features_of_democracy.html',
		'API'	 => '1',
	),
	'src/professionalism_without_elitism.php' => array(
		'dest'	 => 'http/professionalism_without_elitism.html',
		'API'	 => '1',
	),
	'src/project/codeberg.php' => array(
		'dest'	 => 'http/project/codeberg.html',
		'API'	 => '1',
	),
	'src/project/copyright.php' => array(
		'dest'	 => 'http/project/copyright.html',
		'API'	 => '1',
	),
	'src/project/development_scripts.php' => array(
		'dest'	 => 'http/project/development_scripts.html',
		'API'	 => '1',
	),
	'src/project/development_website.php' => array(
		'dest'	 => 'http/project/development_website.html',
		'API'	 => '1',
	),
	'src/project/git.php' => array(
		'dest'	 => 'http/project/git.html',
		'API'	 => '1',
	),
	'src/project/index.php' => array(
		'dest'	 => 'http/project/index.html',
		'API'	 => '1',
	),
	'src/project/participate.php' => array(
		'dest'	 => 'http/project/participate.html',
		'API'	 => '1',
	),
	'src/project/standing_on_the_shoulders_of_giants.php' => array(
		'dest'	 => 'http/project/standing_on_the_shoulders_of_giants.html',
		'API'	 => '1',
	),
	'src/project/updates.php' => array(
		'dest'	 => 'http/project/updates.html',
		'API'	 => '1',
	),
	'src/project/vim.php' => array(
		'dest'	 => 'http/project/vim.html',
		'API'	 => '1',
	),
	'src/project/wikipedia.php' => array(
		'dest'	 => 'http/project/wikipedia.html',
		'API'	 => '1',
	),
	'src/property.php' => array(
		'dest'	 => 'http/property.html',
		'API'	 => '1',
	),
	'src/racism.php' => array(
		'dest'	 => 'http/racism.html',
		'API'	 => '1',
	),
	'src/refugee_camps.php' => array(
		'dest'	 => 'http/refugee_camps.html',
		'API'	 => '1',
	),
	'src/refugees.php' => array(
		'dest'	 => 'http/refugees.html',
		'API'	 => '1',
	),
	'src/religion.php' => array(
		'dest'	 => 'http/religion.html',
		'API'	 => '1',
	),
	'src/renewable_energy.php' => array(
		'dest'	 => 'http/renewable_energy.html',
		'API'	 => '1',
	),
	'src/reporters_without_borders.php' => array(
		'dest'	 => 'http/reporters_without_borders.html',
		'API'	 => '1',
	),
	'src/richard_bluhm.php' => array(
		'dest'	 => 'http/richard_bluhm.html',
		'API'	 => '1',
	),
	'src/rohingya_people.php' => array(
		'dest'	 => 'http/rohingya_people.html',
		'API'	 => '1',
	),
	'src/rolf_dobelli.php' => array(
		'dest'	 => 'http/rolf_dobelli.html',
		'API'	 => '1',
	),
	'src/russia.php' => array(
		'dest'	 => 'http/russia.html',
		'API'	 => '1',
	),
	'src/rwanda.php' => array(
		'dest'	 => 'http/rwanda.html',
		'API'	 => '1',
	),
	'src/sarcasm.php' => array(
		'dest'	 => 'http/sarcasm.html',
		'API'	 => '1',
	),
	'src/saudi_arabia.php' => array(
		'dest'	 => 'http/saudi_arabia.html',
		'API'	 => '1',
	),
	'src/secondary_features_of_democracy.php' => array(
		'dest'	 => 'http/secondary_features_of_democracy.html',
		'API'	 => '1',
	),
	'src/separation_of_powers.php' => array(
		'dest'	 => 'http/separation_of_powers.html',
		'API'	 => '1',
	),
	'src/social_dress_code.php' => array(
		'dest'	 => 'http/social_dress_code.html',
		'API'	 => '1',
	),
	'src/social_justice.php' => array(
		'dest'	 => 'http/social_justice.html',
		'API'	 => '1',
	),
	'src/social_networks.php' => array(
		'dest'	 => 'http/social_networks.html',
		'API'	 => '1',
	),
	'src/somalia.php' => array(
		'dest'	 => 'http/somalia.html',
		'API'	 => '1',
	),
	'src/staffan_lindberg.php' => array(
		'dest'	 => 'http/staffan_lindberg.html',
		'API'	 => '1',
	),
	'src/sweden.php' => array(
		'dest'	 => 'http/sweden.html',
		'API'	 => '1',
	),
	'src/taiwan.php' => array(
		'dest'	 => 'http/taiwan.html',
		'API'	 => '1',
	),
	'src/taiwan_ukraine_relations.php' => array(
		'dest'	 => 'http/taiwan_ukraine_relations.html',
		'API'	 => '1',
	),
	'src/taxes.php' => array(
		'dest'	 => 'http/taxes.html',
		'API'	 => '1',
	),
	'src/technology_and_democracy.php' => array(
		'dest'	 => 'http/technology_and_democracy.html',
		'API'	 => '1',
	),
	'src/tertiary_features_of_democracy.php' => array(
		'dest'	 => 'http/tertiary_features_of_democracy.html',
		'API'	 => '1',
	),
	'src/the_art_of_thinking_clearly.php' => array(
		'dest'	 => 'http/the_art_of_thinking_clearly.html',
		'API'	 => '1',
	),
	'src/the_four_agreements.php' => array(
		'dest'	 => 'http/the_four_agreements.html',
		'API'	 => '1',
	),
	'src/the_hidden_dimension_in_democracy.php' => array(
		'dest'	 => 'http/the_hidden_dimension_in_democracy.html',
		'API'	 => '1',
	),
	'src/the_remains_of_the_day.php' => array(
		'dest'	 => 'http/the_remains_of_the_day.html',
		'API'	 => '1',
	),
	'src/transnational_authoritarianism.php' => array(
		'dest'	 => 'http/transnational_authoritarianism.html',
		'API'	 => '1',
	),
	'src/transparency_international.php' => array(
		'dest'	 => 'http/transparency_international.html',
		'API'	 => '1',
	),
	'src/tunisia.php' => array(
		'dest'	 => 'http/tunisia.html',
		'API'	 => '1',
	),
	'src/turkey.php' => array(
		'dest'	 => 'http/turkey.html',
		'API'	 => '1',
	),
	'src/twilight_of_democracy.php' => array(
		'dest'	 => 'http/twilight_of_democracy.html',
		'API'	 => '1',
	),
	'src/types_of_democracy.php' => array(
		'dest'	 => 'http/types_of_democracy.html',
		'API'	 => '1',
	),
	'src/ukraine.php' => array(
		'dest'	 => 'http/ukraine.html',
		'API'	 => '1',
	),
	'src/united_nations.php' => array(
		'dest'	 => 'http/united_nations.html',
		'API'	 => '1',
	),
	'src/united_nations_high_commissioner_for_refugees.php' => array(
		'dest'	 => 'http/united_nations_high_commissioner_for_refugees.html',
		'API'	 => '1',
	),
	'src/united_states.php' => array(
		'dest'	 => 'http/united_states.html',
		'API'	 => '1',
	),
	'src/v_dem_institute.php' => array(
		'dest'	 => 'http/v_dem_institute.html',
		'API'	 => '1',
	),
	'src/vietnam.php' => array(
		'dest'	 => 'http/vietnam.html',
		'API'	 => '1',
	),
	'src/vision_of_humanity.php' => array(
		'dest'	 => 'http/vision_of_humanity.html',
		'API'	 => '1',
	),
	'src/volodymyr_zelensky.php' => array(
		'dest'	 => 'http/volodymyr_zelensky.html',
		'API'	 => '1',
	),
	'src/voter_suppression.php' => array(
		'dest'	 => 'http/voter_suppression.html',
		'API'	 => '1',
	),
	'src/whistleblowers.php' => array(
		'dest'	 => 'http/whistleblowers.html',
		'API'	 => '1',
	),
	'src/world.php' => array(
		'dest'	 => 'http/world.html',
		'API'	 => '1',
	),
	'src/world_future_council.php' => array(
		'dest'	 => 'http/world_future_council.html',
		'API'	 => '1',
	),
	'src/world_happiness_report.php' => array(
		'dest'	 => 'http/world_happiness_report.html',
		'API'	 => '1',
	),
);


$preprocess_index_dest = array(
	'accountability.html' => array(
		'include'	 => 'src/accountability.php',
		'name'	 => 'page',
		'keyword'	 => 'Accountability',
	),
	'american_constitution_society.html' => array(
		'include'	 => 'src/american_constitution_society.php',
		'name'	 => 'page',
		'keyword'	 => 'American Constitution Society',
	),
	'anne_applebaum.html' => array(
		'include'	 => 'src/anne_applebaum.php',
		'name'	 => 'page',
		'keyword'	 => 'Anne Applebaum',
	),
	'artificial_intelligence.html' => array(
		'include'	 => 'src/artificial_intelligence.php',
		'name'	 => 'page',
		'keyword'	 => 'Artificial intelligence',
	),
	'australia.html' => array(
		'include'	 => 'src/australia.php',
		'name'	 => 'page',
		'keyword'	 => 'Australia',
	),
	'bail.html' => array(
		'include'	 => 'src/bail.php',
		'name'	 => 'page',
		'keyword'	 => 'Bail',
	),
	'beliefs.html' => array(
		'include'	 => 'src/beliefs.php',
		'name'	 => 'page',
		'keyword'	 => 'beliefs',
	),
	'blasphemy.html' => array(
		'include'	 => 'src/blasphemy.php',
		'name'	 => 'page',
		'keyword'	 => 'Blasphemy',
	),
	'brics.html' => array(
		'include'	 => 'src/brics.php',
		'name'	 => 'page',
		'keyword'	 => 'BRICS',
	),
	'capital_punishment.html' => array(
		'include'	 => 'src/capital_punishment.php',
		'name'	 => 'page',
		'keyword'	 => 'Capital punishment',
	),
	'child_labour.html' => array(
		'include'	 => 'src/child_labour.php',
		'name'	 => 'page',
		'keyword'	 => 'child labour',
	),
	'china_and_taiwan.html' => array(
		'include'	 => 'src/china_and_taiwan.php',
		'name'	 => 'page',
		'keyword'	 => 'China and Taiwan',
	),
	'chinese_expansionism.html' => array(
		'include'	 => 'src/chinese_expansionism.php',
		'name'	 => 'page',
		'keyword'	 => 'Chinese expansionism',
	),
	'clark_cunningham.html' => array(
		'include'	 => 'src/clark_cunningham.php',
		'name'	 => 'page',
		'keyword'	 => 'Clark Cunningham',
	),
	'colombia.html' => array(
		'include'	 => 'src/colombia.php',
		'name'	 => 'page',
		'keyword'	 => 'Colombia',
	),
	'conflict_resolution.html' => array(
		'include'	 => 'src/conflict_resolution.php',
		'name'	 => 'page',
		'keyword'	 => 'Conflict resolution',
	),
	'constitution.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/constitution.php',
	),
	'constitution_of_the_philippines.html' => array(
		'include'	 => 'src/constitution_of_the_philippines.php',
		'name'	 => 'page',
		'keyword'	 => 'Constitution of the Philippines',
	),
	'corruption.html' => array(
		'include'	 => 'src/corruption.php',
		'name'	 => 'page',
		'keyword'	 => 'corruption',
	),
	'costa_rica.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/costa_rica.php',
		'keyword'	 => 'Costa Rica',
	),
	'criminal_justice.html' => array(
		'include'	 => 'src/criminal_justice.php',
		'name'	 => 'page',
		'keyword'	 => 'Criminal justice',
	),
	'cyberwarfare.html' => array(
		'include'	 => 'src/cyberwarfare.php',
		'name'	 => 'page',
		'keyword'	 => 'cyberwarfare',
	),
	'data_activism.html' => array(
		'include'	 => 'src/data_activism.php',
		'name'	 => 'page',
		'keyword'	 => 'data activism',
	),
	'data_pop_alliance.html' => array(
		'include'	 => 'src/data_pop_alliance.php',
		'name'	 => 'page',
		'keyword'	 => 'Data-Pop Alliance',
	),
	'decentralized_autonomous_organization.html' => array(
		'include'	 => 'src/decentralized_autonomous_organization.php',
		'name'	 => 'page',
		'keyword'	 => 'decentralized autonomous organization',
	),
	'democracy.html' => array(
		'include'	 => 'src/democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'democracy',
	),
	'democracy_under_attack.html' => array(
		'include'	 => 'src/democracy_under_attack.php',
		'name'	 => 'page',
		'keyword'	 => 'democracy under attack',
	),
	'democracy_wip.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/democracy_wip.php',
	),
	'denise_dresser.html' => array(
		'include'	 => 'src/denise_dresser.php',
		'name'	 => 'page',
		'keyword'	 => 'Denise Dresser',
	),
	'denmark.html' => array(
		'include'	 => 'src/denmark.php',
		'name'	 => 'page',
		'keyword'	 => 'Denmark',
	),
	'disfranchisement.html' => array(
		'include'	 => 'src/disfranchisement.php',
		'name'	 => 'page',
		'keyword'	 => 'Disfranchisement',
	),
	'don_miguel_ruiz.html' => array(
		'include'	 => 'src/don_miguel_ruiz.php',
		'name'	 => 'page',
		'keyword'	 => 'Don Miguel Ruiz',
	),
	'duverger_france_alternance_danger.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/duverger_france_alternance_danger.php',
	),
	'duverger_law.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/duverger_law.php',
	),
	'duverger_syndrome.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/duverger_syndrome.php',
	),
	'duverger_syndrome_alternance.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/duverger_syndrome_alternance.php',
	),
	'duverger_syndrome_duality.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/duverger_syndrome_duality.php',
	),
	'duverger_syndrome_lack_choice.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/duverger_syndrome_lack_choice.php',
	),
	'duverger_syndrome_negative_campaigning.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/duverger_syndrome_negative_campaigning.php',
	),
	'duverger_syndrome_party_politics.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/duverger_syndrome_party_politics.php',
	),
	'duverger_taiwan_2000_2004.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/duverger_taiwan_2000_2004.php',
	),
	'duverger_usa_19_century.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/duverger_usa_19_century.php',
	),
	'economic_social_and_cultural_rights.html' => array(
		'include'	 => 'src/economic_social_and_cultural_rights.php',
		'name'	 => 'page',
		'keyword'	 => 'Economic, social and cultural rights',
	),
	'economist_democracy_index.html' => array(
		'include'	 => 'src/economist_democracy_index.php',
		'name'	 => 'page',
		'keyword'	 => 'Economist Democracy Index',
	),
	'ecowas.html' => array(
		'include'	 => 'src/ecowas.php',
		'name'	 => 'page',
		'keyword'	 => 'ECOWAS',
	),
	'ecuador.html' => array(
		'include'	 => 'src/ecuador.php',
		'name'	 => 'page',
		'keyword'	 => 'Ecuador',
	),
	'education.html' => array(
		'include'	 => 'src/education.php',
		'name'	 => 'page',
		'keyword'	 => 'education',
	),
	'election_integrity.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/election_integrity.php',
	),
	'environment.html' => array(
		'include'	 => 'src/environment.php',
		'name'	 => 'page',
		'keyword'	 => 'Environment',
	),
	'environmental_defenders.html' => array(
		'include'	 => 'src/environmental_defenders.php',
		'name'	 => 'page',
		'keyword'	 => 'Environmental defenders',
	),
	'european_union.html' => array(
		'include'	 => 'src/european_union.php',
		'name'	 => 'page',
		'keyword'	 => 'European Union',
	),
	'fair_share.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/fair_share.php',
	),
	'flat_tax.html' => array(
		'include'	 => 'src/flat_tax.php',
		'name'	 => 'page',
		'keyword'	 => 'flat tax',
	),
	'food_and_agriculture_organization.html' => array(
		'include'	 => 'src/food_and_agriculture_organization.php',
		'name'	 => 'page',
		'keyword'	 => 'Food and Agriculture Organization',
	),
	'foreign_influence_local_media.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/foreign_influence_local_media.php',
	),
	'france.html' => array(
		'include'	 => 'src/france.php',
		'name'	 => 'page',
		'keyword'	 => 'France',
	),
	'freedom.html' => array(
		'include'	 => 'src/freedom.php',
		'name'	 => 'page',
		'keyword'	 => 'Freedom',
	),
	'freedom_house.html' => array(
		'include'	 => 'src/freedom_house.php',
		'name'	 => 'page',
		'keyword'	 => 'Freedom House',
	),
	'freedom_of_association.html' => array(
		'include'	 => 'src/freedom_of_association.php',
		'name'	 => 'page',
		'keyword'	 => 'Freedom of association',
	),
	'freedom_of_conscience.html' => array(
		'include'	 => 'src/freedom_of_conscience.php',
		'name'	 => 'page',
		'keyword'	 => 'Freedom of conscience',
	),
	'freedom_of_speech.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/freedom_of_speech.php',
	),
	'gabon.html' => array(
		'include'	 => 'src/gabon.php',
		'name'	 => 'page',
		'keyword'	 => 'Gabon',
	),
	'gar_alperovitz.html' => array(
		'include'	 => 'src/gar_alperovitz.php',
		'name'	 => 'page',
		'keyword'	 => 'Gar Alperovitz',
	),
	'georgia.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/georgia.php',
	),
	'germany.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/germany.php',
		'keyword'	 => 'Germany',
	),
	'gerrymandering.html' => array(
		'include'	 => 'src/gerrymandering.php',
		'name'	 => 'page',
		'keyword'	 => 'Gerrymandering',
	),
	'global_issues.html' => array(
		'include'	 => 'src/global_issues.php',
		'name'	 => 'page',
		'keyword'	 => 'global issues',
	),
	'global_natural_resources.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/global_natural_resources.php',
	),
	'global_peace_index.html' => array(
		'include'	 => 'src/global_peace_index.php',
		'name'	 => 'page',
		'keyword'	 => 'Global Peace Index',
	),
	'global_witness.html' => array(
		'include'	 => 'src/global_witness.php',
		'name'	 => 'page',
		'keyword'	 => 'Global Witness',
	),
	'green_new_deal.html' => array(
		'include'	 => 'src/green_new_deal.php',
		'name'	 => 'page',
		'keyword'	 => 'Green New Deal',
	),
	'green_new_deal_network.html' => array(
		'include'	 => 'src/green_new_deal_network.php',
		'name'	 => 'page',
		'keyword'	 => 'Green New Deal Network',
	),
	'greenwashing.html' => array(
		'include'	 => 'src/greenwashing.php',
		'name'	 => 'page',
		'keyword'	 => 'Greenwashing',
	),
	'ground_news.html' => array(
		'include'	 => 'src/ground_news.php',
		'name'	 => 'page',
		'keyword'	 => 'Ground News',
	),
	'haiti.html' => array(
		'include'	 => 'src/haiti.php',
		'name'	 => 'page',
		'keyword'	 => 'Haiti',
	),
	'hong_kong.html' => array(
		'include'	 => 'src/hong_kong.php',
		'name'	 => 'page',
		'keyword'	 => 'Hong Kong',
	),
	'housing.html' => array(
		'include'	 => 'src/housing.php',
		'name'	 => 'page',
		'keyword'	 => 'Housing',
	),
	'human_rights_watch.html' => array(
		'include'	 => 'src/human_rights_watch.php',
		'name'	 => 'page',
		'keyword'	 => 'Human Rights Watch',
	),
	'hunger.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/hunger.php',
	),
	'immigration.html' => array(
		'include'	 => 'src/immigration.php',
		'name'	 => 'page',
		'keyword'	 => 'Immigration',
	),
	'index.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/index.php',
	),
	'information_overload.html' => array(
		'include'	 => 'src/information_overload.php',
		'name'	 => 'page',
		'keyword'	 => 'information overload',
	),
	'institute_for_economics_and_peace.html' => array(
		'include'	 => 'src/institute_for_economics_and_peace.php',
		'name'	 => 'page',
		'keyword'	 => 'Institute for Economics and Peace',
	),
	'institutions.html' => array(
		'include'	 => 'src/institutions.php',
		'name'	 => 'page',
		'keyword'	 => 'institutions',
	),
	'international_crisis_group.html' => array(
		'include'	 => 'src/international_crisis_group.php',
		'name'	 => 'page',
		'keyword'	 => 'International Crisis Group',
	),
	'international_monetary_fund.html' => array(
		'include'	 => 'src/international_monetary_fund.php',
		'name'	 => 'page',
		'keyword'	 => 'International Monetary Fund',
	),
	'interpersonal_relationships.html' => array(
		'include'	 => 'src/interpersonal_relationships.php',
		'name'	 => 'page',
		'keyword'	 => 'Interpersonal relationships',
	),
	'iran.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/iran.php',
	),
	'justice.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/justice.php',
	),
	'karoline_wiesner.html' => array(
		'include'	 => 'src/karoline_wiesner.php',
		'name'	 => 'page',
		'keyword'	 => 'Karoline Wiesner',
	),
	'keep_america_beautiful.html' => array(
		'include'	 => 'src/keep_america_beautiful.php',
		'name'	 => 'page',
		'keyword'	 => 'Keep America Beautiful',
	),
	'laurence_tribe.html' => array(
		'include'	 => 'src/laurence_tribe.php',
		'name'	 => 'page',
		'keyword'	 => 'Laurence Tribe',
	),
	'lawmaking.html' => array(
		'include'	 => 'src/lawmaking.php',
		'name'	 => 'page',
		'keyword'	 => 'lawmaking',
	),
	'liberia.html' => array(
		'include'	 => 'src/liberia.php',
		'name'	 => 'page',
		'keyword'	 => 'Liberia',
	),
	'list_of_books.html' => array(
		'include'	 => 'src/list_of_books.php',
		'name'	 => 'page',
		'keyword'	 => 'list of books',
	),
	'list_of_indices.html' => array(
		'include'	 => 'src/list_of_indices.php',
		'name'	 => 'page',
		'keyword'	 => 'list of indices',
	),
	'list_of_movies.html' => array(
		'include'	 => 'src/list_of_movies.php',
		'name'	 => 'page',
		'keyword'	 => 'list of movies',
	),
	'list_of_organisations.html' => array(
		'include'	 => 'src/list_of_organisations.php',
		'name'	 => 'page',
		'keyword'	 => 'list of organisations',
	),
	'list_of_people.html' => array(
		'include'	 => 'src/list_of_people.php',
		'name'	 => 'page',
		'keyword'	 => 'list of people',
	),
	'list_of_research_and_reports.html' => array(
		'include'	 => 'src/list_of_research_and_reports.php',
		'name'	 => 'page',
		'keyword'	 => 'list of research and reports',
	),
	'lists.html' => array(
		'include'	 => 'src/lists.php',
		'name'	 => 'page',
		'keyword'	 => 'lists',
	),
	'living_together.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/living_together.php',
	),
	'mali.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/mali.php',
	),
	'maria_ressa.html' => array(
		'include'	 => 'src/maria_ressa.php',
		'name'	 => 'page',
		'keyword'	 => 'Maria Ressa',
	),
	'media.html' => array(
		'include'	 => 'src/media.php',
		'name'	 => 'page',
		'keyword'	 => 'media',
	),
	'menu.html' => array(
		'include'	 => 'src/menu.php',
		'name'	 => 'page',
		'keyword'	 => 'menu',
	),
	'michael_luttig.html' => array(
		'include'	 => 'src/michael_luttig.php',
		'name'	 => 'page',
		'keyword'	 => ' Michael Luttig',
	),
	'mo_gawdat.html' => array(
		'include'	 => 'src/mo_gawdat.php',
		'name'	 => 'page',
		'keyword'	 => 'Mo Gawdat',
	),
	'myanmar.html' => array(
		'include'	 => 'src/myanmar.php',
		'name'	 => 'page',
		'keyword'	 => 'Myanmar',
	),
	'niger.html' => array(
		'include'	 => 'src/niger.php',
		'name'	 => 'page',
		'keyword'	 => 'Niger',
	),
	'obstruction_of_justice.html' => array(
		'include'	 => 'src/obstruction_of_justice.php',
		'name'	 => 'page',
		'keyword'	 => 'Obstruction of justice',
	),
	'oecd.html' => array(
		'include'	 => 'src/oecd.php',
		'name'	 => 'page',
		'keyword'	 => 'OECD',
	),
	'one_country_two_systems.html' => array(
		'include'	 => 'src/one_country_two_systems.php',
		'name'	 => 'page',
		'keyword'	 => 'One country, two systems',
	),
	'osce.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/osce.php',
	),
	'pakistan.html' => array(
		'include'	 => 'src/pakistan.php',
		'name'	 => 'page',
		'keyword'	 => 'Pakistan',
	),
	'peaceful_resistance_in_times_of_war.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/peaceful_resistance_in_times_of_war.php',
	),
	'philippines.html' => array(
		'include'	 => 'src/philippines.php',
		'name'	 => 'page',
		'keyword'	 => 'Philippines',
	),
	'political_discourse.html' => array(
		'include'	 => 'src/political_discourse.php',
		'name'	 => 'page',
		'keyword'	 => 'political discourse',
	),
	'politics.html' => array(
		'include'	 => 'src/politics.php',
		'name'	 => 'page',
		'keyword'	 => 'politics',
	),
	'poverty.html' => array(
		'include'	 => 'src/poverty.php',
		'name'	 => 'page',
		'keyword'	 => 'Poverty',
	),
	'prc_china.html' => array(
		'include'	 => 'src/prc_china.php',
		'name'	 => 'page',
		'keyword'	 => 'China',
	),
	'primary_features_of_democracy.html' => array(
		'include'	 => 'src/primary_features_of_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'primary features of democracy',
	),
	'professionalism_without_elitism.html' => array(
		'include'	 => 'src/professionalism_without_elitism.php',
		'name'	 => 'page',
		'keyword'	 => 'professionalism without elitism',
	),
	'project/codeberg.html' => array(
		'include'	 => 'src/project/codeberg.php',
		'name'	 => 'page',
		'keyword'	 => 'Codeberg',
	),
	'project/copyright.html' => array(
		'include'	 => 'src/project/copyright.php',
		'name'	 => 'page',
		'keyword'	 => 'copyright',
	),
	'project/development_scripts.html' => array(
		'include'	 => 'src/project/development_scripts.php',
		'name'	 => 'page',
		'keyword'	 => 'development scripts',
	),
	'project/development_website.html' => array(
		'include'	 => 'src/project/development_website.php',
		'name'	 => 'page',
		'keyword'	 => 'development website',
	),
	'project/git.html' => array(
		'include'	 => 'src/project/git.php',
		'name'	 => 'page',
		'keyword'	 => 'git',
	),
	'project/index.html' => array(
		'include'	 => 'src/project/index.php',
		'name'	 => 'page',
		'keyword'	 => 'project',
	),
	'project/participate.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/project/participate.php',
	),
	'project/standing_on_the_shoulders_of_giants.html' => array(
		'include'	 => 'src/project/standing_on_the_shoulders_of_giants.php',
		'name'	 => 'page',
		'keyword'	 => 'standing on the shoulders of giants',
	),
	'project/updates.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/project/updates.php',
	),
	'project/vim.html' => array(
		'include'	 => 'src/project/vim.php',
		'name'	 => 'page',
		'keyword'	 => 'Vim',
	),
	'project/wikipedia.html' => array(
		'include'	 => 'src/project/wikipedia.php',
		'name'	 => 'page',
		'keyword'	 => 'wikipedia',
	),
	'property.html' => array(
		'include'	 => 'src/property.php',
		'name'	 => 'page',
		'keyword'	 => 'Property',
	),
	'racism.html' => array(
		'include'	 => 'src/racism.php',
		'name'	 => 'page',
		'keyword'	 => 'Racism',
	),
	'refugee_camps.html' => array(
		'include'	 => 'src/refugee_camps.php',
		'name'	 => 'page',
		'keyword'	 => 'Refugee camps',
	),
	'refugees.html' => array(
		'include'	 => 'src/refugees.php',
		'name'	 => 'page',
		'keyword'	 => 'Refugees',
	),
	'religion.html' => array(
		'include'	 => 'src/religion.php',
		'name'	 => 'page',
		'keyword'	 => 'Religion',
	),
	'renewable_energy.html' => array(
		'include'	 => 'src/renewable_energy.php',
		'name'	 => 'page',
		'keyword'	 => 'Renewable energy',
	),
	'reporters_without_borders.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/reporters_without_borders.php',
	),
	'richard_bluhm.html' => array(
		'include'	 => 'src/richard_bluhm.php',
		'name'	 => 'page',
		'keyword'	 => 'Richard Bluhm',
	),
	'rohingya_people.html' => array(
		'include'	 => 'src/rohingya_people.php',
		'name'	 => 'page',
		'keyword'	 => 'Rohingya people',
	),
	'rolf_dobelli.html' => array(
		'include'	 => 'src/rolf_dobelli.php',
		'name'	 => 'page',
		'keyword'	 => 'Rolf Dobelli',
	),
	'russia.html' => array(
		'include'	 => 'src/russia.php',
		'name'	 => 'page',
		'keyword'	 => 'Russia',
	),
	'rwanda.html' => array(
		'include'	 => 'src/rwanda.php',
		'name'	 => 'page',
		'keyword'	 => 'Rwanda',
	),
	'sarcasm.html' => array(
		'include'	 => 'src/sarcasm.php',
		'name'	 => 'page',
		'keyword'	 => 'Sarcasm',
	),
	'saudi_arabia.html' => array(
		'include'	 => 'src/saudi_arabia.php',
		'name'	 => 'page',
		'keyword'	 => 'Saudi Arabia',
	),
	'secondary_features_of_democracy.html' => array(
		'include'	 => 'src/secondary_features_of_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'secondary features of democracy',
	),
	'separation_of_powers.html' => array(
		'include'	 => 'src/separation_of_powers.php',
		'name'	 => 'page',
		'keyword'	 => 'Separation of powers',
	),
	'social_dress_code.html' => array(
		'include'	 => 'src/social_dress_code.php',
		'name'	 => 'page',
		'keyword'	 => 'social dress code',
	),
	'social_justice.html' => array(
		'include'	 => 'src/social_justice.php',
		'name'	 => 'page',
		'keyword'	 => 'Social justice',
	),
	'social_networks.html' => array(
		'include'	 => 'src/social_networks.php',
		'name'	 => 'page',
		'keyword'	 => 'Social networks',
	),
	'somalia.html' => array(
		'include'	 => 'src/somalia.php',
		'name'	 => 'page',
		'keyword'	 => 'Somalia',
	),
	'staffan_lindberg.html' => array(
		'include'	 => 'src/staffan_lindberg.php',
		'name'	 => 'page',
		'keyword'	 => 'Staffan Lindberg',
	),
	'sweden.html' => array(
		'include'	 => 'src/sweden.php',
		'name'	 => 'page',
		'keyword'	 => 'Sweden',
	),
	'taiwan.html' => array(
		'include'	 => 'src/taiwan.php',
		'name'	 => 'page',
		'keyword'	 => 'Taiwan',
	),
	'taiwan_ukraine_relations.html' => array(
		'include'	 => 'src/taiwan_ukraine_relations.php',
		'name'	 => 'page',
		'keyword'	 => 'Taiwan–Ukraine relations',
	),
	'taxes.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/taxes.php',
	),
	'technology_and_democracy.html' => array(
		'include'	 => 'src/technology_and_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'technology and democracy',
	),
	'tertiary_features_of_democracy.html' => array(
		'include'	 => 'src/tertiary_features_of_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'tertiary features of democracy',
	),
	'the_art_of_thinking_clearly.html' => array(
		'include'	 => 'src/the_art_of_thinking_clearly.php',
		'name'	 => 'page',
		'keyword'	 => 'The Art of Thinking Clearly',
	),
	'the_four_agreements.html' => array(
		'include'	 => 'src/the_four_agreements.php',
		'name'	 => 'page',
		'keyword'	 => 'The Four Agreements',
	),
	'the_hidden_dimension_in_democracy.html' => array(
		'include'	 => 'src/the_hidden_dimension_in_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'The Hidden Dimension in Democracy',
	),
	'the_remains_of_the_day.html' => array(
		'include'	 => 'src/the_remains_of_the_day.php',
		'name'	 => 'page',
		'keyword'	 => 'The Remains of the Day',
	),
	'transnational_authoritarianism.html' => array(
		'include'	 => 'src/transnational_authoritarianism.php',
		'name'	 => 'page',
		'keyword'	 => 'transnational authoritarianism',
	),
	'transparency_international.html' => array(
		'include'	 => 'src/transparency_international.php',
		'name'	 => 'page',
		'keyword'	 => 'Transparency International',
	),
	'tunisia.html' => array(
		'include'	 => 'src/tunisia.php',
		'name'	 => 'page',
		'keyword'	 => 'Tunisia',
	),
	'turkey.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/turkey.php',
	),
	'twilight_of_democracy.html' => array(
		'include'	 => 'src/twilight_of_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'Twilight of Democracy',
	),
	'types_of_democracy.html' => array(
		'include'	 => 'src/types_of_democracy.php',
		'name'	 => 'page',
		'keyword'	 => 'types of democracy',
	),
	'ukraine.html' => array(
		'include'	 => 'src/ukraine.php',
		'name'	 => 'page',
		'keyword'	 => 'Ukraine',
	),
	'united_nations.html' => array(
		'name'	 => 'page',
		'include'	 => 'src/united_nations.php',
	),
	'united_nations_high_commissioner_for_refugees.html' => array(
		'include'	 => 'src/united_nations_high_commissioner_for_refugees.php',
		'name'	 => 'page',
		'keyword'	 => 'United Nations High Commissioner for Refugees',
	),
	'united_states.html' => array(
		'include'	 => 'src/united_states.php',
		'name'	 => 'page',
		'keyword'	 => 'United States',
	),
	'v_dem_institute.html' => array(
		'include'	 => 'src/v_dem_institute.php',
		'name'	 => 'page',
		'keyword'	 => 'V-Dem Institute',
	),
	'vietnam.html' => array(
		'include'	 => 'src/vietnam.php',
		'name'	 => 'page',
		'keyword'	 => 'Vietnam',
	),
	'vision_of_humanity.html' => array(
		'include'	 => 'src/vision_of_humanity.php',
		'name'	 => 'page',
		'keyword'	 => 'Vision of Humanity',
	),
	'volodymyr_zelensky.html' => array(
		'include'	 => 'src/volodymyr_zelensky.php',
		'name'	 => 'page',
		'keyword'	 => 'Volodymyr Zelensky',
	),
	'voter_suppression.html' => array(
		'include'	 => 'src/voter_suppression.php',
		'name'	 => 'page',
		'keyword'	 => 'Voter suppression',
	),
	'whistleblowers.html' => array(
		'include'	 => 'src/whistleblowers.php',
		'name'	 => 'page',
		'keyword'	 => 'Whistleblower',
	),
	'world.html' => array(
		'include'	 => 'src/world.php',
		'name'	 => 'page',
		'keyword'	 => 'world',
	),
	'world_future_council.html' => array(
		'include'	 => 'src/world_future_council.php',
		'name'	 => 'page',
		'keyword'	 => 'World Future Council',
	),
	'world_happiness_report.html' => array(
		'include'	 => 'src/world_happiness_report.php',
		'name'	 => 'page',
		'keyword'	 => 'World Happiness Report',
	),
);
