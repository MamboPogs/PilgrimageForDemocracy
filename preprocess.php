#!/usr/bin/php
<?php
require_once('include/preprocess.php');
require_once('include/init.php');
global $preprocess_index_src;


$process_all = true;
$src = '';

$show_keyword = false;
if (isset($argv[1])) {
	if ($argv[1] == "wip") {
		$show_wip = true;

		if (isset($argv[2])) {
			$src = $argv[2];
			$process_all = false;
		}
	}
	else if ($argv[1] == "kw") {
		if (isset($argv[2])) {
			$show_keyword = true;
			$src = $argv[2];
			$process_all = false;
		}
	}
	else {
		$src = $argv[1];
		$process_all = false;
	}
}

$keywords_index = array();

if ($process_all) {
	foreach(glob("http/*.html") as $file) {
		unlink($file);
	}

	echo "Preprocessing all targets.\r";
	foreach ($preprocess_index_src AS $src => $data) {
		$dest = $data["dest"];
		$format = $show_wip ? 'wip' : 'body';
		process_page($src, $format);
	}
	save_array('keywords_index', 'index/keywords.php');
	spawn_keywords();
	echo "\033[2K";
}
else {
	if (isset($preprocess_index_src[$src])) {
		echo "Preprocessing $src .\n";
		$dest = $preprocess_index_src[$src]["dest"];
		$format = $show_wip ? 'wip' : 'body';
		process_page($src, $format);

		if ($show_keyword) {
			echo "\n";
			$dest_keyword = substr($dest, 5);
			if (isset($preprocess_index_dest[$dest_keyword])) {
				$keyword = create_auto_keyword($dest_keyword);
				echo $keyword . "\n";
			}
		}
	}
	else {
		echo "	!! The source ${src} is not valid.\n";
	}
}

update_preprocess_index();

$count_commits = shell_exec("git rev-list --count HEAD");
echo "Commits in HEAD:\t $count_commits";
$count_files = shell_exec("git ls-files | wc -l");
echo "Number of files:\t $count_files";
$count_articles = count($preprocess_index_src);
echo "Number of articles:\t $count_articles\n";
