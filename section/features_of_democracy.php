<?php



$div_section_what_is_democracy_quaternary_feature = newSection();
$div_section_what_is_democracy_quaternary_feature['stars'] = 0;
$div_section_what_is_democracy_quaternary_feature['en'] = <<<HTML
	<h3>4: Quaternary features of democracy</h3>
	HTML;

$div_section_what_is_democracy_quinary_feature = newSection();
$div_section_what_is_democracy_quinary_feature['stars'] = 0;
$div_section_what_is_democracy_quinary_feature['en'] = <<<HTML
	<h3>5: Quinary features of democracy</h3>
	HTML;


$div_section_democracy_soloist_choir = newSection();
$div_section_democracy_soloist_choir['stars']   = 0;
$div_section_democracy_soloist_choir['class'][] = '';
$div_section_democracy_soloist_choir['en'] = <<<HTML
	<h3>7: Democracy — the Soloist and the Choir</h3>
	HTML;

$div_section_democracy_saints_and_little_devils = newSection();
$div_section_democracy_saints_and_little_devils['stars'] = 0;
$div_section_democracy_saints_and_little_devils['en'] = <<<HTML
	<h3>8: Democracy — Saints and Little Devils</h3>
	HTML;
