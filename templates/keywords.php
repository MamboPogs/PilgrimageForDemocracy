<?php

// Example usage:
// Link to $constitution article.
// Link to ${'Freedom House'} article.

$The_Hidden_Dimension_in_Democracy = 'The Hidden Dimension in Democracy';
$$The_Hidden_Dimension_in_Democracy = '<a href=\'/the_hidden_dimension_in_democracy.html\'>The Hidden Dimension in Democracy</a>';
$Pilgrimage = '<a href="/">Pilgrimage for Democracy and Social Justice</a>';
$git = '<a href=\'/project/git.html\'>git</a>';
$list_of_movies = 'list of movies';
$$list_of_movies = '<a href=\'/list_of_movies.html\'>list of movies</a>';
$list_of_books = 'list of books';
$$list_of_books = '<a href=\'/list_of_books.html\'>list of books</a>';
$decentralized_autonomous_organization = 'decentralized autonomous organization';
$$decentralized_autonomous_organization = '<a href=\'/decentralized_autonomous_organization.html\'>decentralized autonomous organization</a>';
$Taiwan_Ukraine_relations = 'Taiwan–Ukraine relations';
$$Taiwan_Ukraine_relations = '<a href=\'/taiwan_ukraine_relations.html\'>Taiwan–Ukraine relations</a>';
$list_of_indices = 'list of indices';
$$list_of_indices = '<a href=\'/list_of_indices.html\'>list of indices</a>';
$list_of_research_and_reports = 'list of research and reports';
$$list_of_research_and_reports = '<a href=\'/list_of_research_and_reports.html\'>list of research and reports</a>';
$technology_and_democracy = 'technology and democracy';
$$technology_and_democracy = '<a href=\'/technology_and_democracy.html\'>technology and democracy</a>';
$flat_tax = 'flat tax';
$$flat_tax = '<a href=\'/flat_tax.html\'>flat tax</a>';
$Food_and_Agriculture_Organization = 'Food and Agriculture Organization';
$$Food_and_Agriculture_Organization = '<a href=\'/food_and_agriculture_organization.html\'>Food and Agriculture Organization</a>';
