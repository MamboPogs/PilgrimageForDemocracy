<?php
require_once('objects/section.php');

class CodebergContentSection extends ContentSection {
	private $issue_id = 0;

	public function __construct($country = '') {
		$this->classes[] = 'codeberg';
		parent::__construct();
		$this->stars(NULL);
	}

	public function setTitleLink($link) {
		$this->title_link_ = $link;
		if (preg_match('#https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues/([0-9]+)#', $link, $match)) {
			$this->issue_id = $match[1];
		}
	}

	protected function printLogo() {
			return '<img class="ui tiny image codeberg" src="https://design.codeberg.org/logo-kit/icon_inverted.svg" width="26" height="26">';
	}

	protected function printMainTitle() {
		$out  = $this->printLogo();
		$out .= '<h3>';
		if ($this->issue_id) {
			$out .= "Issue #{$this->issue_id}: &nbsp;&nbsp;&nbsp;";
		}
		$out .= "<a href='{$this->title_link_}' class='codeberg'>{$this->title_text_}</a>";
		$out .= '</h3>';
		return $out;
	}
}
