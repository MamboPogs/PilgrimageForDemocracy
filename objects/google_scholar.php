<?php
require_once('objects/section.php');

class GoogleScholarContentSection extends ContentSection {
	public function __construct($country = '') {
		$this->classes[] = 'google-scholar';
		$this->title_classes .= ' google-scholar ';
		parent::__construct();
		$this->stars(NULL);
	}

	protected function printLogo() {
		$this->title_text_ = "Google Scholar: &nbsp; &nbsp; &nbsp; " . $this->title_text_;
		$out   = '<span class="icon icon--google-scholar" aria-hidden="true">';
		$out .= '<svg xmlns="http://www.w3.org/2000/svg"><use xlink:href="#icon-google-scholar"></use></svg>';
		$out .= '</span>';
		return $out;
	}
}
