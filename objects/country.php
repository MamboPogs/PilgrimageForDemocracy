<?php
require_once 'objects/page.php';
require_once('objects/freedom_house.php');
require_once('objects/indices.php');
include_once('data/democracy_index.php');
include_once('data/press_freedom_index.php');
include_once('data/freedom_house/data.php');


class CountryPage extends Page {
	protected $name;

	public function __construct($country = '') {
		$this->name = $country;
		parent::__construct();
	}

	protected function print_parts($parts) {
		$out = '';
		foreach ($parts AS $item) {
			$part    = $item['part'];
			$options = $item['options'];
			if (is_array($part)) {
				$out .=  printSection($part);
			}
			else if(is_object($part)) {
				$format = (!empty($options) && isset($options['format'])) ? $options['format'] : 'body';
				$out .= $part->print($format, $options);
			}
			else if($part == 'Country indices') {
				$out .= $this->print_indices();
			}
			else {
				$out .=	printPageSection($part);
			}
		}
		return $out;
	}


	private function print_indices() {
		$alternative_names = array(
			"Russian Federation" => "Russia",
			"Islamic Republic of Iran" => "Iran",
		);

		$country = strtolower($this->name);
		$country = str_replace(" ", "-", $country);
		$country = $this->name;

		if (isset($arternatives_names[$country])) {
			$country = $arternatives_names[$country];
		}

		$out  = '';
		$out .= (new DemocracyIndexContentSection                   ($country))->print();
		$out .= (new PressFreedomIndexContentSection                ($country))->print();
		$out .= (new FreedomHouseGlobalFreedomContentSection        ($country))->print();
		$out .= (new FreedomHouseGlobalFreedomIndexContentSection   ($country))->print();
		$out .= (new FreedomHouseInternetFreedomContentSection      ($country))->print();
		$out .= (new FreedomHouseInternetFreedomIndexContentSection ($country))->print();

		return $out;
	}
}
