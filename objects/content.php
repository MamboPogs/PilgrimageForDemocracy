<?php

class Content {
	public function __construct() {}

	public $content = '';

	protected $classes = array();
	protected $tag = 'div';
	protected $id_ = '';
	protected $lang = 'en';
	protected $has_content = true;

	public function id($id) {
		$this->id_ = $id;
	}

	public function print() {
		if (!$this->has_content) return '';

		$out  = "";
		$out .= $this->printOpeningTag();
		$out .= $this->printContent($this->lang);
		$out .= $this->printClosingTag();
		return $out;
	}

	protected function printOpeningTag() {
		return "<{$this->tag} id='{$this->id_}' class='{$this->printClasses()}'>";
	}

	protected function printClosingTag() {
		return "</{$this->tag}>";
	}

	protected function printClasses() {
		return join(' ', $this->classes);
	}

	protected function printContent($lang) {
		$out  = '';
		$out .= $this->printOpeningLanguageTag();
		$out .= $this->content;
		$out .= $this->printClosingLanguageTag();
		return $out;
	}

	protected function printOpeningLanguageTag() {
		return "<div lang='{$this->lang}'>";
	}

	protected function printClosingLanguageTag() {
		return "</div>";
	}

}
