<?php
require_once('objects/section.php');
include_once('data/freedom_house/data.php');

class CountryIndexContentSection extends ContentSection {

	public function __construct($country) {
		$this->name = $country;
		$this->country = strtolower($country);
		$this->country = str_replace(" ", "-", $this->country);
		$this->classes[] = 'country-index';
		$this->setNbLevels($this->nb_levels_);
		parent::__construct();
		$this->stars(NULL);
	}

	protected $name;
	protected $country;
	protected $statuses;

	private $nb_levels_ = 10;
	private $score_level_ = array();

	protected function setNbLevels($nb) {
		$this->nb_levels_ = $nb;
		for ($level = 1; $level <= $this->nb_levels_; $level++) {
			$this->score_level_[$level] = '';
		}
	}

	protected function setLevelScore($level, $score) {
			$this->score_level_[$level] = $score;
	}

	protected function printOpeningLanguageTag() {
		return '';
	}

	protected function printClosingLanguageTag() {
		return '';
	}

	protected function printMainContent() {
		$out = '';
		$out .= $this->printScale();
		$out .= $this->printStatus();
		return $out;
	}

	protected function printScale() {
		$out = '<div class="scale">';
		for ($level = 1; $level <= $this->nb_levels_; $level++) {
			$class = 'level-' . str_pad($level, 2, '0', STR_PAD_LEFT);
			$value = $this->score_level_[$level];
			$out .= "<span class='${class}'>${value}</span>";
		}
		$out .= '</div>';
		return $out;
	}

	protected function printStatus() {
		$out = '<div class="status">';
		foreach ($this->statuses AS $status => $value) {
			$out .= "<span class='{$value['classes']}'>$status</span>";
		}
		$out .= '</div>';
		return $out;
	}


}

class FreedomHouseIndexContentSection extends CountryIndexContentSection {

	protected $index;

	public function __construct($country) {
		parent::__construct($country);
		if (!$this->has_content) return;

		$this->classes[] = 'freedom-house-index';
		$this->name = $this->index[$this->country]['name'];

		$this->statuses = array(
			'Free'        => array('active' => false, 'classes' => 'free'),
			'Partly Free' => array('active' => false, 'classes' => 'partly-free'),
			'Not Free'    => array('active' => false, 'classes' => 'not-free'),
		);

		$this->setNbLevels(9);
		$status = $this->index[$this->country]['status'];
		$score  = $this->index[$this->country]['score'];

		switch ($score) {
			case ($status == 'Free'   &&   $score >= 90):
				$this->setLevelScore(1, $score);
				$this->statuses['Free']['classes'] .= ' active level-01';
				break;
			case ($status == 'Free'   &&   $score >= 80):
				$this->setLevelScore(2, $score);
				$this->statuses['Free']['classes'] .= ' active level-02';
				break;
			case ($status == 'Free'):
				$this->setLevelScore(3, $score);
				$this->statuses['Free']['classes'] .= ' active level-03';
				break;
			case ($status == 'Partly Free'   &&   $score >= 60):
				$this->setLevelScore(4, $score);
				$this->statuses['Partly Free']['classes'] .= ' active level-04';
				break;
			case ($status == 'Partly Free'   &&   $score >= 50):
				$this->setLevelScore(5, $score);
				$this->statuses['Partly Free']['classes'] .= ' active level-05';
				break;
			case ($status == 'Partly Free'):
				$this->setLevelScore(6, $score);
				$this->statuses['Partly Free']['classes'] .= ' active level-06';
				break;
			case ($status == 'Not Free'   &&   $score >= 25):
				$this->setLevelScore(7, $score);
				$this->statuses['Not Free']['classes'] .= ' active level-07';
				break;
			case ($status == 'Not Free'   &&   $score >= 15):
				$this->setLevelScore(8, $score);
				$this->statuses['Not Free']['classes'] .= ' active level-08';
				break;
			case ($status == 'Not Free'):
				$this->setLevelScore(9, $score);
				$this->statuses['Not Free']['classes'] .= ' active level-09';
				break;
		}
	}
}

class FreedomHouseGlobalFreedomIndexContentSection extends FreedomHouseIndexContentSection {
	public function __construct($country) {
		global $freedom_house_global_freedom;
		$this->index = $freedom_house_global_freedom;
		parent::__construct($country);
	}

	protected function printMainTitle() {
		return "<div class='title'>{$this->name} &mdash; <a href='/freedom_house.html'>Freedom House</a> <strong>Global freedom</strong> index (2023)</div>";
	}
}

class FreedomHouseInternetFreedomIndexContentSection extends FreedomHouseIndexContentSection {
	public function __construct($country) {
		global $freedom_house_internet_freedom;
		$this->index = $freedom_house_internet_freedom;
		$this->has_content = isset($freedom_house_internet_freedom[$this->country]['name']);
		parent::__construct($country);
	}

	protected function printMainTitle() {
		return "<div class='title'>{$this->name} &mdash; <a href='/freedom_house.html'>Freedom House</a> <strong>Internet freedom</strong> index (2022)</div>";
	}

}

class PressFreedomIndexContentSection extends CountryIndexContentSection {

	public function __construct($country) {
		global $press_freedom_index;
		parent::__construct($country);

		$this->classes[] = 'press-freedom-index';
		$data = $press_freedom_index['2022'][$country];
		$score = $data['Score'];

		$this->statuses = array(
			'Good'         => array('active' => false, 'classes' => 'good'),
			'Satisfactory' => array('active' => false, 'classes' => 'satisfactory'),
			'Problematic'  => array('active' => false, 'classes' => 'problematic'),
			'Difficult'    => array('active' => false, 'classes' => 'difficult'),
			'Very Serious' => array('active' => false, 'classes' => 'serious'),
		);

		// https://rsf.org/en/index-methodologie-2022?year=2022&data_type=general
		// [85 - 100 points]  good (green)
		// [70 -  85 points[  satisfactory (yellow)
		// [55 -  70 points[  problematic (light orange)
		// [40 -  55 points[  difficult (dark orange)
		// [ 0 -  40 points[  very serious (dark red)

		switch ($score) {
			case ($score >= 93):
				$this->setLevelScore(1, $score);
				$this->statuses['Good']['classes'] .= ' active level-01';
				break;
			case ($score >= 85):
				$this->setLevelScore(2, $score);
				$this->statuses['Good']['classes'] .= ' active level-02';
				break;
			case ($score >= 77):
				$this->setLevelScore(3, $score);
				$this->statuses['Satisfactory']['classes'] .= ' active level-03';
				break;
			case ($score >= 70):
				$this->setLevelScore(4, $score);
				$this->statuses['Satisfactory']['classes'] .= ' active level-04';
				break;
			case ($score >= 62):
				$this->setLevelScore(5, $score);
				$this->statuses['Problematic']['classes'] .= ' active level-05';
				break;
			case ($score >= 55):
				$this->setLevelScore(6, $score);
				$this->statuses['Problematic']['classes'] .= ' active level-06';
				break;
			case ($score >= 47):
				$this->setLevelScore(7, $score);
				$this->statuses['Difficult']['classes'] .= ' active level-07';
				break;
			case ($score >= 40):
				$this->setLevelScore(8, $score);
				$this->statuses['Difficult']['classes'] .= ' active level-08';
				break;
			case ($score >= 20):
				$this->setLevelScore(9, $score);
				$this->statuses['Very Serious']['classes'] .= ' active level-09';
				break;
			default:
				$this->setLevelScore(10, $score);
				$this->statuses['Very Serious']['classes'] .= ' active level-10';
				break;
		}
	}

	protected function printMainTitle() {
		return "<div class='title'>{$this->name} &mdash; <a href='/reporters_without_borders.html#press-freedom-index'>Press Freedom Index</a> (2022)</div>";
	}
}

class DemocracyIndexContentSection extends CountryIndexContentSection {

	public function __construct($country) {
		global $democracy_index;
		parent::__construct($country);

		$this->has_content = isset($democracy_index['2022'][$country]);
		if (!$this->has_content) {
			return;
		}

		$this->classes[] = 'democracy-index';
		$score = $democracy_index['2022'][$country]['score'];

		$this->statuses = array(
			'Full democracy'       => array('active' => false, 'classes' => 'full-democracy'),
			'Flawed democracy'     => array('active' => false, 'classes' => 'flawed-democracy'),
			'Hybrid regime'        => array('active' => false, 'classes' => 'hybrid-regime'),
			'Authoritarian regime' => array('active' => false, 'classes' => 'authoritarian-regime'),
		);

		// https://en.wikipedia.org/wiki/The_Economist_Democracy_Index
		// Full democracy:       8 ~ 10
		// Flawed democracy:     6 ~  8
		// Hybrid regime:        4 ~  6
		// Authoritarian regime: 0 ~  4

		switch ($score) {
			case ($score >= 9):
				$this->setLevelScore(1, $score);
				$this->statuses['Full democracy']['classes'] .= ' active level-01';
				break;
			case ($score >= 8):
				$this->setLevelScore(2, $score);
				$this->statuses['Full democracy']['classes'] .= ' active level-02';
				break;
			case ($score >= 7):
				$this->setLevelScore(3, $score);
				$this->statuses['Flawed democracy']['classes'] .= ' active level-03';
				break;
			case ($score >= 6):
				$this->setLevelScore(4, $score);
				$this->statuses['Flawed democracy']['classes'] .= ' active level-04';
				break;
			case ($score >= 5):
				$this->setLevelScore(5, $score);
				$this->statuses['Hybrid regime']['classes'] .= ' active level-05';
				break;
			case ($score >= 4):
				$this->setLevelScore(6, $score);
				$this->statuses['Hybrid regime']['classes'] .= ' active level-06';
				break;
			case ($score >= 3):
				$this->setLevelScore(7, $score);
				$this->statuses['Authoritarian regime']['classes'] .= ' active level-07';
				break;
			case ($score >= 2):
				$this->setLevelScore(8, $score);
				$this->statuses['Authoritarian regime']['classes'] .= ' active level-08';
				break;
			case ($score >= 1):
				$this->setLevelScore(9, $score);
				$this->statuses['Authoritarian regime']['classes'] .= ' active level-09';
				break;
			default:
				$this->setLevelScore(10, $score);
				$this->statuses['Authoritarian regime']['classes'] .= ' active level-10';
				break;
		}
	}

	protected function printMainTitle() {
		return "<div class='title'>{$this->name} &mdash; <a href='/economist_democracy_index.html'>Democracy Index</a> (2022)</div>";
	}
}
