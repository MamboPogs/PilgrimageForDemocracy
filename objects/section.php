<?php
require_once('objects/header.php');

class ContentSection extends HeaderContent {
	public function __construct() {
		$this->classes[] = 'main-article';
		$this->stars(-1);
	}

	public function print() {
		if (!$this->has_content) return '';

		$out  = "";
		$out .= $this->printOpeningTag();
		$out .= $this->printContent($this->lang);
		$out .= $this->printClosingTag();
		return $out;
	}

	public function setContent($content) {
		$this->content = $content;
	}

	public function stars($star) {
		$this->stars_ = $star;
	}

	public function getStars() {
		return $this->stars_;
	}

	private $stars_ = NULL;


	protected function printContent($lang) {
		$out  = '';
		$out .= $this->printOpeningLanguageTag();
		$out .= $this->printMainTitle();
		$out .= $this->printMainContent();
		$out .= $this->printClosingLanguageTag();
		return $out;
	}

	public function printStars() {
		$out = '';
		switch ($this->stars_) {
			case 0:
				$out .= "<i class='star outline icon'></i>";
				$out .= "<i class='star outline icon'></i>";
				$out .= "<i class='star outline icon'></i>";
				$out .= "<i class='star outline icon'></i>";
				$out .= "<i class='star outline icon'></i>";
				break;
			case 1:
				$out .= "<i class='star red     icon'></i>";
				$out .= "<i class='star outline icon'></i>";
				$out .= "<i class='star outline icon'></i>";
				$out .= "<i class='star outline icon'></i>";
				$out .= "<i class='star outline icon'></i>";
				break;
			case 2:
				$out .= "<i class='star orange  icon'></i>";
				$out .= "<i class='star orange  icon'></i>";
				$out .= "<i class='star outline icon'></i>";
				$out .= "<i class='star outline icon'></i>";
				$out .= "<i class='star outline icon'></i>";
				break;
			case 3:
				$out .= "<i class='star yellow  icon'></i>";
				$out .= "<i class='star yellow  icon'></i>";
				$out .= "<i class='star yellow  icon'></i>";
				$out .= "<i class='star outline icon'></i>";
				$out .= "<i class='star outline icon'></i>";
				break;
			case 4:
				$out .= "<i class='star blue    icon'></i>";
				$out .= "<i class='star blue    icon'></i>";
				$out .= "<i class='star blue    icon'></i>";
				$out .= "<i class='star blue    icon'></i>";
				$out .= "<i class='star outline icon'></i>";
				break;
			case 5:
				$out .= "<i class='star green   icon'></i>";
				$out .= "<i class='star green   icon'></i>";
				$out .= "<i class='star green   icon'></i>";
				$out .= "<i class='star green   icon'></i>";
				$out .= "<i class='star green   icon'></i>";
				break;
		}
		return $out;
	}

	public function printSectionStars() {
		$out = '';
		if ($this->stars_ >= 0) {
			$out .= "<div class='section-stars'>";
			$out .= $this->printStars();
			$out .= "</div>";
		}
		return $out;
	}

	protected function printOpeningLanguageTag() {
		$out  = "<div lang='{$this->lang}'>";
		if (isset($this->stars_)) {
			$out .= $this->printSectionStars();
		}
		return $out;
	}

	protected function printClosingLanguageTag() {
		return "</div>";
	}

	protected function printMainTitle() {
		$out = $this->printLogo();
		if ($this->title_link_ != '') {
			$out .= "<h3 class='{$this->title_classes}'><a href='{$this->title_link_}'>{$this->title_text_}</a></h3>";
		}
		else if ($this->title_text_ != '') {
			$out .= "<h3>{$this->title_text_}</h3>";
		}

		return $out;
	}

	protected function printLogo() {
		return '';
	}

	protected function printMainContent() {
		return $this->content;
	}



	protected function printClasses() {
		$star_classes = array(
			-1 => "no-star-rating",
			0  => "star-rating zero-stars",
			1  => "star-rating one-star",
			2  => "star-rating two-stars",
			3  => "star-rating three-stars",
			4  => "star-rating four-stars",
			5  => "star-rating five-stars"
		);

		if (isset($star_classes[$this->stars_])) {
			$this->classes[] = $star_classes[$this->stars_];
		}

		return join(' ', $this->classes);
	}
}
