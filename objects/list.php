<?php
require_once('objects/content.php');

class ListContent extends Content {
	public function __construct() {
		$this->tag = 'ul';
		parent::__construct();
	}

	protected $items_ = array();
}

class ListOfPages extends ListContent {
	public function __construct() {
		parent::__construct();
		$this->tag = 'p';
	}

	public function add($dest) {
		require('templates/keywords.php');
		require('templates/auto_keywords.php');
		global $div_stub;
		global $div_stars;
		global $preprocess_index_dest;

		if (!isset($preprocess_index_dest[$dest])) {
			fwrite(STDERR, "\t[ListContent] !!Error: page not found: ${dest} \n");
			return;
		}

		require($preprocess_index_dest[$dest]['include']);
		$name = $preprocess_index_dest[$dest]['name'];

		if ($name != 'page') {
			fwrite(STDERR, "\t[ListContent] !!Error: page does not support API 1: ${dest} \n");
			return;
		}

		$page->dest($dest);
		$this->items_[] = $page;
	}

	protected function printContent($lang) {
		$out  = '';
		foreach ($this->items_ AS $page) {
			$link = $page->print('link');
			$stars = $page->print('stars');
			$description = $page->get('snp description');
			$description = is_null($description) ? '' : " &mdash; $description";
			$out .= "$stars $link $description<br>";
		}
		return $out;
	}
}
