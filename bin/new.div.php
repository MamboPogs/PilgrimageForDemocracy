#!/usr/bin/php
<?php
$ok = true;
while ($ok && !file_exists('LICENSE.md')) {
	$ok = chdir('..');
}
include_once('bin/include.php');

$source_dir = '';


if (empty($argv[1])) {
	echo "Please provide a div name as argument.\n";
	exit;
}

$title = "";
$arg = 1;
while (!empty($argv[$arg])) {
	$title .= $argv[$arg] . " ";
	$arg++;
}
$title = trim($title);
$titlePath = tidy_titlePath($title);

$content = file_get_contents($source_dir . 'templates/div.section.php');
$content = str_replace("%title%",     $title,     $content);
$content = str_replace("%titlePath%", $titlePath, $content);


echo $content;
echo "\n";
