#!/usr/bin/php
<?php
$ok = true;
while ($ok && !file_exists('LICENSE.md')) {
	$ok = chdir('..');
}
include_once('bin/include.php');


$summary   = "";
$link      = "";
$class     = "";
$title     = "";
$titlePath = "";
$content   = "";
$prefix    = "";

if (substr($argv[1], 0, 4) == "http") {
	$link = $argv[1];

	$arg = 2;
	while (!empty($argv[$arg])) {
		$title .= $argv[$arg] . " ";
		$arg++;
	}

	if (strstr($link, 'https://freedomhouse.org/')) {
		$class = 'FreedomHouseContentSection';
		$command = "curl -Ss '$link' | hxnormalize -x -l 2000  | hxselect -c -s '\n'  h1 | tr --delete '\n'";
		$prefix = 'freedomhouse_';

	}
	elseif (strstr($link, 'worldfuturecouncil.org/')) {
		$class = 'WorldPolicyCouncilContentSection';
		$command = "curl -Ss '$link' | hxnormalize -x -l 2000  | hxselect -c -s '\n'  h1 | tr --delete '\n'";
		$prefix = 'wpc_';
	}
	elseif (strstr($link, 'futurepolicy.org/')) {
		$class = 'FuturePolicyContentSection';
		$command = "curl -Ss '$link' | hxnormalize -x -l 2000  | hxselect -c -s '\n'  h2 | tr --delete '\n'";
		$prefix = 'wpc_';
	}
	elseif (strstr($link, 'scholar.google')) {
		$class = 'GoogleScholarContentSection';
		$command = "curl -Ss '$link' | hxnormalize -x -l 2000  | hxselect -c -s '\n'  div#gsc_prf_in | tr --delete '\n'";
		$prefix = 'googlescholar_';
	}
	elseif (strstr($link, 'wikipedia.org/')) {
		$class = 'WikipediaContentSection';
		$command = "curl -Ss '$link' | hxnormalize -x -l 2000  | hxselect -c -s '\n'  h1 span | tr --delete '\n'";
		$prefix = 'wikipedia_';
	}
	elseif (strstr($link, 'https://codeberg.org/')) {
		$class = 'CodebergContentSection';
		$command = "curl -Ss '$link' | hxnormalize -x -l 2000  | hxselect -c -s '\n'  h1 span#issue-title | hxremove span | tr --delete '\n'";
		$prefix = 'codeberg_';
	}
	elseif (!empty($argv[2])) {
		$class = 'WebsiteContentSection';
		$command = '';
	}
	else {
		$class = 'WebsiteContentSection';
		$command = "curl -Ss '$link' | hxnormalize -x -l 2000  | hxselect -c -s '\n'  h1 | tr --delete '\n' | html2text -from_encoding UTF-8";
	}

	if (empty($title) && $command) {
		exec ($command, $lines);
	}
	if (!empty($lines)) {
		$title = $lines[0];
	}
	else {
		if (!$title) {
			echo "Failed to extract h1 title from the page. You can try again by passing the title as arguments after the link.\n";
		}
	}
	$titlePath = tidy_titlePath($title);
}


$content = file_get_contents('templates/div.php');
$content = str_replace("%link%", $link, $content);
$content = str_replace("%object-class%", $class, $content);
$content = str_replace("%object-name%", $prefix . $titlePath, $content);
$content = str_replace("%title%", addslashes($title), $content);
$content = str_replace("%summary%", $summary, $content);


echo "\n";
echo $content;
echo "\n";
