#!/usr/bin/php
<?php
include_once('include.php');
require_once('include/preprocess.php');

$wikipedia_body_parts = array();


function get_wikipedia_sources() {

	$sections = '';
	while (true) {
		$wikipediaLink = (string)readline("Link to wikipedia article (leave empty to quit): ");
		if (!$wikipediaLink) {
			break;
		}
		// $wikipedia_body_parts will be populated there.
		$wikipediaSection = create_wikipedia_section($wikipediaLink);
		$sections .= $wikipediaSection;
	}
	return $sections;
}

$directory = "";
if (!empty($argv[1])) {
	$directory = $argv[1];
}

$isCountry = (string)readline("Is the new article a country? (no) ");

if ($isCountry) {
	$articleName = (string)readline("Name of country: ");
}
else {
	$articleName = (string)readline("Title of article: ");
}

$fileName = strtolower($articleName);
$fileName = str_replace(" ", "_", $fileName);
$fileName = tidy_titlePath($fileName);

$src  = 'src/' . $directory . $fileName . ".php";
$dest = 'http/' . $directory . $fileName . ".html";

if (isset($preprocess_index_src[$src])) {
	echo "The file \"$src\" already exists. Aborting.\n";
	exit;
}


$content = file_get_contents('templates/article.php');
$content = str_replace("%ArticleName%", $articleName, $content);
$content = str_replace("%titlePath%", $fileName, $content);

if ($isCountry) {
	$content = str_replace("%newPage%", "\$page = new CountryPage('$articleName');", $content);
}
else {
	$content = str_replace("%newPage%", '$page = new Page();', $content);
}

$content .= get_wikipedia_sources();

$body_parts = array();
$body_parts[] = "\n";
$body_parts[] = "\n";
$body_parts[] = $isCountry ? "\$page->parent('world.html');\n" : "\$page->parent('menu.html');\n";
$body_parts[] = "\$page->body(\$div_stub);\n";
$body_parts[] = "\$page->body(\$div_introduction);\n";
$body_parts[] = $isCountry ? "\$page->body('Country indices');" : "";
$body_parts[] = "\n";
$body_parts[] = "\n";

foreach ($body_parts AS $line) {
	$content .= $line;
}
foreach ($wikipedia_body_parts AS $line) {
	$content .= $line;
}


file_put_contents($src, $content);
chmod($src, 0755);


echo "\n";
echo $src;
echo "\n";

$preprocess_index_src[$src] = array(
	'dest' => $dest,
	'API' => '1',
);

$dest = $fileName . ".html";
$preprocess_index_dest[$directory . $dest] = array(
	'include' => $src,
	'name' => 'page',
	'keyword' => $articleName,
);

update_preprocess_index();
