<?php

function print_footer() {
	$div_footer = newSection();
	$div_footer['id'] = "footer";
	$div_footer['en'] = <<<HTML
		<a href="/">Pilgrimage</a>
		<a href="/menu.html">Menu</a>
		<a href="/project/updates.html">Updates</a>
		<a href="/project/participate.html">Participate!</a>
		<a href="/project/copyright.html">Copyright?</a>
		<a href="/project/">Project</a>
		<a href="https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues">To do</a>
		HTML;

	return printSection($div_footer);
}

function print_HTML_footer() {
	return "</body>\n</html>";
}
