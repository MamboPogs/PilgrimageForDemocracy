<?php
require_once('index/pages.php');

function tidy_titlePath($titlePath) {
	$titlePath = str_replace('&#038;', "_", $titlePath);
	$titlePath = str_replace(" ", "_", $titlePath);
	$titlePath = str_replace("–", "_", $titlePath);
	$titlePath = str_replace("/", "_", $titlePath);
	$titlePath = str_replace("|", "_", $titlePath);
	$titlePath = str_replace("-", "_", $titlePath);
	$titlePath = str_replace(":", "_", $titlePath);
	$titlePath = str_replace(",", "_", $titlePath);
	$titlePath = str_replace(";", "_", $titlePath);
	$titlePath = str_replace(".", "_", $titlePath);
	$titlePath = str_replace("(", "_", $titlePath);
	$titlePath = str_replace(")", "_", $titlePath);
	$titlePath = str_replace("'", "_", $titlePath);
	$titlePath = str_replace("’", "_", $titlePath);
	$titlePath = str_replace("?", "_", $titlePath);
	$titlePath = str_replace("&", "_", $titlePath);
	$titlePath = str_replace("__", "_", $titlePath);
	$titlePath = str_replace("__", "_", $titlePath);
	$titlePath = str_replace("__", "_", $titlePath);
	$titlePath = str_replace("__", "_", $titlePath);
	$titlePath = trim($titlePath, "_");
	return $titlePath;
}

function update_preprocess_index() {
	global $preprocess_index_src;
	ksort ($preprocess_index_src);

	$content  = "<?php\n\n\n";
	$content .= "\$preprocess_index_src = array(\n";
	// ["src/taiwan.php"] => Array (
	//                     ["dest"] => "http/taiwan.html",
	// )

	foreach ($preprocess_index_src as $src => $data) {
		$content .= "\t'${src}' => array(\n";
		foreach ($data as $key => $value) {
			$content .= "\t\t'${key}'\t => '${value}',\n";
		}
		$content .= "\t),\n";
	}
	$content .= ");\n\n\n";

	global $preprocess_index_dest;
	ksort ($preprocess_index_dest);
	$content .= "\$preprocess_index_dest = array(\n";
	foreach ($preprocess_index_dest as $dest => $data) {
		$content .= "\t'${dest}' => array(\n";
		foreach ($data as $key => $value) {
			if (is_array($value)) {
				$content .= "\t\t'${key}' => array(\n";
				foreach ($value as $key1 => $value1) {
					$content .= "\t\t\t'${key1}'\t => '${value1}',\n";
				}
				$content .= "\t\t),\n";
			}
			else {
				$content .= "\t\t'${key}'\t => '${value}',\n";
			}
		}
		$content .= "\t),\n";
	}
	$content .= ");\n";

	file_put_contents('index/pages.php', $content);
}

function spawn_keywords() {
	include_once 'index/keywords.php';
	include('templates/keywords.php');
	global $keywords_index;
	$out  = "<?php\n\n\n";

	foreach ($keywords_index AS $keyword => $dest) {
		if (preg_match('/^[a-zA-Z]*$/', $keyword)) {
			$variable = str_replace('-', '_', $keyword);
			// e.g.: $politics = '<a href='/politics.html'>politics</a>';
			$out .= "\$${variable} = '<a href=\\'/${dest}\\'>${keyword}</a>';\n";
		}
		else {
			$variable = tidy_titlePath($keyword);
			$sKeyword = addslashes($keyword);
			// e.g.: $Freedom_House = 'Freedom House';
			$out .= "\$${variable} = '${sKeyword}';\n";
			// e.g.: $$Freedom_House = '<a href="/freedom_house.html">Freedom House</a>';
			$out .= "\$\$${variable} = '<a href=\\'/${dest}\\'>${sKeyword}</a>';\n";
		}

		if (isset($$variable)) {
			echo "$variable is already set.\n";
		}
	}
	file_put_contents('templates/auto_keywords.php', $out);
}
