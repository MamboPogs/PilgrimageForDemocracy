<?php

function print_header() {
	$div_header = newSection();
	$div_header['id'] = "header";
	$div_header['en'] = <<<HTML
		<a href="/">Pilgrimage</a>
		<a href="/menu.html">Menu</a>
		<a href="/project/updates.html">Updates</a>
		<a href="/project/participate.html">Participate!</a>
		<a href="/project/">Project</a>
		<a href="https://codeberg.org/DemocracyPilgrim/PilgrimageForDemocracy/issues">To do</a>
		HTML;

	return printSection($div_header);
}
