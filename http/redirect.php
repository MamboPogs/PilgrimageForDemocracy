<?php
chdir('..');
include_once 'include/init.php';


$redirect = array();
$redirect['/belief.html']      = 'beliefs.html';
$redirect['/participate.html'] = 'project/participate.html';
$redirect['/copyright.html']   = 'project/copyright.html';
$redirect['/updates.html']     = 'project/updates.html';
$redirect['/copyright.html']   = 'project/copyright.html';
$redirect['/integrity.html']   = 'election_integrity.html';
$redirect['/rights.html']      = 'economic_social_and_cultural_rights.html';


$div_404 = newSection();
$div_404['stars']   = -1;
$div_404['class'][] = '';



$request = parse_url($_SERVER['REQUEST_URI']);
$url = $request['path'];
$domain = parse_url($_SERVER['SERVER_NAME']);

if (isset($redirect[$url])) {
	$h1['en'] = 'The requested page has moved';
	$div_404['en'] = <<<HTML
		<h3>Redirect</h3>
		<p>Please go to: <a href="/{$redirect[$url]}">{$domain['path']}/{$redirect[$url]}</a>.</p>
		HTML;
}
else {
	$h1['en'] = 'Page not found';
	$div_404['en'] = <<<HTML
		<h3>Sorry</h3>
		<p>The requested page cannot be found.</p>
		HTML;
}


$body .= printSection($div_404);

include 'include/page.php';
